//
//  ResetPasswordViewController.swift
//  GPDock
//
//  Created by TecOrb on 24/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordSeparator: UIView!
    @IBOutlet weak var doneButton: UIButton!

    var token: String!

    override func viewDidLoad() {
        self.navigationItem.setHidesBackButton(false, animated:true)
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.doneButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickDoneButton(_ sender: UIButton){
        //hit service to reset password using token
        let newPassword = newPasswordTextField.text!
        let validationResut = self.validateParma(newPassword: newPassword, confirmPassword: confirmedPasswordTextField.text!)
        if !validationResut.success{
            showAlertWith(self, message: "\(validationResut.message)", title:warningMessage.alertTitle.rawValue)
            return
        }else{
          self.resetPassword(token: self.token, newPassword: newPassword)
        }
//        if let newPassword = newPasswordTextField.text{
//            if newPassword.count >= 6{
//                self.resetPassword(token: self.token, newPassword: newPassword)
//            }else{
//                showAlertWith(self, message: warningMessage.validPassword.rawValue, title: "Important Message")
//            }
//        }
    }

    func resetPassword(token:String,newPassword:String) {
        LoginService.sharedInstance.resetPassword(newPassword: newPassword, withToken: token) {(success, message) in
            if success{
                self.showSuccessAlert(message: "\(message)", title: warningMessage.alertTitle.rawValue)
            }else{
                showAlertWith(self, message: "\(message)", title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    func showSuccessAlert(message:String,title:String){
        self.confirmedPasswordTextField.text = ""
        self.newPasswordTextField.text = ""

        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            //self.navigationController?.popToRoot(true)
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: LoginViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
        alert.view.tintColor = UIColor.white
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
    

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    func validateParma(newPassword:String,confirmPassword:String) -> (success:Bool,message:String) {

        if newPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:warningMessage.validPassword.rawValue)
        }
        if newPassword.count < 6{
            return (success:false,message: warningMessage.validPassword.rawValue)
            
        }
        if confirmPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please confirm your password")
        }
        if newPassword != confirmPassword {
            return (success:false,message:"Please enter matching passwords")        }
        
        return (success:true,message:"")
    }
    @IBAction func textFieldDidChangeText(_ sender: UITextField){
        if let cnfrmPassword = self.confirmedPasswordTextField.text{
            if let newpswrd = self.newPasswordTextField.text{
                //self.doneButton.isEnabled = (newpswrd == cnfrmPassword)
                
                self.confirmedPasswordSeparator.backgroundColor = (newpswrd == cnfrmPassword) ? UIColor.lightGray : kApplicationRedColor
            }
        }
    }

}
