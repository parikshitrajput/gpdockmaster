//
//  OTPVarificationViewController.swift
//  GPDock
//
//  Created by TecOrb on 24/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class OTPVarificationViewController: UIViewController {
    @IBOutlet weak var otpView: PinCodeTextField!
    @IBOutlet weak var donebutton: UIButton!
    var otpString = ""
    var email: String!

    override func viewDidLoad() {
        super.viewDidLoad()
    self.navigationItem.setHidesBackButton(false, animated:true)
        self.otpView.keyboardType = .numberPad
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            //self.otpView.becomeFirstResponder()
             self.addDoneButtonOnKeyboard()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        self.donebutton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
        
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.size.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        doneToolbar.barTintColor = UIColor.lightGray
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(OTPVarificationViewController.doneButtonAction))
        done.tintColor = UIColor.white
        done.setTitleTextAttributes([ NSAttributedStringKey.font: UIFont(name: "Raleway-SemiBold", size: 18)!], for: .normal)
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        self.otpView.inputAccessoryView = doneToolbar

    }
    @objc func doneButtonAction()
    {
        self.otpView.resignFirstResponder()

    }

    @IBAction func onClickDoneButton(_ sender: UIButton){
        if let varificationToken = otpView.text{
            if varificationToken.count == 6{
                self.varifyOTP(varificationToken, email: self.email)
            }else{
                showAlertWith(self, message: "Please enter 6 digit code", title: warningMessage.alertTitle.rawValue)
            }
        }else{
            showAlertWith(self, message: "Please enter 6 digit code", title: warningMessage.alertTitle.rawValue)
        }
    }
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    func varifyOTP(_ otp: String,email: String){
        LoginService.sharedInstance.varifyOTPWithEmail(self.email, OTP: otp) { (validation, message) in
            if let otpValidation = validation{
                if otpValidation.isVarified{
                    let resetPasswordVC = AppStoryboard.Main.viewController(ResetPasswordViewController.self)
                    resetPasswordVC.token = otpValidation.token
                    self.navigationController?.pushViewController(resetPasswordVC, animated: true)
                }else{
                    showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
                }
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//extension OTPVarificationViewController: VPMOTPViewDelegate {
//    func hasEnteredAllOTP(hasEntered: Bool) {
//        self.donebutton.isEnabled = hasEntered
//    }
//
//    func enteredOTP(otpString: String) {
//        self.otpString = otpString
//    }
//}

