//
//  LoginViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//


/*
 All the login logic is here
 */

import UIKit


class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var passwordTextField : UITextField!

    @IBOutlet weak var loginButton : UIButton!
    @IBOutlet weak var forgotPasswordButton : UIButton!
    @IBOutlet weak var privacyPoliciesButton : UIButton!
    @IBOutlet weak var termsAndConditionsButton : UIButton!
    @IBOutlet weak var rememberMeButton : UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        //set the field if user already saved the credential

        self.getAppVersion()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getNotificarionAppVersion(_:)), name: .USER_DID_UPDATE_APPLICATION_VERSION, object: nil)
        self.navigationController?.isNavigationBarHidden = false
       self.navigationItem.setHidesBackButton(true, animated:true)
//        if let userName = kUserDefaults.value(forKey: kUserName) as? String, userName != ""{
            rememberMeButton.isSelected = false
//            self.emailTextField.text = userName
//            if let password = kUserDefaults.value(forKey: kPassword) as? String,password != ""{
//                self.passwordTextField.text = password
//            }
//        }

        self.emailTextField.text = DEBUG ? "" : ""
        self.passwordTextField.text = DEBUG ? "" : ""
    }

    
    
    override func viewDidLayoutSubviews() {
        self.loginButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func getAppVersion() {
        
        LoginService.sharedInstance.checkAppVersion() {(sucess, message,currentVersion, versionLevel) in
            if sucess {
                self.checkVersionAndUpdate(currentVersion: currentVersion, versionLevel: versionLevel)
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    
    @objc func getNotificarionAppVersion(_ notification: Notification) {
        
        LoginService.sharedInstance.checkAppVersion() {(sucess, message,currentVersion, versionLevel) in
            if sucess {
                
                print("APP Version>>>\(currentVersion)")
                print("APP Version level>>\(versionLevel)")
                self.checkVersionAndUpdate(currentVersion: currentVersion, versionLevel: versionLevel)
                
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    
    func checkVersionAndUpdate(currentVersion: String, versionLevel: String) {
        let appVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
        if currentVersion != appVersion {
            if versionLevel.lowercased() != "Normal".lowercased() {
                self.forceUPdateVersion(currentVersion: currentVersion, versionLevel: versionLevel)
            }else{
                
                
                self.askForUpdateVersion(currentVersion: currentVersion, versionLevel: versionLevel, title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    
    
    func askForUpdateVersion(currentVersion: String, versionLevel: String, title: String) {
        if !CommonClass.shouldShowNormalUpdate{return}
        let alert = UIAlertController(title: title, message: "A new version GPD Dockmaster is available. Please update to version \(currentVersion) now", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Yes", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            self.goTOAppStore()
        }
        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okayAction)
        alert.addAction(cancelAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: {
            CommonClass.setNormalUpdateKey()
        })
    }
    
    
    func forceUPdateVersion(currentVersion: String, versionLevel: String) {
        
        let alert = UIAlertController(title: "Important message", message: "A new version GPD Dockmaster is available. Please update to version \(currentVersion) now", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Update", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            self.goTOAppStore()
        }
        
        alert.addAction(okayAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: {
            CommonClass.setNormalUpdateKey()
        })
    }
    
    func goTOAppStore() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/gpd-dockmaster/id1347226165"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    @IBAction func onClickOfLoginButton(_ sender:UIButton){
        self.view.endEditing(true)

        let user = self.emailTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let pass = self.passwordTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let validationResult = self.validateParams(user!, password: (self.passwordTextField.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))!)
        if !validationResult.success{
            showAlertWith(self, message: validationResult.message, title: warningMessage.alertTitle.rawValue)
            //showErrorWithMessage(validationResult.message)
            return
        }
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
        }
        self.loginWith(user!, password: pass!)
    }

    @IBAction func onClickOfRememberMeButton(_ sender:UIButton){
        sender.isSelected = !sender.isSelected
    }

    @IBAction func onClickOfForgotPassword(_ sender:UIButton){
        let forgotPasswordVC = AppStoryboard.Main.viewController(ForgotPasswordViewController.self)
        self.view.endEditing(true)
        self.navigationController?.pushViewController(forgotPasswordVC, animated: true)
    }
    @IBAction func onClickOfTermsAndConditions(_ sender:UIButton){
        let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
        termsVc.isPrivacyPolicy = true
        termsVc.isFromMenu = false
        self.view.endEditing(true)
        let nav = UINavigationController(rootViewController: termsVc)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false
        
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.present(nav, animated: true, completion: nil)
        //showErrorWithMessage(warningMessage.functionalityPending.rawValue)
       
    }
    @IBAction func onClickOfPrivacyPolicies(_ sender:UIButton){
        //showErrorWithMessage(warningMessage.functionalityPending.rawValue)
        let termsVc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
        termsVc.isPrivacyPolicy = false
        termsVc.isFromMenu = false
        self.view.endEditing(true)
        
        let nav = UINavigationController(rootViewController: termsVc)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false
        
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = false
        self.present(nav, animated: true, completion: nil)
    }


    func loginWith(_ userName:String,password:String){
        if rememberMeButton.isSelected{
            kUserDefaults.set(userName, forKey: kUserName)
            kUserDefaults.set(password, forKey: kPassword)
        }else{
            kUserDefaults.set("", forKey: kUserName)
            kUserDefaults.set("", forKey: kPassword)
        }
        CommonClass.showLoader(withStatus: "Authenticating..")
        LoginService.sharedInstance.loginWith(userName, password: password) {  (success,responseUser,message) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            if let loggedInUser = responseUser, loggedInUser.ID != ""{
                kUserDefaults.set(true, forKey: kIsLoggedIN)
                CommonClass.sharedInstance.reRegisterForFirebase()
    
                if loggedInUser.marinas.count == 0 {
                    showAlertWith(self, message: "There is no marina, first add your marina", title: warningMessage.alertTitle.rawValue)
                }else{
                    
                    UIApplication.shared.delegate?.window??.rootViewController?.dismiss(animated: false, completion: nil)
                    let nav = AppStoryboard.Home.viewController(MainNavigationController.self)
                    UIApplication.shared.delegate?.window??.rootViewController = nav
                    
                }
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    

    func validateParams(_ userName:String,password:String) -> (success:Bool,message:String) {
        if userName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:warningMessage.emailCanNotBeEmpty.rawValue)
        }

        if !CommonClass.validateEmail(userName){
            return (success:false,message: warningMessage.validEmailAddress.rawValue)
        }
        if password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your password")
        }
        if !CommonClass.validatePassword(password){
            return (success:false,message:"The email and password entered to not match our records. Please reenter the information or reset your password if needed")
        }
        return (success:true,message:"")
    }

}

