//
//  UpdatePasswordViewController.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 03/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class UpdatePasswordViewController: UIViewController {
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordTextField: UITextField!
    @IBOutlet weak var confirmedPasswordSeparator: UIView!
    @IBOutlet weak var doneButton: UIButton!
    var user : User!
    override func viewDidLoad() {
        super.viewDidLoad()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)

        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.doneButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    @IBAction func onClickDoneButton(_ sender: UIButton){
        let oldPassword = self.oldPasswordTextField.text!
        let validationResut = self.validateParma(oldPassword: oldPasswordTextField.text!, newPassword: self.newPasswordTextField.text!, confirmPassword: self.confirmedPasswordTextField.text!)
        if !validationResut.success{
            showAlertWith(self, message: "\(validationResut.message)", title:warningMessage.alertTitle.rawValue)
            return
        }else{
            self.updatePassword(userID: self.user.ID, oldPassword: oldPassword, newPassword: self.newPasswordTextField.text!)

        }
//        if let newPassword = newPasswordTextField.text{
//            if newPassword.count >= 6{
//                self.updatePassword(userID: self.user.ID, oldPassword: oldPassword, newPassword: self.newPasswordTextField.text!)
//            }else{
//                showAlertWith(self, message: warningMessage.validPassword.rawValue, title: "Error!")
//            }
//        }
    }
        
    
    func updatePassword(userID: String, oldPassword: String, newPassword: String ) {
        LoginService.sharedInstance.updatePassword(userID, oldPassword: oldPassword, newPassword: newPassword) {(success, message) in
         
            if success{
                self.showSuccessAlert(message: "\(message)\r\nYou can login with new password", title: warningMessage.alertTitle.rawValue)
            }else{
                showAlertWith(self, message: "\r\n\(message)\r\n", title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    
    func showSuccessAlert(message:String,title:String){
        self.confirmedPasswordTextField.text = ""
        self.newPasswordTextField.text = ""
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
            self.navigationController?.popToRoot(true)
        }
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
        alert.view.tintColor = UIColor.white
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    func validateParma(oldPassword: String,newPassword:String,confirmPassword:String) -> (success:Bool,message:String) {
        if oldPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter your current password")
        }
        if oldPassword.count < 6{
          return (success:false,message:"Please enter a valid current password")
        }
        if newPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:warningMessage.validPassword.rawValue)
        }
         if newPassword.count < 6{
            return (success:false,message: warningMessage.validPassword.rawValue)
 
        }
        if confirmPassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please confirm your password")
        }
        if newPassword != confirmPassword {
         return (success:false,message:"Please enter matching passwords")        }
        
        return (success:true,message:"")
    }

    @IBAction func textFieldDidChangeText(_ sender: UITextField){
        if let cnfrmPassword = self.confirmedPasswordTextField.text{
            if let newpswrd = self.newPasswordTextField.text{
                //self.doneButton.isEnabled = (newpswrd == cnfrmPassword)
                if(newpswrd == cnfrmPassword) {
                    self.confirmedPasswordSeparator.backgroundColor = UIColor.lightGray
                    
                }
                self.confirmedPasswordSeparator.backgroundColor = (newpswrd == cnfrmPassword) ? UIColor.lightGray : kApplicationRedColor
            }
        }
    }
//    @IBAction  func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == newPasswordTextField{
//
//        }
//    }
    


}
