//
//  ForgotPasswordViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var emailTextField : UITextField!
    @IBOutlet weak var sendButton : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(false, animated:true)
        self.emailTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        self.sendButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickOfSend(_ sender:UIButton){
        self.processToOTP()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    func processToOTP() {
        self.view.endEditing(true)
        let email = emailTextField.text!
        if email.isEmpty{
            showAlertWith(self, message: "Please enter an email", title: warningMessage.alertTitle.rawValue)
            return
        }
        if !CommonClass.validateEmail(email){
            showAlertWith(self, message: "Please enter a valid email", title: warningMessage.alertTitle.rawValue)
            return
        }
        LoginService.sharedInstance.forgotPasswordForEmail(email) { (success, message) in
            if success{
                self.goToOTPSceen(email)
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done{
            self.processToOTP()
        }
        return false
    }

    func goToOTPSceen(_ email:String){
        let otpVC = AppStoryboard.Main.viewController(OTPVarificationViewController.self)
        otpVC.email = email
        self.navigationController?.pushViewController(otpVC, animated: true)
    }


    @IBAction func onClickOfBackButton(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }


}
