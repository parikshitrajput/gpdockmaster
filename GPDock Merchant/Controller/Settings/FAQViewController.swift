//
//  FAQViewController.swift
//  GPDock
//
//  Created by TecOrb on 07/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var faqModel : FAQModel!
    @IBOutlet weak var supportTableView: UITableView!

    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 45, y: 0, width: self.view.frame.size.width-120, height: 44)
        //titleView.titleLabel.text = faqModel.category.capitalized
        self.navigationItem.titleView = self.titleView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNavigationTitle()
        self.supportTableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        self.supportTableView.estimatedRowHeight = 60
        self.supportTableView.register(UINib(nibName: "SupportQuestionTableViewCell", bundle: nil), forCellReuseIdentifier: "SupportQuestionTableViewCell")
        self.supportTableView.backgroundView?.backgroundColor = UIColor.groupTableViewBackground
        self.supportTableView.backgroundColor = UIColor.groupTableViewBackground
        self.supportTableView.tableFooterView = UIView(frame:CGRect.zero)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        self.navigationController?.pop(true)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return faqModel.questions.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }else{
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SupportQuestionTableViewCell", for: indexPath) as! SupportQuestionTableViewCell
        let question = faqModel.questions[indexPath.row]
        var queryText = "Q. \(question.question)"

        if let quesM = queryText.last{
            if quesM != "?"{
                queryText = queryText+"?"
            }
        }

        queryText = queryText+"\r\n\r\nAns. \(question.answer)\r\n"
        cell.questionTextLabel.text = queryText
        return cell
    }

}








//
//
//func getFAQsFromServer(completionBlock:@escaping (_ success:Bool, _ faqModels:Array<FAQModel>?, _ message:String) -> Void) {
//    let head =  ["Accept": "application/json"]
//    print_debug("hitting \(FAQ_URL) and headers :\(head)")
//    Alamofire.request(FAQ_URL,method: .get , headers:head).responseJSON { response in
//        switch response.result {
//        case .success:
//            if let value = response.result.value {
//                let json = JSON(value)
//                print_debug("faqs json: \(json)")
//                let faqParser = FAQParser(json: json)
//                completionBlock((faqParser.code == 200),faqParser.faqs,faqParser.message)
//            }else{
//                completionBlock(false,nil,response.error?.localizedDescription ?? "Something went wrong")
//            }
//        case .failure(let error):
//            completionBlock(false,nil,error.localizedDescription)
//        }
//    }
//
//}

