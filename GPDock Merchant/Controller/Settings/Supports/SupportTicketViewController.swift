//
//  TicketViewController.swift
//  GPDock
//
//  Created by TecOrb on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
//SupportTicketViewController

protocol SupportTicketViewControllerDelegate {
    func ticket(_ ticket: SupportQuery, didUpdateStatusTo status: Bool)
}

class SupportTicketViewController: UIViewController, UITextViewDelegate {
    @IBOutlet private weak var chatTableView: UITableView!
    // var comments = Array<SupportMessage>()
    var ticket : SupportQuery!
    var delegate : SupportTicketViewControllerDelegate?
    
    
    let toolbar: Toolbar = Toolbar()
    
    var textView: UITextView?
    var item0: ToolbarItem?
    var item1: ToolbarItem?
    var toolbarBottomConstraint: NSLayoutConstraint?
    var user : User!
    var titleView : NavigationTitleView!
    func setupNavigationTitle() {
        titleView = NavigationTitleView.instanceFromNib()
        titleView.frame = CGRect(x: 60, y: 0, width: self.view.frame.size.width-120, height: 44)
        titleView.titleButton.setTitle("\(self.ticket.referenceNumber)", for: .normal)
        self.navigationItem.titleView = self.titleView
    }
    override func loadView() {
        super.loadView()
        self.view.addSubview(toolbar)
        if #available(iOS 11.0, *) {
            self.toolbarBottomConstraint = self.view.bottomAnchor.constraint(equalTo: self.toolbar.bottomAnchor, constant: 0)
        } else {
            // Fallback on earlier versions
        }
        self.toolbarBottomConstraint?.isActive = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.user = User(json:User.loadUserInfo())
        self.toolbar.maximumHeight = 100
        self.toolbar.backgroundColor = UIColor.lightGray
        let view: UITextView = UITextView(frame: .zero)
        view.delegate = self
        view.font = fonts.Raleway.regular.font(.medium)
        self.textView = view
        self.item0 = ToolbarItem(customView: view)
        self.item1 = ToolbarItem(title: "Send", target: self, action: #selector(send))
        self.toolbar.setItems([self.item0!, self.item1!], animated: false)
        //        self.toolbar.isUserInteractionEnabled = !ticket.status
        self.setupNavigationTitle()
        let gestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hide))
        self.view.addGestureRecognizer(gestureRecognizer)
        self.item1?.isEnabled = false
        
        
        chatTableView.register(UINib(nibName: "TicketDetailsCell", bundle: nil), forCellReuseIdentifier: "TicketDetailsCell")
        chatTableView.register(UINib(nibName: "ImageOfTicketDetailCell", bundle: nil), forCellReuseIdentifier: "ImageOfTicketDetailCell")
        
        chatTableView.register(UINib(nibName: "UserCommentCell", bundle: nil), forCellReuseIdentifier: "UserCommentCell")
        
        chatTableView.rowHeight = UITableViewAutomaticDimension
        chatTableView.estimatedRowHeight = 20.0
        self.viewTicketDetails(ticketID: self.ticket.id)
        self.chatTableView.dataSource = self
        self.chatTableView.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    var bottomSafeArea: CGFloat = 0 {
        didSet {
            if #available(iOS 11.0, *) {
                self.additionalSafeAreaInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.bottomSafeArea, right: 0)
            } else {
                // Fallback on earlier versions
            }
            self.view.setNeedsLayout()
        }
    }
    
    @objc func setSafeArea() {
        if #available(iOS 11.0, *) {
            self.bottomSafeArea = self.additionalSafeAreaInsets.bottom == 0 ? 0 : 0
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    
    
    @objc func hide() {
        self.textView?.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc final func keyboardWillShow(notification: Notification) {
        moveToolbar(up: true, notification: notification)
    }
    
    @objc final func keyboardWillHide(notification: Notification) {
        moveToolbar(up: false, notification: notification)
    }
    
    final func moveToolbar(up: Bool, notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        self.view.layoutIfNeeded()
        let animationDuration: TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardHeight = up ? (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height : 0
        
        // Animation
        self.toolbarBottomConstraint?.constant = keyboardHeight
        UIView.animate(withDuration: animationDuration, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func send() {
        self.item1?.isEnabled = false
        guard let text = self.textView?.text else {
            return
        }
        if text.isEmpty{return}
        SupportService.sharedInstance.addCommentForSupportQuery(self.user.ID, supportID: self.ticket.id, message: text) { (success, resMessage, message) in
            self.textView?.resignFirstResponder()
            if success{
                if let chatMessage = resMessage{
                    self.ticket.supportChats.append(chatMessage)
                    self.ticket.status = false
                    self.delegate?.ticket(self.ticket, didUpdateStatusTo: false)
                    self.chatTableView.reloadData()
                    self.textView?.text = nil
                    if let constraint: NSLayoutConstraint = self.constraint {
                        self.textView?.removeConstraint(constraint)
                    }
                    self.toolbar.setNeedsLayout()
                    if self.ticket.supportChats.count > 0{
                        let indexPath = IndexPath(row: self.ticket.supportChats.count-1, section: 1)
                        self.chatTableView.scrollToRow(at: indexPath, at: .none, animated: false)
                    }
                }else{
                    self.item1?.isEnabled = false
                    GPD_Dockmaster.showErrorWithMessage(message)
                }
            }else{
                self.item1?.isEnabled = false
                GPD_Dockmaster.showErrorWithMessage(message)
            }
        }
        self.textView?.resignFirstResponder()
        self.textView?.text = nil
        if let constraint: NSLayoutConstraint = self.constraint {
            self.textView?.removeConstraint(constraint)
        }
        self.toolbar.setNeedsLayout()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        //        self.isMenuHidden = true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.item1?.isEnabled = !textView.text.isEmpty
        
        let size: CGSize = textView.sizeThatFits(textView.bounds.size)
        if let constraint: NSLayoutConstraint = self.constraint {
            textView.removeConstraint(constraint)
        }
        self.constraint = textView.heightAnchor.constraint(equalToConstant: size.height)
        self.constraint?.priority = UILayoutPriority.defaultHigh
        self.constraint?.isActive = true
    }
    
    var constraint: NSLayoutConstraint?
    
    // MARK: -
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        self.toolbar.setNeedsUpdateConstraints()
    }
}

extension SupportTicketViewController:UITableViewDataSource,UITableViewDelegate{
    func viewTicketDetails(ticketID: String){
        CommonClass.showLoader(withStatus: "Loading..")
        SupportService.sharedInstance.getDetailsForSupportWith(ticketID) { (success, resTicket, message) in
            CommonClass.hideLoader()
            if success{
                if let theTicket = resTicket{
                    self.ticket = theTicket
                    self.setupNavigationTitle()
                    self.chatTableView.reloadData()
                    //                    self.toolbar.isUserInteractionEnabled = !self.ticket.status
                    if self.ticket.supportChats.count > 0{
                        let indexPath = IndexPath(row: self.ticket.supportChats.count-1, section: 1)
                        self.chatTableView.scrollToRow(at: indexPath, at: .none, animated: false)
                    }
                }else{
                    GPD_Dockmaster.showAlertWithReset(viewController: self, message: message, title: warningMessage.alertTitle.rawValue, complitionBlock: {(done) in
                        self.navigationController?.pop(true)
                    })
                }
            }else{
                GPD_Dockmaster.showAlertWithReset(viewController: self, message: message, title: warningMessage.alertTitle.rawValue, complitionBlock: {(done) in
                    self.navigationController?.pop(true)
                })
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.ticket.image.isEmpty{
            return (section == 0) ? 1 : self.ticket.supportChats.count
        }else{
            return (section == 0) ? 2 : self.ticket.supportChats.count
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 44
        switch (indexPath.section,indexPath.row) {
        case (0,0):
            height = self.chatTableView.frame.width
        case (0,1):
            height = 50
        default:
            height = 44
        }
        return height
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 44
        switch (indexPath.section,indexPath.row) {
        case (0,0):
            height = self.ticket.image.isEmpty ? UITableViewAutomaticDimension : self.chatTableView.frame.width
        case (0,1):
            height = UITableViewAutomaticDimension
        default:
            height = UITableViewAutomaticDimension
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (indexPath.section == 0) ? self.detailsCell(tableView, cellForRowAt: indexPath) : self.commentCellCell(tableView, cellForRowAt: indexPath)
        return cell
    }
    
    
    func detailsCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if self.ticket.image.isEmpty{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TicketDetailsCell") as? TicketDetailsCell else {
                return UITableViewCell()
            }
            cell.statusLabel.text = ticket.status ? "Closed" : "Opened"
            cell.statusLabel.textColor = ticket.status ? kApplicationRedColor : kNavigationColor
            cell.referenceNumberLabel.text = ticket.referenceNumber
            cell.messageLabel.text = ticket.message
            return cell
        }else{
            if indexPath.row == 0{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ImageOfTicketDetailCell") as? ImageOfTicketDetailCell else {
                    return UITableViewCell()
                }
                cell.ticketImageView.setShowActivityIndicator(true)
                cell.ticketImageView.setIndicatorStyle(.gray)
                cell.ticketImageView.sd_setImage(with: URL(string:ticket.image),placeholderImage: #imageLiteral(resourceName: "placeholder_city"))
                return cell
            }else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "TicketDetailsCell") as? TicketDetailsCell else {
                    return UITableViewCell()
                }
                cell.statusLabel.text = ticket.status ? "Closed" : "Opened"
                cell.statusLabel.textColor = ticket.status ? kApplicationRedColor : kNavigationColor
                cell.referenceNumberLabel.text = ticket.referenceNumber
                cell.messageLabel.text = ticket.message
                return cell
            }
        }
    }
    
    func commentCellCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserCommentCell") as? UserCommentCell else {
            return UITableViewCell()
        }
        let comment = self.ticket.supportChats[indexPath.row]
        cell.configure(withMessage: comment.reply, currentUserIsSender: (comment.replyBy == .you))
        return cell
    }
    
    
}






