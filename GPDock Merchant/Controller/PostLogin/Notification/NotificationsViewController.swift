//
//  NotificationsViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 08/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import MarqueeLabel


class NotificationsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
 var user : User!
  var notifications = [NotificationModel]()
    @IBOutlet weak var notificationsTableView : UITableView!
        var isNewDataLoading = false
    var pageNumber = 1
    var recordsPerPage = 10
    var selectedIndex = -1
    var bookingStatus:String = ""
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(NotificationsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.notificationsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.notificationsTableView.dataSource = self
        self.notificationsTableView.delegate = self
         self.notificationsTableView.addSubview(self.refreshControl)
        self.loadNotificationFor(self.user.ID, role: "business", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
    self.navigationController?.pop(true)
    }
    
    func loadNotificationFor(_ userId:String,role:String, pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            notifications.removeAll()
            notificationsTableView.reloadData()
        }
        //self.isNewDataLoading = true
        NotificationService.sharedInstance.getNotificaation(userId, role: role, page:pageNumber,perPage: recordPerPage) { (response) in

            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.notifications.append(contentsOf: newJobArray)
                self.notificationsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
        }

        }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.isNewDataLoading = true
        self.notificationsTableView.reloadData()
        NotificationService.sharedInstance.getNotificaation(self.user.ID, role: "business",page:pageNumber,perPage: recordsPerPage) { (response) in
            self.notifications.removeAll()
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newNotificationArray = response{
                self.notifications.append(contentsOf: newNotificationArray)
                self.notificationsTableView.reloadData()
            }
        }
    }

    func getDateFromString(_ dateString: String) -> Date?
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            return date
        }else{
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (notifications.count > 0) ? notifications.count : 1 ;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if notifications.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Notification found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsCell", for: indexPath) as! NotificationsCell
             let notification = self.notifications[indexPath.row]
            if notification.notificationType == .payAtMarina{
                cell.message.attributedText = self.attributtedStringFor(notification.message)
            }else{
                cell.message.text = notification.message
            }
            
            let notifyType = notification.notificationType
            let changeNotifyType = notifyType.rawValue.replacingOccurrences(of: "_", with: " ")
            cell.messageType.text = changeNotifyType.capitalized
           
            cell.receiveTime.text = notification.receivedAt
            cell.profileImage.setIndicatorStyle(.gray)
            cell.profileImage.setShowActivityIndicator(true)
            cell.profileImage.layer.cornerRadius = 5
            cell.profileImage.layer.masksToBounds = true

            cell.profileImage.sd_setImage(with: URL(string:notification.booking.user.profileImage),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if notifications.count == 0{
            return
        }
        let notification = self.notifications[indexPath.row]
        if (notification.notificationType == .customerCreateBooking) {
            self.bookingStatus = "Occupied"
            self.navigateToOccupiedeBookingDetails(notification: notification, status: self.bookingStatus)

        }else if(notification.notificationType == .businessCreateBooking){
            self.bookingStatus = "Occupied"
            self.navigateToOccupiedeBookingDetails(notification: notification, status: self.bookingStatus)
        }else if(notification.notificationType == .bookingCancelByCustomer){
            self.navigateToCancellBookingDetails(notification: notification)
        }else if(notification.notificationType == .bookingCancelByBusiness){
            self.navigateToCancellBookingDetails(notification: notification)
        }else if(notification.notificationType == .bookingCompleteByBusiness){
            self.bookingStatus = "Completed"
            self.navigateToOccupiedeBookingDetails(notification: notification, status: self.bookingStatus)
        }else if(notification.notificationType == .payoutByAdmin){
             let PayoutDetailVC = AppStoryboard.Earning.viewController(PayoutsViewController.self)
            self.navigationController?.pushViewController(PayoutDetailVC, animated: true)
        }else if(notification.notificationType == .payAtMarina) {
            let PayoutDetailVC = AppStoryboard.Earning.viewController(PayoutsViewController.self)
            self.navigationController?.pushViewController(PayoutDetailVC, animated: true)
        }else if(notification.notificationType == .supportReplyByAdmin) {
           let supportVC = AppStoryboard.Settings.viewController(AllTicketsViewController.self)
            self.navigationController?.pushViewController(supportVC, animated: true)

        }else if(notification.notificationType == .changeMarinaStateByAdmin) {
            
        }else if(notification.notificationType == .rebookedInfo) {
          self.navigateToUpCommingBookingDetail(notification: notification)
        }else{
            let PayoutDetailVC = AppStoryboard.Earning.viewController(PayoutsViewController.self)
            self.navigationController?.pushViewController(PayoutDetailVC, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == notificationsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        pageNumber+=1
                        self.loadNotificationFor(self.user.ID, role: "business", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                    }
                }
            }
        }
        
    }
    
    func attributtedStringFor(_ message: String) -> NSAttributedString{
        //"____ paid $____ for _______________________"
        
        let attributtedString  = NSMutableAttributedString(string: message)
        
        let startIndexOfUserName = message.startIndex
        
        guard let endIndexofUserName = message.index(of: "paid") else {
            return attributtedString
        }
        
        guard let startIndexOfAmount = message.index(of: "$") else {
            return attributtedString
        }
        
        guard let endIndexofAmount = message.index(of: "for") else {
            return attributtedString
        }
        
        let fontAttributes: [NSAttributedStringKey: Any] = [
            .font : fonts.Raleway.semiBold.font(.medium)
        ]
        
        let userNameRange: Range = startIndexOfUserName..<endIndexofUserName
        let amountRange : Range = startIndexOfAmount..<endIndexofAmount
        
        attributtedString.addAttributes(fontAttributes, range: userNameRange.nsRange)
        attributtedString.addAttributes(fontAttributes, range: amountRange.nsRange)

        return attributtedString
    }
    
    
    func navigateToOccupiedeBookingDetails(notification: NotificationModel, status:String) -> Void {
        let occupiedDetailVC = AppStoryboard.Booking.viewController(NotificationOccupiedViewController.self)
        occupiedDetailVC.booking = notification.booking
        occupiedDetailVC.slip =  notification.booking.parkingSpace
        occupiedDetailVC.bookingStatus = status
        self.navigationController?.pushViewController(occupiedDetailVC, animated: true)
    }
    func navigateToCancellBookingDetails(notification: NotificationModel) -> Void {
        let completeDetailVC = AppStoryboard.Booking.viewController(CancelledBookingDetailViewController.self)
        completeDetailVC.booking = notification.booking
        self.navigationController?.pushViewController(completeDetailVC, animated: true)
    }
    
    func navigateToUpCommingBookingDetail(notification: NotificationModel) -> Void {
        let completeDetailVC = AppStoryboard.Booking.viewController(UpcomingBookingDetailViewController.self)
        completeDetailVC.booking = notification.booking
        self.navigationController?.pushViewController(completeDetailVC, animated: true)
    }
    
    
}


class NotificationsCell: UITableViewCell {
   // @IBOutlet weak var message : MarqueeLabel!
    @IBOutlet weak var message : UILabel!

    @IBOutlet weak var messageType :UILabel!

    @IBOutlet weak var receiveTime : UILabel!
    @IBOutlet weak var profileImage : UIImageView!
}

