//
//  MenuViewController.h
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//


import UIKit
import SDWebImage

let kDarkLeftMenuCellColor = UIColor(red:46.0/255.0, green:53.0/255.0, blue:67.0/255, alpha:1.0)
let kLightLeftMenuCellColor = UIColor(red:50.0/255.0, green:58.0/255.0, blue:71.0/255, alpha:1.0)


class LeftMenuViewController: UIViewController, UITableViewDataSource,UITableViewDelegate  {

    @IBOutlet weak var tableView: UITableView!
    var slideOutAnimationEnabled : Bool!
    // let leftMenuOptions = ["Profile","Book Your Ride","Your Rides","Payments","Settings","Share","About Us","Terms & Conditions","Logout"]
    // let leftMenuIcons = ["user_img","item-icon","watch","rupeeWhite","setting_icon","share_icon","aboutUs","term_icon","logout_icon"]
    let leftMenuOptions = ["Profile","Book Your Marina","Your Bookings","Filter","Settings","Share","About Us","Terms & Conditions","Logout"]
    let leftMenuIcons = ["user_img","item-icon","watch","filterMenu","setting_icon","share_icon","aboutUs","term_icon","logout_icon"]

    var user : User!
    //var myBookingVC : MyBookingsViewController!

    override func viewDidLoad() {
        slideOutAnimationEnabled = false
        super.viewDidLoad()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        //self.myBookingVC = AppStoryboard.Home.viewController(MyBookingsViewController.self)
//        MainNavigationController.sharedInstance().enableShadow = false
//        MainNavigationController.sharedInstance().enableSwipeGesture = true
//        SlideNavigationController.sharedInstance().portraitSlideOffset = UIScreen.main.bounds.size.width * 1 / 4
//        self.tableView.separatorColor = UIColor.white
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0)
        self.automaticallyAdjustsScrollViewInsets = false
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if tableView != nil{
            self.tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return leftMenuOptions.count;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 0) ? 90 : 48

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let colorView = UIView()
        colorView.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuProfileCell", for: indexPath) as! MenuProfileCell
            cell.profileIcon.sd_setImage(with: URL(string:self.user.profileImage), placeholderImage: #imageLiteral(resourceName: "dp"))
            cell.nameLabel.text = "\(self.user.firstName) \(self.user.lastName)"
            cell.selectedBackgroundView = colorView
            return cell
        }else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath) as! LeftMenuCell
        cell.cellIcon.image = UIImage(named:leftMenuIcons[indexPath.row])
        cell.nameLabel.text = leftMenuOptions[indexPath.row]
        cell.selectedBackgroundView = colorView

        return cell
        }
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        var vc : UIViewController!
//        switch indexPath.row {
//        case 0:
//            return
//        // vc = AppStoryboard.Profile.viewController(ProfileViewController.self)
//        case 1:
//            vc = AppStoryboard.Home.viewController(HomeTabBarViewController.self)
//        case 2:
//            vc = AppStoryboard.Home.viewController(MyBookingsViewController.self)//myBookingVC//AppStoryboard.Booking.viewController(MyBookingsViewController.self)
//            //SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
//
//        case 3:
//            return
//        //vc = AppStoryboard.Settings.viewController(SettingsViewController.self)
//        case 4:
//            //shareApplication()
//            return
//        case 5:
//            return
//        // vc = AppStoryboard.Settings.viewController(AboutUsViewController.self)
//        case 6:
//            return
//        //vc = AppStoryboard.Settings.viewController(TermsAndConditionsViewController.self)
//        case 7:
//            // askForLogout()
//            return
//        default:
//            return
//        }
//        //SlideNavigationController.sharedInstance().popToRootAndSwitch(to: vc, withSlideOutAnimation: self.slideOutAnimationEnabled, andCompletion: nil)
//    }
    func shareApplication(){
        // SlideNavigationController.sharedInstance().closeMenu {
            let textToShare = "Check this application, you will definitily get some benifit!"
            let objectsToShare = [textToShare]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            activityVC.popoverPresentationController?.sourceView = appDelegate.window?.rootViewController?.view
            appDelegate.window?.rootViewController?.present(activityVC, animated: true, completion: nil)
        //}
    }

    func rateTheAppApplication(){
        // SlideNavigationController.sharedInstance().closeMenu {
            self.rateApp(appId: "1241899105", completion: { (success) in
                if !success{
                    showErrorWithMessage("Couldn't be reviewed")
                }
            })
        // }
    }

    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8&id=" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }

    func askForLogout() {
            let alert = UIAlertController(title: "GPDock", message: "\r\n\r\nWould you really want to logout?\r\n\r\n", preferredStyle: .alert)
            let okayAction = UIAlertAction(title: "Okay", style: .default){(action) in
                alert.dismiss(animated: true, completion: nil)
                self.logout()
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel){(action) in
                alert.dismiss(animated: true, completion: nil)
            }
        _ = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        // subview.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background"))
        alert.view.tintColor = UIColor.white

            alert.addAction(okayAction)
            alert.addAction(cancelAction)
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }

    func logout() {

        kUserDefaults.set(false, forKey: kIsLoggedIN)
        let user = User()
        user.saveUserInfo(user)
        for key in kUserDefaults.dictionaryRepresentation().keys {
            kUserDefaults.removeObject(forKey: key)
        }

        //SlideNavigationController.sharedInstance().closeMenu {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let nav = mainStoryboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
            let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
            appDelegate.window?.rootViewController = nav
        }
    //}

}



