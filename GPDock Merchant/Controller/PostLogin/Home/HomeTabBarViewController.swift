//
//  HomeTabBarViewController.swift
//  GPDock
//
//  Created by TecOrb on 09/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
import AVFoundation
import UIKit


class HomeTabBarViewController: UITabBarController {

    var floorPlan = FloorPlan()
    var user : User!
    var bookingTabButton : UITabBarItem!
    var homeTabButton : UITabBarItem!
    var profileTabButton : UITabBarItem!
    var earningTabButton : UITabBarItem!
    var marinaPicker : UIPickerView!
    var statusBarView : UIView!
    var navigationTitleView : NavigationTitleView!
    var selectedMarina : Marina!
    var per : Int = 0
    var setBadge:Double = 0.0



    override func viewDidLoad() {
        super.viewDidLoad()

        let userjson = User.loadUserInfo()
        self.user = User(json: userjson)
       // self.tabBar.barTintColor = kNavigationColor
        //self.tabBar.backgroundColor = .white
       self.tabBar.tintColor = UIColor.white
        homeTabButton = UITabBarItem()
        homeTabButton.title = "Home"

        bookingTabButton = UITabBarItem()
        bookingTabButton.title = "Bookings"

        earningTabButton = UITabBarItem()
        earningTabButton.title = "Reports"

        profileTabButton = UITabBarItem()
        profileTabButton.title = "Account"

        setUpTabBarElements()

        self.selectedMarina = CommonClass.sharedInstance.getSelectedMarina()
        let homeVC = AppStoryboard.Home.viewController(FloorPlanViewController.self)
        homeVC.delegate = self
        homeVC.marina = self.selectedMarina
        homeVC.tabBarItem = homeTabButton

        let bookingsVC = AppStoryboard.Booking.viewController(MyBookingsViewController.self)
        bookingsVC.tabBarItem = bookingTabButton
        

        let earningVC = AppStoryboard.Earning.viewController(EarningsViewController.self)
        earningVC.tabBarItem = earningTabButton

        let profileVC = AppStoryboard.Profile.viewController(ProfileViewController.self)
        profileVC.tabBarItem = profileTabButton

        self.setupRightBarButtons()
        self.viewControllers = [homeVC,bookingsVC,earningVC,profileVC]
        self.selectedIndex = 0
        self.selectedMarina = CommonClass.sharedInstance.getSelectedMarina()
        self.setnavView(title: "Occupancy rate")
        //self.setUpNavigationView(marina: self.selectedMarina)
    }
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: UPDATE_LIST_NOTIFICATION), object: nil)
    }
    
    deinit{
        removeObserver()
            NotificationCenter.default.removeObserver(self)
    }

    override func viewWillAppear(_ animated: Bool) {


        self.navigationController?.navigationBar.isHidden = false
        super.viewWillAppear(animated)

        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        self.statusBarView = statusBar
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
            statusBar.backgroundColor = UIColor.groupTableViewBackground//(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 1.0)
        }
    }

   override func viewDidLayoutSubviews() {
        self.tabBar.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0,1.0])

    }

    func setUpTabBarElements() {
        let selAttDict = [NSAttributedStringKey.foregroundColor:UIColor.white]
        let normalAttDict = [NSAttributedStringKey.foregroundColor:UIColor(red:155.0/255.0, green:180.0/255.0, blue:247.0/255.0, alpha:1.0)]

        self.homeTabButton.image = UIImage(named: "home_unsel")?.withRenderingMode(.alwaysOriginal)
        self.homeTabButton.selectedImage = UIImage(named: "home_sel")?.withRenderingMode(.alwaysOriginal)
        self.homeTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.homeTabButton.setTitleTextAttributes(normalAttDict, for: .normal)
        
        self.bookingTabButton.image = UIImage(named:"booking_unsel")?.withRenderingMode(.alwaysOriginal)
        self.bookingTabButton.selectedImage = UIImage(named: "booking_sel")?.withRenderingMode(.alwaysOriginal)
        self.bookingTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.bookingTabButton.setTitleTextAttributes(normalAttDict, for: .normal)

        self.earningTabButton.image = UIImage(named: "report_unsel")?.withRenderingMode(.alwaysOriginal)
        self.earningTabButton.selectedImage = UIImage(named: "report_sel")?.withRenderingMode(.alwaysOriginal)
        self.earningTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.earningTabButton.setTitleTextAttributes(normalAttDict, for: .normal)

        self.profileTabButton.image = UIImage(named: "account_unsel")?.withRenderingMode(.alwaysOriginal)
        self.profileTabButton.selectedImage = UIImage(named: "account_sel")?.withRenderingMode(.alwaysOriginal)
        self.profileTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.profileTabButton.setTitleTextAttributes(normalAttDict, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        //this line is for farzi request to check weather the session it expired or not
       // LoginService.sharedInstance.updateDeviceTokeOnServer()
        // End of farzi line
        
        if item == homeTabButton{
            self.navigationTitleView.titleLabel.isHidden = true
            self.setnavView(title: "Occupancy rate")
            setTittleBadge(rate: setBadge)
        }else if item == bookingTabButton{
            self.navigationTitleView.titleLabel.isHidden = true
            self.setnavView(title: "Bookings")
            self.navigationTitleView.titleButton.badgeValue = ""

//              self.navigationTitleView.titleButton.isHidden = true
//              self.navigationTitleView.titleLabel.text = "Bookings"
        }else if item == earningTabButton{
            self.navigationTitleView.titleLabel.isHidden = true
            self.setnavView(title: "Reports")
            self.navigationTitleView.titleButton.badgeValue = ""

//            self.navigationTitleView.titleButton.isHidden = true
//            self.navigationTitleView.titleLabel.text = "Reports"

        }else{
            self.navigationTitleView.titleLabel.isHidden = true
            self.setnavView(title: "My Account")
            self.navigationTitleView.titleButton.badgeValue = ""


//            self.navigationTitleView.titleButton.isHidden = true
//            self.navigationTitleView.titleLabel.text = "My Account"

        }
    }



    // MARK: - Actions
    func setupRightBarButtons() {
        let mailButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        mailButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        mailButton.setImage(#imageLiteral(resourceName: "notification_button"), for: .normal)
        mailButton.addTarget(self, action: #selector(onClickMessageButton(_:)), for: .touchUpInside)
        let mailBarButton = UIBarButtonItem(customView: mailButton)
        let scannerButton = UIButton(frame: CGRect(x:0, y:0, width:35, height:35))
        scannerButton.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        scannerButton.setImage(#imageLiteral(resourceName: "scan_action"), for: .normal)
        scannerButton.addTarget(self, action: #selector(onClickScannerButton(_:)), for: .touchUpInside)
        let scannerBarButton = UIBarButtonItem(customView: scannerButton)
        self.navigationItem.setRightBarButtonItems(nil, animated: false)
        self.navigationItem.setRightBarButtonItems([/*negativeSpacer,*/scannerBarButton], animated: false)
        self.navigationItem.setLeftBarButtonItems([mailBarButton/*negativeSpacer,*/], animated: false)
    }

    @IBAction func onClickMessageButton(_ sender: UIButton){
        let notificationVC = AppStoryboard.Booking.viewController(NotificationsViewController.self)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }

    @IBAction func onClickScannerButton(_ sender: UIButton){
        let scannerVC = AppStoryboard.Scanner.viewController(ScannerViewController.self)
        self.navigationController?.pushViewController(scannerVC, animated: false)
    }

    func setnavView(title:String) {
        self.navigationTitleView = NavigationTitleView.instanceFromNib()
        self.navigationTitleView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 120, height: 44)
        self.navigationTitleView.titleButton.setTitle(title, for: .normal)
        self.navigationItem.titleView = self.navigationTitleView
        self.navigationTitleView.setNeedsLayout()
        self.navigationTitleView.layoutIfNeeded()
        //self.setTittleBadge(rate: self.setBadge)
    }
    func setTittleBadge(rate:Double) {
        self.navigationTitleView.titleButton.badgeValue = String(format: "%0.0f",rate) + "%"
        self.navigationTitleView.titleButton.badgeOriginY =  8.0
        self.navigationTitleView.titleButton.badgeOriginX = self.navigationTitleView.titleButton.frame.size.width + 2.0
        self.navigationTitleView.titleButton.badgeBGColor = kNavigationColor
        self.navigationTitleView.titleButton.badgeTextColor = UIColor.white
    }
}

extension HomeTabBarViewController: FloorPlanViewControllerDelegate {
    func floorPlanViewController(viewController: FloorPlanViewController, didGetOccupancyRate rate: Double) {
        
        self.setBadge = rate
        self.navigationTitleView.titleButton.badgeValue = String(format: "%0.0f",rate) + "%"
        self.navigationTitleView.titleButton.badgeOriginY =  8.0
        self.navigationTitleView.titleButton.badgeOriginX = self.navigationTitleView.titleButton.frame.size.width + 2.0
        self.navigationTitleView.titleButton.badgeBGColor = kNavigationColor
        self.navigationTitleView.titleButton.badgeTextColor = UIColor.white
    }
    
    
}





//if let preSelectedDates = self.selectedBookingDate{
//    self.date1 = preSelectedDates.fromDate
//    self.date2 = preSelectedDates.toDate
//    calendar.select(self.date1, scrollToDate: false)
//    calendar.select(self.date2, scrollToDate: true)
//    self.configureVisibleCells()
//}

