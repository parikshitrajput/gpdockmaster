//
//  DashBoardTabBarController.swift
//  CarDetailingApp
//
//  Created by TecOrb on 23/01/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
let SELECT_BRANCH_NOTIFICATION = "SelectBranchNotification"
let ACCEPT_BOOKING_NOTIFICATION = "AcceptBookingNotification"
let CANCEL_BOOKING_NOTIFICATION = "CancelBookingNotification"
let COMPLETE_BOOKING_NOTIFICATION = "CompleteBookingNotification"
import UIKit
//import BTNavigationDropdownMenu
let kWeatherSummary = "weatherSummary"
let kTemperature = "temperature"
let kDaysSinceLastService = "daysSinceLastService"
let kWeatherUpdateNotification = "WeatherUpdateNotification"
let kDaysSinceLastServiceNotification = "DaysSinceLastServiceNotification"
let tabBarHeight : CGFloat = 55
class DashBoardTabBarController: UITabBarController {
    var requestTabButton : UITabBarItem!
    var upComingTabButton : UITabBarItem!
    var addTabButton : UIButton!
    var addTabButtonTabButton : UITabBarItem!
    var menuView : BTNavigationDropdownMenu!
    var notAvilableTabButton : UITabBarItem!
    var cancelledTabButton : UITabBarItem!
    var statusBarView : UIView!
    var user: User!
    var branch = [Branches]()
    //var selectIndex = 2
    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson)
        self.tabBar.backgroundColor = UIColor.init(colorLiteralRed: 44.0/255, green: 151.0/255, blue: 161.0/255, alpha: 0.5)
        self.tabBar.barTintColor = UIColor.init(colorLiteralRed: 44.0/255, green: 151.0/255, blue: 161.0/255, alpha: 0.5)
        NotificationCenter.default.addObserver(self, selector:#selector(acceptBookingAddInCurrentBookingSuccessfully(_:)) , name: NSNotification.Name(ACCEPT_BOOKING_NOTIFICATION), object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(cancelBookingAddInCurrentBookingSuccessfully(_:)) , name: NSNotification.Name(CANCEL_BOOKING_NOTIFICATION), object: nil)

        self.tabBar.tintColor = UIColor.white
        requestTabButton = UITabBarItem()
        requestTabButton.title = "Request"
        upComingTabButton = UITabBarItem()
        upComingTabButton.title = "UpComing"
        notAvilableTabButton = UITabBarItem()
        notAvilableTabButton.title = "complete"
        cancelledTabButton = UITabBarItem()
        cancelledTabButton.title = "Cancelled"
        addTabButtonTabButton = UITabBarItem()
        //addTabButtonTabButton.title = "All"

        setUpTabBarElements()
        let storyboard = UIStoryboard(name: "Main", bundle:nil)
        
        let requestVC = storyboard.instantiateViewController(withIdentifier: "PendingRequestViewController") as! PendingRequestViewController
        requestVC.tabBarItem = requestTabButton
        
        let upComingVC = storyboard.instantiateViewController(withIdentifier: "UpComingSecondViewController") as! UpComingSecondViewController
        upComingVC.tabBarItem = upComingTabButton
        
        let addVC = storyboard.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        addVC.tabBarItem = addTabButtonTabButton

        let notAvailableVC = storyboard.instantiateViewController(withIdentifier: "UpComingViewController") as! UpComingViewController
        notAvailableVC.tabBarItem = notAvilableTabButton
        
        let cancelledVC = storyboard.instantiateViewController(withIdentifier: "TableCancelledViewController") as! TableCancelledViewController
        cancelledVC.tabBarItem = cancelledTabButton
        self.viewControllers = [requestVC, upComingVC,addVC, cancelledVC,notAvailableVC]
        setupMiddleButton()
        selectedIndex = 2
        self.showIndex()
//        self.navigationItem.title = "Dashboard"
        self.setupRightBarButton()
        addTabButton.isSelected = (self.selectedIndex == 2)
        self.loadRestaurantsFromServer(userID: self.user.ID)
    }
    
    func showIndex() {
        if selectedIndex == 3 {
            self.selectedIndex = 3
            addTabButton.isSelected = (self.selectedIndex == 3)
        }else if selectedIndex == 0 {
            self.selectedIndex = 0
            addTabButton.isSelected = (self.selectedIndex == 0)
        }else {
            self.selectedIndex = 2
            addTabButton.isSelected = (self.selectedIndex == 2)
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        self.statusBarView = statusBar
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)){
//            statusBar.backgroundColor = UIColor.black
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if self.menuView != nil{
            self.menuView.hide()
        }
        self.showIndex()
        self.loadRestaurantsFromServer(userID: self.user.ID)
    }
    
    func setupDropDown(){
       // let items = [["title":"Test Branch 1","details":"45"], ["title":"Test Branch 2","details":"40"], ["title":"Test Branch 3","details":"47"], ["title":"Test Branch 4","details":"70"], ["title":"Test Branch 78","details":"49"]]
        let items = self.setBranchItem()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor(patternImage: #imageLiteral(resourceName: "header_bar"))
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        var menuTitle = items[0]["title"]
        if let selectedBranchID = kUserDefaults.value(forKey: kBranchID) as? String{
            let sb = branch.filter({ (b) -> Bool in
                return b.ID == selectedBranchID
            })
            if sb.count > 0{
                menuTitle = sb[0].address
            }else{
                menuTitle = items[0]["title"]!
            }
        }
        menuView = BTNavigationDropdownMenu(navigationController: self.navigationController, containerView: self.navigationController!.view, title: menuTitle!, items: items as [AnyObject])
        menuView.cellBackgroundColor = self.navigationController?.navigationBar.barTintColor
        menuView.cellSelectionColor = UIColor(red: 0.0/255.0, green:148.0/255.0, blue:163.0/255.0, alpha: 1.0)
        menuView.shouldKeepSelectedCellColor = true
        menuView.cellTextLabelColor = UIColor.white
        menuView.cellTextLabelFont = UIFont(name: "Avenir-Heavy", size: 17)
        menuView.cellTextLabelAlignment = .left // .Center // .Right // .Left
        menuView.checkMarkImage = #imageLiteral(resourceName: "more_tick")
        menuView.animationDuration = 0.5
        menuView.maskBackgroundColor = UIColor.black
        menuView.maskBackgroundOpacity = 0.3
        menuView.didSelectItemAtIndexHandler = {(indexPath: Int) -> () in
            let id = self.branch[indexPath].ID
            kUserDefaults.set(id, forKey: kBranchID)
            print(id)
//            self.viewDidLoad()
             NotificationCenter.default.post(name: NSNotification.Name(SELECT_BRANCH_NOTIFICATION), object: nil, userInfo: ["branchID":id])
        }
        self.navigationItem.titleView = menuView
    }
    
    func setBranchItem() -> Array<Dictionary<String,String>> {
        var itemArray = [Dictionary<String,String>]()
        for branch in self.branch{
            var dict = Dictionary<String,String>()
            dict.updateValue(branch.address, forKey: "title")
            dict.updateValue(branch.pendingBooking, forKey: "details")
            itemArray.append(dict)
        }
        return itemArray
    }
    
    func setupMiddleButton() {
        let width : CGFloat = self.view.frame.size.width/5
        let height = tabBarHeight //self.tabBar.frame.size.height
        addTabButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: width, height: height+5))
        var menuButtonFrame = addTabButton.frame
        menuButtonFrame.origin.y = self.view.bounds.height - menuButtonFrame.height-64
        menuButtonFrame.origin.x = self.view.bounds.width/2 - menuButtonFrame.size.width/2
        addTabButton.frame = menuButtonFrame
        addTabButton.backgroundColor = UIColor.clear
        self.view.addSubview(addTabButton)
        //addTabButton.setImage(UIImage(named: "menu_icon1_sel")!.withRenderingMode(.alwaysOriginal), for: UIControlState.normal)
        addTabButton.setBackgroundImage(#imageLiteral(resourceName: "add_bg"), for: .normal)
            addTabButton.setImage(#imageLiteral(resourceName: "category_icon_sel"), for: .selected)
        addTabButton.setImage( #imageLiteral(resourceName: "category_icon"), for: .normal)
        addTabButton.isUserInteractionEnabled = false
        //self.setupLeftBarButton()
        self.tabBar.setNeedsDisplay()
        self.tabBar.layoutIfNeeded()
        self.view.layoutIfNeeded()
        self.view.setNeedsDisplay()
    }
//    override func viewDidDisappear(_ animated: Bool) {
//        let statusBar: UIView = UIApplication.sharedApplication().valueForKey("statusBar") as! UIView
//        if statusBar.respondsToSelector(Selector("setBackgroundColor:")) {
//            statusBar.backgroundColor = UIColor.blackColor()
//        }
//    }

//    override func viewWillLayoutSubviews() {
//        var tabFrame = self.tabBar.frame
//        tabFrame.size.height = tabBarHeight
//        tabFrame.origin.y = self.view.frame.size.height - tabBarHeight
//        self.tabBar.frame = tabFrame
//    }
    
    func acceptBookingAddInCurrentBookingSuccessfully(_ notification: Notification) {
        if let userInfo = notification.userInfo as? [String:AnyObject]{
            if let aBooking = userInfo["acceptBooking"] as? String{
                //                self.user = user
//                let id = kUserDefaults.value(forKey: kBranchID)
                branch.removeAll()
                self.loadRestaurantsFromServer(userID: self.user.ID)
//                if upSecondtableView != nil{
//                    self.upSecondtableView.reloadData()
//                }
            }
        }
    }
    
    func cancelBookingAddInCurrentBookingSuccessfully(_ notification: Notification) {
        if let userInfo = notification.userInfo as? [String:AnyObject]{
            if let aBooking = userInfo["cancelBooking"] as? String{
                //                self.user = user
//                let id = kUserDefaults.value(forKey: kBranchID)
                branch.removeAll()
                self.loadRestaurantsFromServer(userID: self.user.ID)
//                if cancelledtableView != nil{
//                    self.cancelledtableView.reloadData()
//                }
            }
        }
    }
    
    func loadRestaurantsFromServer(userID: String) {
        if let token = kUserDefaults.value(forKey: kAppToken) as? String{
            RestaurantService.sharedInstance.loadRestaurants(userID: userID, appToken: token) { (responseUsers) in
                if let someUser = responseUsers as? User{
                    self.user = someUser
                    if self.user.restaurants.count > 0{
                       self.branch = someUser.restaurants[0].branches
                       self.setupDropDown()
                    }else{
                        showErrorWithMessage("No restaurant found")
                    }
                }
            }
        }
    }

    func setUpTabBarElements() {
        let selAttDict = [NSForegroundColorAttributeName:UIColor.white]

        let normalAttDict = [NSForegroundColorAttributeName:UIColor(red:144.0/255.0, green:209.0/255.0, blue:215.0/255.0, alpha:1.0)]
        self.requestTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.requestTabButton.setTitleTextAttributes(normalAttDict, for: .normal)
        self.upComingTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.upComingTabButton.setTitleTextAttributes(normalAttDict, for: .normal)
        self.notAvilableTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.notAvilableTabButton.setTitleTextAttributes(normalAttDict, for: .normal)
        self.cancelledTabButton.setTitleTextAttributes(selAttDict, for: .selected)
        self.cancelledTabButton.setTitleTextAttributes(normalAttDict, for: .normal)
        
        self.requestTabButton.image = UIImage(named:"request_icon")?.withRenderingMode(.alwaysOriginal)
        self.upComingTabButton.image = UIImage(named: "upcoming")?.withRenderingMode(.alwaysOriginal)
        self.notAvilableTabButton.image = UIImage(named: "icon")?.withRenderingMode(.alwaysOriginal)
        self.cancelledTabButton.image = UIImage(named: "cancel_icon")?.withRenderingMode(.alwaysOriginal)
        self.requestTabButton.selectedImage = UIImage(named: "request_icon_sel")?.withRenderingMode(.alwaysOriginal)
        self.upComingTabButton.selectedImage = UIImage(named: "upcoming_sel")?.withRenderingMode(.alwaysOriginal)
        self.notAvilableTabButton.selectedImage = UIImage(named: "icon_sel")?.withRenderingMode(.alwaysOriginal)
        self.cancelledTabButton.selectedImage = UIImage(named: "cancel_icon_sel")?.withRenderingMode(.alwaysOriginal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Actions
    func setupLeftBarButton() {
        if self.menuView != nil{
            self.menuView.hide()
        }
        let button = UIButton(frame: CGRect(x:0,y: 0,width: 40,height: 40))
        button.setImage(UIImage(named: "menu_icon"), for: .normal)
        button.addTarget(self, action: #selector(onClickLeftNavigationBarButton(_:)), for: .touchUpInside)
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.fill
        let leftBarButtonItem = UIBarButtonItem(customView: button)
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = 0
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.setLeftBarButtonItems([negativeSpacer,leftBarButtonItem], animated: false)
    }
    
    func setupRightBarButton() {
        if self.menuView != nil{
            self.menuView.hide()
        }
        let leftBarButtonItem = UIBarButtonItem(title: "Scan Me", style: .plain, target: self, action:#selector(onClickRightNavigationBarButton(_:)))
        self.navigationItem.setRightBarButton(nil, animated: true)
        self.navigationItem.setRightBarButton(leftBarButtonItem,animated: true)
    }
//
    @IBAction func onClickLeftNavigationBarButton(_ sender: UIButton){
        if self.menuView != nil{
            self.menuView.hide()
        }
        SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    
    @IBAction func onClickRightNavigationBarButton(_ sender: UIBarButtonItem){
        if self.menuView != nil{
            self.menuView.hide()
        }        
        let vc = AppStoryboard.Main.viewController(viewControllerClass: ScannerViewController.self)
        vc.didShowMenu = false
        let navVC = UINavigationController(rootViewController: vc)
        let img = UIImage(named: "header_bar")
        navVC.navigationBar.setBackgroundImage(img, for: .default)
        navVC.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.present(navVC, animated: true, completion: nil)
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

extension DashBoardTabBarController: SlideNavigationControllerDelegate{
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        if self.menuView != nil{
            self.menuView.hide()
        }
        return true
    }
    func slideNavigationControllerShouldDisplayRightMenu() -> Bool {
        if self.menuView != nil{
            self.menuView.hide()
        }
        return false
    }
}

extension DashBoardTabBarController{
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item == requestTabButton{
//            self.navigationItem.title = "Requested"
            self.navigationItem.setRightBarButton(nil, animated: true)
            addTabButton.isSelected = false

        }else  if item == upComingTabButton{
//            self.navigationItem.title = "Upcoming"
            self.navigationItem.setRightBarButton(nil, animated: true)
            addTabButton.isSelected = false

        }else if item == notAvilableTabButton{
//            self.navigationItem.title = "Complete"
            self.navigationItem.setRightBarButton(nil, animated: true)
            addTabButton.isSelected = false

        }else if item == cancelledTabButton{
//            self.navigationItem.title = "Cancelled"
            self.navigationItem.setRightBarButton(nil, animated: true)
            addTabButton.isSelected = false
        }else{
//            self.navigationItem.title = "Dashboard"
            addTabButton.isSelected = true
            self.setupRightBarButton()
        }
        
    }
}
extension UIImage {
    
    func makeImageWithColorAndSize(_ color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        color.setFill()
        UIRectFill(CGRect(x:0, y:0, width:size.width, height:size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    //    func trim(trimRect trimRect :CGRect) -> UIImage {
    //        if CGRectContainsRect(CGRect(origin: CGPointZero, size: self.size), trimRect) {
    //            if let imageRef = CGImageCreateWithImageInRect(self.CGImage, trimRect) {
    //                return UIImage(CGImage: imageRef)
    //            }
    //        }
    //
    //        UIGraphicsBeginImageContextWithOptions(trimRect.size, true, self.scale)
    //        self.drawInRect(CGRect(x: -trimRect.minX, y: -trimRect.minY, width: self.size.width, height: self.size.height))
    //        let trimmedImage = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        guard let image = trimmedImage else { return self }
    //
    //        return image
    //    }
}
