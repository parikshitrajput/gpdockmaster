//
//  SplashAnimationViewController.swift
//  GPDock
//
//  Created by TecOrb on 23/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SplashAnimationViewController: UIViewController,SKSplashDelegate {
    @IBOutlet weak var imageView : UIImageView!
    var splashView : SKSplashView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Default")!)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.animationGPDock()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func animationGPDock(){

        let imageView = UIImageView(frame: self.view.frame)
        imageView.image = UIApplication.shared.screenShot
        self.view.addSubview(imageView)
        let splashIcon = SKSplashIcon(image: #imageLiteral(resourceName: "logo"), animationType: .bounce)
        splashIcon?.iconSize = self.imageView.frame.size
        let color = UIColor.clear//UIColor(red:16.0/255.0, green:122.0/255.0, blue:196.0/255, alpha:1.0)
        self.splashView = SKSplashView(splashIcon: splashIcon, animationType: .none, frame: self.view.frame)
        self.splashView.delegate = self;
        self.splashView.backgroundColor = color;
        self.splashView.animationDuration = 2.5
//        self.splashView.backgroundImage = UIImage(named: "Default")
        self.view.addSubview(splashView)

//        DispatchQueue.main.async {
            self.splashView.startAnimation()
       // }
    }

    func splashView(_ splashView: SKSplashView!, didBeginAnimatingWithDuration duration: Float) {

    }

    func splashViewDidEndAnimating(_ splashView: SKSplashView!) {
        if CommonClass.isLoggedIn{
            let nav = AppStoryboard.Home.viewController(MainNavigationController.self)
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
            UIApplication.shared.applicationIconBadgeNumber = 0
        }else{
            let nav = AppStoryboard.Main.viewController(LoginNavigationController.self)
            (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
    }

}
