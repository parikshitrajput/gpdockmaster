//
//  NKCalenderViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 23/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import FSCalendar
import EasyTipView

public struct BookingDates {
    public var totalDays : UInt = 0

    public var fromDate: Date {
        didSet{
            self.totalDays = UInt(abs(toDate.interval(ofComponent: .day, fromDate: fromDate)))
        }
    }

    public var toDate: Date{
        didSet{
            self.totalDays = UInt(abs(toDate.interval(ofComponent: .day, fromDate: fromDate)))
        }
    }


    public init(){
        let startDate = Date()
        let tillDate = Date()

        self.fromDate = startDate
        self.toDate = tillDate
        self.totalDays = UInt(abs(tillDate.interval(ofComponent: .day, fromDate: startDate)))
    }

    public init(fromDate: Date, toDate: Date){
        self.fromDate = fromDate
        self.toDate = toDate
        self.totalDays = UInt(abs(toDate.interval(ofComponent: .day, fromDate: fromDate)))

    }
}


protocol NKCalenderViewControllerDelegate {
    func nkcalendarViewController(_ viewController:NKCalenderViewController, didSelectedDate selectedDates:BookingDates)
}


class NKCalenderViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate {
    var delegate : NKCalenderViewControllerDelegate?
    @IBOutlet weak var calenarView : UIView!
    var confirmShowToolTip : Bool!
    var isEmptyDate = false
//    var rangeCalenderShow = ""
//    var payoutCalenderShow = ""

    var shouldShowToolTip = false{
        didSet{
            self.confirmShowToolTip  = shouldShowToolTip
        }
    }

    var showTipAfter: Int = 0
    var monthPositionToShowTip : FSCalendarMonthPosition = .current
    var selectedBookingDate : BookingDates!
    var calendar: FSCalendar!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!

    var minDate : Date?
    var maxDate : Date?

    var tipView : EasyTipView!//(text: "Some text", preferences: preferences)
    var dateFormatter: DateFormatter?
    var gregorian: Calendar!
    // The start date of the range
    var date1: Date?
    // The end date of the range
    var date2: Date?
    

    @IBAction func previousClicked(_ sender: UIButton) {
        self.dismissTipView()
        let currentMonth: Date? = calendar?.currentPage
        let previousMonth: Date? = gregorian?.date(byAdding: .month, value: -1, to: currentMonth!)
        calendar?.setCurrentPage(previousMonth!, animated: true)
    }
    @IBAction func onClickClose(_ sender: UIButton){
        self.dismissTipView()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onclickDone(_ sender: UIButton) {
        self.dismissTipView()
        if selectedBookingDate != nil{
            delegate?.nkcalendarViewController(self, didSelectedDate: self.selectedBookingDate)
        }
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction func nextClicked(_ sender: UIButton) {
        self.dismissTipView()
        let currentMonth: Date? = calendar?.currentPage
        let nextMonth: Date? = gregorian?.date(byAdding: .month, value: 1, to: currentMonth!)
        calendar?.setCurrentPage(nextMonth!, animated: true)
    }


    override func viewDidLoad() {
        gregorian = Calendar(identifier: .iso8601)
        let width: CGFloat = self.view.frame.size.width-30
        let height: CGFloat = width + 40

        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: width, height: height))
        calendar.appearance.titleTodayColor = kNavigationColor
        calendar.backgroundColor = UIColor.white
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        calendar.headerHeight = 50
        calendar.appearance.caseOptions = .weekdayUsesSingleUpperCase
        calendar.appearance.weekdayTextColor = kNavigationColor
        calendar.appearance.titleWeekendColor = UIColor.black//kNavigationColor
        calendar.appearance.weekdayFont = UIFont(name: fonts.Raleway.regular.rawValue, size: 16)
        calendar.pagingEnabled = true
        calendar.allowsMultipleSelection = true
        calendar.rowHeight = 40
        calendar.placeholderType = FSCalendarPlaceholderType.none
        self.calendar = calendar
        self.calenarView.addSubview(calendar)
        calendar.dataSource = self
        calendar.delegate = self
    
        CommonClass.makeViewCircularWithCornerRadius(self.calenarView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 4)
        calendar.appearance.titleDefaultColor = UIColor.black
        calendar.appearance.titleSelectionColor = kNavigationColor
        calendar.appearance.headerTitleColor = kNavigationColor
        calendar.appearance.titleFont = UIFont(name: fonts.Raleway.regular.rawValue, size: fontSize.large.rawValue)
        calendar.appearance.headerTitleFont = UIFont(name: fonts.Raleway.semiBold.rawValue, size: fontSize.large.rawValue)
        calendar.weekdayHeight = 40
        calendar.swipeToChooseGesture.isEnabled = true
        calendar.scrollDirection = .horizontal
        calendar.scrollEnabled = false
        calendar.register(RangePickerCell.self, forCellReuseIdentifier: "cell")
        if let preSelectedDates = self.selectedBookingDate{
            self.date1 = preSelectedDates.fromDate
            self.date2 = preSelectedDates.toDate
            calendar.select(self.date1, scrollToDate: false)
            calendar.select(self.date2, scrollToDate: true)
            self.configureVisibleCells()
        }
    }
 
   override func viewDidLayoutSubviews() {
      self.doneButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
    
    }

    // MARK: - FSCalendarDataSource
    func minimumDate(for calendar: FSCalendar) -> Date {
        return minDate ?? Date()
    }

    func maximumDate(for calendar: FSCalendar) -> Date {
        return maxDate ?? (gregorian?.date(byAdding: .month, value: 4, to: Date()))!
    }


    //    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String? {
    ////        if (gregorian?.isDateInToday(date))! {
    //           // return "Today"
    ////        }
    //        return nil
    //    }

    func calendar(_ calendar: FSCalendar, cellFor date: Date, at monthPosition: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell: RangePickerCell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: monthPosition) as! RangePickerCell
        return cell
    }

    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        configureCell(cell, for: date, at: monthPosition)
    }

    // MARK: - FSCalendarDelegate
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if monthPosition != .current{self.dismissTipView()}
        if (date > (self.minDate ?? (gregorian?.date(byAdding: .day, value: -1, to: Date()))!)) && (date < (self.maxDate ?? (gregorian?.date(byAdding: .month, value: 4, to: Date()))!)) {
            return true
        }
        return false //monthPosition == .current
    }

    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        return false
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {

        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
            self.monthPositionToShowTip = monthPosition
            showTipAfter = 1
        }else{
            showTipAfter = 0
        }

        if self.tipView != nil{
            self.dismissTipView()
        }
        if calendar.swipeToChooseGesture.state == .changed{
            if monthPosition == .previous || monthPosition == .next {
                showTipAfter = 1
            }else{
                showTipAfter = 0
            }
            self.monthPositionToShowTip = monthPosition

            // If the selection is caused by swipe gestures
            if !(date1 != nil) {
                date1 = date
            }
            else {
                if (date2 != nil) {
                    calendar.deselect(date2!)
                }
                date2 = date
            }
        }else {
            if (date2 != nil) {
                calendar.deselect(date1!)
                calendar.deselect(date2!)
                date1 = date
                date2 = nil
            }
            else if !(date1 != nil) {
                date1 = date
            }
            else {
                date2 = date
            }
        }

        if let startDate = date1{
            self.selectedBookingDate = BookingDates(fromDate: startDate, toDate: startDate)
            self.shouldShowToolTip = false
            if let toDate = date2{
                let sortedDate = CommonClass.shortSelectedDate(startDate, toDate: toDate)
                self.selectedBookingDate = BookingDates(fromDate: sortedDate.fromDate, toDate: sortedDate.toDate)
                self.shouldShowToolTip = true
            }
        }
        configureVisibleCells()
    }

    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        // print("did deselect date \(String(describing: dateFormatter?.string(from: date)))")
        configureVisibleCells()
    }

    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor] {
        if gregorian.isDateInToday(date){
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }

    func calendarCurrentMonthDidChange(_ calendar: FSCalendar) {
        self.dismissTipView()
    }


    // MARK: - Private methods
    func configureVisibleCells() {
        for cell in (calendar.visibleCells()){
            let date: Date? = calendar.date(for: cell)
            let position: FSCalendarMonthPosition = calendar.monthPosition(for: cell)
            self.configureCell(cell, for: date!, at: position)
        }
    }

    func configureCell(_ cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        let rangeCell: RangePickerCell? = cell as? RangePickerCell
        if position != .current {
            rangeCell?.rightMiddleLayer?.isHidden = true
            rangeCell?.leftMiddleLayer?.isHidden = true
            rangeCell?.selectionLayer?.isHidden = true
            return
        }

        if (date1 != nil) && (date2 != nil) {
            // The date is in the middle of the range
            let isMiddle: Bool = date.compare(date1!) != date.compare(date2!)

            //rangeCell?.middleLayer?.isHidden = !isMiddle
            rangeCell?.rightMiddleLayer?.isHidden = !isMiddle
            rangeCell?.leftMiddleLayer?.isHidden = !isMiddle

            if date == self.selectedBookingDate.fromDate{
                rangeCell?.rightMiddleLayer?.isHidden = false
                rangeCell?.leftMiddleLayer?.isHidden = true
            }else if date == self.selectedBookingDate.toDate{
                rangeCell?.rightMiddleLayer?.isHidden = true
                rangeCell?.leftMiddleLayer?.isHidden = false
            }else{
                rangeCell?.rightMiddleLayer?.isHidden = !isMiddle
                rangeCell?.leftMiddleLayer?.isHidden = !isMiddle
            }

            if shouldShowToolTip{
                if gregorian.isDate(date, inSameDayAs: selectedBookingDate.toDate) && (self.monthPositionToShowTip != .previous){
                    let when = showTipAfter*800
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(when)) {
                        self.showTip(forView: cell, withinSuperview: self.calenarView)
                    }
                    self.shouldShowToolTip = false
                    self.monthPositionToShowTip = .current
                }else if gregorian.isDate(date, inSameDayAs: selectedBookingDate.fromDate) && self.monthPositionToShowTip == .previous{
                    let when = showTipAfter*800
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(when)) {
                        self.showTip(forView: cell, withinSuperview: self.calenarView)
                    }
                    //                    self.showTip(forView: cell, withinSuperview: self.calenarView)
                    self.shouldShowToolTip = false
                    self.monthPositionToShowTip = .current
                }
            }

        }
        else {
            //rangeCell?.middleLayer?.isHidden = true
                rangeCell?.rightMiddleLayer?.isHidden = true
                rangeCell?.leftMiddleLayer?.isHidden = true
        }
        var isSelected: Bool = false
        isSelected = isSelected || ( (date1 != nil) && (gregorian?.isDate(date, inSameDayAs: date1!))!)
        isSelected = isSelected || ((date2 != nil) && (gregorian?.isDate(date, inSameDayAs: date2!))!)
        rangeCell?.selectionLayer?.isHidden = !isSelected
    }
}

extension NKCalenderViewController: EasyTipViewDelegate{
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismissTipView()
    }

    func showTip(forView: UIView,withinSuperview superview: UIView) -> Void {
        if self.tipView != nil{ self.tipView.dismiss()}
        //if confirmShowToolTip{
        let fromDate = CommonClass.formattedDateWith(selectedBookingDate.fromDate, format: "dd MMM")
        let toDate = CommonClass.formattedDateWith(selectedBookingDate.toDate, format: "dd MMM")
        let titleText = "\(fromDate) - \(toDate) \(selectedBookingDate.totalDays) Nights\r\nDrag to modify"
        self.tipView = EasyTipView(text: titleText, preferences: self.configurationForEasyTip())
        self.tipView.show(forView: forView)
        //}
    }

    func dismissTipView() {
        if self.tipView != nil{ self.tipView.dismiss()}
    }

    func configurationForEasyTip()-> EasyTipView.Preferences {
        var preferences = EasyTipView.Preferences()
        // preferences.drawing.font = UIFont(name: fonts.OpenSans.regular.rawValue, size: fontSize.small.rawValue)!
        preferences.drawing.foregroundColor = UIColor.white
        preferences.drawing.backgroundColor = kNavigationColor//UIColor(red: 60.0/255.0, green: 154.0/255.0, blue: 252.0/255.0, alpha: 1.0)
        preferences.drawing.arrowPosition = EasyTipView.ArrowPosition.bottom
        preferences.animating.dismissTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialTransform = CGAffineTransform(translationX: 0, y: -15)
        preferences.animating.showInitialAlpha = 0
        preferences.animating.showDuration = 1
        preferences.animating.dismissDuration = 0
        return preferences
    }

    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
        //        let tipView = EasyTipView(text: "Some text", preferences: preferences)
        //        tipView.show(forView: someView, withinSuperview: someSuperview)
        //
        //        // later on you can dismiss it
        //        tipView.dismiss()
    }
}

