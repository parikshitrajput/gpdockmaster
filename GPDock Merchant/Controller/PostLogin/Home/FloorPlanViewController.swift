//
//  FloorPlanViewController.swift
//  GPDock
//
//  Created by TecOrb on 14/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
protocol FloorPlanViewControllerDelegate {
    func floorPlanViewController(viewController: FloorPlanViewController, didGetOccupancyRate rate:Double)
}
class FloorPlanViewController: UIViewController,ZSeatSelectorDelegate {
    var lengthPicker:UIPickerView!
    var widthPicker:UIPickerView!
    var depthPicker:UIPickerView!
    var ampPicker:UIPickerView!

    @IBOutlet weak var lengthTextField:FloatLabelTextField!
    @IBOutlet weak var widthTextField:FloatLabelTextField!
    @IBOutlet weak var depthTextField:FloatLabelTextField!
    @IBOutlet weak var ampTextField:FloatLabelTextField!
    @IBOutlet weak var selectedDateLabel:UILabel!
    @IBOutlet weak var resetFilterButton: UIButton!


    var minLength = 15
    var minWidth = 15
    var minDepth = 15
    var minAMP = 20

    var maxLength = 200
    var maxWidth = 70
    var maxDepth = 50
    var maxAMP = 100

    var marina: Marina!
    var selectedBoatSize: BoatSize = BoatSize()//(length: 20, width: 6, depth: 4,amp:20)
    var bookingDates: BookingDates = BookingDates()
    var selectedBoat : ParkingSpace!
    var userBoat : Boat?
    var delegate : FloorPlanViewControllerDelegate?
    @IBOutlet weak var singleDateIndicatorView : UIView!
    @IBOutlet weak var rangeDateIndicatorView : UIView!

    @IBOutlet weak var parkingPlanView : UIView!
    //@IBOutlet weak var beachTextField : UITextField!
    var floorPlan = FloorPlan()
    var user: User!
    var willRefresh = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.getAppVersion()
        NotificationCenter.default.addObserver(self, selector: #selector(self.getNotificarionAppVersion(_:)), name: .USER_DID_UPDATE_APPLICATION_VERSION, object: nil)
        
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson)
        self.setupMarina()
        self.parkingPlanView.backgroundColor = UIColor.white//(patternImage: #imageLiteral(resourceName: "sea"))
        self.configurePickers()
    }

    override func viewWillAppear(_ animated: Bool) {
        reSetupAall(willRefresh: self.willRefresh)
        
    }
    func getAppVersion() {
        
        LoginService.sharedInstance.checkAppVersion() {(sucess, message,currentVersion, versionLevel) in
            if sucess {
                
                print("APP Version>>>\(currentVersion)")
                print("APP Version level>>\(versionLevel)")
                self.checkVersionAndUpdate(currentVersion: currentVersion, versionLevel: versionLevel)
                
            }else{
                showAlertWith(self, message: message, title: "Important Message")
            }
        }
    }
    
    @objc func getNotificarionAppVersion(_ notification: Notification) {
        
        LoginService.sharedInstance.checkAppVersion() {(sucess, message,currentVersion, versionLevel) in
            if sucess {
                
                print("APP Version>>>\(currentVersion)")
                print("APP Version level>>\(versionLevel)")
                self.checkVersionAndUpdate(currentVersion: currentVersion, versionLevel: versionLevel)
                
            }else{
                showAlertWith(self, message: message, title: "Important Message")
            }
        }
    }
    
    func checkVersionAndUpdate(currentVersion: String, versionLevel: String) {
        let appVersion = UIApplication.appVersion()+"."+UIApplication.appBuild()
        if currentVersion != appVersion {
            if versionLevel.lowercased() != "Normal".lowercased() {
                self.forceUPdateVersion(currentVersion: currentVersion, versionLevel: versionLevel)
            }else{
                self.askForUpdateVersion(currentVersion: currentVersion, versionLevel: versionLevel, title: "Important Message")
            }
        }
    }
    
    
    func askForUpdateVersion(currentVersion: String, versionLevel: String, title: String) {
        if !CommonClass.shouldShowNormalUpdate{return}
        let alert = UIAlertController(title: title, message: "A new version GPD Dockmaster is available. Please update to version \(currentVersion) now", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Yes", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            self.goTOAppStore()
        }
        let cancelAction = UIAlertAction(title: "Nope", style: .cancel){(action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okayAction)
        alert.addAction(cancelAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: {
            CommonClass.setNormalUpdateKey()
        })
    }
    
    
    func forceUPdateVersion(currentVersion: String, versionLevel: String) {
        
        let alert = UIAlertController(title: "Important message", message: "A new version GPD Dockmaster is available. Please update to version \(currentVersion) now", preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Update", style: .default){(action) in
            alert.dismiss(animated: true, completion: nil)
            self.goTOAppStore()
        }
        
        alert.addAction(okayAction)
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.window?.rootViewController?.present(alert, animated: true, completion: {
            CommonClass.setNormalUpdateKey()
        })
    }
    
    func goTOAppStore() {
        if let url = URL(string: "itms-apps://itunes.apple.com/app/gpd-dockmaster/id1347226165"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    
    @IBAction func onClickArrivingButton(_ sender: UIButton) {
     let  bookingListVc = AppStoryboard.Booking.viewController(ListOfBookingViewController.self)
        bookingListVc.bookingDates = self.bookingDates
        bookingListVc.bookingStatus = "arrivial"
        self.navigationController?.pushViewController(bookingListVc, animated: true)
    }
    @IBAction func onClickDepartingButton(_ sender: UIButton) {
        let  bookingListVc = AppStoryboard.Booking.viewController(ListOfBookingViewController.self)
        bookingListVc.bookingDates = self.bookingDates
        bookingListVc.bookingStatus = "departure"
        self.navigationController?.pushViewController(bookingListVc, animated: true)
    }
    @IBAction func onClickOccupiedButton(_ sender: UIButton) {
        let  bookingListVc = AppStoryboard.Booking.viewController(ListOfBookingViewController.self)
        bookingListVc.bookingDates = self.bookingDates
        bookingListVc.bookingStatus = "occupied"
        self.navigationController?.pushViewController(bookingListVc, animated: true)
    }
    @IBAction func onClickBookedButton(_ sender: UIButton) {
        let  bookingListVc = AppStoryboard.Booking.viewController(ListOfBookingViewController.self)
        bookingListVc.bookingDates = self.bookingDates
        bookingListVc.bookingStatus = "booked"
        self.navigationController?.pushViewController(bookingListVc, animated: true)
    }
    @IBAction func onClickPartiallyButton(_ sender: UIButton) {
        let  bookingListVc = AppStoryboard.Booking.viewController(ListOfBookingViewController.self)
        bookingListVc.bookingDates = self.bookingDates
        bookingListVc.bookingStatus = "partially"
        self.navigationController?.pushViewController(bookingListVc, animated: true)
    }



    func reSetupAall(willRefresh:Bool) {
        if willRefresh{
            self.setupMarina()
        }
        self.willRefresh = true
    }

    func setupMarina() {
        //resetable after new marina
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        
        self.setUpSelectedDate()
        self.getMarinaFloorPlan(self.selectedBoatSize, bookingDates: self.bookingDates)
    }

    func configurePickers() -> Void {
//        self.setupAmpPicker()
        self.setupLengthPicker()
        self.setupWidthPicker()
        self.setupDepthPicker()
    }

    func seatMapping(){
        let seats = ZSeatSelector()
        seats.rangeType = (self.bookingDates.totalDays == 0) ? SeatSelectorRange.singleDate : SeatSelectorRange.rangeOfDate
        seats.isDirectionalLockEnabled = true
        seats.frame = CGRect(x: 0, y: 0, width: self.parkingPlanView.frame.size.width, height:  self.parkingPlanView.frame.size.height)
        seats.setSeatSize(CGSize(width: 30, height: 30))
        seats.setAvailableImage()
        seats.floorPlan(floorPlan)
        seats.selected_seat_limit  = 1
        seats.seatSelectorDelegate = self
        seats.maximumZoomScale = 5.0
        self.parkingPlanView.addSubview(seats)
    }

    func resetFloor() {
        for view in self.parkingPlanView.subviews{
            view.removeFromSuperview()
        }
    }

    @IBAction func onClickSelectDateButton(_ sender: UIButton){
        let calendarVC = AppStoryboard.Home.viewController(NKCalenderViewController.self)
//        calendarVC.rangeCalenderShow = "show floor"
        calendarVC.selectedBookingDate = self.bookingDates
        calendarVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        calendarVC.delegate = self
        let nav = UINavigationController(rootViewController: calendarVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    @IBAction func onClickresetFilterButton(_ sender: UIButton){
      self.lengthTextField.text = ""
      self.widthTextField.text = ""
      self.depthTextField.text = ""
         self.selectedBoatSize.length = 0
        self.selectedBoatSize.width = 0
        self.selectedBoatSize.depth = 0
        self.unSelectText(textView: self.lengthTextField)
        self.unSelectText(textView: self.widthTextField)
        self.unSelectText(textView: self.depthTextField)
        self.widthTextField.resignFirstResponder()
        self.lengthTextField.resignFirstResponder()
        self.depthTextField.resignFirstResponder()
        self.setupMarina()
        
    }
    
    
    func seatSelected(_ seat: ZSeat) {
        if self.bookingDates.totalDays < 1{
            switch seat.slipStatus{
            case .arriving:
                self.openArrivingSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            case .isDeparting:
                self.openDepartingSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            case .available:
                self.openAvailableSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            case .booked:
                self.openOccupiedSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            case .partiallyBooked:
                self.openPartiallyBookedSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            case .isArrivingAndDeparture:
                self.openArrivingPlusDepartingSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            case .unavailable:
                break
            }
        }else{
            switch seat.slipStatus{
            case .available:
                self.openAvailableSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            case .booked:
                self.openFullyBookedSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            case .partiallyBooked:
                self.openPartiallyBookedSlip(slip: seat.boat, selectedBookingDates: self.bookingDates)
            default:
                break
            }
        }
    }

    func openAvailableSlip(slip: ParkingSpace,selectedBookingDates:BookingDates) {
        let availableSlipVC = AppStoryboard.Booking.viewController(AvailableSlipDetailsViewController.self)
        availableSlipVC.slip = slip
        availableSlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(availableSlipVC, animated: true)
    }

    func openFullyBookedSlip(slip: ParkingSpace,selectedBookingDates:BookingDates) {
        let fullyBookedSlipVC = AppStoryboard.Booking.viewController(FullyBookedViewController.self)
        fullyBookedSlipVC.slip = slip
        fullyBookedSlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(fullyBookedSlipVC, animated: true)
    }

    func openPartiallyBookedSlip(slip: ParkingSpace,selectedBookingDates:BookingDates) {
        let partiallySlipVC = AppStoryboard.Booking.viewController(PartiallyBookedViewController.self)
        partiallySlipVC.slip = slip
        partiallySlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(partiallySlipVC, animated: true)
    }

    func openArrivingSlip(slip: ParkingSpace,selectedBookingDates:BookingDates) {
        let arrivingSlipVC = AppStoryboard.Booking.viewController(ArrivingViewController.self)
        arrivingSlipVC.slip = slip
        arrivingSlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(arrivingSlipVC, animated: true)
    }
    func openArrivingPlusDepartingSlip(slip: ParkingSpace,selectedBookingDates:BookingDates) {
        let arrivingSlipVC = AppStoryboard.Booking.viewController(ArrivingAndDepartingViewController.self)
        arrivingSlipVC.slip = slip
        arrivingSlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(arrivingSlipVC, animated: true)
    }

    func openDepartingSlip(slip: ParkingSpace,selectedBookingDates:BookingDates) {
        let departingSlipVC = AppStoryboard.Booking.viewController(DepartingViewController.self)
        departingSlipVC.slip = slip
        departingSlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(departingSlipVC, animated: true)
    }

    func openOccupiedSlip(slip: ParkingSpace,selectedBookingDates:BookingDates) {
        let occupiedSlipVC = AppStoryboard.Booking.viewController(OccupiedViewController.self)
        occupiedSlipVC.slip = slip
        occupiedSlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(occupiedSlipVC, animated: true)
    }

    func locakParking(_ userID: String, parkingID: String,completion:@escaping (Bool)->Void) {
        MarinaService.sharedInstance.lockParkingSpaceFor(userID,parkingID: parkingID) { (done) in
            completion(done)
        }
    }

    func getSelectedSeats(_ seats: NSMutableArray) {
        for _ in 0..<seats.count {
//            let seat:ZSeat  = seats.object(at: i) as! ZSeat
//            self.selectedBoat = seat.boat
        }
    }



    func setUpIndicatorView() -> Void {
        if self.bookingDates.totalDays == 0{
            self.singleDateIndicatorView.isHidden = true
            self.rangeDateIndicatorView.isHidden = false
            self.rangeDateIndicatorView.cornerRadius = 10
            self.rangeDateIndicatorView.layer.borderWidth = 2
            self.rangeDateIndicatorView.layer.borderColor = kApplicationLightGrayColor.cgColor
            self.rangeDateIndicatorView.layer.shadowColor = kApplicationLightGrayColor.cgColor
            self.rangeDateIndicatorView.layer.shadowOpacity = 1
            self.rangeDateIndicatorView.layer.shadowOffset = CGSize.zero
            self.rangeDateIndicatorView.layer.shadowRadius = 10


        }else{
            self.singleDateIndicatorView.isHidden = false
            self.rangeDateIndicatorView.isHidden = true
            self.singleDateIndicatorView.cornerRadius = 10
            self.singleDateIndicatorView.layer.borderWidth = 2
            self.singleDateIndicatorView.layer.borderColor = kApplicationLightGrayColor.cgColor
            self.singleDateIndicatorView.layer.shadowColor = kApplicationLightGrayColor.cgColor
            self.singleDateIndicatorView.layer.shadowOpacity = 1
            self.singleDateIndicatorView.layer.shadowOffset = CGSize.zero
            self.singleDateIndicatorView.layer.shadowRadius = 10
              //self.singleDateIndicatorView.cornerRadius = 2

        }
        self.lengthTextField.layer.borderWidth = 0.2
        self.lengthTextField.layer.borderColor = kApplicationLightGrayColor.cgColor
        self.widthTextField.layer.borderWidth = 0.2
        self.widthTextField.layer.borderColor = kApplicationLightGrayColor.cgColor
        self.depthTextField.layer.borderWidth = 0.2
        self.lengthTextField.layer.borderColor = kApplicationLightGrayColor.cgColor
    }

    func setUpSelectedDate() -> Void {
        self.selectedDateLabel.layer.borderWidth = 1
        self.selectedDateLabel.layer.borderColor = kApplicationLightGrayColor.cgColor
        let fromDate = CommonClass.formattedDateWith(bookingDates.fromDate, format: "MM-dd-YYYY")
        let toDate = CommonClass.formattedDateWith(bookingDates.toDate, format: "MM-dd-YYYY")
        self.selectedDateLabel.text = (self.bookingDates.totalDays == 0) ? "\(fromDate)" : "\(fromDate) To \(toDate)"
        self.setUpIndicatorView()
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getMarinaFloorPlan(_ boatSize: BoatSize,bookingDates:BookingDates) -> Void {
        CommonClass.showLoader(withStatus: "Accessing..")
        if self.bookingDates.totalDays == 0{//access for single day
            MarinaService.sharedInstance.getFloorPlanForSingleDate(self.user.ID, marinaID: self.marina.ID, bookingDate: self.bookingDates,boatSize: boatSize){ (resFloorPlan) in
                self.resetFloor()
                if let aFloorPlan = resFloorPlan{
                    var occRate: Double = 0.0
                    if((aFloorPlan.availablity.booked == 0) && (aFloorPlan.availablity.total == 0) && (aFloorPlan.availablity.available == 0)){
                        occRate = 0
                    }else{
                //NotificationCenter.default.post(name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil, userInfo:nil)
                     occRate = (Double(aFloorPlan.availablity.booked)/Double(aFloorPlan.availablity.total))*100
                    }
                    //let occRate = (Double(aFloorPlan.availablity.total-aFloorPlan.availablity.available)/Double(aFloorPlan.availablity.total))*100
                    self.delegate?.floorPlanViewController(viewController: self, didGetOccupancyRate: occRate)
                    self.floorPlan = aFloorPlan

                    //self.setUpNavigationView()
                    self.seatMapping()
                }
                CommonClass.hideLoader()

            }
        }else{
            MarinaService.sharedInstance.getFloorPlanForMarina(self.marina.ID, boatSize: boatSize, bookingDate: bookingDates) { (resFloorPlan) in
                self.resetFloor()
                if let aFloorPlan = resFloorPlan{
                    var occRate: Double = 0.0
                    if((aFloorPlan.availablity.booked == 0) && (aFloorPlan.availablity.total == 0) && (aFloorPlan.availablity.available == 0)){
                        occRate = 0
                    }else{
                     occRate = (Double(aFloorPlan.availablity.booked)/Double(aFloorPlan.availablity.total))*100
                    }
                    //let occRate = (Double(aFloorPlan.availablity.total-aFloorPlan.availablity.available)/Double(aFloorPlan.availablity.total))*100
                    self.delegate?.floorPlanViewController(viewController: self, didGetOccupancyRate: occRate)
                    self.floorPlan = aFloorPlan
                    //NotificationCenter.default.post(name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil, userInfo:nil)
                    //self.setUpNavigationView()
                    self.seatMapping()
                }
                CommonClass.hideLoader()

            }
        }

    }


}
extension FloorPlanViewController: UITextFieldDelegate {

    @IBAction func onTextDidChangeEdit(_ textField: UITextField){
        if textField == lengthTextField{
            self.selectedBoatSize.length = Int(textField.text!) ?? 0
            if lengthTextField.text == "" {
                unSelectText(textView: lengthTextField)
   
            } else{
                selectText(textView: lengthTextField)
            }
        }else if textField == widthTextField{
            self.selectedBoatSize.width = Int(textField.text!) ?? 0
            if widthTextField.text == "" {
                unSelectText(textView: widthTextField)
            }else{
               selectText(textView: widthTextField)
            }
        }else if textField == depthTextField{
            self.selectedBoatSize.depth = Int(textField.text!) ?? 0
            if depthTextField.text == "" {
                unSelectText(textView: depthTextField)
            }else{
                selectText(textView: depthTextField)
            }
            
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.endEditing(true)
        self.setupMarina()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == lengthTextField{
            //lengthTextField.backgroundColor = kApplicationBlueColor
            if lengthTextField.text == "" {
                    unSelectText(textView: lengthTextField)

            }
            
            self.selectedBoatSize.length = Int(textField.text!) ?? 0
        }else if textField == widthTextField{
            self.selectedBoatSize.width = Int(textField.text!) ?? 0
            if widthTextField.text == "" {
                unSelectText(textView: widthTextField)
            }
        }else if textField == depthTextField{
            self.selectedBoatSize.depth = Int(textField.text!) ?? 0
            if depthTextField.text == "" {
                unSelectText(textView: depthTextField)
                
            }

        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if(textField == self.lengthTextField) {
            return CommonClass.validateMaxValue(textField, maxValue: 9999, range: range, replacementString: string)
        }else if(textField == self.widthTextField) {
            return CommonClass.validateMaxValue(textField, maxValue: 9999, range: range, replacementString: string)
        }else if(textField == self.depthTextField) {
            return CommonClass.validateMaxValue(textField, maxValue: 9999, range: range, replacementString: string)
        }
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == lengthTextField{
            self.selectedBoatSize.length = Int(textField.text!) ?? 0
           // lengthTextField.backgroundColor = kApplicationBlueColor
        }else if textField == widthTextField{
            self.selectedBoatSize.width = Int(textField.text!) ?? 0
            //widthTextField.backgroundColor = kApplicationBlueColor

        }else if textField == depthTextField{
            self.selectedBoatSize.depth = Int(textField.text!) ?? 0
           // depthTextField.backgroundColor = kApplicationBlueColor

        }
    }
}

extension FloorPlanViewController: UIPickerViewDataSource,UIPickerViewDelegate{

    func setupLengthPicker() {


        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
        toolBar.barTintColor = kNavigationColor
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickDoneOfLenthPicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        spaceButton.title = "Length"

        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickCancelOfLenthPicker(_:)))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
//        lengthTextField.inputView = lengthPicker
        lengthTextField.delegate = self

        lengthTextField.inputAccessoryView = toolBar

    }

    func setupWidthPicker() {


        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
        toolBar.barTintColor = kNavigationColor
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickDoneOfWidthPicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        spaceButton.title = "Width"

        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickCancelOfWidthPicker(_:)))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

//        widthTextField.inputView = widthPicker
        widthTextField.delegate = self
        widthTextField.inputAccessoryView = toolBar
    }

    func setupDepthPicker() {


        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
        toolBar.barTintColor = kNavigationColor

        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickDoneOfDepthPicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        spaceButton.title = "Depth"
        spaceButton.tintColor = UIColor.white
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickCancelOfDepthPicker(_:)))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

//        depthTextField.inputView = depthPicker
        depthTextField.delegate = self
        depthTextField.inputAccessoryView = toolBar
        
    }


    //finish inputview handling
    func unSelectText(textView:FloatLabelTextField ) {
        textView.backgroundColor = UIColor.white
        textView.textColor = kApplicationBlueColor
        textView.titleTextColour = UIColor.black//kNavigationColor
        textView.titleActiveTextColour = kNavigationColor
    }
    func selectText(textView:FloatLabelTextField ) {
        textView.textColor = UIColor.white
        textView.backgroundColor = kApplicationBlueColor
        textView.titleTextColour = UIColor.white
        textView.titleActiveTextColour = UIColor.white

    }
    
    //func allTextSelect(textField:FloatLabelTextField )
    
    @objc func onClickDoneOfLenthPicker(_ sender: UIBarButtonItem) {

        self.setupMarina()
        self.lengthTextField.resignFirstResponder()

    }
    @objc func onClickDoneOfWidthPicker(_ sender: UIBarButtonItem) {
        self.setupMarina()
        self.widthTextField.resignFirstResponder()

    }
    @objc func onClickDoneOfDepthPicker(_ sender: UIBarButtonItem) {
        self.setupMarina()
        self.depthTextField.resignFirstResponder()

    }

    func onClickDoneOfAMPPicker(_ sender: UIBarButtonItem) {

        self.ampTextField.resignFirstResponder()

    }

    //cancel inputView handling
    @objc func onClickCancelOfLenthPicker(_ sender: UIBarButtonItem) {
        self.lengthTextField.resignFirstResponder()
        self.setupMarina()

    }
    @objc func onClickCancelOfWidthPicker(_ sender: UIBarButtonItem) {
        self.widthTextField.resignFirstResponder()
        self.setupMarina()

    }
    @objc func onClickCancelOfDepthPicker(_ sender: UIBarButtonItem) {
        self.depthTextField.resignFirstResponder()
        self.setupMarina()


    }
    func onClickCancelOfAMPPicker(_ sender: UIBarButtonItem) {
        self.ampTextField.resignFirstResponder()
        self.setupMarina()

    }



    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var rows = 0
        if pickerView == lengthPicker{rows=(maxLength-minLength)+1}else if pickerView == widthPicker{rows=(maxWidth-minWidth)+1}else if pickerView == depthPicker{rows=(maxDepth-minDepth)+1}else{rows=((maxAMP-minAMP)/5)+1}
        return rows
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == lengthPicker{
            let length = row+minLength
            return "\(length)"
        }else if pickerView == widthPicker{
            let width = row+minWidth
            return "\(width)"
        }else if pickerView == depthPicker{
            let depth = row+minDepth
            return "\(depth)"
        }else{
            let amp = minAMP + (5*row)
            return "\(amp)"
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == lengthPicker{
            let length = row+minLength
            self.lengthTextField.text = "\(length)"
            self.selectedBoatSize.length = length
        }else if pickerView == widthPicker{
            let width = row+minWidth
            self.widthTextField.text = "\(width)"
            self.selectedBoatSize.width = width

        }else if pickerView == depthPicker{
            let depth = row+minDepth
            self.depthTextField.text = "\(depth)"
            self.selectedBoatSize.depth = depth
        }else{
            let amp = minAMP + (5*row)
            self.ampTextField.text = "\(amp)"
            //self.selectedBoatSize.amp = amp
        }
    }

}

extension FloorPlanViewController: NKCalenderViewControllerDelegate{
    func nkcalendarViewController(_ viewController: NKCalenderViewController, didSelectedDate selectedDates: BookingDates) {
        self.bookingDates = selectedDates
        self.setUpSelectedDate()
        self.setupMarina()
    }
}
