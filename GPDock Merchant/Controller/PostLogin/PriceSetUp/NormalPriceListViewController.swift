//
//  NormalPriceListViewController.swift
//  GPDock
//
//  Created by Parikshit on 10/08/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class NormalPriceListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var priceListTable: UITableView!
    
    var marina : Marina!
    
    var isNewDataLoading = false
    var marinaPriceSessionReview =  MarinaPriceSessionReview()
    
    let lightGrayColor = UIColor(red:234.0/255.0, green:235.0/255.0, blue:237.0/255, alpha:1.0)
    
    let lightBlackColor = UIColor(red:111.0/255.0, green:113.0/255.0, blue:121.0/255, alpha:1.0)
    
    var allheight: CGFloat = 180
    var weekdayHeight:CGFloat = 130
    var countForPriceDetails = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "PRICES"
        self.priceListTable.dataSource = self
        self.priceListTable.delegate = self
        self.priceListTable.backgroundColor = UIColor.groupTableViewBackground
        self.priceListTable.register(UINib(nibName: "SessionTableViewCell", bundle: nil), forCellReuseIdentifier: "SessionTableViewCell")
        
        self.getNormalMarinaPrice(marinaID: self.marina.ID, priceFor: "default", PriceDate: "", priceType: "", weekendType: "")
        // Do any additional setup after loading the view.
        print("marinaPriceType>>>>>>\(self.marina.priceType)")
        print("weekendTypeType>>>>>>\(self.marina.weekendType)")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    
    func caseCountSection() ->Int {
        var count = 0
        let _aMarinaPrices = self.marinaPriceSessionReview
        // 0.for not in all case
        if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            count = 0
        }
            // 1.for low season
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            count = 1
        }
            // 2.high season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            count = 1
        }
            // 3.high season and low season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            count = 2
        }
            //4. weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            count = 1
        }
            // 5. weekend and low
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            count = 2
        }
            // 6. weekend and high
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            count = 2
        }
            // 7. weekend , high and low
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            count = 3
        }
            // 8. regular day
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            count = 1
        }
            // 9. regular day and low season
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            count = 2
        }
            // 10. regular day and high season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            count = 2
        }
            
            // 11. regular , high and low season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            count = 3
        }
            // 12. regular and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            count = 2
        }
            // 13. regular , low and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            count = 3
        }
            // 14. regular, high and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            count = 3
        }
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            count = 4
        }
        else{
            count = 4
        }
        print("caseCount>>>\(count)")
        
        return count
    }
    //
    
    
    
    
    
    func showTableCell(tableView: UITableView, indexPath:IndexPath) -> UITableViewCell {
        
        let _aMarinaPrices = self.marinaPriceSessionReview
        // 0.for not in all case
        if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            return UITableViewCell()
            
        }
            // 1.for low season
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
            // 2.high season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
            // 3.high season and low season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
            
        }
            //4. weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
            // 5. weekend and low
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
            
        }
            // 6. weekend and high
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            switch indexPath.row {
                
            case 0:
                let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            case 1:
                let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
        }
            // 7. weekend , high and low
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 2:
                let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
            
        }
            // 8. regular day
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
            // 9. regular day and low season
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
                
            default:
                return UITableViewCell()
            }
        }
            // 10. regular day and high season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            default:
                return UITableViewCell()
            }
        }
            
            // 11. regular , high and low season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 2:
                let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
        }
            // 12. regular and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
        }
            // 13. regular , low and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 2:
                let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
        }
            // 14. regular, high and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 2:
                let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
        }
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            switch indexPath.row {
            case 0:
                let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 2:
                let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 3:
                let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
        }
        else{
            switch indexPath.row {
            case 0:
                let cell = self.weekdayPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 1:
                let cell = self.highSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 2:
                let cell = self.lowSeasonPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
            case 3:
                let cell = self.weekendPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
            default:
                return UITableViewCell()
            }
            
        }
        
        return UITableViewCell()
        
    }
    
    
    
    
    
    func rowCountHeight(tableView: UITableView, indexPath:IndexPath) -> CGFloat{
        var height: CGFloat = 0
        let _aMarinaPrices = self.marinaPriceSessionReview
        // 0.for not in all case
        if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            height = 0
        }
            // 1.for low season
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            height = allheight
        }
            // 2.high season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            height = allheight
        }
            // 3.high season and low season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            height = allheight
        }
            //4. weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            height = allheight
        }
            // 5. weekend and low
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            height = allheight
        }
            // 6. weekend and high
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            height = allheight
        }
            // 7. weekend , high and low
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice == 0.0) && (_aMarinaPrices.regularCost.weekPrice == 0.0) && (_aMarinaPrices.regularCost.monthPrice == 0.0))  {
            
            height = allheight
        }
            // 8. regular day
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            if indexPath.row == 0 {
                height = self.weekdayHeight
            }else{
                height = self.allheight
                
            }
        }
            // 9. regular day and low season
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            if indexPath.row == 0 {
                height = self.weekdayHeight
            }else{
                height = self.allheight
                
            }
        }
            // 10. regular day and high season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            if indexPath.row == 0 {
                height = self.weekdayHeight
            }else{
                height = self.allheight
                
            }    }
            
            // 11. regular , high and low season
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType == "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            if indexPath.row == 0 {
                height = self.weekdayHeight
            }else{
                height = self.allheight
                
            }    }
            // 12. regular and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            if indexPath.row == 0 {
                height = self.weekdayHeight
            }else{
                height = self.allheight
                
            }
            
        }
            // 13. regular , low and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate == "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            if indexPath.row == 0 {
                height = self.weekdayHeight
            }else{
                height = self.allheight
                
            }
        }
            // 14. regular, high and weekend
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate == "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            if indexPath.row == 0 {
                height = self.weekdayHeight
            }else{
                height = self.allheight
                
            }
            
        }
        else if (_aMarinaPrices.highSession.sessionStartDate != "") && (_aMarinaPrices.lowSession.sessionStartDate != "") && (_aMarinaPrices.weekEndCost.weekendType != "") && ((_aMarinaPrices.regularCost.nightPrice != 0.0) || (_aMarinaPrices.regularCost.weekPrice != 0.0) || (_aMarinaPrices.regularCost.monthPrice != 0.0))  {
            
            if indexPath.row == 0 {
                height = self.weekdayHeight
            }else{
                height = self.allheight
                
            }
            
        }
        else{
            height = self.allheight
            
        }
        
        return height
    }
    
    
    
    
    
    func getNormalMarinaPrice(marinaID:String,priceFor:String,PriceDate:String,priceType: String,weekendType:String) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
            
        }
        
        self.isNewDataLoading = true
        CommonClass.showLoader(withStatus: "loading..")
        MarinaService.sharedInstance.getNormalMarinaPrices(forMarinaID: marinaID, priceFor: priceFor, priceType: priceType, priceDate: PriceDate, weekendType: weekendType)  { (success, resPriceModel, message) in
            CommonClass.hideLoader()
            self.isNewDataLoading = false
            
            if success{
                if let priceModel = resPriceModel{
                    self.marinaPriceSessionReview = priceModel
                    //self.countForPriceDetails = self.newRowCountPriceDetailSection()
                    
                    self.countForPriceDetails = self.caseCountSection()
                    
                    self.priceListTable.dataSource = self
                    self.priceListTable.delegate = self
                    self.priceListTable.reloadData()
                    print("Count>>>>\(self.countForPriceDetails)")
                }else{
                    showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
                }
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
            //                        self.rangeTableView.reloadData()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return self.countForPriceDetails
            
        }else if section == 1{
            if self.marinaPriceSessionReview.specialCosts.count != 0{
                
                return 1
            }
        }else{
            if self.marinaPriceSessionReview.specialCosts.count != 0{
                
                return self.marinaPriceSessionReview.specialCosts.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            let height = self.rowCountHeight(tableView: tableView, indexPath: indexPath)
            return height
        }
        else if indexPath.section == 1{
            if self.marinaPriceSessionReview.specialCosts.count != 0{
                
                return 60
            }
        }else{
            if self.marinaPriceSessionReview.specialCosts.count != 0{
                
                return 30
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell =  self.showTableCell(tableView: tableView, indexPath: indexPath)
            return cell
        }
        else if indexPath.section == 1{
            if self.marinaPriceSessionReview.specialCosts.count != 0{
                
                let cell = self.SpecialPriceHeader(tableView: tableView, indexPath: indexPath)
                return cell
            }
        }else{
            if self.marinaPriceSessionReview.specialCosts.count != 0{
                
                let cell = self.SpecialPriceCell(tableView: tableView, indexPath: indexPath)
                return cell
                
                
            }
        }
        return UITableViewCell()
    }
    
    
    
    
    func highSeasonPriceCell(tableView: UITableView, indexPath:IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SessionTableViewCell", for: indexPath) as! SessionTableViewCell
        let highSession = self.marinaPriceSessionReview.highSession
        cell.priceView.layer.backgroundColor = UIColor.white.cgColor
        
        cell.priceTypeLabel.text = "HIGH SESSION PRICE"
        cell.nightPriceLabel.text =  String(format: "%0.2lf", highSession.nightPrice)
        cell.weekPriceLabel.text =  String(format: "%0.2lf", highSession.weekPrice)
        cell.monthPriceLabel.text =  String(format: "%0.2lf", highSession.monthPrice)
        cell.fromDateLabel.text = highSession.sessionStartDate
        cell.endDateLabel.text = highSession.sessionEndDate
        return cell
    }
    
    func lowSeasonPriceCell(tableView: UITableView, indexPath:IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SessionTableViewCell", for: indexPath) as! SessionTableViewCell
        
        let highSession = self.marinaPriceSessionReview.lowSession
        cell.priceView.layer.backgroundColor = UIColor.white.cgColor
        
        cell.priceTypeLabel.text = "LOW SESSION PRICE"
        cell.nightPriceLabel.text =  String(format: "%0.2lf", highSession.nightPrice)
        cell.weekPriceLabel.text =  String(format: "%0.2lf", highSession.weekPrice)
        cell.monthPriceLabel.text =  String(format: "%0.2lf", highSession.monthPrice)
        cell.fromDateLabel.text = highSession.sessionStartDate
        cell.endDateLabel.text = highSession.sessionEndDate
        return cell
        
    }
    
    func weekdayPriceCell(tableView: UITableView, indexPath:IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeekDayTableViewCell", for: indexPath) as! WeekDayTableViewCell
        cell.priceView.layer.backgroundColor = UIColor.white.cgColor
        
        let highSession = self.marinaPriceSessionReview.regularCost
        cell.priceTypeLabel.text = "WEEKDAY PRICE"
        cell.nightPriceLabel.text =  String(format: "%0.2lf", highSession.nightPrice)
        cell.weekPriceLabel.text =  String(format: "%0.2lf", highSession.weekPrice)
        cell.monthPriceLabel.text =  String(format: "%0.2lf", highSession.monthPrice)
        print("specific count>>>>>\(self.marinaPriceSessionReview.specialCosts.count)")
        return cell
    }
    
    func weekendPriceCell(tableView: UITableView, indexPath:IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeekendPriceSetUpTableViewCell", for: indexPath) as! WeekendPriceSetUpTableViewCell
        cell.priceView.layer.backgroundColor = UIColor.white.cgColor
        
        cell.priceTypeLabel.text = "WEEKEND PRICE "
        cell.priceLabel.text = String(format: "%0.2lf", self.marinaPriceSessionReview.weekEndCost.price)
        cell.weekendView.layer.backgroundColor = UIColor.white.cgColor
        cell.weekendView.layer.shadowOpacity = 0.2
        self.weekendType(tableView: self.priceListTable, indexPath:indexPath, cell: cell)
        return cell
    }
    
    func SpecialPriceCell(tableView: UITableView, indexPath:IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialDayPriceTableViewCell", for: indexPath) as! SpecialDayPriceTableViewCell
        cell.priceView.layer.backgroundColor = UIColor.white.cgColor
        
        let specialCost = self.marinaPriceSessionReview.specialCosts[indexPath.row]
        cell.dateLabel.text = specialCost.date + " :"
        cell.priceLabel.text = String(format: "%0.2lf", specialCost.price)
        return cell
    }
    
    func SpecialPriceHeader(tableView: UITableView, indexPath:IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell", for: indexPath) as! HeaderTableViewCell
        cell.headerLabel.text = "SPECIAL PRICES"
        
        return cell
    }
    
    
    func weekendType(tableView: UITableView, indexPath:IndexPath, cell: WeekendPriceSetUpTableViewCell){
        if (self.marina.weekendType == "weekend2") {
            cell.secondStackView.isHidden = true
            cell.firstStackView.isHidden = false
            cell.w1Friday.text = "    Friday    "
            cell.w1Saturday.text = "    Saturday   "
            cell.w1Sunday.text = "    Sunday   "
            //cell?.w1Friday.layer.borderWidth = 0.5
            cell.w1Friday.layer.cornerRadius = 15.0
            cell.w1Friday.layer.masksToBounds = true
            cell.w1Friday.layer.backgroundColor = lightGrayColor.cgColor
            
            //cell?.w1Saturday.layer.borderWidth = 0.5
            cell.w1Saturday.layer.cornerRadius = 15.0
            cell.w1Saturday.layer.masksToBounds = true
            cell.w1Saturday.layer.backgroundColor = lightGrayColor.cgColor
            
            //cell?.w1Sunday.layer.borderWidth = 0.5
            cell.w1Sunday.layer.cornerRadius = 15.0
            cell.w1Sunday.layer.masksToBounds = true
            cell.w1Sunday.layer.backgroundColor = lightGrayColor.cgColor
        }else{
            cell.secondStackView.isHidden = false
            cell.firstStackView.isHidden = true
            cell.w2Friday.text = "    Saturday    "
            cell.w2Saturday.text = "    Sunday    "
            //cell?.w2Friday.layer.borderWidth = 0.5
            cell.w2Friday.layer.cornerRadius = 15.0
            cell.w2Friday.layer.masksToBounds = true
            cell.w2Friday.layer.backgroundColor = lightGrayColor.cgColor
            
            //cell?.w2Saturday.layer.borderWidth = 0.5
            cell.w2Saturday.layer.cornerRadius = 15.0
            cell.w2Saturday.layer.masksToBounds = true
            cell.w2Saturday.layer.backgroundColor = lightGrayColor.cgColor
        }
        
    }
    
}

