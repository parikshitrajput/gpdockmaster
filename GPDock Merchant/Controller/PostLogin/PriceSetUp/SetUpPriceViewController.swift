//
//  SetUpPriceViewController.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
enum priceSelect: String{
    case defaultPrice = "NORMAL PRICE"
    case weekend = "WEEKEND PRICE"
    case special = "SPECIAL DAY PRICE"
    case highSeason = "HIGH SEASON PRICE"
    case lowSeason = "LOW SEASON PRICE"
        
}


class SetUpPriceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var priceSettingTable: UITableView!
    var specialDates:Array<String>?
    var marina : Marina!
    let lightGrayColor = UIColor(red:234.0/255.0, green:235.0/255.0, blue:237.0/255, alpha:1.0)

    let lightBlackColor = UIColor(red:111.0/255.0, green:113.0/255.0, blue:121.0/255, alpha:1.0)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.priceSettingTable.backgroundColor = UIColor.groupTableViewBackground
        self.priceSettingTable.register(UINib(nibName: "SetUpPriceTableViewCell", bundle: nil), forCellReuseIdentifier: "SetUpPriceTableViewCell")
        self.priceSettingTable.register(UINib(nibName: "WeekendPriceSetUpTableViewCell", bundle: nil), forCellReuseIdentifier: "WeekendPriceSetUpTableViewCell")
        self.priceSettingTable.register(UINib(nibName: "RangesPricesTableViewCell", bundle: nil), forCellReuseIdentifier: "RangesPricesTableViewCell")
        self.priceSettingTable.dataSource = self
        self.priceSettingTable.delegate = self
        self.getdatesAllSelectPrices(marinaID: self.marina.ID)
        print("marinaPriceType>>>>>>\(self.marina.priceType)")
        print("weekendTypeType>>>>>>\(self.marina.weekendType)")

    }
    
    
    func getdatesAllSelectPrices(marinaID:String) {
        CommonClass.showLoader(withStatus: "Loading..")
        
        MarinaService.sharedInstance.getDatesAllSelectPrices(forMarina: marinaID) {(pricesData) in
            CommonClass.hideLoader()
            if let Dates = pricesData as? Array<String>{
                self.specialDates = Dates
            }else{
                
            }
        }
    }

    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    

    

//    @objc func weekendPriceSetUp(){
//        let selectRangeVC = AppStoryboard.PriceSetup.viewController(PricePerRangeSelectViewController.self)
//        selectRangeVC.showNavTitle = priceSelect.weekend.rawValue//"weekend"
//        selectRangeVC.priceFor = "weekend"
//        selectRangeVC.marina = marina
//
//        self.navigationController?.pushViewController(selectRangeVC, animated: true)
//    }
    
     func onClickOpenCalander() {
        self.view.endEditing(true)
        let calendarVC = AppStoryboard.PriceSetup.viewController(SpecialDayPriceCalendarViewController.self)
        calendarVC.specialPrices = self.specialDates
        calendarVC.marina = self.marina
        calendarVC.priceType = "night"
       // calendarVC.delegate = self
         self.navigationController?.pushViewController(calendarVC, animated: true)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
            
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RangesPricesTableViewCell", for: indexPath) as! RangesPricesTableViewCell
            cell.priceTypeLabel.text = "NORMAL PRICE "
            cell.priceTypeDescriptionLabel.text = "Normal price is a Weekday day price."
            cell.priceTypeDescriptionLabel.textColor = lightBlackColor
            cell.priceView.layer.backgroundColor = UIColor.white.cgColor
            cell.nightButton.addTarget(self, action: #selector(onClickNightButton(_:)), for: .touchUpInside)
            cell.weekButton.addTarget(self, action: #selector(onClickWeekButton(_:)), for: .touchUpInside)
            cell.monthButton.addTarget(self, action: #selector(onClickMonthButton(_:)), for: .touchUpInside)
            self.selectButtonDesign(cell: cell)

            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RangesPricesTableViewCell", for: indexPath) as! RangesPricesTableViewCell
            cell.priceTypeLabel.text = "HIGH SEESION PRICE "
            cell.priceTypeDescriptionLabel.text = " High Season price is a particular season price."
            cell.priceTypeDescriptionLabel.textColor = lightBlackColor
            cell.priceView.layer.backgroundColor = UIColor.white.cgColor
            cell.nightButton.addTarget(self, action: #selector(onClickNightButton(_:)), for: .touchUpInside)
            cell.weekButton.addTarget(self, action: #selector(onClickWeekButton(_:)), for: .touchUpInside)
            cell.monthButton.addTarget(self, action: #selector(onClickMonthButton(_:)), for: .touchUpInside)
            self.selectButtonDesign(cell: cell)

            return cell
            
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "RangesPricesTableViewCell", for: indexPath) as! RangesPricesTableViewCell
            cell.priceTypeLabel.text = "LOW SESSION PRICE"
            cell.priceTypeDescriptionLabel.text = "Low Season price is a particular season price."
            cell.priceTypeDescriptionLabel.textColor = lightBlackColor
            cell.priceView.layer.backgroundColor = UIColor.white.cgColor
            cell.nightButton.addTarget(self, action: #selector(onClickNightButton(_:)), for: .touchUpInside)
            cell.weekButton.addTarget(self, action: #selector(onClickWeekButton(_:)), for: .touchUpInside)
            cell.monthButton.addTarget(self, action: #selector(onClickMonthButton(_:)), for: .touchUpInside)
            self.selectButtonDesign(cell: cell)
            return cell

        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeekendPriceSetUpTableViewCell", for: indexPath) as! WeekendPriceSetUpTableViewCell
            cell.priceTypeLabel.text = "WEEKEND PRICE "
            cell.priceTypeDescriptionLabel.text = "Weekend price is either two days(saturday,sunday) or three day(friday, saturday, sunday) price."
            cell.priceTypeDescriptionLabel.textColor = lightBlackColor
            cell.weekendDescriptionView.layer.backgroundColor = UIColor.white.cgColor
            cell.weekendView.layer.backgroundColor = UIColor.white.cgColor
            cell.weekendView.layer.shadowOpacity = 0.2
            self.weekendType(tableView: self.priceSettingTable, indexPath:indexPath, cell: cell)
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SetUpPriceTableViewCell", for: indexPath) as! SetUpPriceTableViewCell
            cell.priceTypeLabel.text = "SPECIAL DAY PRICE "
            cell.priceTypeDescriptionLabel.text = "Special day price is a particular day price."
            cell.priceTypeDescriptionLabel.textColor = lightBlackColor
            cell.priceView.layer.backgroundColor = UIColor.white.cgColor
            //cell.priceTypeSelectButton.addTarget(self, action: #selector(onClickOpenCalander(_:)), for: .touchUpInside)
            
            return cell
        default:
            return UITableViewCell()

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 3:
            self.navigatetToPriceRangeVC(priceFor: "weekend", priceType: "night", navTitle: priceSelect.weekend.rawValue)
        case 4:
              self.onClickOpenCalander()
        default:
            break
        }
    }
    
    func selectButtonDesign(cell: RangesPricesTableViewCell) {
        
        cell.nightButton.layer.cornerRadius = 15.0
        cell.nightButton.layer.masksToBounds = true
        cell.nightButton.layer.backgroundColor = lightGrayColor.cgColor
        
        cell.weekButton.layer.cornerRadius = 15.0
        cell.weekButton.layer.masksToBounds = true
        cell.weekButton.layer.backgroundColor = lightGrayColor.cgColor
        
        cell.monthButton.layer.cornerRadius = 15.0
        cell.monthButton.layer.masksToBounds = true
        cell.monthButton.layer.backgroundColor = lightGrayColor.cgColor
    }
    
    
    func weekendType(tableView: UITableView, indexPath:IndexPath, cell: WeekendPriceSetUpTableViewCell){
        if (self.marina.weekendType == "weekend2") {
            cell.secondStackView.isHidden = true
            cell.firstStackView.isHidden = false
            cell.w1Friday.text = "    Friday    "
            cell.w1Saturday.text = "    Saturday   "
            cell.w1Sunday.text = "    Sunday   "
            //cell?.w1Friday.layer.borderWidth = 0.5
            cell.w1Friday.layer.cornerRadius = 15.0
            cell.w1Friday.layer.masksToBounds = true
            cell.w1Friday.layer.backgroundColor = lightGrayColor.cgColor
            
            //cell?.w1Saturday.layer.borderWidth = 0.5
            cell.w1Saturday.layer.cornerRadius = 15.0
            cell.w1Saturday.layer.masksToBounds = true
            cell.w1Saturday.layer.backgroundColor = lightGrayColor.cgColor
            
            //cell?.w1Sunday.layer.borderWidth = 0.5
            cell.w1Sunday.layer.cornerRadius = 15.0
            cell.w1Sunday.layer.masksToBounds = true
            cell.w1Sunday.layer.backgroundColor = lightGrayColor.cgColor
        }else{
            cell.secondStackView.isHidden = false
            cell.firstStackView.isHidden = true
            cell.w2Friday.text = "    Saturday    "
            cell.w2Saturday.text = "    Sunday    "
            //cell?.w2Friday.layer.borderWidth = 0.5
            cell.w2Friday.layer.cornerRadius = 15.0
            cell.w2Friday.layer.masksToBounds = true
            cell.w2Friday.layer.backgroundColor = lightGrayColor.cgColor
            
            //cell?.w2Saturday.layer.borderWidth = 0.5
            cell.w2Saturday.layer.cornerRadius = 15.0
            cell.w2Saturday.layer.masksToBounds = true
            cell.w2Saturday.layer.backgroundColor = lightGrayColor.cgColor
        }

    }
    
    
    @IBAction func onClickNightButton(_ sender: UIButton) {
        if let indexPath = sender.tableViewIndexPath(self.priceSettingTable) as IndexPath?{

            
            switch indexPath.row {
            case 0:
                navigatetToPriceRangeVC(priceFor: "default", priceType: "night", navTitle: priceSelect.defaultPrice.rawValue)
            case 1:
                navigatetToPriceRangeVC(priceFor: "high_season", priceType: "night", navTitle: priceSelect.highSeason.rawValue)
            case 2:
                navigatetToPriceRangeVC(priceFor: "low_season", priceType: "night", navTitle: priceSelect.lowSeason.rawValue)

            default:
                break
            }

        }
    }
    
    @IBAction func onClickWeekButton(_ sender: UIButton) {
        if let indexPath = sender.tableViewIndexPath(self.priceSettingTable) as IndexPath?{
        switch indexPath.row {

        case 0:
            navigatetToPriceRangeVC(priceFor: "default", priceType: "week", navTitle: priceSelect.defaultPrice.rawValue)
        case 1:
            navigatetToPriceRangeVC(priceFor: "high_season", priceType: "week", navTitle: priceSelect.highSeason.rawValue)
        case 2:
            navigatetToPriceRangeVC(priceFor: "low_season", priceType: "week", navTitle: priceSelect.lowSeason.rawValue)
 
        default:
            break
        }
        }

    }
    
    @IBAction func onClickMonthButton(_ sender: UIButton) {
        if let indexPath = sender.tableViewIndexPath(self.priceSettingTable) as IndexPath?{
            
            switch indexPath.row {
            case 0:
                navigatetToPriceRangeVC(priceFor: "default", priceType: "month", navTitle: priceSelect.defaultPrice.rawValue)
            case 1:
                navigatetToPriceRangeVC(priceFor: "high_season", priceType: "month", navTitle: priceSelect.highSeason.rawValue)
            case 2:
                navigatetToPriceRangeVC(priceFor: "low_season", priceType: "month", navTitle: priceSelect.lowSeason.rawValue)

            default:
                break
            }
            
        }
    }
    
    func navigatetToPriceRangeVC(priceFor:String, priceType: String, navTitle: String) {
        let nextVC = AppStoryboard.PriceSetup.viewController(PricePerRangeSelectViewController.self)
        nextVC.priceFor = priceFor
        nextVC.priceType = priceType
        nextVC.marina = marina
        nextVC.showNavTitle = navTitle
        self.navigationController?.pushViewController(nextVC, animated: true)
        
    }

}

