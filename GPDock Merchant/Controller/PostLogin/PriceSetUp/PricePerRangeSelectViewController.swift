//
//  PricePerRangeSelectViewController.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//


import UIKit

class PricePerRangeSelectViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var headerHeight:CGFloat = 75

    @IBOutlet weak var rangeTableView: UITableView!
    @IBOutlet weak var submitButton: UIButton!

    var marinaPriceRange =  MarinaRangePrice()
    var dateArray: Array<String>?
    var showNavTitle = ""
    var priceFor:String = ""
    var priceType: String = ""
    var user : User!
    var marina : Marina!
    var isNewDataLoading = false
    var  showDateHeader : ShowDate!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.rangeTableView.backgroundColor = UIColor.groupTableViewBackground
        self.setupNavigationTitle()

        self.rangeTableView.register(UINib(nibName: "PriceWithRangeSelectTableViewCell", bundle: nil), forCellReuseIdentifier: "PriceWithRangeSelectTableViewCell")
        self.rangeTableView.dataSource = self
        self.rangeTableView.delegate = self
        self.getrangeAndPrice()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setupNavigationTitle() {

        if showNavTitle == priceSelect.special.rawValue {
            let stringDate = convertToShowFormatDate(dateArr: dateArray ?? [""])
            navigationItem.title = stringDate[0]
        }else{
            navigationItem.title = showNavTitle
        }
    }

    func convertToShowFormatDate(dateArr: Array<String>) -> Array<String> {
        var tempArray = Array<String>()
        for dateString in dateArray ?? [""] {
            let dateFormatterDate = DateFormatter()
            dateFormatterDate.dateFormat = "YYYY-MM-dd " //Your date format
            let serverDate: Date = dateFormatterDate.date(from: dateString)! //according to date format your date string
            let dateFormatterString = DateFormatter()
            dateFormatterString.dateFormat = "dd-MMMM" //Your New Date format as per requirement change it own
            let newDate: String = dateFormatterString.string(from: serverDate)
            tempArray.append(newDate)
        }
        return tempArray
    }
    
    
    
    func getrangeAndPrice() {
        self.getPriceWithRange(marinaID: self.marina.ID, priceFor: self.priceFor, priceType: priceType, PriceDate: dateArray?[0] ?? "", weekendType: (priceFor == "weekend") ? self.marina.weekendType : "")
        
    }
    
    func getPriceWithRange(marinaID:String,priceFor:String,priceType:String,PriceDate:String,weekendType:String) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        
        self.isNewDataLoading = true
        MarinaService.sharedInstance.getMarinaPriceByRange(forMarinaID: marinaID, priceFor: priceFor, priceType: priceType, priceDate: PriceDate, weekendType: weekendType) { (success, resPriceModel, message) in
                        self.isNewDataLoading = false

                        if success{
                            if let priceModel = resPriceModel{
                                self.marinaPriceRange = priceModel
                                self.rangeTableView.reloadData()
                            }else{
                                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
                            }
                        }else{
                            showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
                        }
//                        self.rangeTableView.reloadData()
                    }
        
    }
    

    
    private func showAlertWithAddPrice(message:String,title:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) in
            self.navigationController?.popToRoot(true)
        }
        
        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
        alert.view.tintColor = UIColor.white
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    @IBAction func onClickSubmitButton(_ sender: UIButton){
        self.view.endEditing(true)
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return (marinaPriceRange.prices.count > 0) ? marinaPriceRange.prices.count : 1 ;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if marinaPriceRange.prices.count != 0  {
            
            if (self.priceFor == "high_season" || self.priceFor == "low_season" ){
                self.showDateHeader = ShowDate.instanceFromNib()
                self.showDateHeader.frame = CGRect(x: 0, y:0, width: tableView.frame.size.width, height: headerHeight)
                self.showDateHeader.fromDateLabel.text = self.marinaPriceRange.seasonStartDate
                self.showDateHeader.toDateLabel.text = self.marinaPriceRange.seasonEndDate
                
                self.showDateHeader.setNeedsLayout()
                self.showDateHeader.layoutIfNeeded()
            }
            
        }
        
        return self.showDateHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var tempHeight: CGFloat = 75
        if marinaPriceRange.prices.count != 0  {
            
            if (self.priceFor == "high_season" || self.priceFor == "low_season" ){
                tempHeight = self.headerHeight
            }else{
                tempHeight = 0
                
            }
            
        }else{
            tempHeight = 0
            
        }
        return  tempHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.marinaPriceRange.prices.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text =  self.isNewDataLoading ? "Loading.." : "No price range Present"
            return cell
        }else{
        let cell = rangeTableView.dequeueReusableCell(withIdentifier: "PriceWithRangeSelectTableViewCell", for: indexPath) as! PriceWithRangeSelectTableViewCell
        cell.rangeSelectLabel.text = marinaPriceRange.prices[indexPath.row].range  //committedPrices[indexPath.row].range
        cell.rangePrice.placeholder = ""
        cell.rangeView.layer.backgroundColor = UIColor.white.cgColor
        cell.currencyLabel.isHidden = true

        cell.rangePrice.numberValue = NSDecimalNumber(value: marinaPriceRange.prices[indexPath.row].price)

        return cell
        
    }
    }

}

