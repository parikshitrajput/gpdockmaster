//
//  ProfileViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import Foundation
import UIKit
import Messages
import MessageUI

class ProfileViewController: UITableViewController {
    var user : User!
    var marina : Marina!
    var profileView : UITableView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileBackgroundView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var marinaNameLabel: UILabel!
    @IBOutlet weak var userContact: UILabel!
    @IBOutlet weak var profileContainner: UIVisualEffectView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson)
        self.setUpUserDetails(user: user)
        //NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.userDidLoggedInHandler(_:)), name: .USER_DID_LOGGED_IN_NOTIFICATION, object: nil)
        self.addGestureRecognizer()
        self.profileView = self.tableView
        self.clearsSelectionOnViewWillAppear = false
        

    }
    
    func addGestureRecognizer(){
        let profileTapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(imageTapped(_:)))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(profileTapGestureRecognizer)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        self.profileView.setNeedsLayout()
        self.profileView.layoutIfNeeded()
        self.profileView.reloadData()
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircular(profileImageView, borderColor: .black, borderWidth: 0)
        self.profileContainner.addshadow(top: true, left: true, bottom: false, right: true, shadowRadius: 4, shadowOpacity: 0.4)
        if let profileCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)){
            profileCell.setNeedsLayout()
            profileCell.layoutIfNeeded()
        }    }
    

    @objc func imageTapped(_ tapGestureRecognizer: UITapGestureRecognizer)
    {
        if let imageView = tapGestureRecognizer.view as? UIImageView{
            guard let profileImage = imageView.image else{
                return
            }
            let imageViewerVC = AppStoryboard.Profile.viewController(ImageViewerViewController.self)
            imageViewerVC.profileImage = profileImage
            self.present(imageViewerVC, animated: true, completion: nil)
        }
    }


    func setUpUserDetails(user: User) {
        profileImageView.setIndicatorStyle(.white)
        profileImageView.setShowActivityIndicator(true)
        profileImageView.sd_setImage(with: URL(string:user.profileImage) ?? URL(string:BASE_URL), placeholderImage:#imageLiteral(resourceName: "user_img"))
        //CommonClass.makeViewCircular(profileImageView, borderColor: .darkGray, borderWidth: 0.5)
        CommonClass.makeViewCircular(profileImageView, borderColor: .black, borderWidth: 0)
        profileImageView.addshadow(top: true, left: true, bottom: true, right: true)

        profileBackgroundView.sd_setImage(with: URL(string:user.profileImage) ?? URL(string:BASE_URL), placeholderImage:CommonClass.sharedInstance.userAvatarImage(username:user.firstName+" "+user.lastName))
        fullNameLabel.text = user.firstName+" "+user.lastName
        marinaNameLabel.text = user.email
        userContact.text = user.contact
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        self.tableView.reloadData()

        if let profileCell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)){
            profileCell.setNeedsLayout()
            profileCell.layoutIfNeeded()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickMyBookings(_ sender: UIButton){
        self.getMarinaPriceType(marinaID: self.marina.ID)
//        if marina.priceType == graduallyPriceType {
//        let slipPricingVC = AppStoryboard.PriceSetup.viewController(SetUpPriceViewController.self)
//            slipPricingVC.marina = marina
//            self.navigationController?.pushViewController(slipPricingVC, animated: true)
//        }else{
//        let slipPricingVC = AppStoryboard.PriceSetup.viewController(NormalPriceListViewController.self)
//            slipPricingVC.marina = marina
//            self.navigationController?.pushViewController(slipPricingVC, animated: true)
//        }

    }
    
    @IBAction func onClickPaymentMethods(_ sender: UIButton){
        let bankSettingVC = AppStoryboard.Earning.viewController(GatewayViewController.self)
        self.navigationController?.pushViewController(bankSettingVC, animated: true)
    }
    
    @IBAction func onClickMyBoats(_ sender: UIButton){
                let payoutsVC = AppStoryboard.Earning.viewController(PayoutsViewController.self)
                self.navigationController?.pushViewController(payoutsVC, animated: true)
    }
    @IBAction func onClickNotifications(_ sender: UIButton){
        let notificationVC = AppStoryboard.Main.viewController(UpdatePasswordViewController.self)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func onClickReportUs(_ sender: UIButton){
        self.composeEmail(reciepents: kReportUsEmail, subject: "Report of GPDock Merchant!")
    }
    
    @IBAction func onClickContactUs(_ sender: UIButton){
        self.showAlertToChoosePhone()
    }

    @IBAction func onClickEditProfile(_ sender: UIButton){
        let editProfileVC = AppStoryboard.Profile.viewController(EditProfileViewController.self)
        editProfileVC.user = self.user
        editProfileVC.userImage = self.profileImageView.image
        editProfileVC.delegate = self
        self.present(editProfileVC, animated: true, completion: nil)

    }

    @IBAction func onClickLogoutProfile(_ sender: UIButton){
              self.showAlertToConfirmBooking()
    }
    
    @IBAction func onClickDownlordScanCode(_ sender: UIButton){
        let notificationVC = AppStoryboard.Profile.viewController(DisplayMarinaQRCodeViewController.self)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    func composeEmail(reciepents:String,subject:String){
        if !MFMailComposeViewController.canSendMail(){
            showAlertWith(self, message: "Configure mail account", title: warningMessage.alertTitle.rawValue)
            return
        }
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([reciepents])
        composeVC.setSubject(subject)
        composeVC.setMessageBody("Hey Team Here is my feedback", isHTML: false)
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func showAlertToConfirmBooking(){
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message:"Are you sure you want to logout?", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "No", style: .cancel) { action -> Void in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        let doneAction: UIAlertAction = UIAlertAction(title: "Yes", style: .default)
        { action -> Void in
            self.logoutProceed()
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(doneAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func logoutProceed() {
        LoginService.sharedInstance.logOut(self.user.ID) {  (success,responseUser,message) in
            if success {
                kUserDefaults.set(false, forKey: kIsLoggedIN)
                let user = User()
                user.saveUserInfo(user)
                let marina = Marina()
                marina.saveMarinaInfo(marina)
                let loginVC = AppStoryboard.Main.viewController(LoginViewController.self)
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return (tableView.frame.size.height-49)/2//(tableView.frame.size.height - 49)*520/1138
        }else{
            return (tableView.frame.size.height-49-10)/2//(tableView.frame.size.height - 49)*434/1138
        }

    }

}

extension ProfileViewController: EditProfileViewControllerDelegate{
    func user(didEditProfile viewController: EditProfileViewController, withUpdatedUser user: User) {
        self.user = user
        self.setUpUserDetails(user: user)
        viewController.dismiss(animated: true, completion: nil)
    }
    
    
    func showAlertToChoosePhone(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let phoneAction: UIAlertAction = UIAlertAction(title: "Phone", style: .default)
        { action -> Void in
            let tollFreeUrl = "telprompt://\(tollFreeNumber)"
            self.showPhoneApp(tollFreeUrl: tollFreeUrl)
        }
        actionSheet.addAction(phoneAction)

        let skypeAction: UIAlertAction = UIAlertAction(title: "Skype", style: .default)
        { action -> Void in
            self.callFromSkype()
        }
        actionSheet.addAction(skypeAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func callFromSkype()  {
        let installed = UIApplication.shared.canOpenURL(NSURL(string: "skype:")! as URL)
        if installed {
            UIApplication.shared.openURL(NSURL(string: "skype:\(tollFreeNumber)")! as URL)
        } else {
            UIApplication.shared.openURL(NSURL(string: "https://itunes.apple.com/in/app/skype/id304878510?mt=8")! as URL)
        }
    }
    func showPhoneApp(tollFreeUrl: String)  {
        if (UIApplication.shared.canOpenURL(URL(string:tollFreeUrl)!))
        {
            UIApplication.shared.openURL(URL(string:tollFreeUrl)!)
        } else
        {
            showAlertWith(nil, message: "Cann't able to call right now\r\nPlease try later!", title: warningMessage.alertTitle.rawValue)
        }
    }
    
}


extension ProfileViewController: MFMailComposeViewControllerDelegate{
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        var message = ""
        
        switch result {
        case .saved:
            message = "Saved as draft"
        case .cancelled:
            message = "Cancelled by you"
        case .sent:
            message = "Mail has been sent"
        case .failed:
            message = "Mail sent failed"
        }
        
        controller.dismiss(animated: true, completion: {
            showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
        })
        
    }
}


extension ProfileViewController{
    
    func getMarinaPriceType(marinaID: String) {
        CommonClass.showLoader(withStatus: "Processing..")
        
        MarinaService.sharedInstance.getUpdateMarinaPriceType(marinaID) {(success,resMarina,message) in
            if let newMarina = resMarina {
                CommonClass.hideLoader()
                self.marina.priceType = newMarina.priceType
                self.marina.weekendType = newMarina.weekendType
                
                if self.marina.priceType == graduallyPriceType {
                    let slipPricingVC = AppStoryboard.PriceSetup.viewController(SetUpPriceViewController.self)
                    slipPricingVC.marina = self.marina
                    self.navigationController?.pushViewController(slipPricingVC, animated: true)
                }else{
                    let slipPricingVC = AppStoryboard.PriceSetup.viewController(NormalPriceListViewController.self)
                    slipPricingVC.marina = self.marina
                    self.navigationController?.pushViewController(slipPricingVC, animated: true)
                }
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
            
            
        }
        
    }
    
}
