//
//  EditProfileViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

protocol EditProfileViewControllerDelegate {
    func user(didEditProfile viewController: EditProfileViewController,withUpdatedUser user: User)
}

class EditProfileViewController: UIViewController {
    var user : User!
    var delegate : EditProfileViewControllerDelegate?
    var shouldPostData : Bool = false
    var imagePickerController : UIImagePickerController!

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileBackgroundView: UIImageView!

    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateNameTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var zipcodeTextField: UITextField!
    @IBOutlet weak var doneButton: UIButton!

    var userImage: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.addGestureRecognizer()
        self.refreshUserDetails(user: self.user)
    }

    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircular(profileImageView, borderColor: .black, borderWidth: 0)
        self.doneButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickDone(_ sender: UIButton){
        guard let name = self.fullNameTextField.text else {
            showAlertWith(self, message: "Enter a valid first name", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let mobile = self.phoneNumberTextField.text else {
            showAlertWith(self, message: "Enter mobile number", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let lastName = self.lastNameTextField.text else {
            showAlertWith(self, message: "Enter a valid last name", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let address = self.addressTextField.text else {
            showAlertWith(self, message: "Enter a valid address", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let city = self.cityTextField.text else {
            showAlertWith(self, message: "Enter a valid city", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let state = self.stateNameTextField.text else {
            showAlertWith(self, message: "Enter a valid state", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let country = self.countryTextField.text else {
            showAlertWith(self, message: "Enter a valid country", title: warningMessage.alertTitle.rawValue)
            return
        }
        guard let zipCode = self.zipcodeTextField.text else {
            showAlertWith(self, message: "Enter a valid zipcode", title: warningMessage.alertTitle.rawValue)
            return
        }
//        let validation = self.validateUserDetails(name, lastName: lastName,mobile: mobile, address: address, city: city, state: state, country: country, zipcode: zipCode)
//        //let validation = self.validateUserDetails(name, mobile: mobile)
//        if !validation.result{
//            showAlertWith(self, message: validation.message, title: "Error!")
//            return
//        }
        self.updateProfile(self.user.ID, fName: name, lastName: lastName, email: self.user.email, mobile: mobile, address: address, city: city, state: state, country: country, zipcode: zipCode, profileImage: self.userImage)
       // self.updateProfile(self.user.ID, name: name, email: self.user.email, mobile: mobile, profileImage: self.userImage)

    }
    func validateUserDetails(_ fName:String,lastName:String,mobile:String,address:String,city:String,state:String,country:String,zipcode:String) -> (result:Bool,message:String) {

        
        if fName.trimmingCharacters(in: .whitespaces) == ""{
            return (result:false,message:"Enter a valid  first name")
        }
        if lastName.trimmingCharacters(in: .whitespaces) == ""{
            return (result:false,message:"Enter a valid last name")
        }
        if mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (result:false,message:"Mobile cann't be empty!")
        }
        if address.trimmingCharacters(in: .whitespaces) == ""{
            return (result:false,message:"Enter a valid address")
        }
        if city.trimmingCharacters(in: .whitespaces) == ""{
            return (result:false,message:"Enter a valid city")
        }
        if state.trimmingCharacters(in: .whitespaces) == ""{
            return (result:false,message:"Enter a valid state")
        }
        if country.trimmingCharacters(in: .whitespaces) == ""{
            return (result:false,message:"Enter a valid country")
        }
        if zipcode.trimmingCharacters(in: .whitespaces) == ""{
            return (result:false,message:"Enter a valid zipcode")
        }

        if !CommonClass.validateNumber(mobile.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)){
            return (result:false,message:"Mobile number should be in correct format!")
        }
        return (result:true,message:"")
    }

    func addGestureRecognizer(){
        let profileTapGestureRecognizer = UITapGestureRecognizer(target: self, action:#selector(imageTapped(_:)))
        profileImageView.isUserInteractionEnabled = true
        profileImageView.addGestureRecognizer(profileTapGestureRecognizer)
    }

    @objc func imageTapped(_ tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.showAlertToChooseAttachmentOption()
    }

    func refreshUserDetails(user: User) {
        profileImageView.setIndicatorStyle(.white)
        profileImageView.setShowActivityIndicator(true)
        profileImageView.sd_setImage(with: URL(string:user.profileImage) ?? URL(string:BASE_URL), placeholderImage:CommonClass.sharedInstance.userAvatarImage(username:user.firstName+" "+user.lastName))
        CommonClass.makeViewCircular(profileImageView, borderColor: .black, borderWidth: 0)
        profileImageView.addshadow(top: true, left: true, bottom: true, right: true)
        profileBackgroundView.sd_setImage(with: URL(string:user.profileImage) ?? URL(string:BASE_URL), placeholderImage:(self.userImage ?? CommonClass.sharedInstance.userAvatarImage(username:user.firstName+" "+user.lastName)))
        fullNameTextField.text = user.firstName
        phoneNumberTextField.text = user.contact
        emailTextField.text = user.email
        lastNameTextField.text = user.lastName
        addressTextField.text = user.address
        cityTextField.text = user.city
        countryTextField.text = user.country
        stateNameTextField.text = user.state
        zipcodeTextField.text = user.zipCode
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }

func updateProfile(_ userID:String,fName:String,lastName:String,email:String,mobile:String,address:String,city:String,state:String,country:String,zipcode:String,profileImage: UIImage?){
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        CommonClass.showLoader(withStatus: "Updating..")
    LoginService.sharedInstance.updateUserWith(userID, fName: fName, lastName: lastName, email: email, mobile: mobile, address: address, city: city, state: state, country: country, zipcode: zipcode, profileImage: profileImage) { (success, updatedUser, message) in
            CommonClass.hideLoader()
            if success{
                if let freshUser = updatedUser{
                    self.delegate?.user(didEditProfile: self, withUpdatedUser: freshUser)
                }
            }
        }
    }

}
extension EditProfileViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){

        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.userImage = tempImage
            self.profileImageView.image = self.userImage
            self.profileBackgroundView.image = self.userImage
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.userImage = tempImage
            self.profileImageView.image = self.userImage
            self.profileBackgroundView.image = self.userImage
        }
        picker.dismiss(animated: true) {}
    }
}

//MARK: - RSKImageCropViewControllerDelegate
//extension EditProfileViewController:RSKImageCropViewControllerDelegate{
//    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
//        self.navigationController?.pop(true)
//    }
//
//
//    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
//        self.userImage = croppedImage
//        self.profileImageView.image = croppedImage
//        self.profileBackgroundView.image = croppedImage
//        self.navigationController?.pop(true)
//    }
//
//}

