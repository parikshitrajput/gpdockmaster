//
//  ImageViewerViewController.swift
//  GPDock
//
//  Created by TecOrb on 19/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ImageViewerViewController: UIViewController {
    @IBOutlet weak var profileImageView : UIImageView!
    @IBOutlet weak var backgroundImageView : UIImageView!

    var profileImage : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileImageView.image = profileImage
        self.backgroundImageView.image = profileImage
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onClickClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
