//
//  DisplayMarinaQRCodeViewController.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 30/04/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Photos
import SDWebImage

class DisplayMarinaQRCodeViewController: UIViewController {
    @IBOutlet weak var QRImageView: UIImageView!
    @IBOutlet weak var marinaTittleLabel: UILabel!
    //@IBOutlet weak var qrCodeLabel: UILabel!


     var marina : Marina!
    var qrProfileMarina: Marina!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
     //self.QRImageView.image = #imageLiteral(resourceName: "scan_action")
        self.getQRCodeProfile(marinaID: self.marina.ID)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
    self.navigationController?.pop(true)
    }
    @IBAction func onCkickShareButton(_ sender: UIBarButtonItem) {
        showAlertToChooseAttachmentOption()
    }
    
    func getQRCodeProfile(marinaID: String) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
            
        }
        if !CommonClass.isLoaderOnScreen{
            CommonClass.showLoader(withStatus: "Loading..")
        }
        MarinaService.sharedInstance.getMarinaQRCode(marinaID) { (response) in
            CommonClass.hideLoader()
            if let marinaProfile = response {
              self.qrProfileMarina = marinaProfile
                self.setMarinaImageAndTitle()
            }
        }
    }
    
    func setMarinaImageAndTitle() {
      self.marinaTittleLabel.text = self.qrProfileMarina.title
        self.QRImageView.sd_setImage(with: URL(string:qrProfileMarina.qrImage)!)
        //let image = Barcode.fromString(string: "https://www.gpdock.com/scan/marina/10")
        //self.QRImageView.image = image
    }
    func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            self.saveQRCode()
        //handle authorized status
        case .denied, .restricted :
            CommonClass.sharedInstance.showGotoPhotoGallerySettingAlert(in: self){ isEnabling in
                if isEnabling{
                    self.saveQRCode()

                }
            }
            //self.saveQRCode()
        //handle denied status
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized:
                    self.saveQRCode()
                // as above
                case .denied, .restricted:
                    CommonClass.sharedInstance.showGotoPhotoGallerySettingAlert(in: self){ isEnabling in
                        if isEnabling{
                            self.saveQRCode()
                            
                        }
                    }
                    //self.saveQRCode()


                case .notDetermined:
                      return
                    
                }
            }
        }
    }
    
    func saveQRCode() {
        DispatchQueue.main.async {
        if (self.QRImageView.image != nil) {
            try? PHPhotoLibrary.shared().performChangesAndWait {
                PHAssetChangeRequest.creationRequestForAsset(from: self.QRImageView.image!)
            }
            showAlertWith(self, message: "Your picture has been saved into Photo Library", title: warningMessage.alertTitle.rawValue)
        }
        }
    }
    
    func shareMarinaQRCode(){
        if qrProfileMarina.qrImage != "" {
        let imageurl = self.qrProfileMarina.qrImage
            guard let imageURL = URL(string: imageurl)else{
                return
            }
            //if let image = Barcode.fromString(string: "https://www.gpdock.com/scan/marina/10") {

            if let image = SDImageCache.shared().imageFromMemoryCache(forKey: imageURL.absoluteString) {
                let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                self.present(activityVC, animated: true, completion: nil)
            }
        
    }
    }


}
extension DisplayMarinaQRCodeViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let ShareImageAction: UIAlertAction = UIAlertAction(title: "Share", style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
            self.shareMarinaQRCode()
        }
        actionSheet.addAction(ShareImageAction)
        
        let SaveImageAction: UIAlertAction = UIAlertAction(title: "Save to Gallery", style: .default)
        { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
         //self.saveQRCode()
            self.checkPhotoLibraryPermission()
        }
        actionSheet.addAction(SaveImageAction)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    
}

//class Barcode {
//
//    class func fromString(string : String) -> UIImage? {
//
//        let data = string.data(using: String.Encoding.ascii)
//
//        if let filter = CIFilter(name: "CIQRCodeGenerator") {
//            filter.setValue(data, forKey: "inputMessage")
//            let transform = CGAffineTransform(scaleX: 3, y: 3)
//
//            if let output = filter.outputImage?.transformed(by: transform) {
//                return UIImage(ciImage: output)
//            }
//        }
//
//        return nil
//    }
//
//}



