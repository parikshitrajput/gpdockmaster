//
//  ScanManuallyViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 22/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ScanManuallyViewController: UIViewController {
    
    var marina : Marina!
    var user: User!

    @IBOutlet weak var qrcodeTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    var toolSubmitButton:UIButton!
    var toolBar:UIToolbar!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json: User.loadUserInfo())
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        self.setupToolBarButton()
        self.qrcodeTextField.becomeFirstResponder()

        //qrcodeTextField.delegate = self
    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        self.qrcodeTextField.becomeFirstResponder()
//
//    }

    
    override func viewDidLayoutSubviews() {

        CommonClass.makeViewCircularWithCornerRadius(self.submitButton, borderColor: kApplicationGreenColor, borderWidth: 0, cornerRadius: 2)
        self.submitButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
        self.toolSubmitButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    private func setupToolBarButton() {
        toolSubmitButton = UIButton(type: .custom)
        toolSubmitButton.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        toolSubmitButton.backgroundColor = kNavigationColor
        toolSubmitButton.setTitleColor(.white, for: UIControlState())
        toolSubmitButton.setTitle("Submit", for: UIControlState())

        self.toolSubmitButton.addTarget(self, action: #selector(ScanManuallyViewController.onClickSubmit(_:)), for: .touchUpInside)
       // CommonClass.makeViewCircularWithCornerRadius(toolSubmitButton, borderColor: kApplicationGreenColor, borderWidth: 0, cornerRadius: 2)
        self.toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = false
        toolBar.isOpaque = true
        toolBar.tintColor = .white
        toolBar.barTintColor = .white
        toolBar.backgroundColor = .white
        toolBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        let doneButton = UIBarButtonItem(customView: self.toolSubmitButton)
        toolBar.setItems([doneButton], animated: true)
        toolBar.isUserInteractionEnabled = true
        self.qrcodeTextField.inputAccessoryView = toolBar
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(false)
    }

    
    @IBAction func onClickSubmit(_ sender: UIButton){
        self.view.endEditing(true)
        //self.qrcodeTextField.resignFirstResponder()

        //here goes manually completion of booking by marina owner
        if !CommonClass.isConnectedToNetwork{
            GPD_Dockmaster.showAlertWith(self, message:warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        if let qrCodeRawString = self.qrcodeTextField.text{
            let qrCodeStr = qrCodeRawString.trimmingCharacters(in: .whitespaces)
            self.scanQRCode(with: qrCodeStr)
        }
//        self.toolBar.isHidden = true
//        self.qrcodeTextField.resignFirstResponder()
    }

    private func scanQRCode(with qrCodeID: String) -> Void {
        if qrCodeID == ""{
            GPD_Dockmaster.showAlertWith(self, message: "Please enter the booking ID", title: warningMessage.alertTitle.rawValue)
            return
        }
        CommonClass.showLoader(withStatus: "Completing..")
        BookingService.sharedInstance.scanQRCodeWithQRCodeID(qrCodeString: qrCodeID,on: self.marina.ID) { (success,resBooking,message) in
            CommonClass.hideLoader()
            if success{
                if let booking = resBooking{
                    if booking.ID == ""{
                        GPD_Dockmaster.showAlertWith(self, message: "Please enter the booking ID", title: warningMessage.alertTitle.rawValue)
                    }else{
                        self.showAlertWith(message: "\r\nYour boat would be gone to \(booking.parkingSpace.title)\r\n", title: "Welcome \(booking.user.firstName) \(booking.user.lastName)")
                    }
                }else{
                    GPD_Dockmaster.showAlertWith(self, message: "Please enter a valid booking ID", title: warningMessage.alertTitle.rawValue)
                }
            }else{
                GPD_Dockmaster.showAlertWith(self, message: "Please enter a valid booking ID", title: warningMessage.alertTitle.rawValue)
            }

        }

    }


    private func showAlertWith(message:String,title:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) in
            self.navigationController?.popToRoot(true)
        }

        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
        alert.view.tintColor = UIColor.white
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
    

    
}
//extension ScanManuallyViewController: UITextFieldDelegate{
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.qrcodeTextField.resignFirstResponder()
//        return true
//
//    }
//
//}

