//
//  ScannerViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 22/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import AVFoundation


class ScannerViewController: UIViewController {
    var marina : Marina!
    var user: User!
    @IBOutlet weak var previewView: QRCodeReaderView!
    @IBOutlet weak var orLabel: UILabel!
    lazy var reader: QRCodeReader = QRCodeReader()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json: User.loadUserInfo())
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        self.previewView.addTopBorderWithColor(kNavigationColor, width: 2)//addBorder(kNavigationColor)
        self.previewView.addBottomBorderWithColor(kNavigationColor, width: 2)
        self.previewView.addLeftBorderWithColor(kNavigationColor, width: 2)
        self.previewView.addRightBorderWithColor(kNavigationColor, width: 2)
        self.startScanner()
    }
    override func viewDidLayoutSubviews() {
        CommonClass.makeViewCircular(self.orLabel, borderColor: .clear, borderWidth: 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.reader.startScanning()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func startScanner(){
        guard checkScanPermissions(), !reader.isRunning else { return }
        self.previewView.setupComponents(showCancelButton: false, showSwitchCameraButton: false, showTorchButton: false, showOverlayView: true, reader: reader)

        reader.startScanning()
        reader.didFindCode = { result in
            if let qrCodeID = result.value as String?{
                self.scanQRCode(with: qrCodeID)
            }
        }
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
         self.reader.stopScanning()
        self.navigationController?.pop(true)
        
    }

    @IBAction func onClickQRCodeButton(_ sender: UIButton){
        self.reader.stopScanning()
        let manualScannerVC = AppStoryboard.Scanner.viewController(ScanManuallyViewController.self)
        self.navigationController?.pushViewController(manualScannerVC, animated: false)
    }

    private func scanQRCode(with qrCodeID: String) -> Void {
        if qrCodeID == ""{
            GPD_Dockmaster.showAlertWith(self, message: "Please enter the booking ID", title: warningMessage.alertTitle.rawValue)
            return
        }
        CommonClass.showLoader(withStatus: "Completing..")
        BookingService.sharedInstance.scanQRCodeWithQRCodeID(qrCodeString: qrCodeID,on: self.marina.ID) { (success,resBooking,message) in
            CommonClass.hideLoader()
            if success{
                if let booking = resBooking{
                    if booking.ID == ""{
                        GPD_Dockmaster.showAlertWith(self, message: "Please enter the booking ID", title: warningMessage.alertTitle.rawValue)
                        self.reader.startScanning()
                    }else{
                        self.showAlertWith(message: "\r\nYour boat would be gone to \(booking.parkingSpace.title)\r\n", title: "Welcome \(booking.user.firstName) \(booking.user.lastName)")
                    }
                }else{
                    GPD_Dockmaster.showAlertWith(self, message: "Please enter a valid booking ID", title: warningMessage.alertTitle.rawValue)
                    self.reader.startScanning()
                }
            }else{
                   GPD_Dockmaster.showAlertWith(self, message: "Please enter a valid booking ID", title: warningMessage.alertTitle.rawValue)
                    self.reader.startScanning()
            }

        }
    }


    
    private func showAlertWith(message:String,title:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) in
            self.reader.stopScanning()
            self.navigationController?.popToRoot(true)
         
        }

        let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
        subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
        alert.view.tintColor = UIColor.white
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Actions

    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController?

            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)

                alert?.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))

                alert?.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            case -11814:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert?.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            default:
                alert = nil
            }

            guard let vc = alert else { return false }

            present(vc, animated: true, completion: nil)

            return false
        }
    }
}


