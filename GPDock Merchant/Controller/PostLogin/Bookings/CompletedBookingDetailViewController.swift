//
//  CompletedBookingDetailViewController.swift
//  GPDock
//
//  Created by TecOrb on 14/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
 import MarqueeLabel

protocol CompletedBookingDetailViewControllerDelegate {
    func bookingRatingDidUpdatedByMarinaOwner(viewController: CompletedBookingDetailViewController, booking:Booking)
}

class CompletedBookingDetailViewController: UIViewController {
    var delegate : CompletedBookingDetailViewControllerDelegate?
    var booking :Booking!
    @IBOutlet weak var SlipTableView: UITableView!
    var marina : Marina!
    var marinaRating:Float = 0.0
    var RatingCellStatus = ""
 

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.navigationItem.title = "Booking Id: "+self.booking.ID
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        self.SlipTableView.dataSource = self
        self.SlipTableView.delegate = self
            SlipTableView.reloadData()
        

    }

    

    func registerCells() {
        self.SlipTableView.register(UINib(nibName: "BoatRepresentingCell", bundle: nil), forCellReuseIdentifier: "BoatRepresentingCell")
        self.SlipTableView.register(UINib(nibName: "CheckInCheckOutCell", bundle: nil), forCellReuseIdentifier: "CheckInCheckOutCell")
        self.SlipTableView.register(UINib(nibName: "OtherCell", bundle: nil), forCellReuseIdentifier: "OtherCell")
        self.SlipTableView.register(UINib(nibName: "SlipSizeCell", bundle: nil), forCellReuseIdentifier: "SlipSizeCell")
        self.SlipTableView.register(UINib(nibName: "SlipStatusCell", bundle: nil), forCellReuseIdentifier: "SlipStatusCell")
        self.SlipTableView.register(UINib(nibName: "NoDataCell", bundle: nil), forCellReuseIdentifier: "NoDataCell")
        self.SlipTableView.register(UINib(nibName: "SlipIDTableViewCell", bundle: nil), forCellReuseIdentifier: "SlipIDTableViewCell")
                self.SlipTableView.register(UINib(nibName: "MarqueOtherCellCell", bundle: nil), forCellReuseIdentifier: "MarqueOtherCellCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(_ animated: Bool) {
        
    }
    //MARK:- Actions
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
    NotificationCenter.default.post(name: .BOOKING_DID_RATING_UPDATED_NOTIFICATION, object: nil, userInfo: ["booking":self.booking])
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSupport(_ sender: UIButton){
        let  supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
        self.navigationController?.pushViewController(supportVC, animated: true)    }



    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM-dd-YYYY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }
    
    func marinaRatingToBooking(marinaID:String,bookingID:String,rating:Float) {
        CommonClass.showLoader(withStatus: "Sending..")
        MarinaService.sharedInstance.rateMarinaToBooking(marinaID:marinaID,bookingID:bookingID,rating:rating){(rating) in
            CommonClass.hideLoader()
            if let someData = rating as? Booking {
                self.booking.ratingOfMarinaOwer = someData.ratingOfMarinaOwer
                self.delegate?.bookingRatingDidUpdatedByMarinaOwner(viewController: self, booking: self.booking)
            }
            self.SlipTableView.reloadData()
        }
    }
}

extension CompletedBookingDetailViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (booking.ID.trimmingCharacters(in: .whitespacesAndNewlines) == ""){
            return 2
        }else if booking.amount == 0.0{
            if self.RatingCellStatus == "show" {
             return 8
            }
            else{
                return 7
            }
        }else if booking.amount != 0.0{
            if self.RatingCellStatus == "show" {
                return 9
            }
            else{
                return 8
            }
        }
        else{
            return (booking.ID.trimmingCharacters(in: .whitespacesAndNewlines) == "") ? 3 : 9
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipIDTableViewCell", for: indexPath) as! SlipIDTableViewCell
            cell.slipID.text = "Slip : "+self.booking.parkingSpace.title
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipSizeCell", for: indexPath) as! SlipSizeCell
            cell.lengthLabel.text = "\(self.booking.parkingSpace.boatSize.length)" + " ft"
            cell.widthLabel.text = "\(self.booking.parkingSpace.boatSize.width)" + " ft"
            cell.depthLabel.text = "\(self.booking.parkingSpace.boatSize.depth)" + " ft"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipStatusCell", for: indexPath) as! SlipStatusCell
            cell.statusLabel.text = "Completed: "
            //cell.bgView.backgroundColor = UIColor(red: 246.0/255.0, green: 171.0/255.0, blue: 81.0/255.0, alpha: 1.0)
            cell.datesLabel.textColor = UIColor.darkGray
            cell.calenderButton.isHidden = true
            let onDate = self.getDateFromString(self.booking.completedAt)
            cell.datesLabel.text = "\(onDate)"
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckInCheckOutCell", for: indexPath) as! CheckInCheckOutCell
            cell.checkInDate.text = self.getDateFromString(self.booking.fromDate)
            cell.checkOutDate.text = self.getDateFromString(self.booking.toDate)
            cell.checkInTime.text = self.marina.checkIn
            cell.checkOutTime.text = self.marina.checkOut
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BoatRepresentingCell", for: indexPath) as! BoatRepresentingCell
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.boatNameLabel.text = booking.boat.name
            cell.UserNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.lengthLabel.text = "\(booking.boat.boatSize.length)"
            cell.widthLabel.text = "\(booking.boat.boatSize.width)"
            cell.depthLabel.text = "\(booking.boat.boatSize.depth)"
            if booking.bookingType == "offline" {
                cell.bookingType.text = "Offline"
            }
            else{
                cell.bookingType.text = "Online"
            }
            return cell
        case 5:
            return (booking.amount == 0.0) ? self.ratingCell(tableView,indexPath: indexPath) : self.amountPaidCell(tableView,indexPath: indexPath)
        case 6:
            return (booking.amount == 0.0) ? self.reviewCell(tableView,indexPath: indexPath) : self.ratingCell(tableView,indexPath: indexPath)
       case 7:
        if booking.amount == 0.0 {
            if self.RatingCellStatus == "show" {
          return  self.rateToBookingCell(tableView,indexPath: indexPath)
            }
            else{
            return  self.reviewCell(tableView,indexPath: indexPath)
            }
        }
        else{
        return  self.reviewCell(tableView,indexPath: indexPath)
            
        }
        
//            return (booking.amount == 0.0) ?   self.rateToBookingCell(tableView,indexPath: indexPath): self.reviewCell(tableView,indexPath: indexPath)
        case 8:
           // return self.rateToBookingCell(tableView,indexPath: indexPath)
            if booking.amount == 0.0 {
                 return self.emptyReviewCell(tableView,indexPath: indexPath)
            }
            else {
                if self.RatingCellStatus == "show" {
                    return  self.rateToBookingCell(tableView,indexPath: indexPath)
                }
                else{

                    return self.emptyReviewCell(tableView,indexPath: indexPath)
                }
                
            }
            
           // return (booking.amount == 0.0) ? self.rateToBookingCell(tableView,indexPath: indexPath) :
       // self.emptyReviewCell(tableView,indexPath: indexPath)
        default:
            return UITableViewCell()
        }
    }

    func amountPaidCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
        cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
        cell.otherTitleLabel.text = "Amount Paid"
        cell.otherDetailsLabel.text = "$ "+String(format: "%0.2lf", booking.businessAmount)
        cell.separatorHeight.constant = 1
        cell.needsUpdateConstraints()
        cell.updateConstraints()
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func ratingCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
      cell.otherIcon.image = #imageLiteral(resourceName: "rating")
        cell.otherTitleLabel.text = "Rating"
        cell.otherDetailsLabel.text =  (self.booking.review.rating > 0) ? String(format: "%0.1f", self.booking.review.rating) : "Not Rated"
        cell.separatorHeight.constant = 1
        cell.needsUpdateConstraints()
        cell.updateConstraints()
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func reviewCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarqueOtherCellCell", for: indexPath) as! MarqueOtherCellCell
        cell.otherIcon.image = #imageLiteral(resourceName: "review")
        cell.otherTitleLabel.text = "Review"
        cell.otherDescriptionLabel.text = (self.booking.review.reviewDescription != "") ? "\(self.booking.review.reviewDescription)" : "Not Review"
         cell.otherDescriptionLabel.tag = 201
        //cell.otherDescriptionLabel.type = .continuous
        cell.otherDescriptionLabel.textAlignment = .right
         cell.otherDescriptionLabel.lineBreakMode = .byTruncatingTail
        cell.otherDescriptionLabel.speed = .duration(10.0)
        cell.otherDescriptionLabel.fadeLength = 15.0
        cell.otherDescriptionLabel.trailingBuffer = 40
        cell.separatorHeight.constant = 1
        cell.needsUpdateConstraints()
        cell.updateConstraints()
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }
    func emptyReviewCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = ""
            return cell
    }
    
    func rateToBookingCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "RatingToBookingCell", for: indexPath) as! RatingToBookingCell

        if self.booking.ratingOfMarinaOwer.rating == 0.0 {
            cell.ratingView.isUserInteractionEnabled = true
            cell.ratingView.delegate = self
        }else {
            cell.ratingView.isUserInteractionEnabled = false
            cell.ratingView.rating = self.booking.ratingOfMarinaOwer.rating
       }
        
//            cell.ratingView.rating = self.booking.ratingOfMarinaOwer.rating
//            cell.ratingView.isUserInteractionEnabled = true //(cell.ratingView.rating <= 0.0)
//            cell.ratingView.rating = self.booking.ratingOfMarinaOwer.rating
//            cell.ratingView.delegate = self
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 64
        switch indexPath.row {
        case 0://slipID
            height = 20
        case 1://slipsize
            height = 64
        case 2: //status
            height = 50
        case 3: //checkin checkout
            height = 70
        case 4: //Boat representingcell
            height = 95
        case 5: //amount paid
            height = (booking.amount == 0.0) ? 64 :72
        case 6: //amount paid
            height = (booking.amount == 0.0) ? 72 : 64
        case 7: //amount paid
            //height = 72
           height = 72
        case 8: //amount paid
//            if self.RatingCellStatus == "show" {
//            height = 72
//            }
//            else {
//                height = 0
//            }
            height = 72
            
        default:
            break
        }
        return height
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 5{
//            //show price breakups details
//            //self.showPriceBreakUpsAlert()
//        }
        
//        switch indexPath.row {
//        case 6:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingToBookingCell", for: indexPath) as! RatingToBookingCell
//            cell.ratingView.contentMode = UIViewContentMode.scaleAspectFit
//            self.marinaRatingToBooking(marinaID:self.marina.ID,bookingID: self.booking.ID,rating:self.ratingStore)
//        default:
//            break
//        }
    }
}
extension CompletedBookingDetailViewController: NKFloatRatingViewDelegate{
    func floatRatingView(_ ratingView: NKFloatRatingView, didUpdate rating: Float) {
        self.marinaRating = rating
        self.marinaRatingToBooking(marinaID:self.marina.ID,bookingID: self.booking.ID,rating:marinaRating)

    }
    
}
class RatingToBookingCell: UITableViewCell {
    @IBOutlet weak var ratingView: NKFloatRatingView!
}

    
    


