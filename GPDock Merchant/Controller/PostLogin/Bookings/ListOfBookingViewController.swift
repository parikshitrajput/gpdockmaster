//
//  ListOfBookingViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 11/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ListOfBookingViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var bookingsTableView : UITableView!

    var bookingDates : BookingDates!
     var bookings = Array<Booking>()
    var user : User!
    var marina : Marina!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var selectedIndex = -1
    var bookingStatus = ""
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(ListOfBookingViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        NotificationCenter.default.addObserver(self, selector: #selector(ListOfBookingViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_USER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ListOfBookingViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_CANCELLED_BY_USER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ListOfBookingViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_CANCELLED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ListOfBookingViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        self.bookingsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.bookingsTableView.dataSource = self
        self.bookingsTableView.delegate = self
        self.bookingsTableView.register(UINib(nibName: "BookingForMarinaCell", bundle: nil), forCellReuseIdentifier: "BookingForMarinaCell")
          self.bookingsTableView.addSubview(self.refreshControl)
        if ((self.bookingStatus == "arrivial") || (self.bookingStatus == "departure") || (self.bookingStatus == "occupied")) {
        self.loadListForBookingFor(self.marina.ID, userID: self.user.ID, dateFrom: self.bookingDates.fromDate, status: self.bookingStatus, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
            self.navigationItem.title = "Today's " + (self.bookingStatus).firstUppercased  + "s"

        }else{
        self.loadListForPartiallyAndBooked(self.marina.ID, userID: self.user.ID, dateFrom: self.bookingDates.fromDate, dateTo: self.bookingDates.toDate, status: self.bookingStatus, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        self.navigationItem.title = (self.bookingStatus).firstUppercased  + " Booking"

        }
    }
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
    self.navigationController?.pop(true)
    }
    func loadListForBookingFor(_ marinaID:String,userID:String,dateFrom: Date,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            bookingsTableView.reloadData()
        }
        BookingService.sharedInstance.getListForBookings(marinaID, userID: userID, dateFrom: dateFrom, status: status, page: pageNumber, perPage: recordPerPage){ (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.bookings.append(contentsOf: newJobArray)
                self.bookingsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
        }
    }
    func loadListForPartiallyAndBooked(_ marinaID:String,userID:String,dateFrom: Date,dateTo:Date,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            bookingsTableView.reloadData()
        }
        BookingService.sharedInstance.getPartiallAndBookedListForBookings(marinaID, userID: userID, dateFrom: dateFrom, dateTo: dateTo, status: status, page: pageNumber, perPage: recordPerPage){ (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.bookings.append(contentsOf: newJobArray)
                self.bookingsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
        }
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.isNewDataLoading = true
        self.bookingsTableView.reloadData()
        if ((self.bookingStatus == "arrivial") || (self.bookingStatus == "departure") || (self.bookingStatus == "occupied")) {
        BookingService.sharedInstance.getListForBookings(self.marina.ID, userID: self.user.ID, dateFrom: self.bookingDates.fromDate, status: self.bookingStatus, page: self.pageNumber, perPage: self.recordsPerPage){ (response) in
            self.bookings.removeAll()
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newBookingArray = response{
                self.bookings.append(contentsOf: newBookingArray)
                self.bookingsTableView.reloadData()
            }
        }
        }else{
            BookingService.sharedInstance.getPartiallAndBookedListForBookings(self.marina.ID, userID: self.user.ID, dateFrom: self.bookingDates.fromDate, dateTo: self.bookingDates.toDate, status: self.bookingStatus, page: self.pageNumber, perPage: self.recordsPerPage){(response) in
                self.bookings.removeAll()
                refreshControl.endRefreshing()
                self.isNewDataLoading = false
                if let newBookingArray = response{
                    self.bookings.append(contentsOf: newBookingArray)
                    self.bookingsTableView.reloadData()
                }
            }
        }
    }
    @objc func bookingDidCancelledByUser(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            if let removingBooking = userInfo["booking"]{
                for i in 0..<self.bookings.count{
                    if self.bookings[i].ID == removingBooking.ID{
                        self.bookings.remove(at: i)
                        self.bookingsTableView.reloadData()
                        break;
                    }
                }
            }
            
        }
    }
//    func getArrivalDepartueAndOccupiedRefreshHandler(_ refreshControl: UIRefreshControl){
//    }
//    func getBookedAndPartiallRefreshHandler() {
//
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (bookings.count > 0) ? bookings.count : 1 ;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return (bookings.count > 0) ? 82 : 250
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if bookings.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Booking found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingForMarinaCell", for: indexPath) as! BookingForMarinaCell
            let booking = bookings[indexPath.row] //BookingCell
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.userNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.boatNameLabel.text = (booking.boat.name == "") ? "Unnamed" : booking.boat.name.capitalized
            // let dateFormatter = DateFormatter()
            cell.bookingDate.text = CommonClass.convertToShowFormatDate(dateString: booking.fromDate)+" To "+CommonClass.convertToShowFormatDate(dateString: booking.toDate)
            //cell.bookingDate.text = booking.fromDate+" To "+booking.toDate
            cell.slipPosition.text = booking.parkingSpace.title

            if let fromDate = self.getDateFromString(booking.fromDate){
                if let toDate = self.getDateFromString(booking.toDate){
                    let days = toDate.days(from: fromDate)
                    cell.totalNightLabel.text = (days > 1) ? "\(days)" : "\(days)"
                }else{
                    cell.totalNightLabel.text = "--"
                }
            }else{
                cell.totalNightLabel.text = "--"
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bookings.count == 0{
            return
        }
        let booking = self.bookings[indexPath.row]
        if (self.bookingStatus == "arrivial") {
        self.navigateToArrivingBookingDetails(booking: booking)
        }else if(self.bookingStatus == "departure") {
            self.navigateToDepartureBookingDetails(booking: booking)
        }else if(self.bookingStatus == "occupied") {
         self.navigateToOccupiedBookingDetails(booking: booking)
        }else if (self.bookingStatus == "booked") {
           self.navigateToFullyBookedBookingDetails(booking: booking)
        }else{
            self.navigateToPartiallyBookedBookingDetails(booking: booking)
        }
    }
    
    func navigateToArrivingBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(ArrivingViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.slip = booking.parkingSpace
        bookingDetailVC.arrivingStatus = "arrival"
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    func navigateToDepartureBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(DepartingViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.slip = booking.parkingSpace
        bookingDetailVC.departingStatus = "departure"
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    func navigateToOccupiedBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(OccupiedViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.slip = booking.parkingSpace
        bookingDetailVC.occupiedStatus = "occupied"
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    func navigateToFullyBookedBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(FullyBookedViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.slip = booking.parkingSpace
        bookingDetailVC.bookedStatus = "booked"
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    func navigateToPartiallyBookedBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(PartiallyBookedViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.slip = booking.parkingSpace
        bookingDetailVC.partiallyStatus = "partially"
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    
    
    func getDateFromString(_ dateString: String) -> Date?
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            return date
        }else{
            return nil
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == bookingsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        pageNumber+=1
                        if ((self.bookingStatus == "arrivial") || (self.bookingStatus == "departure") || (self.bookingStatus == "occupied")) {
                   self.loadListForBookingFor(self.marina.ID, userID: self.user.ID, dateFrom: self.bookingDates.fromDate, status: self.bookingStatus, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                        }else{
                            self.loadListForPartiallyAndBooked(self.marina.ID, userID: self.user.ID, dateFrom: self.bookingDates.fromDate, dateTo: self.bookingDates.toDate, status: self.bookingStatus, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                        }
                    }
                }
            }
        }
        
    }


}
