//
//  TicketViewController.swift
//  GPDock
//
//  Created by TecOrb on 03/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class TicketViewController: UITableViewController {
    var booking : Booking!
    @IBOutlet weak var marinaImageView : UIImageView!
    @IBOutlet weak var bookingIDLabel : UILabel!
    @IBOutlet weak var marinaNameLabel : UILabel!
    @IBOutlet weak var dateFromLabel : UILabel!
    @IBOutlet weak var dateToLabel : UILabel!
    @IBOutlet weak var parkingSpaceLabel : UILabel!
    @IBOutlet weak var numberOfParkingSpaceLabel : UILabel!
    @IBOutlet weak var qrContainnerView : UIView!
    @IBOutlet weak var qrCodeImageView : UIImageView!
    @IBOutlet weak var qrCodeLabel : UILabel!
    var isBrowsing = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setBookingData()
        // Do any additional setup after loading the view.
    }

    func setBookingData() -> Void {
        self.bookingIDLabel.text = booking.ID

        self.marinaImageView.setShowActivityIndicator(true)
        self.marinaImageView.setIndicatorStyle(.gray)
        self.marinaImageView.sd_setImage(with: URL(string:booking.marina.image.url))

        self.marinaNameLabel.text = self.booking.marina.title
        self.dateFromLabel.text = booking.fromDate //CommonClass.formattedDateWithString(booking.fromDate, format: "MM/dd/YYYY")
        self.dateToLabel.text = booking.toDate//CommonClass.formattedDateWithString(booking.toDate, format: "MM/dd/YYYY")
        self.parkingSpaceLabel.text = "\(booking.parkingSpace.facing.rawValue)-\(booking.parkingSpace.position)"
        self.numberOfParkingSpaceLabel.text = "1"
        self.qrCodeLabel.text = booking.QRCode

        self.qrCodeImageView.setShowActivityIndicator(true)
        self.qrCodeImageView.setIndicatorStyle(.gray)
        self.qrCodeImageView.sd_setImage(with: URL(string:booking.QRImage))

    }

    func getBookingDataFromServer(withBookingID bookingID: String) -> Void {
        CommonClass.showLoader(withStatus: "Loading..")
        PaymentService.sharedInstance.getBookingDetails(self.booking.ID) { (resBooking) in
            if let aBooking = resBooking{
                self.booking = aBooking
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        if self.isBrowsing{
            self.navigationController?.pop(false)
        }else{
        self.navigationController?.popToRoot(true)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
