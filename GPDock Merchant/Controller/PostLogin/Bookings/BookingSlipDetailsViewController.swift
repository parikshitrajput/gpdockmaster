//
//  BookingSlipDetailsViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 06/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BookingSlipDetailsViewController: UIViewController {
@IBOutlet weak var SlipSizeView: UIView!
    @IBOutlet weak var lengthLabel : UILabel!
    @IBOutlet weak var widthLabel : UILabel!
    @IBOutlet weak var depthLabel : UILabel!
    @IBOutlet weak var slipLabel : UILabel!
    var parkingSpace = ParkingSpace()
    var booking = Booking()
    var storeBookingID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(TodaySlipDetailsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_CANCELLED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TodaySlipDetailsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        slipLabel.text = "\(parkingSpace.title)"
        lengthLabel.text = "\(parkingSpace.boatSize.length)" + " ft"
        widthLabel.text = "\(parkingSpace.boatSize.width)" + " ft"
        depthLabel.text = "\(parkingSpace.boatSize.depth)" + " ft"
        let todayBookingVC = AppStoryboard.Booking.viewController(TodaySlipDetailsViewController.self)
        todayBookingVC.booking = self.booking
        todayBookingVC.parkingSpace = self.parkingSpace
        todayBookingVC.title = "Today"
        
        let comingUpBookingVC = AppStoryboard.Booking.viewController(UpComingSlipDetailsViewController.self)
        comingUpBookingVC.booking = self.booking
        comingUpBookingVC.parkingSpace = self.parkingSpace
        comingUpBookingVC.title = "Up Coming"
        
        let completedBookingVC = AppStoryboard.Booking.viewController(CompletedSlipDetailsViewController.self)
        completedBookingVC.booking = self.booking
        completedBookingVC.parkingSpace = self.parkingSpace
        completedBookingVC.title = "Completed"
    
        let cancelledBookingVC = AppStoryboard.Booking.viewController(CancelledSlipDetailsViewController.self)
        cancelledBookingVC.booking = self.booking
        cancelledBookingVC.parkingSpace = self.parkingSpace
        cancelledBookingVC.title = "Cancelled"
        
        let menuItemColor = UIColor.white
        let menuItemSelectedColor = UIColor.white
        let menuBGColor = UIColor.clear //kNavigationColor
        let statusHeight:CGFloat = UIApplication.shared.statusBarFrame.size.height
        let navigationHeight:CGFloat = self.navigationController?.navigationBar.frame.size.height ?? 44
        let containnerVC = YSLContainerViewController(controllers: [todayBookingVC,comingUpBookingVC,completedBookingVC,cancelledBookingVC], topBarHeight: 80,subtractableHeight:(statusHeight + navigationHeight), parentViewController: self)
        
        //Customize the containner menu
        containnerVC?.menuBackGroudColor = menuBGColor
        containnerVC?.menuItemSelectedTitleColor = menuItemSelectedColor
        containnerVC?.menuIndicatorColor = menuItemSelectedColor
        containnerVC?.menuItemTitleColor = menuItemColor
        containnerVC?.menuItemFont = UIFont(name: "Raleway-SemiBold", size: 18)

        containnerVC?.delegate = self
        self.view.addSubview((containnerVC?.view)!)
        containnerVC?.view.setNeedsLayout()
        containnerVC?.view.layoutIfNeeded()
    }

  @objc  func bookingDidCancelledByUser(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            if let removingBooking = userInfo["booking"]{
            self.storeBookingID = removingBooking.ID
            }
            
        }
    }
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem) {
        if self.booking.ID == self.storeBookingID {
            //print("Booking:-> \(self.booking.ID)")
//           self.navigationController?.popToRootViewController(animated: true)
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
       }
        else {
            self.navigationController?.pop(true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension BookingSlipDetailsViewController: YSLContainerViewControllerDelegate{
    func containerViewItemIndex(_ index: Int, currentController controller: UIViewController!) {
        
    }
    
}
