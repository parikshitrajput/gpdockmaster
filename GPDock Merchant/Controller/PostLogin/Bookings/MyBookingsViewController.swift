//
//  MyBookingsViewController.swift
//  GPDock
//
//  Created by TecOrb on 04/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
//cash current
//card completed

import UIKit

class MyBookingsViewController: UIViewController{
    @IBOutlet weak var backMenu: UIView!
    var bookings = Array<Booking>()
    var bookingCount = CountBooking()
    var marina : Marina!
    var user : User!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var selectedIndex = -1
    var containnerVC : YSLContainerViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
//        for vc in self.childViewControllers{
//            NotificationCenter.default.removeObserver(vc)
//        }
        

        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        let todayBookingVC = AppStoryboard.Booking.viewController(TodayBookingsViewController.self)
        todayBookingVC.title = "Today"

        let comingUpBookingVC = AppStoryboard.Booking.viewController(CurrentBookingsViewController.self)
        comingUpBookingVC.title = "Up Coming"
        
        
        let completedBookingVC = AppStoryboard.Booking.viewController(CompletedBookingsViewController.self)
        completedBookingVC.title = "Completed"
        
        let cancelledBookingVC = AppStoryboard.Booking.viewController(CancelledBookingsViewController.self)
        cancelledBookingVC.title = "Cancelled"
        
        /*Notification center observer for all childViewControllers*/
        //1- today
       // NotificationCenter.default.addObserver(todayBookingVC, selector:#selector(TodayBookingsViewController.bookingDidRefresh(_:)) , name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil)
        //2- comingup
        //NotificationCenter.default.addObserver(comingUpBookingVC, selector:#selector(CurrentBookingsViewController.bookingDidRefresh(_:)) , name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil)
        //3-completed
       // NotificationCenter.default.addObserver(completedBookingVC, selector:#selector(CompletedBookingsViewController.bookingDidRefresh(_:)) , name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil)
        //4- cancelled
        //NotificationCenter.default.addObserver(cancelledBookingVC, selector:#selector(CancelledBookingsViewController.bookingDidRefresh(_:)) , name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil)
        
        /*End of notification center observer all childViewControllers*/
        let menuItemColor = UIColor.white
        let menuItemSelectedColor = UIColor.white
        let menuBGColor = UIColor.clear //kNavigationColor
        
        let statusHeight:CGFloat = UIApplication.shared.statusBarFrame.size.height
        let navigationHeight:CGFloat = self.navigationController?.navigationBar.frame.size.height ?? 44
        containnerVC = YSLContainerViewController(controllers: [todayBookingVC,comingUpBookingVC,completedBookingVC,cancelledBookingVC], topBarHeight: 0,subtractableHeight:(statusHeight + navigationHeight), parentViewController: self)
        containnerVC?.menuItemFont = UIFont(name: fonts.Raleway.semiBold.rawValue, size: fontSize.large.rawValue)
        containnerVC?.menuBackGroudColor = menuBGColor
        containnerVC?.menuItemSelectedTitleColor = menuItemSelectedColor
        containnerVC?.menuIndicatorColor = menuItemSelectedColor
        containnerVC?.menuItemTitleColor = menuItemColor
        containnerVC.menuItemFont = UIFont(name: "Raleway-SemiBold", size: 18)
        //containnerVC?.menuItemFont = UIFont(name: fonts.OpenSans.semiBold.rawValue, size: fontSize.medium.rawValue)
        containnerVC?.delegate = self
        self.view.addSubview((containnerVC?.view)!)
        containnerVC?.view.setNeedsLayout()
        containnerVC?.view.layoutIfNeeded()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.perform(#selector(updatebadge), with: nil, afterDelay: 1)

    }
    override func viewDidAppear(_ animated: Bool) {
        //self.perform(#selector(updatebadge), with: nil, afterDelay: 1)
       // NotificationCenter.default.post(name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil, userInfo:nil)

    }
//    deinit{
//        for vc in self.childViewControllers{
//            NotificationCenter.default.removeObserver(vc)
//        }
//        NotificationCenter.default.removeObserver(self)
//    }

    @objc func updatebadge() {
        self.loadBookingCountFor(self.marina.ID)
    }
    
    func loadBookingCountFor(_ marinaID:String) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
        }
        BookingService.sharedInstance.getCountForBooking(with: marinaID){(resCount) in
            if let someCount = resCount {
                self.bookingCount = someCount
                self.containnerVC?.todayBadgeValue = (self.bookingCount.todayBookingCount)
                self.containnerVC.upcomingBadgeValue = (self.bookingCount.upComingBookingCount)
                self.containnerVC?.view.setNeedsLayout()
                self.containnerVC?.view.layoutIfNeeded()
                self.containnerVC.refreshMenu()
    }
    }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickMenuButton(_ sender: UIButton){
        //SlideNavigationController.sharedInstance().toggleLeftMenu()
    }
    
}

extension MyBookingsViewController: YSLContainerViewControllerDelegate{
    func containerViewItemIndex(_ index: Int, currentController controller: UIViewController!) {
 

    }
    
}



