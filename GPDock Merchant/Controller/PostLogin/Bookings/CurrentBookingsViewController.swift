//
//  CurrentBookingsViewController.swift
//  GPDock
//
//  Created by TecOrb on 04/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CurrentBookingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    var bookings = Array<Booking>()
    @IBOutlet weak var bookingsTableView : UITableView!
    @IBOutlet weak var upcomingMarinaSearch : UISearchBar!
    @IBOutlet weak var searchBarButton : UIButton!
    @IBOutlet weak var selectedDateLabel : UILabel!
    @IBOutlet weak var selectedDateButton : UIButton!

    var user : User!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var dateSearchActive: Bool = false
    var rangeDateActive: Bool = false
    var selectedIndex = -1
    var marina : Marina!
    var selectedDates: BookingDates!
    var fromDateStr = ""
    var toDateStr = ""
    //var nextStatus = "upcoming"

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CurrentBookingsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
//        searchBarButton.layer.borderWidth = 0.5
//        searchBarButton.layer.borderColor = UIColor.lightGray.cgColor
//        searchBarButton.layer.cornerRadius = 5
         selectedDateLabel.backgroundColor = kApplicationLightGrayColor
//        selectedDateButton.layer.borderWidth = 0.5
//        selectedDateButton.layer.borderColor = UIColor.black.cgColor
//        selectedDateButton.layer.cornerRadius = 5
        
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        NotificationCenter.default.addObserver(self, selector: #selector(CurrentBookingsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_CANCELLED_BY_USER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CurrentBookingsViewController .bookingDidCancelledByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_USER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CurrentBookingsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_CANCELLED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CurrentBookingsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(CurrentBookingsViewController.bookingDidRefresh(_:)) , name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil)
        self.bookingsTableView.register(UINib(nibName: "BookingForMarinaCell", bundle: nil), forCellReuseIdentifier: "BookingForMarinaCell")
        self.bookingsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.bookingsTableView.dataSource = self
        self.bookingsTableView.delegate = self
        self.bookings.removeAll()
        self.bookingsTableView.addSubview(self.refreshControl)
        self.loadBookingFor(self.marina.ID, status: "upcoming", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        bookingsTableView.reloadData()
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }


    @objc func bookingDidRefresh(_ notification: Notification) {
        self.bookings.removeAll()
        self.loadBookingFor(self.marina.ID, status: "upcoming", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        bookingsTableView.reloadData()
        
    }
    @IBAction func onClickSearchBarButton(_ sender: UIButton) {
        
        self.searchBarButton.titleLabel?.isHidden = false
        self.searchBarButton.backgroundColor = kApplicationLightGrayColor
        self.selectedDateLabel.isHidden = true
        let searchBarVC = AppStoryboard.Booking.viewController(SearchBookingsViewController.self)
        searchBarVC.bookings = self.bookings
        searchBarVC.reserveArray = self.bookings
        searchBarVC.currentStatus = "upcoming"
        searchBarVC.marina = self.marina
        self.navigationController?.pushViewController(searchBarVC, animated: true)
    }
    @IBAction func onClickSelectDateButton(_ sender: UIButton) {
        self.rangeDateActive = true
            let calendarVC = AppStoryboard.Home.viewController(NKCalenderViewController.self)
            calendarVC.delegate = self
         calendarVC.selectedBookingDate = self.selectedDates
            calendarVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            let nav = UINavigationController(rootViewController: calendarVC)
            nav.navigationBar.barTintColor = kNavigationColor
            nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            nav.navigationBar.isHidden = true
            self.navigationController?.present(nav, animated: true, completion: nil)
    }


    func setupDates(selectedDate:BookingDates){
        self.fromDateStr = CommonClass.formattedDateWith(selectedDate.fromDate, format: "dd MMM")
        self.toDateStr = CommonClass.formattedDateWith(selectedDate.toDate, format: "dd MMM")
        self.searchBarButton.titleLabel?.isHidden = true
        self.searchBarButton.backgroundColor = UIColor.clear
//        //self.searchBarButton.backgroundColor = UIColor.clear
        self.selectedDateLabel.isHidden = false
        self.selectedDateLabel.text = ( selectedDate.totalDays > 0) ? "\(fromDateStr) to \(toDateStr)" : fromDateStr
    
    }

    func loadBookingFor(_ marinaID:String,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
        }
        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            bookingsTableView.reloadData()
        }

        BookingService.sharedInstance.getBookingsForMarina(marinaID, status: status,page:pageNumber,perPage: recordPerPage) { (response) in

            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.bookings.append(contentsOf: newJobArray)
                self.bookingsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
        }
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.isNewDataLoading = true
        self.rangeDateActive = false
        self.selectedDateLabel.text = ""
        self.searchBarButton.titleLabel?.isHidden = false
        self.bookingsTableView.reloadData()
        BookingService.sharedInstance.getBookingsForMarina(self.marina.ID, status: "upcoming",page: self.pageNumber,perPage: self.recordsPerPage) { (response) in
            self.bookings.removeAll()
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newBookingArray = response{
                self.bookings.append(contentsOf: newBookingArray)
                self.bookingsTableView.reloadData()
            }
        }
    }


    @objc func bookingDidCancelledByUser(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            if let removingBooking = userInfo["booking"]{
                for i in 0..<self.bookings.count{
                    if self.bookings[i].ID == removingBooking.ID{
                        self.bookings.remove(at: i)
                        self.bookingsTableView.reloadData()
                        break;
                    }
                }
            }
            
        }
    }
    func loadDateWiseBookingList(_ marinaID:String,userID:String,dateFrom: Date,dateTo:Date,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            bookingsTableView.reloadData()
        }
        BookingService.sharedInstance.getDateWiseBookingListBookings(marinaID, userID: userID, dateFrom: dateFrom, dateTo: dateTo, status: status, page: pageNumber, perPage: recordPerPage){ (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.bookings.append(contentsOf: newJobArray)
                self.bookingsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
        }
    }
    
    func getDateStringFromDate(_ date: Date) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        return dayTimePeriodFormatter.string(from:date)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (bookings.count > 0) ? bookings.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (bookings.count > 0) ? 82 : 250
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if bookings.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Booking found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingForMarinaCell", for: indexPath) as! BookingForMarinaCell
            let booking = bookings[indexPath.row] //BookingCell
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.userNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.boatNameLabel.text = (booking.boat.name == "") ? "Unnamed" : booking.boat.name
            cell.bookingDate.text = CommonClass.convertToShowFormatDate(dateString: booking.fromDate)+" To "+CommonClass.convertToShowFormatDate(dateString: booking.toDate)
            cell.slipPosition.text = booking.parkingSpace.title
            if let fromDate = self.getDateFromString(booking.fromDate){
                if let toDate = self.getDateFromString(booking.toDate){
                    let days = toDate.days(from: fromDate)
                    cell.totalNightLabel.text = (days > 1) ? "\(days)" : "\(days)"
                }else{
                    cell.totalNightLabel.text = "--"
                }
            }else{
                cell.totalNightLabel.text = "--"
            }

            return cell
        }
    }

    func getDateFromString(_ dateString: String) -> Date?
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            return date
        }else{
            return nil
        }
    }



    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bookings.count == 0{
            return
        }
        let booking = self.bookings[indexPath.row]
        self.navigateToBookingDetails(booking: booking)
    }

    func navigateToBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(UpcomingBookingDetailViewController.self)
        bookingDetailVC.booking = booking
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }

    @IBAction func onClickViewTicket(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.bookingsTableView) as IndexPath?{
            let ticketVC = AppStoryboard.Home.viewController(TicketViewController.self)
            ticketVC.booking = bookings[indexPath.row]
            ticketVC.isBrowsing = true
            self.navigationController?.pushViewController(ticketVC, animated: true)
        }
    }

    @IBAction func onClickCancelTicket(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.bookingsTableView) as IndexPath?{
            let booking = bookings[indexPath.row]
            sender.isEnabled = false
            self.cancelBooking(booking, completionBlock: { (done) in
                sender.isEnabled = true
                if done{
                    if let index = self.bookings.index(of: booking){
                        self.bookings.remove(at: index)
                        self.selectedIndex = -1
                        self.bookingsTableView.reloadData()
                        showAlertWith(self, message: "Your booking has been cancelled", title: warningMessage.alertTitle.rawValue)
                    }
                }else{
                    showAlertWith(self, message: "Your booking couldn't be cancelled\r\nPlease try again", title: warningMessage.alertTitle.rawValue)
                }
            })
        }
    }

    @IBAction func onClickUpdateTicket(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.bookingsTableView) as IndexPath?{
            let ticketVC = AppStoryboard.Home.viewController(TicketViewController.self)
            ticketVC.booking = bookings[indexPath.row]
            ticketVC.isBrowsing = true
            self.navigationController?.pushViewController(ticketVC, animated: true)
        }
    }

    func cancelBooking(_ booking:Booking,completionBlock:@escaping (_ done :Bool)->Void) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            completionBlock(false)
            return
        }
        CommonClass.showLoader(withStatus: "Cancelling..")
        BookingService.sharedInstance.cancelBookings(with: booking.ID, userId: booking.user.ID){ (success) in
            CommonClass.hideLoader()
            completionBlock(success)
        }
    }


    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == bookingsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        pageNumber+=1
                        if self.rangeDateActive {
                            self.loadDateWiseBookingList(self.marina.ID, userID: self.user.ID, dateFrom:self.selectedDates.fromDate , dateTo: selectedDates.toDate, status: "upcoming", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                        }else{
                        self.loadBookingFor(self.marina.ID, status: "upcoming", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                        }
                    }
                }
            }
        }

    }

}

extension CurrentBookingsViewController: NKCalenderViewControllerDelegate{
    func nkcalendarViewController(_ viewController: NKCalenderViewController, didSelectedDate selectedDates: BookingDates) {
        self.selectedDates = selectedDates
        bookingsTableView.reloadData()
        self.loadDateWiseBookingList(self.marina.ID, userID: self.user.ID, dateFrom: self.selectedDates.fromDate, dateTo: self.selectedDates.toDate, status: "upcoming", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        self.setupDates(selectedDate: selectedDates)
        
    }
}









