//
//  UserBoatListViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 12/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit


protocol UserBoatListViewControllerDelegate {
    func userBoatListViewController(viewController: UserBoatListViewController, didBoatBranch boat: Boat, withStatus status: Bool)
}
class UserBoatListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var boatArray =  Array<Boat>()
    var selectedIndex : Int = 0
    var user: User!
    var delegate : UserBoatListViewControllerDelegate?
    @IBOutlet weak var boatListTableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.boatListTableView.dataSource = self
        self.boatListTableView.delegate = self
        // Do any additional setup after loading the view.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return boatArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BoaterListNameCell", for: indexPath) as! BoaterListNameCell
        let vb = self.boatArray[indexPath.row]
        cell.boatNameLabel.text = "\(vb.name) "
        cell.checkButton.isSelected = (selectedIndex == indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == selectedIndex{
            return
        }else{
            selectedIndex = indexPath.row
            let boat = boatArray[selectedIndex]
            delegate?.userBoatListViewController(viewController:self, didBoatBranch: boat, withStatus: true)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ((self.view.frame.size.height*5/9) - 44)/5
    }
    
    @IBAction func onClickClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

class BoaterListNameCell: UITableViewCell{
    @IBOutlet weak var boatNameLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        self.checkButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
        self.checkButton.setImage(#imageLiteral(resourceName: "radioSel"), for: .selected)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
