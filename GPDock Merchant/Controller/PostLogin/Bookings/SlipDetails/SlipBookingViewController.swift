//
//  SlipBookingViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 04/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
protocol SlipBookingViewControllerDelegate {
    func slipBookingViewController(viewController:SlipBookingViewController,didBooked success: Bool)
}

class SlipBookingViewController: UITableViewController {
   // @IBOutlet weak var dateLabel: UILabel!
    //@IBOutlet weak var boatNameLabel: UILabel!
    @IBOutlet weak var lengthTextField:FloatLabelTextField!
    @IBOutlet weak var widthTextField:FloatLabelTextField!
    @IBOutlet weak var depthTextField:FloatLabelTextField!
    @IBOutlet weak var fristNameTextField:FloatLabelTextField!
    @IBOutlet weak var lastNameTextField:FloatLabelTextField!
    @IBOutlet weak var emailTextField:FloatLabelTextField!
    @IBOutlet weak var contactTextField:FloatLabelTextField!
    @IBOutlet weak var boatNameTextField:FloatLabelTextField!
    @IBOutlet weak var uploadImageButton: UIButton!
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var boatImageView: UIImageView!
    @IBOutlet weak var statusLabel : UILabel!
    @IBOutlet weak var datesLabel : UILabel!
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var lengthLabel : UILabel!
    @IBOutlet weak var widthLabel : UILabel!
    @IBOutlet weak var depthLabel : UILabel!
    
    var delegate : SlipBookingViewControllerDelegate?
    var imagePickerController : UIImagePickerController!
    var boatImage : UIImage?
    var slip : ParkingSpace!
    var selectedDate: BookingDates!
    var boatSize = BoatSize()
    var booking :Booking = Booking()
    var marina : Marina!
    var user: User!
    var bookingForUser: User!
    var toolSubmitButton:UIButton!
    var toolBar:UIToolbar!
    var boatArray =  Array<Boat>()
    var selectedBoat = Boat()
    var marinaPriceReview:MarinaPriceReview?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.user = User(json: User.loadUserInfo())
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        setupViews()
        self.imagePickerController = UIImagePickerController()
         let fromDate = CommonClass.formattedDateWith(selectedDate.fromDate, format: "MM-dd-YYYY")
         let toDate = CommonClass.formattedDateWith(selectedDate.toDate, format: "MM-dd-YYYY")
        self.navigationItem.title = "Slip: "  + self.slip.title
        self.datesLabel.text = "\(fromDate) to \(toDate)"
        self.statusLabel.text = "Available"
        self.lengthLabel.text = "\(self.slip.boatSize.length)" + " ft"
        self.widthLabel.text = "\(self.slip.boatSize.width)" + " ft"
        self.depthLabel.text = "\(self.slip.boatSize.depth)" + " ft"
        self.imagePickerController.delegate = self
        self.addDoneButtonOnKeyboard()
    }
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.size.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default
        //        doneToolbar.barStyle = UIBarStyle.default
        //        doneToolbar.isTranslucent = false
        //        doneToolbar.isOpaque = true
        //        doneToolbar.tintColor = .white
        doneToolbar.barTintColor = UIColor.lightGray
        //        doneToolbar.backgroundColor = .white

        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SlipBookingViewController.doneButtonAction))
        done.tintColor = UIColor.white
        done.setTitleTextAttributes([ NSAttributedStringKey.font: UIFont(name: "Raleway-SemiBold", size: 18)!], for: .normal)

        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)

        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()

        self.lengthTextField.inputAccessoryView = doneToolbar
        self.widthTextField.inputAccessoryView = doneToolbar
        self.depthTextField.inputAccessoryView = doneToolbar
        self.fristNameTextField.inputAccessoryView = doneToolbar
        self.lastNameTextField.inputAccessoryView = doneToolbar
        self.emailTextField.inputAccessoryView = doneToolbar
        self.contactTextField.inputAccessoryView = doneToolbar
        self.boatNameTextField.inputAccessoryView = doneToolbar

    }
    @objc func doneButtonAction()
    {
        self.lengthTextField.resignFirstResponder()
        self.widthTextField.resignFirstResponder()
        self.depthTextField.resignFirstResponder()
        self.fristNameTextField.resignFirstResponder()
        self.lastNameTextField.resignFirstResponder()
        self.emailTextField.resignFirstResponder()
        self.contactTextField.resignFirstResponder()
        self.boatNameTextField.resignFirstResponder()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }


    func loadForUserEmail(_ email :String){
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
        }
        MarinaService.sharedInstance.getUserEmailInfo(email) { (userEmailDetail, boats) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            
             if let emailInfo = userEmailDetail {
                self.bookingForUser = emailInfo
                self.emailTextField.text = self.bookingForUser.email
                self.fristNameTextField.text = self.bookingForUser.firstName
                self.lastNameTextField.text = self.bookingForUser.lastName
                self.contactTextField.text = self.bookingForUser.contact
        }
            
        if let userBoats = boats{
            self.boatArray = userBoats
            if self.boatArray.count != 0{
                self.showSizePickerDialog(user: self.bookingForUser,boats:self.boatArray)
            }
        }

        }
    }
    
    func showSizePickerDialog(user : User,boats:Array<Boat>) -> Void {
        let sizeSelectorVC = AppStoryboard.Booking.viewController(SizeSelectorViewController.self)
        sizeSelectorVC.bookingDates = self.selectedDate
        sizeSelectorVC.marina = self.marina
        sizeSelectorVC.boatsArray = boats
        sizeSelectorVC.delegate = self
        sizeSelectorVC.user = self.bookingForUser
        sizeSelectorVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        CommonClass.makeViewCircularWithCornerRadius(sizeSelectorVC.containnerView, borderColor: UIColor.lightGray, borderWidth: 0, cornerRadius: 5)
        let nav = UINavigationController(rootViewController: sizeSelectorVC)
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.isTranslucent = false
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    
    func setupViews() {
        lengthTextField.delegate = self
        widthTextField.delegate = self
        depthTextField.delegate = self
        fristNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        contactTextField.delegate = self
        boatNameTextField.delegate = self

        CommonClass.makeViewCircularWithCornerRadius(self.bookButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 4)

    }

    override func viewDidLayoutSubviews() {
        self.lengthTextField.addBottomBorderWithColor(.lightGray, width: 1)
        self.widthTextField.addBottomBorderWithColor(.lightGray, width: 1)
        self.depthTextField.addBottomBorderWithColor(.lightGray, width: 1)
        self.fristNameTextField.addBottomBorderWithColor(.lightGray, width: 1)
        self.lastNameTextField.addBottomBorderWithColor(.lightGray, width: 1)
        self.emailTextField.addBottomBorderWithColor(.lightGray, width: 1)
        self.contactTextField.addBottomBorderWithColor(.lightGray, width: 1)
        self.boatNameTextField.addBottomBorderWithColor(.lightGray, width: 1)
    }
    func getMarinaPrice(){
        let fromDate = CommonClass.formattedDateWith(self.selectedDate.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.formattedDateWith(self.selectedDate.toDate, format: "YYYY-MM-dd")
        self.marina.getMarinaPriceByDate(boatSize: self.boatSize, fromDate: fromDate, toDate: toDate, marinaPriceType: self.marina.priceType) { (resMarinaPrice) in
            if let aMarinaPrice = resMarinaPrice{
                self.marinaPriceReview = aMarinaPrice
                //self.countForPriceDetails = self.rowsCountForPriceDetailsSection()
            }
        }
    }
    
    func totalBookingAmount() -> Double {
        var totalAmount: Double = 0.0
         totalAmount = self.marinaPriceReview?.totalPrice ?? 0.0
        
        return totalAmount
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func onClickBack(_ sender: UIButton){
         self.navigationController?.pop(true)
    }

    @IBAction func onClickBook(_ sender: UIButton){
        let validationResut = self.validateParma(boatSize: self.boatSize, selectedDate:self.selectedDate, firstName: self.fristNameTextField.text!, lastName: self.lastNameTextField.text!, email: emailTextField.text!, contact: contactTextField.text!, boatName: boatNameTextField.text! )
        if !validationResut.success{
            showAlertWith(self, message: "\(validationResut.message)", title:warningMessage.alertTitle.rawValue)
            return
        }
        let total = self.totalBookingAmount()
        let amount = String(format: "%0.2f", total)
//        let total = self.marina.pricePerFeet * Double(boatSize.length) * Double(selectedDate.totalDays)
//        let amount = String(format:"%0.2lf",total)
        self.showAlertToConfirmBooking(amount:amount)
    }
    @IBAction func onClickUploadImage(_ sender: UIButton){
        self.showAlertToChooseAttachmentOption()
    }
    func refreshUploadImageButton() -> Void {
        //self.uploadImageButton.setTitle((self.boatImage != nil) ? "Change Image" : "Upload Image", for: UIControlState())
    }

    func showAlertToConfirmBooking(amount: String){
        let alert = UIAlertController(title: "Booking Total", message:"Collect $"+(amount)+" from the customer", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        let doneAction: UIAlertAction = UIAlertAction(title: "Proceed", style: .default)
        { action -> Void in
            self.proceedToBook()
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(doneAction)
        self.present(alert, animated: true, completion: nil)
    }

    func validateParma(boatSize:BoatSize,selectedDate:BookingDates,firstName: String,lastName:String,email:String,contact:String, boatName:String) -> (success:Bool,message:String) {
        if email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter an email")
        }
        
        if !CommonClass.validateEmail(email){
            return (success:false,message:"Please enter a valid email")
        }
        if firstName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter the first name")
        }
        if lastName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter the last name")
        }
        if contact.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter the contact name")
        }
        if boatName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == ""{
            return (success:false,message:"Please enter the boat's name")
        }
        if boatSize.length < 1{
            return (success:false,message:"Please enter the boat's length")
        }
        if boatSize.width < 1{
            return (success:false,message:"Please enter the boat's width")
        }
        if boatSize.depth < 1{
            return (success:false,message:"Please enter the boat's depth")
        }

        return (success:true,message:"")
    }

    func navToViewController() {
        let boatListVC = AppStoryboard.Booking.viewController(UserBoatListViewController.self)
        boatListVC.delegate = self
        boatListVC.boatArray = self.boatArray
        boatListVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        let nav = UINavigationController(rootViewController: boatListVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
     
    }
    
    
    
    func proceedToBook() {
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        CommonClass.showLoader(withStatus: "Booking..")
        PaymentService.sharedInstance.createOfflineBookingFor(self.user.ID,boatName:boatNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines), bookingDates: self.selectedDate, boatSize: self.boatSize, parkingSpaceID: self.slip.ID, pricePerFeet: self.marina.pricePerFeet, firstName: self.fristNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines), lastName: lastNameTextField.text!, email: self.emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines), contact: contactTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines), boatImage: self.boatImage) { (success, booking, message) in
            print("Image URL --->>\(String(describing: self.boatImage))")
            CommonClass.hideLoader()
            if success{
                if let _ = booking{
                    NotificationCenter.default.post(name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil, userInfo:nil)

                   // showAlertWith(self, message: "Booking Success\r\nBooking ID:\(aBooking.ID)", title: "GPDock")
                    //self.dismiss(animated: true, completion: nil)
                    self.navigationController?.pop(true)

                    self.delegate?.slipBookingViewController(viewController: self, didBooked: true)
                }
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
        }

    }

}

extension SlipBookingViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        textField.endEditing(true)
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == lengthTextField{
            self.boatSize.length = Int(textField.text!) ?? 0
            self.selectedBoat.boatSize.length = Int(textField.text!) ?? 0

            //self.boatImageView.image = self.boatImage ?? UIImage(named: "\(self.boatSize.range)")

        }else if textField == widthTextField{
            self.boatSize.width = Int(textField.text!) ?? 0
            self.selectedBoat.boatSize.width = Int(textField.text!) ?? 0

        }else if textField == depthTextField{
            self.boatSize.depth = Int(textField.text!) ?? 0
            self.selectedBoat.boatSize.depth = Int(textField.text!) ?? 0

        }else if textField == boatNameTextField{
            self.selectedBoat.name = textField.text ?? ""
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if(textField == self.lengthTextField) {
            //self.boatImageView.image = self.boatImage ?? UIImage(named: "\(self.boatSize.range)")
            return CommonClass.validateMaxValue(textField, maxValue: self.slip.boatSize.length, range: range, replacementString: string)
        }else if(textField == self.widthTextField) {
            return CommonClass.validateMaxValue(textField, maxValue: self.slip.boatSize.width, range: range, replacementString: string)
        }else if(textField == self.depthTextField) {
            return CommonClass.validateMaxValue(textField, maxValue: self.slip.boatSize.depth, range: range, replacementString: string)
        }

        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == lengthTextField{
            self.boatSize.length = Int(textField.text!) ?? 0
            self.getMarinaPrice()

           // self.boatImageView.image = self.boatImage ?? UIImage(named: "\(self.boatSize.range)")
        }else if textField == widthTextField{
            self.boatSize.width = Int(textField.text!) ?? 0
        }else if textField == depthTextField{
            self.boatSize.depth = Int(textField.text!) ?? 0
        }else if textField == emailTextField{
            self.loadForUserEmail(self.emailTextField.text!)
        }else if textField == boatNameTextField{
            self.selectedBoat.name = textField.text ?? ""
        }

    }
}

extension SlipBookingViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func showAlertToChooseAttachmentOption(){
        let actionSheet = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            actionSheet.dismiss(animated: true, completion: nil)
        }
        actionSheet.addAction(cancelAction)
        let openGalleryAction: UIAlertAction = UIAlertAction(title: "Choose from Gallery", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openGalleryAction)

        let openCameraAction: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                self.imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePickerController.allowsEditing = true
                self.imagePickerController.modalPresentationStyle = UIModalPresentationStyle.currentContext
                self.present(self.imagePickerController, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(openCameraAction)
        self.present(actionSheet, animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :Any]){

        if let tempImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            self.boatImage = tempImage
            self.boatImageView.image = tempImage
            self.refreshUploadImageButton()
            // self.imageAction = "update"
        }else if let tempImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.boatImage = tempImage
            self.boatImageView.image = tempImage
            self.refreshUploadImageButton()

            //self.imageAction = "update"
        }
        picker.dismiss(animated: true, completion: nil)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 64
        case 1:
            return 50
        case 2:
            return 150
        case 3:
            return 200//145
        case 4:
            return 100//165
        default:
            return 0
        }
    }
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//             switch indexPath.row {
//             case 0 :
//        let cell = tableView.dequeueReusableCell(withIdentifier: "customerBookingDateCell", for: indexPath) as! customerBookingDateCell
//        //cell.dateLabel.text = "\(self.selectedDate.fromDate) to \(self.selectedDate.toDate)"
//              return cell
//        default:
//        return UITableViewCell()
//        }
//
//    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0//self.view.frame.size.height/8
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0//self.view.frame.size.height/8
    }
}

extension SlipBookingViewController: UserBoatListViewControllerDelegate,SizeSelectorViewControllerDelegate{
    func sizeSelectorViewController(_ viewController: SizeSelectorViewController, didSelectBoatSize boatSize: BoatSize, withUserOwnBoat userBoat: Boat?) {
        if let boat = userBoat{
            self.selectedBoat = boat
            self.boatSize = boat.boatSize
            self.boatNameTextField.text = self.selectedBoat.name
            self.lengthTextField.text = "\(self.selectedBoat.boatSize.length)"
            self.widthTextField.text = "\(self.selectedBoat.boatSize.width)"
            self.depthTextField.text = "\(self.selectedBoat.boatSize.depth)"
            self.boatImageView.sd_setImage(with: URL(string:self.selectedBoat.image), placeholderImage: #imageLiteral(resourceName: "boatPlaceHolder"))
            self.getMarinaPrice()

        }else{
            self.selectedBoat = Boat()
            self.boatNameTextField.text = self.selectedBoat.name
            self.lengthTextField.text = "\(self.selectedBoat.boatSize.length)"
            self.widthTextField.text = "\(self.selectedBoat.boatSize.width)"
            self.depthTextField.text = "\(self.selectedBoat.boatSize.depth)"
            self.boatImageView.sd_setImage(with: URL(string:self.selectedBoat.image), placeholderImage: #imageLiteral(resourceName: "boatPlaceHolder"))
            self.getMarinaPrice()

        }
        
    }
    func userBoatListViewController(viewController: UserBoatListViewController, didBoatBranch boat: Boat, withStatus status: Bool) {
        if status{
            self.selectedBoat = boat
            self.boatNameTextField.text = self.selectedBoat.name
            self.lengthTextField.text = "\(self.selectedBoat.boatSize.length)"
            self.widthTextField.text = "\(self.selectedBoat.boatSize.width)"
            self.depthTextField.text = "\(self.selectedBoat.boatSize.depth)"
            self.boatImageView.sd_setImage(with: URL(string:self.selectedBoat.image), placeholderImage: #imageLiteral(resourceName: "boatPlaceHolder"))
        }else{
            self.selectedBoat = Boat()
            self.boatNameTextField.text = self.selectedBoat.name
            self.lengthTextField.text = "\(self.selectedBoat.boatSize.length)"
            self.widthTextField.text = "\(self.selectedBoat.boatSize.width)"
            self.depthTextField.text = "\(self.selectedBoat.boatSize.depth)"
            self.boatImageView.sd_setImage(with: URL(string:self.selectedBoat.image), placeholderImage: #imageLiteral(resourceName: "boatPlaceHolder"))
        }
    }
    
}


class customerBookingDateCell: UITableViewCell {
    @IBOutlet weak var datesLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

