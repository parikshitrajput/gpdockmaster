//
//  DepartingViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 03/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class DepartingViewController: UIViewController {
    var slip = ParkingSpace()
    var bookingDates : BookingDates!
    var booking :Booking = Booking()
    var departingStatus = ""
    var marina : Marina!

    @IBOutlet weak var SlipTableView: UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
      
        self.marina = CommonClass.sharedInstance.getSelectedMarina()

        self.SlipTableView.dataSource = self
        self.SlipTableView.delegate = self
        if self.departingStatus != "departure" {
        let dFrom = CommonClass.formattedDateWith(self.bookingDates.fromDate, format: "YYYY-MM-dd")
        self.getBookingDataFromServer(withSlipID: self.slip.ID,dateFrom: dFrom, dateTo:"" )
        }
         self.navigationItem.title = "Booking Id: "+self.booking.ID
    }
    func getBookingDataFromServer(withSlipID slipID: String,dateFrom: String, dateTo: String) -> Void {
        CommonClass.showLoader(withStatus: "Loading..")
        MarinaService.sharedInstance.getBookingDetailsForSlip(slipID, dateFrom: dateFrom, toDate:dateTo) { (resBooking) in
            CommonClass.hideLoader()
            if let aBooking = resBooking?.first{
                self.booking = aBooking
                self.navigationItem.title = "Booking Id: "+self.booking.ID
                self.SlipTableView.reloadData()
            }
        }
    }


    func registerCells() {
        self.SlipTableView.register(UINib(nibName: "BoatRepresentingCell", bundle: nil), forCellReuseIdentifier: "BoatRepresentingCell")
        self.SlipTableView.register(UINib(nibName: "CheckInCheckOutCell", bundle: nil), forCellReuseIdentifier: "CheckInCheckOutCell")
        self.SlipTableView.register(UINib(nibName: "OtherCell", bundle: nil), forCellReuseIdentifier: "OtherCell")
        self.SlipTableView.register(UINib(nibName: "SlipSizeCell", bundle: nil), forCellReuseIdentifier: "SlipSizeCell")
        self.SlipTableView.register(UINib(nibName: "SlipStatusCell", bundle: nil), forCellReuseIdentifier: "SlipStatusCell")
        self.SlipTableView.register(UINib(nibName: "SlipIDTableViewCell", bundle: nil), forCellReuseIdentifier: "SlipIDTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //MARK:- Actions
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSupport(_ sender: UIButton){
        let  supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
        self.navigationController?.pushViewController(supportVC, animated: true)
        //showAlertWith(self, message: warningMessage.functionalityPending.rawValue, title: "Under Development!")
    }
    @IBAction func onClickReebok(_ sender: UIButton){
        if booking.departure {
        let availableSlipVC = AppStoryboard.Booking.viewController(AvailableSlipDetailsViewController.self)
        availableSlipVC.booking = booking
        availableSlipVC.bookingDates = bookingDates
        availableSlipVC.slip = slip
        //availableSlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(availableSlipVC, animated: true)
        }else{
            showAlertWith(self, message: "Can't be Rebook", title: warningMessage.alertTitle.rawValue)
        }
    }
    
    @IBAction func onClickNext(_ sender: UIBarButtonItem) {
        let SlipDetailsVC = AppStoryboard.Booking.viewController(BookingSlipDetailsViewController.self)
        SlipDetailsVC.booking = self.booking
        SlipDetailsVC.parkingSpace = self.slip
        self.navigationController?.pushViewController(SlipDetailsVC, animated: true)
    }


    @IBAction func onClickComplete(_ sender: UIButton){
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        if booking.ID != ""{
            self.completeBooking(self.booking, completionBlock: { (done) in
                sender.isUserInteractionEnabled = true
                if done{
                    self.navigationController?.pop(true)

                }
            })
        }
        
    }
    
    func completeBooking(_ booking:Booking,completionBlock:@escaping (_ done :Bool)->Void) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            completionBlock(false)
            return
        }
        CommonClass.showLoader(withStatus: "Please Wait..")
        BookingService.sharedInstance.completeBookingOne(with: booking.ID) { (success, message, status) in
            CommonClass.hideLoader()
            if status {
            NotificationCenter.default.post(name: .BOOKING_DID_COMPLETED_BY_USER_NOTIFICATION, object: nil, userInfo: ["booking":self.booking])
            completionBlock(status)
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
                completionBlock(status)
            }
            }
        

    }

//    @IBAction func onClickCancel(_ sender: UIButton){
//        if !CommonClass.isConnectedToNetwork{
//            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: "Error!")
//            return
//        }
//        if booking.ID != ""{
//            self.cancelBooking(self.booking, completionBlock: { (done) in
//                sender.isUserInteractionEnabled = true
//                if done{
//                    self.navigationController?.pop(true)
//                }
//            })
//        }
//    }


//    func cancelBooking(_ booking:Booking,completionBlock:@escaping (_ done :Bool)->Void) -> Void {
//        if !CommonClass.isConnectedToNetwork{
//            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: "Error!")
//            completionBlock(false)
//            return
//        }
//
//        CommonClass.showLoader(withStatus: "Cancelling..")
//        BookingService.sharedInstance.cancelBookings(with: booking.ID, userId: booking.user.ID ){ (success) in
//            CommonClass.hideLoader()
//            //            NotificationCenter.default.post(name: .BOOKING_DID_CANCELLED_BY_USER_NOTIFICATION, object: nil, userInfo: ["booking":self.booking])
//            completionBlock(success)
//        }
//    }




    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM-dd-YYYY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }

}


extension DepartingViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (booking.ID.trimmingCharacters(in: .whitespacesAndNewlines) == "") {
            return 3
        }else{
            if booking.bookingType == "offline" {
                return 5
            }else{
                return 6
            }

        }
        
        //return (booking.ID.trimmingCharacters(in: .whitespacesAndNewlines) == "") ? 3 : 6
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipIDTableViewCell", for: indexPath) as! SlipIDTableViewCell
            cell.slipID.text = "Slip : "+self.booking.parkingSpace.title
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipSizeCell", for: indexPath) as! SlipSizeCell
            cell.lengthLabel.text = "\(self.booking.parkingSpace.boatSize.length)" + " ft"
            cell.widthLabel.text = "\(self.booking.parkingSpace.boatSize.width)" + " ft"
            cell.depthLabel.text = "\(self.booking.parkingSpace.boatSize.depth)" + " ft"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipStatusCell", for: indexPath) as! SlipStatusCell
            cell.statusLabel.text = "Departing: "
            //cell.bgView.backgroundColor = UIColor(red: 248.0/255.0, green: 170.0/255.0, blue: 70.0/255.0, alpha: 1.0)
            cell.datesLabel.textColor = UIColor.darkGray
            cell.calenderButton.isHidden = true
            let fromDate = self.getDateFromString(self.booking.fromDate)
            let toDate = self.getDateFromString(self.booking.toDate)
            cell.datesLabel.text = "\(fromDate) to \(toDate)"
            cell.calenderButton.isHidden = true

            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckInCheckOutCell", for: indexPath) as! CheckInCheckOutCell
            cell.checkInDate.text = self.getDateFromString(self.booking.fromDate)
            cell.checkOutDate.text = self.getDateFromString(self.booking.toDate)
            cell.checkInTime.text = self.booking.marina.checkIn
            cell.checkOutTime.text = self.booking.marina.checkOut
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BoatRepresentingCell", for: indexPath) as! BoatRepresentingCell
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.boatNameLabel.text = booking.boat.name
            cell.UserNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.lengthLabel.text = "\(booking.boat.boatSize.length)"
            cell.widthLabel.text = "\(booking.boat.boatSize.width)"
            cell.depthLabel.text = "\(booking.boat.boatSize.depth)"
            if booking.bookingType == "offline" {
                cell.bookingType.text = "Offline"
            }
            else{
                cell.bookingType.text = "Online"
            }
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
            cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
            cell.otherTitleLabel.text = "Amount Paid"
            cell.otherDetailsLabel.text = "$ "+String(format: "%0.2lf", booking.businessAmount)
            cell.separatorHeight.constant = 1
            cell.needsUpdateConstraints()
            cell.updateConstraints()
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        default:
            return UITableViewCell()
        }
    }



    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        switch indexPath.row {
        case 0://slip title
            height = 20
        case 1://slipsize
            height = 64
        case 2: //status
            height = 50
        case 3: //checkin checkout
            height = 70
        case 4: //Boat representingcell
            height = 95
        case 5: //amount paid
            height = 64
        default:
            break
        }
        return height
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            //show price breakups details
            //self.showPriceBreakUpsAlert()
        }
    }


}


