//
//  AvailableSlipDetailsViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 03/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit


class AvailableSlipDetailsViewController: UIViewController {
    var slip = ParkingSpace()
    var bookingDates : BookingDates!
     var booking :Booking = Booking()
    var fromDate = ""
    var toDate = ""
    @IBOutlet weak var SlipTableView: UITableView!
    @IBOutlet weak var bookButton: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.navigationItem.title = "Slip "+self.slip.title
        self.SlipTableView.dataSource = self
        self.SlipTableView.delegate = self
        CommonClass.makeViewCircularWithCornerRadius(self.bookButton, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 4)

    }



    func registerCells() {
        self.SlipTableView.register(UINib(nibName: "SlipSizeCell", bundle: nil), forCellReuseIdentifier: "SlipSizeCell")
        self.SlipTableView.register(UINib(nibName: "SlipStatusCell", bundle: nil), forCellReuseIdentifier: "SlipStatusCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //MARK:- Actions
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickNext(_ sender: UIBarButtonItem) {
        let SlipDetailsVC = AppStoryboard.Booking.viewController(BookingSlipDetailsViewController.self)
        SlipDetailsVC.booking = self.booking
        SlipDetailsVC.parkingSpace = self.slip
        self.navigationController?.pushViewController(SlipDetailsVC, animated: true)
    }
    @IBAction func onClickBook(_ sender: UIButton){
        let bookingVC = AppStoryboard.Booking.viewController(SlipBookingViewController.self)
        bookingVC.slip = self.slip
        bookingVC.booking = booking
        bookingVC.selectedDate = self.bookingDates
        bookingVC.delegate = self
        self.navigationController?.pushViewController(bookingVC, animated: true)
//        let nav = UINavigationController(rootViewController: bookingVC)
//        nav.navigationBar.barTintColor = kNavigationColor
//        nav.navigationBar.isTranslucent = false
//        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
//        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        nav.navigationBar.isHidden = true
//        self.navigationController?.present(nav, animated: true, completion: nil)
    }

//    func showPriceBreakUpsAlert() -> Void {
//        let priceBreakupsVC = AppStoryboard.Booking.viewController(PriceBreakupViewController.self)
//        priceBreakupsVC.booking = self.booking
//
//        priceBreakupsVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
//        CommonClass.makeViewCircularWithCornerRadius(priceBreakupsVC.containnerView, borderColor: .clear, borderWidth: 0, cornerRadius: 2)
//
//        let nav = UINavigationController(rootViewController: priceBreakupsVC)
//        nav.navigationBar.barTintColor = kNavigationColor
//        nav.navigationBar.isTranslucent = false
//        nav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
//        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
//        nav.navigationBar.isHidden = true
//        self.navigationController?.present(nav, animated: true, completion: nil)
//    }

    func setUpSelectedDate() -> Void {
        
         self.fromDate = CommonClass.formattedDateWith(bookingDates.fromDate, format: "MM-dd-YYYY")
         self.toDate = CommonClass.formattedDateWith(bookingDates.toDate, format: "MM-dd-YYYY")

    }
    
    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "dd MMM YYYY"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM-dd-YYYY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }
     @IBAction func onClickSelectDateButton(_ sender: UIButton){
        
        let calendarVC = AppStoryboard.Home.viewController(NKCalenderViewController.self)
        calendarVC.delegate = self
        calendarVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        let nav = UINavigationController(rootViewController: calendarVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

}

extension AvailableSlipDetailsViewController: UITableViewDataSource,UITableViewDelegate,SlipBookingViewControllerDelegate{
    func slipBookingViewController(viewController: SlipBookingViewController, didBooked success: Bool) {
        if !self.booking.departure {
        self.navigationController?.pop(true)
        }else{
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        }
    }



    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipSizeCell", for: indexPath) as! SlipSizeCell
            cell.lengthLabel.text = "\(self.slip.boatSize.length)"
            cell.widthLabel.text = "\(self.slip.boatSize.width)"
            cell.depthLabel.text = "\(self.slip.boatSize.depth)"
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipStatusCell", for: indexPath) as! SlipStatusCell
            cell.statusLabel.text = "Available: "
            //cell.bgView.backgroundColor = UIColor(red: 69.0/255.0, green: 163.0/255.0, blue: 218.0/255.0, alpha: 1.0)
            cell.datesLabel.textColor = UIColor.darkGray//(red: 202.0/255.0, green: 202.0/255.0, blue: 202.0/255.0, alpha: 1.0)
            if booking.departure {
            cell.calenderButton.addTarget(self, action: #selector(onClickSelectDateButton(_:)), for: .touchUpInside)
             cell.datesLabel.text = "\(self.fromDate) to \(self.toDate)"
            }else{
            //let fromDate = CommonClass.formattedDateWith(self.bookingDates.fromDate, format: "dd MMM YYYY")
            if self.bookingDates.totalDays < 1{
                bookingDates.toDate = self.bookingDates.fromDate.addingTimeInterval(24*60*60)
            }
            let toDate = CommonClass.formattedDateWith(self.bookingDates.toDate, format: "MM-dd-YYYY")
            let fDate = CommonClass.formattedDateWith(self.bookingDates.fromDate, format: "MM-dd-YYYY")

            cell.datesLabel.text = "\(fDate) to \(toDate)"
                cell.calenderButton.isHidden = true
            }
            return cell
        default:
            return UITableViewCell()
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        switch indexPath.row {
        case 0://bookingid
            height = 70
        case 1: //MarinaRepresentingCell
            height = 50
        default:
            break
        }
        return height
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            //show price breakups details
            //self.showPriceBreakUpsAlert()
        }
    }
}
extension AvailableSlipDetailsViewController: NKCalenderViewControllerDelegate{
    func nkcalendarViewController(_ viewController: NKCalenderViewController, didSelectedDate selectedDates: BookingDates) {
       self.bookingDates = selectedDates
        self.setUpSelectedDate()
        self.SlipTableView.reloadData()
    }
}
