//
//  ArrivingAndDepartingViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 22/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ArrivingAndDepartingViewController: UIViewController {
    @IBOutlet weak var arrivingID: UILabel!
    @IBOutlet weak var departingID: UILabel!
    @IBOutlet weak var arrivingImage: UIImageView!
    @IBOutlet weak var departingImage: UIImageView!
    var bookings = Array<Booking>()
    var slip = ParkingSpace()
    var bookingDates : BookingDates!
    var isFromBookingList = false
    var arrivingStatus = ""
    var user : User!
    var marina : Marina!

     var navigationTitleView : ArrivingAndDepartureNavigationTittle!
    @IBOutlet weak var SlipTableView: UITableView!
    let arrivingColor = UIColor(red: 238.0/255.0, green: 231.0/255.0, blue: 48.0/255.0, alpha: 1.0)
    let departingColor = UIColor(red: 248.0/255.0, green: 170.0/255.0, blue: 70.0/255.0, alpha: 1.0)

    override func viewDidLoad() {
        super.viewDidLoad()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()

        self.SlipTableView.dataSource = self
        self.SlipTableView.delegate = self
        self.SlipTableView.register(UINib(nibName: "SlipIDTableViewCell", bundle: nil), forCellReuseIdentifier: "SlipIDTableViewCell")
        self.SlipTableView.register(UINib(nibName: "SlipSizeCell", bundle: nil), forCellReuseIdentifier: "SlipSizeCell")
        let dFrom = CommonClass.formattedDateWith(self.bookingDates.fromDate, format: "YYYY-MM-dd")
        self.getBookingDataFromServer(withSlipID: self.slip.ID,dateFrom: dFrom, toDate: "")
        self.setnavView()
        // Do any additional setup after loading the view.
    }
    
    func setnavView() {
        self.navigationTitleView = ArrivingAndDepartureNavigationTittle.instanceFromNib()
        self.navigationTitleView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width-200, height: 44)
        //self.navigationTitleView.titleButton.setTitle("Occupancy rate", for: .normal)
        //self.navigationTitleView.titleButton.addTarget(self, action: #selector(onclickShowMarinaPicker(_:)), for: .touchUpInside)
//        self.navigationTitleView.firstIDLabel.text = "1234"
//        self.navigationTitleView.seconfIDLabel.text = "0987"
        self.navigationItem.titleView = self.navigationTitleView
        self.navigationTitleView.setNeedsLayout()
        self.navigationTitleView.layoutIfNeeded()
    }

    func getBookingDataFromServer(withSlipID slipID: String,dateFrom: String, toDate: String) -> Void {
        CommonClass.showLoader(withStatus: "Loading..")
        MarinaService.sharedInstance.getBookingDetailsForSlip(slipID, dateFrom: dateFrom, toDate:toDate) { (resBooking) in
        CommonClass.hideLoader()
            if let someBookings = resBooking as Array<Booking>?{
                self.bookings = someBookings
                //self.navigationItem.title = "Booking : "+self.booking.ID
                
                self.SlipTableView.reloadData()
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    
    @IBAction func onClickSupport(_ sender: UIButton){
        let  supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
        self.navigationController?.pushViewController(supportVC, animated: true)
        //showAlertWith(self, message: warningMessage.functionalityPending.rawValue, title: "Under Development!")
    }
    
    @IBAction func onClickNext(_ sender: UIBarButtonItem) {
        let SlipDetailsVC = AppStoryboard.Booking.viewController(BookingSlipDetailsViewController.self)
       // SlipDetailsVC.booking = self.departingbooking
        SlipDetailsVC.parkingSpace = self.slip
        self.navigationController?.pushViewController(SlipDetailsVC, animated: true)
    }
    
    
    
    
    
    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM-dd-YYYY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }

}

extension ArrivingAndDepartingViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
}
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if section == 0{
        rowCount = 2
        }
        if section == 1{
            rowCount =  self.bookings.count
        }
        return rowCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SlipIDTableViewCell", for: indexPath) as! SlipIDTableViewCell
                cell.slipID.text = "Slip : "+slip.title
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SlipSizeCell", for: indexPath) as! SlipSizeCell
                cell.lengthLabel.text = "\(self.slip.boatSize.length)" + "ft"
                cell.widthLabel.text = "\(self.slip.boatSize.width)" + "ft"
                cell.depthLabel.text = "\(self.slip.boatSize.depth)" + "ft"
                return cell
            default:
                  break
        }
        }
        if indexPath.section == 1{

        let cell = tableView.dequeueReusableCell(withIdentifier: "ArrivingAndDepartingTableViewCell", for: indexPath) as! ArrivingAndDepartingTableViewCell
         let booking = bookings[indexPath.row]
            let fromDate = self.getDateFromString(booking.fromDate)
            let toDate = self.getDateFromString(booking.toDate)
            cell.statusLabel.text =  booking.arrival ? "Arriving: " : "Departing: "
            if booking.arrival{
        //cell.bgView.backgroundColor = arrivingColor
                self.navigationTitleView.firstIDLabel.text = booking.ID
            }else{
       //cell.bgView.backgroundColor = departingColor
        self.navigationTitleView.seconfIDLabel.text = booking.ID

            }
            cell.bgView.backgroundColor = kApplicationLightGrayColor
        cell.datesLabel.text = isFromBookingList ? "\(fromDate)" : "\(fromDate) to \(toDate)"
        cell.checkInDate.text = self.getDateFromString(booking.fromDate)
        cell.checkOutDate.text = self.getDateFromString(booking.toDate)
        cell.datesLabel.textColor = UIColor.darkGray
            cell.checkInTime.text = booking.marina.checkIn
            cell.checkOutTime.text = booking.marina.checkOut
        cell.boatImageView.setIndicatorStyle(.gray)
        cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
        cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
        cell.boatNameLabel.text = booking.boat.name
        cell.UserNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
        cell.lengthLabel.text = "\(booking.boat.boatSize.length)"
        cell.widthLabel.text = "\(booking.boat.boatSize.width)"
        cell.depthLabel.text = "\(booking.boat.boatSize.depth)"
            if booking.bookingType == "offline" {
                cell.bookingType.text = "Offline"
            }
            else{
                cell.bookingType.text = "Online"
            }
        cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
        //cell.otherTitleLabel.text = "Amount Paid"
        cell.otherDetailsLabel.text = "$ "+String(format: "%0.2lf", booking.businessAmount)
           
            return cell
            }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        if indexPath.section == 0{
            switch indexPath.row {
            case 0://slipTitle
                height = 30
            case 1://slipsize
                height = 64
            default:
                break
        }
        }
            if indexPath.section == 1{

                height = 300
    }
        return height
    
}
}
