//
//  BookingDetailViewController.swift
//  GPDock
//
//  Created by TecOrb on 12/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CancelledBookingDetailViewController: UIViewController {
    var booking :Booking = Booking()
    var refund = RefundBookingDetail()
    var marina : Marina!

    @IBOutlet weak var SlipTableView: UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        self.navigationItem.title = "Booking Id: "+self.booking.ID
        self.SlipTableView.dataSource = self
        self.SlipTableView.delegate = self
        self.marina = CommonClass.sharedInstance.getSelectedMarina()

        self.getRefundDataFromServer(withBookingID: self.booking.ID)
    }
    
    func getRefundDataFromServer(withBookingID bookingID: String) -> Void {
        CommonClass.showLoader(withStatus: "Loading..")
        PaymentService.sharedInstance.getBookingRefundStatus(bookingID, completionBlock: { (resRefund) in
            if let aRefund = resRefund{
                CommonClass.hideLoader()
                self.refund = aRefund
                self.SlipTableView.reloadData()
            }
        })
    }


    func registerCells() {
        self.SlipTableView.register(UINib(nibName: "BoatRepresentingCell", bundle: nil), forCellReuseIdentifier: "BoatRepresentingCell")
        self.SlipTableView.register(UINib(nibName: "CheckInCheckOutCell", bundle: nil), forCellReuseIdentifier: "CheckInCheckOutCell")
        self.SlipTableView.register(UINib(nibName: "OtherCell", bundle: nil), forCellReuseIdentifier: "OtherCell")
        self.SlipTableView.register(UINib(nibName: "SlipSizeCell", bundle: nil), forCellReuseIdentifier: "SlipSizeCell")
        self.SlipTableView.register(UINib(nibName: "SlipStatusCell", bundle: nil), forCellReuseIdentifier: "SlipStatusCell")
        self.SlipTableView.register(UINib(nibName: "RateAndReviewCell", bundle: nil), forCellReuseIdentifier: "RateAndReviewCell")
        self.SlipTableView.register(UINib(nibName: "SlipIDTableViewCell", bundle: nil), forCellReuseIdentifier: "SlipIDTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //MARK:- Actions
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSupport(_ sender: UIButton){
        let  supportVC = AppStoryboard.Settings.viewController(SupportsViewController.self)
        self.navigationController?.pushViewController(supportVC, animated: true)    }

    @IBAction func onClickComplete(_ sender: UIButton){
        showAlertWith(self, message: warningMessage.functionalityPending.rawValue, title: "Under Development!")
    }

    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM-dd-YYYY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }

}

extension CancelledBookingDetailViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (booking.ID.trimmingCharacters(in: .whitespacesAndNewlines) == ""){
            return 3
        }else if booking.amount == 0.0{
            return 5
        }else{
            return (booking.ID.trimmingCharacters(in: .whitespacesAndNewlines) == "") ? 3 : 9 
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipIDTableViewCell", for: indexPath) as! SlipIDTableViewCell
            cell.slipID.text = "Slip : "+self.booking.parkingSpace.title
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipSizeCell", for: indexPath) as! SlipSizeCell
            cell.lengthLabel.text = "\(self.booking.parkingSpace.boatSize.length)" + " ft"
            cell.widthLabel.text = "\(self.booking.parkingSpace.boatSize.width)" + " ft"
            cell.depthLabel.text = "\(self.booking.parkingSpace.boatSize.depth)" + " ft"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipStatusCell", for: indexPath) as! SlipStatusCell
            cell.statusLabel.text = "Cancelled: "
            //cell.bgView.backgroundColor = UIColor(red: 83.0/255.0, green: 186.0/255.0, blue: 173.0/255.0, alpha: 1.0)
            cell.datesLabel.textColor = UIColor.darkGray
            cell.calenderButton.isHidden = true
            let onDate = self.getDateFromString(self.booking.cancelledAt)
            cell.datesLabel.text = "\(onDate)"
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckInCheckOutCell", for: indexPath) as! CheckInCheckOutCell
            cell.checkInDate.text = self.getDateFromString(self.booking.fromDate)
            cell.checkOutDate.text = self.getDateFromString(self.booking.toDate)
            cell.checkInTime.text = self.marina.checkIn
            cell.checkOutTime.text = self.marina.checkOut
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BoatRepresentingCell", for: indexPath) as! BoatRepresentingCell
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.boatNameLabel.text = booking.boat.name
            cell.UserNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.lengthLabel.text = "\(booking.boat.boatSize.length)"
            cell.widthLabel.text = "\(booking.boat.boatSize.width)"
            cell.depthLabel.text = "\(booking.boat.boatSize.depth)"
            if booking.bookingType == "offline" {
                cell.bookingType.text = "Offline"
            }
            else{
                cell.bookingType.text = "Online"
            }
            return cell
        case 5:
            return self.refundProcessCell(tableView, indexPath: indexPath)
        case 6:
            return self.amountRefundCell(tableView, indexPath: indexPath)
        case 7:
            return self.refundProcessingDateCell(tableView, indexPath: indexPath)
        case 8:
            return self.amountPaidCell(tableView, indexPath: indexPath)
        default:
            return UITableViewCell()
        }
    }

    func amountPaidCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
        cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
        cell.otherTitleLabel.text = "Amount Paid"
        cell.otherDetailsLabel.text = "$ "+String(format: "%0.2lf", booking.businessAmount)
        cell.separatorHeight.constant = 1
        cell.needsUpdateConstraints()
        cell.updateConstraints()
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func amountRefundCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
        cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
        cell.otherTitleLabel.text = "Amount Refund"
        cell.otherDetailsLabel.text = "$ "+String(format: "%0.2lf", refund.amount)
        cell.separatorHeight.constant = 1
        cell.needsUpdateConstraints()
        cell.updateConstraints()
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func refundProcessingDateCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
        cell.otherIcon.image = #imageLiteral(resourceName: "process_date")
        cell.otherTitleLabel.text = "Procesed Date"
        let onDate = self.getDateFromString(self.refund.updatedAt)
        cell.otherDetailsLabel.text = onDate
        cell.separatorHeight.constant = 1
        cell.needsUpdateConstraints()
        cell.updateConstraints()
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func refundProcessCell(_ tableView: UITableView,indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
        cell.otherIcon.image = #imageLiteral(resourceName: "refund")
        cell.otherTitleLabel.text = "Refund Process"
        cell.otherDetailsLabel.text = self.refund.currentStage
        cell.separatorHeight.constant = 1
        cell.needsUpdateConstraints()
        cell.updateConstraints()
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 64
        switch indexPath.row {
        case 0://slipID
            height = 20
        case 1://slipsize
            height = 64
        case 2: //status
            height = 50
        case 3: //checkin checkout
            height = 70
        case 4: //Boat representingcell
            height = 95
        case 5: //amount paid
            height = 64
        case 6: //amount paid
            height = 64
        case 7: //amount paid
            height = 64
        case 8: //amount paid
            height = 72
        default:
            break
        }
        return height
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            //show price breakups details
            //self.showPriceBreakUpsAlert()
        }
    }


}




