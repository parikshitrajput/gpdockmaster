//
//  CompletedBookingsViewController.swift
//  GPDock
//
//  Created by TecOrb on 04/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CompletedBookingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var bookings = Array<Booking>()
    @IBOutlet weak var bookingsTableView : UITableView!
    @IBOutlet weak var completeMarinaSearch : UISearchBar!
    @IBOutlet weak var searchBarButton : UIButton!
    @IBOutlet weak var selectedDateLabel : UILabel!
    @IBOutlet weak var selectedDateButton : UIButton!
    var fromDateStr = ""
    var toDateStr = ""
    var user : User!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var rangeDateActive: Bool = false
    var selectedIndex = -1
    var marina: Marina!
    var selectedDates: BookingDates!
    //var nextStatus = "completed"
    var previousratingStatus = "show"


    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CompletedBookingsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()


    override func viewDidLoad() {
        super.viewDidLoad()

        //selectedDateLabel.backgroundColor = kApplicationLightGrayColor
        //searchBarButton.backgroundColor = kApplicationLightGrayColor
//        searchBarButton.layer.borderWidth = 0.5
//        searchBarButton.layer.borderColor = UIColor.lightGray.cgColor
//        searchBarButton.layer.cornerRadius = 5
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        NotificationCenter.default.addObserver(self, selector: #selector(CompletedBookingsViewController.bookingDidCompletedByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_USER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CompletedBookingsViewController.bookingDidCompletedByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CompletedBookingsViewController.updatedRating(_:)), name: .BOOKING_DID_RATING_UPDATED_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(bookingDidRefresh(_:)) , name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil)
        self.bookingsTableView.register(UINib(nibName: "BookingForMarinaCell", bundle: nil), forCellReuseIdentifier: "BookingForMarinaCell")

        self.bookingsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.bookingsTableView.dataSource = self
        self.bookingsTableView.delegate = self
        self.bookings.removeAll()
        self.bookingsTableView.addSubview(self.refreshControl)
        self.loadBookingFor(self.marina.ID, status: "completed", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        bookingsTableView.reloadData()
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }
     @objc func bookingDidRefresh(_ notification: Notification) {
        self.bookings.removeAll()
        self.loadBookingFor(self.marina.ID, status: "completed", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        bookingsTableView.reloadData()

    }


    @IBAction func onClickSelectDateButton(_ sender: UIButton) {
        self.rangeDateActive = true
            let calendarVC = AppStoryboard.Home.viewController(NKCalenderViewController.self)
         calendarVC.selectedBookingDate = self.selectedDates
            calendarVC.minDate = Calendar(identifier: .iso8601).date(byAdding: .year, value: -1, to: Date())
            calendarVC.maxDate = Calendar(identifier: .iso8601).date(byAdding: .year, value: +1, to: Date())
            calendarVC.delegate = self
            calendarVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            let nav = UINavigationController(rootViewController: calendarVC)
            nav.navigationBar.barTintColor = kNavigationColor
            nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            nav.navigationBar.isHidden = true
            self.navigationController?.present(nav, animated: true, completion: nil)
    }
    func setupDates(selectedDate:BookingDates){
        self.fromDateStr = CommonClass.formattedDateWith(selectedDate.fromDate, format: "dd MMM")
        self.toDateStr = CommonClass.formattedDateWith(selectedDate.toDate, format: "dd MMM")
        self.searchBarButton.titleLabel?.isHidden = true
        self.searchBarButton.backgroundColor = UIColor.clear
        self.selectedDateLabel.isHidden = false
        self.selectedDateLabel.text = ( selectedDate.totalDays > 0) ? "\(fromDateStr) to \(toDateStr)" : fromDateStr
        
    }
    @IBAction func onClickSearchBarButton(_ sender: UIButton) {
        self.searchBarButton.titleLabel?.isHidden = false
        self.searchBarButton.backgroundColor = kApplicationLightGrayColor
        self.selectedDateLabel.isHidden = true
        let searchBarVC = AppStoryboard.Booking.viewController(SearchBookingsViewController.self)
        searchBarVC.bookings = self.bookings
        searchBarVC.reserveArray = self.bookings
        searchBarVC.currentStatus = "completed"
        searchBarVC.marina = self.marina
        self.navigationController?.pushViewController(searchBarVC, animated: true)
    }
     @objc func bookingDidCompletedByUser(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            if let removingBooking = userInfo["booking"]{
                self.bookings.insert(removingBooking, at: 0)
                self.bookingsTableView.reloadData()
            }
        }
    }
    
    @objc func updatedRating(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            print("UserInfoDetail--->\(userInfo.count)")
            self.loadBookingFor(self.marina.ID, status: "completed", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
            self.bookingsTableView.reloadData()

        }
    }
    
    func loadBookingFor(_ marinaID:String,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
        }

        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            bookingsTableView.reloadData()
        }

        BookingService.sharedInstance.getBookingsForMarina(marinaID, status: status,page:pageNumber,perPage: recordPerPage) { (response) in

            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.bookings.append(contentsOf: newJobArray)
                self.bookingsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
        }
    }

    func loadDateWiseBookingList(_ marinaID:String,userID:String,dateFrom: Date,dateTo:Date,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            bookingsTableView.reloadData()
        }
        BookingService.sharedInstance.getDateWiseBookingListBookings(marinaID, userID: userID, dateFrom: dateFrom, dateTo: dateTo, status: status, page: pageNumber, perPage: recordPerPage){ (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.bookings.append(contentsOf: newJobArray)
                self.bookingsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
        }
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.isNewDataLoading = true
        self.rangeDateActive = false
        self.selectedDateLabel.text = ""
        self.searchBarButton.titleLabel?.isHidden = false
        self.bookingsTableView.reloadData()
        BookingService.sharedInstance.getBookingsForMarina(self.marina.ID, status: "completed",page: self.pageNumber,perPage: self.recordsPerPage) { (response) in
            self.bookings.removeAll()
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newBookingArray = response{
                self.bookings.append(contentsOf: newBookingArray)
                self.bookingsTableView.reloadData()
            }
        }
    }



    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (bookings.count > 0) ? bookings.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return (indexPath.row == selectedIndex) ? 250 : 82
        return (bookings.count > 0) ? 82 : 250
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if bookings.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Booking found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingForMarinaCell", for: indexPath) as! BookingForMarinaCell
            let booking = bookings[indexPath.row] //BookingCell
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.userNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.boatNameLabel.text = (booking.boat.name == "") ? "Unnamed" : booking.boat.name
            cell.bookingDate.text = CommonClass.convertToShowFormatDate(dateString: booking.fromDate)+" To "+CommonClass.convertToShowFormatDate(dateString: booking.toDate)
            cell.slipPosition.text = booking.parkingSpace.title
            if let fromDate = self.getDateFromString(booking.fromDate){
                if let toDate = self.getDateFromString(booking.toDate){
                    let days = toDate.days(from: fromDate)
                    cell.totalNightLabel.text = (days > 1) ? "\(days)" : "\(days)"
                }else{
                    cell.totalNightLabel.text = "--"
                }
            }else{
                cell.totalNightLabel.text = "--"
            }

            return cell
        }
    }

    func getDateFromString(_ dateString: String) -> Date?
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            return date
        }else{
            return nil
        }
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bookings.count == 0{
            return
        }
        let booking = self.bookings[indexPath.row]
        self.navigateToBookingDetails(booking: booking)
    }

    func navigateToBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(CompletedBookingDetailViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.delegate = self
        bookingDetailVC.RatingCellStatus = self.previousratingStatus
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }



    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == bookingsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        pageNumber+=1
                        if self.rangeDateActive == true {
                            self.loadDateWiseBookingList(self.marina.ID, userID: self.user.ID, dateFrom:self.selectedDates.fromDate , dateTo: selectedDates.toDate, status: "completed", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                        }else{
                        self.loadBookingFor(self.marina.ID, status: "completed", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                        }
                    }
                }
            }
        }
        
    }
    
    
}

extension CompletedBookingsViewController: CompletedBookingDetailViewControllerDelegate{
    func bookingRatingDidUpdatedByMarinaOwner(viewController: CompletedBookingDetailViewController, booking: Booking) {
        for i in 0..<bookings.count{
            if booking.ID == bookings[i].ID{
                bookings[i] = booking
                self.bookingsTableView.reloadData()
                break
            }
        }
    }
    
    
}
extension CompletedBookingsViewController: NKCalenderViewControllerDelegate{
    func nkcalendarViewController(_ viewController: NKCalenderViewController, didSelectedDate selectedDates: BookingDates) {
        self.selectedDates = selectedDates
        bookings.removeAll()
        bookingsTableView.reloadData()
        self.loadDateWiseBookingList(self.marina.ID, userID: self.user.ID, dateFrom: self.selectedDates.fromDate, dateTo: self.selectedDates.toDate, status: "completed", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        self.setupDates(selectedDate: selectedDates)
        
    }
}

