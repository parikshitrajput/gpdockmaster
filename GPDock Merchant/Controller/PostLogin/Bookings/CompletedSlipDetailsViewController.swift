//
//  CompletedSlipDetailsViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 06/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CompletedSlipDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var bookings = [Booking]()
    @IBOutlet weak var bookingsTableView : UITableView!
    var user : User!
    var marina : Marina!
    var booking : Booking!
    var parkingSpace : ParkingSpace!

      var selectedIndex = -1
    var isNewDataLoading = false
    var saveMarinaSpaceId = ""
    var previousratingStatus = "hide"

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CompletedSlipDetailsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        NotificationCenter.default.addObserver(self, selector: #selector(CompletedSlipDetailsViewController.bookingDidCompletedByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        self.bookingsTableView.register(UINib(nibName: "BookingForMarinaCell", bundle: nil), forCellReuseIdentifier: "BookingForMarinaCell")
        self.bookingsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.bookingsTableView.dataSource = self
        self.bookingsTableView.delegate = self
        self.bookingsTableView.addSubview(self.refreshControl)
        self.loadMarinaSpaceFor(parkingSpace.ID, status: "completed")
    }
    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   @objc func  bookingDidCompletedByUser(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            if let removingBooking = userInfo["booking"]{
                self.bookings.insert(removingBooking, at: 0)
                self.bookingsTableView.reloadData()
            }
        }
    }
    func loadMarinaSpaceFor(_ marinaSpaceID:String,status:String) {
        self.saveMarinaSpaceId = marinaSpaceID
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return
        }
        self.isNewDataLoading = true

        BookingService.sharedInstance.getParkingSpaceInfo(marinaSpaceID, status: status) { (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            self.isNewDataLoading = false
            if let newJobArray = response as Array<Booking>?{
                self.bookings.removeAll()
                self.bookings.append(contentsOf: newJobArray)
                self.bookingsTableView.reloadData()
            }
        }
    }
    func getDateFromString(_ dateString: String) -> Date?
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            return date
        }else{
            return nil
        }
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.isNewDataLoading = true
        self.bookingsTableView.reloadData()
        BookingService.sharedInstance.getParkingSpaceInfo(self.saveMarinaSpaceId, status: "completed") { (response) in
            self.bookings.removeAll()
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newBookingArray = response{
                self.bookings.append(contentsOf: newBookingArray)
                self.bookingsTableView.reloadData()
            }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (bookings.count > 0) ? bookings.count : 1 ;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //return (indexPath.row == selectedIndex) ? 250 : 82
        return (bookings.count > 0) ? 82 : 250
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if bookings.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Booking found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingForMarinaCell", for: indexPath) as! BookingForMarinaCell
            let booking = bookings[indexPath.row] //BookingCell
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.userNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.boatNameLabel.text = (booking.boat.name == "") ? "Unnamed" : booking.boat.name
            cell.bookingDate.text = CommonClass.convertToShowFormatDate(dateString: booking.fromDate)+" To "+CommonClass.convertToShowFormatDate(dateString: booking.toDate)
            cell.slipPosition.text = booking.parkingSpace.title
            if let fromDate = self.getDateFromString(booking.fromDate){
                if let toDate = self.getDateFromString(booking.toDate){
                    let days = toDate.days(from: fromDate)
                    cell.totalNightLabel.text = (days > 1) ? "\(days)" : "\(days)"
                }else{
                    cell.totalNightLabel.text = "--"
                }
            }else{
                cell.totalNightLabel.text = "--"
            }
            
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bookings.count == 0{
            return
        }
        let booking = self.bookings[indexPath.row]
        self.navigateToBookingDetails(booking: booking)
    }
    
    func navigateToBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(CompletedBookingDetailViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.RatingCellStatus = self.previousratingStatus
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if scrollView == bookingsTableView{
//            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
//            {
//                if !isNewDataLoading{
//                    if CommonClass.isConnectedToNetwork{
//                        isNewDataLoading = true
//                        self.bookings.removeAll()
//                        self.loadMarinaSpaceFor(self.saveMarinaSpaceId, status: "completed")
//                    }
//                }
//            }
//        }
//        
//    }

}
