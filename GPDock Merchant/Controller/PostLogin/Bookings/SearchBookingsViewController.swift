import UIKit

class SearchBookingsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var bookings = [Booking]()
    var reserveArray = [Booking]()
    @IBOutlet weak var aSearchBar : UISearchBar!
    @IBOutlet weak var searchBookingsTableView : UITableView!
    var currentStatus = ""
    var user : User!
    var marina : Marina!
    var booking : Booking!
    var pageNumber = 1
    var recordsPerPage = 10
    var isLoading = false
    var isSeachActive = false
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bookings.removeAll()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        self.aSearchBar.delegate = self
        self.aSearchBar.backgroundColor = kApplicationLightGrayColor
        //self.aSearchBar.barTintColor = kApplicationLightGrayColor
        //self.aSearchBar.tintColor = kApplicationBlueColor
        //self.aSearchBar.barStyle = .blackTranslucent
        self.navigationItem.title = "Search "  + (self.currentStatus).firstUppercased  + " Booking"
        self.searchBookingsTableView.register(UINib(nibName: "BookingForMarinaCell", bundle: nil), forCellReuseIdentifier: "BookingForMarinaCell")
        self.bookings = self.reserveArray
        self.searchBookingsTableView.reloadData()
        self.searchBookingsTableView.dataSource = self
        self.searchBookingsTableView.delegate = self
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 1
        if bookings.count != 0{
            rows = bookings.count
        }
        return rows
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (bookings.count > 0) ? 82 : 250
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.bookings.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            if isLoading{
                cell.messageLabel.text = "Searching.."
            }else if !isLoading && !isSeachActive{
                cell.messageLabel.text = "Search "  + (self.currentStatus).firstUppercased  + " Booking"
            }else{
                cell.messageLabel.text = "No Booking found"
            }
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingForMarinaCell", for: indexPath) as! BookingForMarinaCell
            let booking = bookings[indexPath.row] //BookingCell
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.userNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.boatNameLabel.text = (booking.boat.name == "") ? "Unnamed" : booking.boat.name.capitalized
            cell.bookingDate.text = booking.fromDate+" To "+booking.toDate
            cell.slipPosition.text = booking.parkingSpace.title
            if let fromDate = self.getDateFromString(booking.fromDate){
                if let toDate = self.getDateFromString(booking.toDate){
                    let days = toDate.days(from: fromDate)
                    cell.totalNightLabel.text = (days > 1) ? "\(days)" : "\(days)"
                }else{
                    cell.totalNightLabel.text = "--"
                }
            }else{
                cell.totalNightLabel.text = "--"
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.bookings.count != 0 {
            let booking = self.bookings[indexPath.row]
            self.navigateToBookingDetails(booking: booking)
        }
    }
    
    func navigateToBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(TodayBookingViewController.self)
        bookingDetailVC.booking = booking
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    func getDateFromString(_ dateString: String) -> Date?
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            return date
        }else{
            return nil
        }
    }
    
    func loadJobsFor(_ marinaID:String,searchKey:String, status:String) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        self.isLoading = true
        BookingService.sharedInstance.searchBookings(marinaID, searchKey: searchKey, status: status){ (response) in
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
            self.isLoading = false
            
            if let newJobArray = response as Array<Booking>?{
                self.bookings.removeAll()
                self.bookings.append(contentsOf: newJobArray)
                self.searchBookingsTableView.reloadData()
            }else{
                print("Not Sucessfully enter")
            }
        }
    }
    
    func loadBookingFor(_ marinaID:String,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        self.isLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            searchBookingsTableView.reloadData()
        }
        
        BookingService.sharedInstance.getBookingsForMarina(marinaID, status: status,page:pageNumber,perPage: recordPerPage) { (response) in

            self.isLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.bookings.append(contentsOf: newJobArray)
                self.searchBookingsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
          if scrollView == searchBookingsTableView{
            self.aSearchBar.resignFirstResponder()
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == searchBookingsTableView{
            self.aSearchBar.resignFirstResponder()
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                
                if !isLoading{
                    if CommonClass.isConnectedToNetwork{
                        isLoading = true
                        pageNumber+=1
                        self.loadBookingFor(self.marina.ID, status: currentStatus, pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                    }
                }
            }
        }
        
    }
}

extension SearchBookingsViewController: UISearchBarDelegate {
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //isSeachActive = false
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text?.count == 0 {
            return
        }
        isSeachActive = true
        self.isLoading = true
        self.loadJobsFor(self.marina.ID, searchKey: searchBar.text!, status: self.currentStatus)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        self.isLoading = false
        isSeachActive = searchText.count > 0
        if searchText.count < 0 {
            self.isLoading = false
            self.pageNumber = 1
            self.bookings.removeAll()
            self.bookings = self.reserveArray
            self.searchBookingsTableView.reloadData()
            
        }else{
            self.isLoading = true
            self.loadJobsFor(self.marina.ID, searchKey: searchBar.text!, status: self.currentStatus)
        }
        
    }
    
}


