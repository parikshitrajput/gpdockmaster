//
//  TodayBookingsViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 23/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

let UPDATE_LIST_NOTIFICATION = "SelectTodayListNotification"
protocol TodayBookingsViewControllerDelegate {
    func todayBookingsViewController(viewController:TodayBookingsViewController,didBooked success: Bool)
}
class TodayBookingsViewController:  UIViewController,UITableViewDataSource,UITableViewDelegate {
    var bookings = Array<Booking>()
    @IBOutlet weak var bookingsTableView : UITableView!
    @IBOutlet weak var searchBarButton : UIButton!

    var user : User!
    var marina : Marina!
    var pageNumber = 1
    var recordsPerPage = 10
    var isNewDataLoading = false
    var selectedIndex = -1
    //var nextStatus = "today"
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(TodayBookingsViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
//        searchBarButton.layer.borderWidth = 0.5
//        searchBarButton.layer.borderColor = UIColor.lightGray.cgColor
//        searchBarButton.layer.cornerRadius = 5

        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        
        NotificationCenter.default.addObserver(self, selector: #selector(TodayBookingsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_CANCELLED_BY_USER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TodayBookingsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_USER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TodayBookingsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_CANCELLED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(TodayBookingsViewController.bookingDidCancelledByUser(_:)), name: .BOOKING_DID_COMPLETED_BY_MARINA_OWNER_NOTIFICATION, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(TodayBookingsViewController.bookingDidRefresh(_:)) , name: NSNotification.Name(UPDATE_LIST_NOTIFICATION), object: nil)
        self.bookingsTableView.register(UINib(nibName: "BookingForMarinaCell", bundle: nil), forCellReuseIdentifier: "BookingForMarinaCell")
        
        self.bookingsTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.bookingsTableView.dataSource = self
        self.bookingsTableView.delegate = self
        self.bookings.removeAll()
        self.bookingsTableView.addSubview(self.refreshControl)
        self.loadBookingFor(self.marina.ID, status: "today", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        bookingsTableView.reloadData()
    }

    deinit{
        NotificationCenter.default.removeObserver(self)
    }

    @objc func bookingDidRefresh(_ notification: Notification) {
        self.bookings.removeAll()
      self.loadBookingFor(self.marina.ID, status: "today", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
        bookingsTableView.reloadData()

    }


    @IBAction func onClickSearchBarButton(_ sender: UIButton) {
        let searchBarVC = AppStoryboard.Booking.viewController(SearchBookingsViewController.self)
        searchBarVC.bookings = self.bookings
        searchBarVC.reserveArray = self.bookings
        searchBarVC.currentStatus = "today"
        searchBarVC.marina = self.marina
        self.navigationController?.pushViewController(searchBarVC, animated: true)
    }
    
    func loadBookingFor(_ marinaID:String,status:String,pageNumber:Int,recordPerPage:Int) {
        if !CommonClass.isConnectedToNetwork{
            showErrorWithMessage(warningMessage.networkIsNotConnected.rawValue)
            return         }
        self.isNewDataLoading = true
        if pageNumber == 1{
            if !CommonClass.isLoaderOnScreen{
                CommonClass.showLoader(withStatus: "Loading..")
            }
            bookings.removeAll()
            bookingsTableView.reloadData()
        }

        BookingService.sharedInstance.getBookingsForMarina(marinaID, status: status,page:pageNumber,perPage: recordPerPage) { (response) in

            self.isNewDataLoading = false
            if let newJobArray = response{
                if newJobArray.count == 0{
                    if self.pageNumber > 1{
                        self.pageNumber = self.pageNumber - 1
                    }
                }
                self.bookings.append(contentsOf: newJobArray)
                self.bookingsTableView.reloadData()
            }else{
                if self.pageNumber > 1{
                    self.pageNumber = self.pageNumber - 1
                }
            }
            if CommonClass.isLoaderOnScreen{
                CommonClass.hideLoader()
            }
        }
    }


    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        pageNumber = 1
        self.isNewDataLoading = true
        self.bookingsTableView.reloadData()
        BookingService.sharedInstance.getBookingsForMarina(self.marina.ID, status: "today",page: self.pageNumber,perPage: self.recordsPerPage) { (response) in
            self.bookings.removeAll()
            refreshControl.endRefreshing()
            self.isNewDataLoading = false
            if let newBookingArray = response{
                self.bookings.append(contentsOf: newBookingArray)
                self.bookingsTableView.reloadData()
            }
        }
    }

    

    @objc func bookingDidCancelledByUser(_ notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary<String,Booking>{
            if let removingBooking = userInfo["booking"]{
                for i in 0..<self.bookings.count{
                    if self.bookings[i].ID == removingBooking.ID{
                        self.bookings.remove(at: i)
                        self.bookingsTableView.reloadData()
                        break;
                    }
                }
            }

        }
    }


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (bookings.count > 0) ? bookings.count : 1 ;
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (bookings.count > 0) ? 82 : 250

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if bookings.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoDataCell", for: indexPath) as! NoDataCell
            cell.messageLabel.text = self.isNewDataLoading ? "Loading.." : "No Booking found\r\nPlease pull down to refresh"
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BookingForMarinaCell", for: indexPath) as! BookingForMarinaCell
            let booking = bookings[indexPath.row] //BookingCell
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.layer.cornerRadius = 5
            cell.boatImageView.layer.masksToBounds = true
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.userNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.boatNameLabel.text = (booking.boat.name == "") ? "Unnamed" : booking.boat.name
            // let dateFormatter = DateFormatter()
            cell.bookingDate.text = CommonClass.convertToShowFormatDate(dateString: booking.fromDate)+" To "+CommonClass.convertToShowFormatDate(dateString: booking.toDate)
            //cell.bookingDate.text = booking.fromDate+" To "+booking.toDate
            cell.slipPosition.text = booking.parkingSpace.title
            if booking.arrival == true {
                cell.arrivalDepartureLabel.text = "  Arriving  "
                
            }else {
                cell.arrivalDepartureLabel.text = "  Departing  "
            }
            cell.arrivalDepartureLabel.backgroundColor = UIColor(red: 20.0/255.0, green: 115.0/255.0, blue: 244.0/255.0, alpha: 1.0)
            cell.arrivalDepartureLabel.textColor = UIColor.white
            if let fromDate = self.getDateFromString(booking.fromDate){
                if let toDate = self.getDateFromString(booking.toDate){
                    let days = toDate.days(from: fromDate)
                    cell.totalNightLabel.text = (days > 1) ? "\(days)" : "\(days)"
                }else{
                    cell.totalNightLabel.text = "--"
                }
            }else{
                cell.totalNightLabel.text = "--"
            }

            return cell
        }
    }


    func getDateFromString(_ dateString: String) -> Date?
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            return date
        }else{
            return nil
        }
    }




    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if bookings.count == 0{
            return
        }
        let booking = self.bookings[indexPath.row]
        if booking.arrival == true  {
        self.navigateToArrivingBookingDetails(booking: booking)
        }else{
         self.navigateToDepartureBookingDetails(booking: booking)
        }
    }

    func navigateToArrivingBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(ArrivingViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.slip = booking.parkingSpace
        bookingDetailVC.arrivingStatus = "arrival"
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    func navigateToDepartureBookingDetails(booking: Booking) -> Void {
        let bookingDetailVC = AppStoryboard.Booking.viewController(DepartingViewController.self)
        bookingDetailVC.booking = booking
        bookingDetailVC.slip = booking.parkingSpace
        bookingDetailVC.departingStatus = "departure"
        self.navigationController?.pushViewController(bookingDetailVC, animated: true)
    }
    
    
    
    func openArrivingSlip(slip: ParkingSpace,selectedBookingDates:BookingDates) {
        let arrivingSlipVC = AppStoryboard.Booking.viewController(ArrivingViewController.self)
        arrivingSlipVC.slip = slip
        arrivingSlipVC.bookingDates = selectedBookingDates
        self.navigationController?.pushViewController(arrivingSlipVC, animated: true)
    }

    @IBAction func onClickViewTicket(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.bookingsTableView) as IndexPath?{
            let ticketVC = AppStoryboard.Home.viewController(TicketViewController.self)
            ticketVC.booking = bookings[indexPath.row]
            ticketVC.isBrowsing = true
            self.navigationController?.pushViewController(ticketVC, animated: true)
        }
    }

    @IBAction func onClickCancelTicket(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.bookingsTableView) as IndexPath?{
            let booking = bookings[indexPath.row]
            sender.isEnabled = false
            self.cancelBooking(booking, completionBlock: { (done) in
                sender.isEnabled = true
                if done{
                    if let index = self.bookings.index(of: booking){
                        self.bookings.remove(at: index)
                        self.selectedIndex = -1
                        self.bookingsTableView.reloadData()
                        showAlertWith(self, message: "Your booking has been cancelled", title: warningMessage.alertTitle.rawValue)
                    }
                }else{
                    showAlertWith(self, message: "Your booking couldn't be cancelled\r\nPlease try again", title: warningMessage.alertTitle.rawValue)
                }
            })
        }
    }

    @IBAction func onClickUpdateTicket(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.bookingsTableView) as IndexPath?{
            let ticketVC = AppStoryboard.Home.viewController(TicketViewController.self)
            ticketVC.booking = bookings[indexPath.row]
            ticketVC.isBrowsing = true
            self.navigationController?.pushViewController(ticketVC, animated: true)
        }
    }

    func cancelBooking(_ booking:Booking,completionBlock:@escaping (_ done :Bool)->Void) -> Void {
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            completionBlock(false)
            return
        }
        CommonClass.showLoader(withStatus: "Cancelling..")
        BookingService.sharedInstance.cancelBookings(with: booking.ID, userId: booking.user.ID){ (success) in
            CommonClass.hideLoader()
            completionBlock(success)
        }
    }


    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == bookingsTableView{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                if !isNewDataLoading{
                    if CommonClass.isConnectedToNetwork{
                        isNewDataLoading = true
                        pageNumber+=1
                        self.loadBookingFor(self.marina.ID, status: "today", pageNumber: self.pageNumber, recordPerPage: self.recordsPerPage)
                    }
                }
            }
        }
        
    }
    
}
