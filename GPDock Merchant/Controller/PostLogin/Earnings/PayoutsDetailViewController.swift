//
//  PayoutsDetailViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 09/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class PayoutsDetailViewController: UIViewController {
    var payouts = [Payouts]()
    @IBOutlet weak var payforGrocy: UILabel!
    @IBOutlet weak var payforGasoline: UILabel!
    @IBOutlet weak var payforOther: UILabel!
    @IBOutlet weak var payforPumpout: UILabel!
    @IBOutlet weak var grandTotal: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var customerNote: UILabel!

     //@IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var cancellButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        infoView.layer.cornerRadius = 5
        infoView.layer.masksToBounds = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickBackButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
        

}

//let value = Int(entry.yValue * 1000)
//
//if (value % 100 == 0) {
//    dec = decimalplaces - 1
//}else {
//    dec = decimalplaces
//}

