//
//  PayoutBookingDetailViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 10/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class PayoutBookingDetailViewController: UIViewController {
     var user : User!
    var booking :Booking = Booking()
     var bookingID = ""
    var marina : Marina!
@IBOutlet weak var SlipTableView: UITableView!
    let arrivingColor = UIColor(red: 238.0/255.0, green: 231.0/255.0, blue: 48.0/255.0, alpha: 1.0)
    let departingColor = UIColor(red: 248.0/255.0, green: 170.0/255.0, blue: 70.0/255.0, alpha: 1.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerCells()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        self.SlipTableView.dataSource = self
        self.SlipTableView.delegate = self
       // self.navigationItem.title = "Booking : "+self.booking.ID
         print("bookingID----->>\(bookingID)")
        self.getBookingDetail(withBookingID: self.bookingID)
        // Do any additional setup after loading the view.
    }

    
    @IBAction func onClickBack(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }
    func getBookingDetail(withBookingID bookingID: String) -> Void {
          CommonClass.showLoader(withStatus: "Loading..")
        PaymentService.sharedInstance.getBookingDetails(bookingID){ (resBooking) in
            CommonClass.hideLoader()
            if let aBooking = resBooking {
                self.booking = aBooking
                self.navigationItem.title = "Booking : "+self.booking.ID
                self.SlipTableView.reloadData()
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func registerCells() {
        self.SlipTableView.register(UINib(nibName: "BoatRepresentingCell", bundle: nil), forCellReuseIdentifier: "BoatRepresentingCell")
        self.SlipTableView.register(UINib(nibName: "CheckInCheckOutCell", bundle: nil), forCellReuseIdentifier: "CheckInCheckOutCell")
        self.SlipTableView.register(UINib(nibName: "OtherCell", bundle: nil), forCellReuseIdentifier: "OtherCell")
        self.SlipTableView.register(UINib(nibName: "SlipSizeCell", bundle: nil), forCellReuseIdentifier: "SlipSizeCell")
        self.SlipTableView.register(UINib(nibName: "SlipStatusCell", bundle: nil), forCellReuseIdentifier: "SlipStatusCell")
        self.SlipTableView.register(UINib(nibName: "SlipIDTableViewCell", bundle: nil), forCellReuseIdentifier: "SlipIDTableViewCell")
    }

    func getDateFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        var dateStr = dateString
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM-dd-YYYY"
            dateStr = dayTimePeriodFormatter.string(from:date)
        }
        return dateStr
    }

}
extension PayoutBookingDetailViewController: UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (booking.ID.trimmingCharacters(in: .whitespacesAndNewlines) == ""){
            return 3
        }else if booking.amount == 0.0{
            return 5
        }else{
            return (booking.ID.trimmingCharacters(in: .whitespacesAndNewlines) == "") ? 2 : 6
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipIDTableViewCell", for: indexPath) as! SlipIDTableViewCell
            cell.slipID.text = "Slip : "+self.booking.parkingSpace.title
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipSizeCell", for: indexPath) as! SlipSizeCell
            cell.lengthLabel.text = "\(self.booking.parkingSpace.boatSize.length)" + " ft"
            cell.widthLabel.text = "\(self.booking.parkingSpace.boatSize.width)" + " ft"
            cell.depthLabel.text = "\(self.booking.parkingSpace.boatSize.depth)" + " ft"
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SlipStatusCell", for: indexPath) as! SlipStatusCell
                cell.bgView.backgroundColor = departingColor
                cell.statusLabel.text = "Completed"
                cell.bgView.backgroundColor = kApplicationLightGrayColor
 
                
                //                cell.statusLabel.text = "Departure Today".uppercased()
                //                cell.bgView.backgroundColor = UIColor(red: 246.0/255.0, green: 171.0/255.0, blue: 81.0/255.0, alpha: 1.0)
                //            }
            //            }else{
            //                cell.statusLabel.text = "Departure Today".uppercased()
            //                cell.bgView.backgroundColor = UIColor(red: 246.0/255.0, green: 171.0/255.0, blue: 81.0/255.0, alpha: 1.0)
            //            }
            cell.calenderButton.isHidden = true
            cell.datesLabel.textColor = UIColor.darkGray
            let fromDate = self.getDateFromString(self.booking.completedAt)
            cell.datesLabel.text = "\(fromDate)"
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckInCheckOutCell", for: indexPath) as! CheckInCheckOutCell
            cell.checkInDate.text = self.getDateFromString(self.booking.fromDate)
            cell.checkOutDate.text = self.getDateFromString(self.booking.toDate)
            cell.checkInTime.text = booking.marina.checkIn
            cell.checkOutTime.text = booking.marina.checkOut
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BoatRepresentingCell", for: indexPath) as! BoatRepresentingCell
            cell.boatImageView.setIndicatorStyle(.gray)
            cell.boatImageView.setShowActivityIndicator(true)
            cell.boatImageView.sd_setImage(with: URL(string:booking.boat.image),placeholderImage:#imageLiteral(resourceName: "boatPlaceHolder"))
            cell.boatNameLabel.text = booking.boat.name
            cell.UserNameLabel.text = booking.user.firstName.capitalized+" "+booking.user.lastName.capitalized
            cell.lengthLabel.text = "\(booking.boat.boatSize.length)"
            cell.widthLabel.text = "\(booking.boat.boatSize.width)"
            cell.depthLabel.text = "\(booking.boat.boatSize.depth)"
            
            if booking.bookingType == "offline" {
                cell.bookingType.text = "Offline"
                //                cell.bookingType.textColor = UIColor.white
                //                cell.bookingType.layer.backgroundColor = UIColor.blue.cgColor
            }
            else{
                cell.bookingType.text = "Online"
                //                cell.bookingType.textColor = UIColor.green
            }
            
            
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "OtherCell", for: indexPath) as! OtherCell
            cell.otherIcon.image = #imageLiteral(resourceName: "amount_paid")
            cell.otherTitleLabel.text = "Amount Paid"
            cell.otherDetailsLabel.text = "$ "+String(format: "%0.2lf", booking.businessAmount)
            //cell.separatorHeight.constant = 1
            cell.needsUpdateConstraints()
            cell.updateConstraints()
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 0
        switch indexPath.row {
        case 0://slipID
            height = 20
        case 1://slipsize
            height = 64
        case 2: //status
            height = 50
        case 3: //checkin checkout
            height = 70
        case 4: //Boat representingcell
            height = 95
        case 5: //amount paid
            height = 64
        default:
            break
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
            //show price breakups details
            //self.showPriceBreakUpsAlert()
        }
    }
    
    
}

