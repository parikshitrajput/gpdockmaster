//
//  BankDetailViewController.swift
//  GPDock Merchant
//
//  Created by Parikshit on 24/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import BKMoneyKit
import Stripe
let accountUnit = ["Individual","Company"]

protocol BankDetailViewControllerDelegate {
    func bankDetailViewController(viewController: BankDetailViewController,didAddCard card:BankAccount)
}

class BankDetailViewController: UIViewController, UITextFieldDelegate {
    var accountPicker: UIPickerView!
    @IBOutlet weak var accountHolderName : UITextField!
    @IBOutlet weak var accountNumber : UITextField!
    @IBOutlet weak var routingNumber : UITextField!
    @IBOutlet weak var accountType : UITextField!
    @IBOutlet weak var submitButton : UIButton!
    var toolDoneButton:UIButton!
    var toolBar:UIToolbar!
    var pickerView : UIPickerView!
    var bankDetailFooter : BankDetailFooter!
    var user : User!
    var marina: Marina!
    var account = BankAccount()

     var delegate : BankDetailViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.accountHolderName.layer.borderWidth = 1
        self.accountHolderName.layer.borderColor = kApplicationLightGrayColor.cgColor
        self.accountNumber.layer.borderWidth = 1
        self.accountNumber.layer.borderColor = kApplicationLightGrayColor.cgColor
        self.routingNumber.layer.borderWidth = 1
        self.routingNumber.layer.borderColor = kApplicationLightGrayColor.cgColor
        self.accountType.layer.borderWidth = 1
        self.accountType.layer.borderColor = kApplicationLightGrayColor.cgColor
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        accountNumber.delegate = self
        routingNumber.delegate = self

        self.addDoneButtonOnKeyboard()
        self.setupPicker()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  override  func viewDidLayoutSubviews() {
        self.submitButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
    }
    
    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func setupPicker() {
        pickerView = UIPickerView()
        pickerView.showsSelectionIndicator = true
        pickerView.delegate = self
        pickerView.dataSource = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .white
        toolBar.barTintColor = kNavigationColor
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickDoneOfPickerView(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(onClickCancelOfPickerView(_:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.accountType.inputView = pickerView
        self.accountType.inputAccessoryView = toolBar
        
    }

    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0, y:0, width:self.view.frame.size.width, height:50))
        doneToolbar.barStyle = UIBarStyle.default

        doneToolbar.barTintColor = UIColor.lightGray

        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(BankDetailViewController.doneButtonAction))
        done.tintColor = UIColor.white
        done.setTitleTextAttributes([ NSAttributedStringKey.font: UIFont(name: "Raleway-SemiBold", size: 18)!], for: .normal)

        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)

        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()

        self.accountHolderName.inputAccessoryView = doneToolbar
        self.accountNumber.inputAccessoryView = doneToolbar
        self.routingNumber.inputAccessoryView = doneToolbar

    }
    @objc func doneButtonAction()
    {
        self.accountHolderName.resignFirstResponder()
        self.accountNumber.resignFirstResponder()
        self.routingNumber.resignFirstResponder()

    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func clickOnSubmitButton(_ sender: UIButton) {
        self.view.endEditing(true)

        let accountHolderName = self.accountHolderName.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let accountType = self.accountType.text
        let routingNumber = self.routingNumber.text
        let accountNumber = self.accountNumber.text
        
        let inputValidation = self.validteAccountInputs(accountHolderName!, accountNumber: accountNumber!, routingNumber: routingNumber!, routingType: self.accountType.text!)
         if !inputValidation.success {
            showAlertWith(self, message: inputValidation.message, title: warningMessage.alertTitle.rawValue)
//            showAlertWith(self, message: "Please enter a valid card details to proceed the booking process", title: "Invalid Card!")
            
            return
        }
        self.createTokenAndProceedBookingWith(accountHolderName!, accountNumber: accountNumber!, routingNumber: routingNumber!, accountType: accountType!, country: "US", currrency: "usd")
        
        
    }


    func createTokenAndProceedBookingWith(_ accountHolderName:String,accountNumber:String,routingNumber: String,accountType:String, country: String, currrency: String){
        CommonClass.showLoader(withStatus: "Adding..")
        
        let accountParams = STPBankAccountParams()
        accountParams.accountNumber = accountNumber
        accountParams.accountHolderName = accountHolderName
        accountParams.routingNumber = routingNumber
        accountParams.country = country
        accountParams.currency = currrency
        if accountType == "Individual" {
            accountParams.accountHolderType = .individual
        }
        else{
            accountParams.accountHolderType = .company
        }
        
        STPAPIClient.shared().createToken(withBankAccount: accountParams) { (stpToken, error) in
            if error != nil{
                CommonClass.hideLoader()
               showAlertWith(self, message: "\(String(describing: error!.localizedDescription))", title: warningMessage.alertTitle.rawValue)
                return
            }else{
                guard let token = stpToken else{
                    CommonClass.hideLoader()
                    showAlertWith(self, message: "Blocked by account!", title: warningMessage.alertTitle.rawValue)
                    return
                }
                //print("\(token.tokenId)")
                self.addBankFor(self.user.ID, sourceToken: token.tokenId)
            }
            
        }
    }
    
    func addBankFor(_ userID: String,sourceToken:String){
        PaymentService.sharedInstance.addBanksForUser(userID, sourceToken: sourceToken){ (success, resAccount,message)  in
            CommonClass.hideLoader()
               if success{
            if let aCard = resAccount{
                self.delegate?.bankDetailViewController(viewController: self, didAddCard: aCard)

            }
            else {
                showAlertWith(self, message:message, title: warningMessage.alertTitle.rawValue)
            }
               }else{
               showAlertWith(self, message:message, title: warningMessage.alertTitle.rawValue)
            }
        }
    }
    
    @objc func onClickDoneOfPickerView(_ sender: UIBarButtonItem) {
        let row = self.pickerView.selectedRow(inComponent: 0)
        let unitSelected = accountUnit[row]
        self.accountType.text = unitSelected
        self.accountType.resignFirstResponder()
    }
    
    //cancel inputView handling
     @objc func onClickCancelOfPickerView(_ sender: UIBarButtonItem) {
        self.accountType.resignFirstResponder()
    }
    

    func validteAccountInputs(_ accountHolderName:String,accountNumber:String,routingNumber: String, routingType: String) -> (success:Bool,message:String) {
        if accountHolderName.count == 0{
            return(false, "Please enter the account holder's name")
        }
        if routingType.count == 0{
            return(false, "Please select an account type")
        }
        if routingNumber.count == 0{
            return(false, "Please enter the routing number")
        }
        if routingNumber.count < 9{
            return(false, "Please enter a valid routing number")
        }
        if accountNumber.count == 0{
            return(false, "Please enter the account number")
        }
        if accountNumber.count < 12{
            return(false, "Please enter a valid account number")
        }


        return(true,"")
    }
}

extension BankDetailViewController: UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountUnit.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return accountUnit[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let title = accountUnit[row]
        self.accountType.text = title
    }
}


