 //
//  PriceSettingViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 29/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import BKMoneyKit

class PriceSettingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    var headerHeight:CGFloat = 84
    @IBOutlet weak var settingsTableView : UITableView!
    var marina : Marina!
    var isDefaultPriceForWeekEnds = false
    var isAllSpecialSame = false
    var  holidayPriceHeader : HolidayPriceHeader!
    var marinaPriceModel = MarinaPriceModel()
    var uncommittedPrices = Array<DayPrice>()
    var committedPrices = Array<DayPrice>()
    var isEditted : Bool = false
    var WeekendType: String = ""
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(PriceSettingViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        self.marinaPriceModel.regularPrice = self.marina.pricePerFeet
        self.marinaPriceModel.weekendPrice.price = self.marina.pricePerFeet
        self.registerCells()
        self.settingsTableView.dataSource = self
        self.settingsTableView.delegate = self
        //self.settingsTableView.addSubview(refreshControl)
        self.loadMarinaPrices(marinaID: self.marina.ID)
        self.settingsTableView.reloadData()
    }

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        //self.view.endEditing(true)
        self.marinaPriceModel.specialDays.removeAll()
        self.settingsTableView.reloadData()
        self.loadMarinaPrices(marinaID: self.marina.ID)
    }
    func loadMarinaPrices(marinaID: String){
        MarinaService.sharedInstance.getMarinaPrices(by: marinaID) { (success, resPriceModel, message) in
            if self.refreshControl.isRefreshing {self.refreshControl.endRefreshing()}
            if success{
                if let _priceModel = resPriceModel{
                    self.marinaPriceModel = _priceModel

                    self.uncommittedPrices.removeAll()
                    self.committedPrices = self.marinaPriceModel.specialDays
                }
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
            self.settingsTableView.reloadData()
        }
    }

    func registerCells() {
        self.settingsTableView.register(UINib(nibName: "EditDefaultChargeCell", bundle: nil), forCellReuseIdentifier: "EditDefaultChargeCell")
        self.settingsTableView.register(UINib(nibName: "EditSpecialDayChargeCell", bundle: nil), forCellReuseIdentifier: "EditSpecialDayChargeCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.view.endEditing(true)
        self.navigationController?.pop(true)
    }

    @IBAction func onClickSaveSetting(_ sender: UIBarButtonItem){
        self.view.endEditing(true)
        if !CommonClass.isConnectedToNetwork{
            showAlertWith(self, message: warningMessage.networkIsNotConnected.rawValue, title: warningMessage.alertTitle.rawValue)
            return
        }
        if self.marinaPriceModel.regularPrice < 0.5 {
            showAlertWith(self, message: "Default price can't set less than 0.5", title: warningMessage.alertTitle.rawValue)
            return
        }
        
        if isDefaultPriceForWeekEnds {
            if self.marinaPriceModel.regularPrice < 0.5 {
                showAlertWith(self, message: "Weekend price can't set less than 0.5", title: warningMessage.alertTitle.rawValue)
                return
            }
        }else{
            if self.marinaPriceModel.weekendPrice.price < 0.5 {
                showAlertWith(self, message: "Weekend price can't set less than 0.5", title: warningMessage.alertTitle.rawValue)
                return
            }
        }
        
        self.saveSettings(prices: self.marinaPriceModel.specialDays)
    }
    
    
    func saveSettings(prices : Array<DayPrice>) {
        CommonClass.showLoader(withStatus: "Saving..")
        var params = Dictionary<String,Double>()

        for uncommitedPrice in prices{
            
            if uncommitedPrice.price > 0.0{
                params.updateValue(isAllSpecialSame ? prices[0].price : uncommitedPrice.price , forKey: uncommitedPrice.date)
            }
        }

        MarinaService.sharedInstance.setMarinaPrices(for: self.marina.ID, regularPrice: self.marinaPriceModel.regularPrice, weekendPrice: isDefaultPriceForWeekEnds ? self.marinaPriceModel.regularPrice : self.marinaPriceModel.weekendPrice.price,weekendType: self.WeekendType ,specialPrice: params) { (success, resPriceModel, message) in
            CommonClass.hideLoader()
            if success{
                if let priceModel = resPriceModel{
                    print("price:>\(priceModel.weekendPrice.price)")
                    self.marinaPriceModel = priceModel
                    self.uncommittedPrices.removeAll()
                    self.committedPrices = self.marinaPriceModel.specialDays
                }else{
                    showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
                }
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
            self.settingsTableView.reloadData()
        }
    }
    
    @IBAction func onClickToggleDefaultPrice(_ sender: UIButton){
        self.view.endEditing(true)
        sender.isSelected = !sender.isSelected
        self.isDefaultPriceForWeekEnds = !self.isDefaultPriceForWeekEnds
        settingsTableView.reloadData()
    }
    

    @IBAction func onClickToggleSameForAllSpecialDay(_ sender: UIButton){
        self.view.endEditing(true)
        sender.isSelected = !sender.isSelected
        self.isAllSpecialSame = !self.isAllSpecialSame
        settingsTableView.reloadData()
    }

    @IBAction func onClickOpenCalander(_ sender: UIButton) {
        self.view.endEditing(true)
        let calendarVC = AppStoryboard.Earning.viewController(PriceCalendarViewController.self)
        calendarVC.specialPrices = self.committedPrices
        calendarVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        calendarVC.delegate = self
        let nav = UINavigationController(rootViewController: calendarVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    

    

    
    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 120
            
        }else {
            return 50
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? 1 : self.marinaPriceModel.specialDays.count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return nil
        }else{
            if self.holidayPriceHeader != nil{
                self.holidayPriceHeader.frame = CGRect(x: 0, y:0, width: tableView.frame.size.width, height: headerHeight)
                self.holidayPriceHeader.setNeedsLayout()
                self.holidayPriceHeader.layoutIfNeeded()
            }else{
                self.holidayPriceHeader = HolidayPriceHeader.instanceFromNib()
                self.holidayPriceHeader.frame = CGRect(x: 0, y:0, width: tableView.frame.size.width, height: headerHeight)
                self.holidayPriceHeader.setNeedsLayout()
                self.holidayPriceHeader.layoutIfNeeded()
            }

            self.holidayPriceHeader.selectDateButton.addTarget(self, action: #selector(onClickOpenCalander(_:)), for: .touchUpInside)

            return self.holidayPriceHeader
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  (section == 0) ? 0 : headerHeight
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditDefaultChargeCell", for: indexPath) as! EditDefaultChargeCell
            cell.toggleWeekendPriceButton.isSelected = self.isDefaultPriceForWeekEnds
            cell.weekendDaySwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
            //cell.weekendDaySwitch.setOn(false, animated: true)
            if self.marinaPriceModel.weekendPrice.weekendType == "weekend1" {
//            if self.WeekendType == "weekend1" {
                //cell.weekendDaySwitch.setOn(true, animated: true)
                cell.weekendDaySwitch.isOn = true
                cell.weekendDaySwitch.onTintColor = kNavigationColor
            cell.weekendDayLabel.text = "Weekend Day(Friday, Saturday, Sunday)"
            }else{
                cell.weekendDaySwitch.isOn = false
                cell.weekendDaySwitch.onTintColor = kApplicationLightGrayColor
             cell.weekendDayLabel.text = "Weekend Day(Saturday, Sunday)"
            }
            self.WeekendType = self.marinaPriceModel.weekendPrice.weekendType
            cell.weekendDaySwitch.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)
            cell.regularPriceTextField.numberValue = NSDecimalNumber(value: marinaPriceModel.regularPrice)
            cell.weekendPriceTextField.numberValue =  isDefaultPriceForWeekEnds ? NSDecimalNumber(value: marinaPriceModel.regularPrice) : NSDecimalNumber(value: marinaPriceModel.weekendPrice.price)
            cell.toggleWeekendPriceButton.addTarget(self, action: #selector(onClickToggleDefaultPrice(_:)), for: .touchUpInside)
            cell.regularPriceTextField.delegate = self
            cell.weekendPriceTextField.delegate = self
            cell.regularPriceTextField.addTarget(self, action: #selector(textFieldTextDidChange(_:)), for: .editingChanged)
            cell.weekendPriceTextField.addTarget(self, action: #selector(textFieldTextDidChange(_:)), for: .editingChanged)
            return cell

        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EditSpecialDayChargeCell", for: indexPath) as! EditSpecialDayChargeCell
            cell.dateLabel.text = self.getDateStringFromString(marinaPriceModel.specialDays[indexPath.row].date)
            cell.priceTextField.numberValue = isAllSpecialSame ? NSDecimalNumber(value:marinaPriceModel.specialDays[0].price) : NSDecimalNumber(value: marinaPriceModel.specialDays[indexPath.row].price)
            if  cell.priceTextField.numberValue == nil {
                cell.deleteButton.isHidden = true
            }else{
                cell.deleteButton.isHidden = false
                
                
            cell.deleteButton.addTarget(self, action: #selector(onClickRemovePriceButton(_:)), for: .touchUpInside)
            }
            cell.priceTextField.delegate = self
            cell.priceTextField.addTarget(self, action: #selector(textFieldTextDidChange(_:)), for: .editingChanged)
            return cell

        }
    }
    
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        if indexPath.section == 0{
//            return false
//        }else{
//          return true
//        }
//    }
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//
////        marinaPriceModel.specialDays.remove(at: indexPath.row)
////        tableView.deleteRows(at: [indexPath], with: .fade)
//
//        //self.marinaPriceModel.specialDays =  committedPrices
//        //let date = self.getDateStringFromString(marinaPriceModel.specialDays[indexPath.row].date)
//        if indexPath.section != 0 {
//        let fromDate = marinaPriceModel.specialDays[indexPath.row].date
//        print("Date--->\(fromDate)")
//        self.removePriseList(self.marina.ID, date: fromDate, indexPath: indexPath)
//       self.committedPrices = marinaPriceModel.specialDays
//        self.settingsTableView.reloadData()
//        }
//
//
//    }
    
    @objc func switchValueDidChange(_ sender: UISwitch) {
        if let indexPath = sender.tableViewIndexPath(self.settingsTableView) as IndexPath?{
            if indexPath.section == 0 {
            let cell = settingsTableView.cellForRow(at: indexPath) as? EditDefaultChargeCell
        if (sender.isOn == true){
            cell?.weekendDaySwitch.onTintColor = kNavigationColor
            print("Weekend Day(Friday, Saturday, Sunday)")
            cell?.weekendDayLabel.text = "Weekend Day(Friday, Saturday, Sunday)"
            self.WeekendType = "weekend1"
        }
        else{
            cell?.weekendDaySwitch.onTintColor = kApplicationLightGrayColor
            print("Weekend Day(Saturday, Sunday)")
            cell?.weekendDayLabel.text = "Weekend Day(Saturday, Sunday)"
            self.WeekendType = "weekend2"

        }
            }
        }
    }
    
    
    @IBAction func onClickRemovePriceButton(_ sender: UIButton){
        
        if let indexPath = sender.tableViewIndexPath(self.settingsTableView) as IndexPath?{
            let fromDate = marinaPriceModel.specialDays[indexPath.row].date
            self.removePriseList(self.marina.ID, date: fromDate, indexPath: indexPath)
            self.committedPrices = marinaPriceModel.specialDays
            self.settingsTableView.reloadData()
        }

    }
    func removePriseList(_ marinaID:String,date:String,indexPath: IndexPath) -> Void {
        MarinaService.sharedInstance.removeDateWisePriceList(self.marina.ID, date: date) { (success, message) in
            if success {
                               self.marinaPriceModel.specialDays.remove(at: indexPath.row)
                                self.committedPrices.remove(at: indexPath.row)
                                self.settingsTableView.deleteRows(at: [indexPath], with: .automatic)
            }
            
            
        }
    }
    func dateStringFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        dayTimePeriodFormatter.dateFormat = "MM/dd/YYYY"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
            return dayTimePeriodFormatter.string(from: date)
        }else{
            return ""
        }
    }


}

extension PriceSettingViewController: UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.settingsTableView) as IndexPath?{
            switch indexPath.section{
            case 0:
                if let cell = settingsTableView.cellForRow(at: indexPath) as? EditDefaultChargeCell{
                    if let iTxtFiled = textField as? BKCurrencyTextField{
                        if iTxtFiled == cell.regularPriceTextField{
                            if iTxtFiled.numberValue != nil  {
                                self.marinaPriceModel.regularPrice  = iTxtFiled.numberValue.doubleValue
                            }else{
                                self.marinaPriceModel.regularPrice = 0.0
                            }
                        }else{
                            if iTxtFiled.numberValue != nil{
                                self.marinaPriceModel.weekendPrice.price  = iTxtFiled.numberValue.doubleValue
                            }else{
                                self.marinaPriceModel.weekendPrice.price = 0.0
                            }
                        }
                    }
                }
            case 1:
                if let iTxtFiled = textField as? BKCurrencyTextField{
                    if iTxtFiled.numberValue != nil{
                        self.marinaPriceModel.specialDays[indexPath.row].price = iTxtFiled.numberValue.doubleValue
                    }else{
                        self.marinaPriceModel.specialDays[indexPath.row].price = 0.0
                    }
                }
            default:
                break
            }
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let indexPath = textField.tableViewIndexPath(self.settingsTableView) as IndexPath?{
            switch indexPath.section{
            case 0:
                if let cell = settingsTableView.cellForRow(at: indexPath) as? EditDefaultChargeCell{
                    if let iTxtFiled = textField as? BKCurrencyTextField{
                        if iTxtFiled == cell.regularPriceTextField{
                            if iTxtFiled.numberValue != nil{
                                self.marinaPriceModel.regularPrice  = iTxtFiled.numberValue.doubleValue
                            }else{
                                self.marinaPriceModel.regularPrice = 0.0
                            }
                        }else{
                            if iTxtFiled.numberValue != nil{
                                self.marinaPriceModel.weekendPrice.price  = iTxtFiled.numberValue.doubleValue
                            }else{
                                self.marinaPriceModel.weekendPrice.price = 0.0
                            }
                        }
                    }
                }
            case 1:
                if let iTxtFiled = textField as? BKCurrencyTextField{
                    if iTxtFiled.text == "" {
                    showAlertWith(self, message: "Special price can't set less than 0.5" , title: warningMessage.alertTitle.rawValue)
                        self.marinaPriceModel.specialDays[indexPath.row].price = 0.0
                        self.settingsTableView.reloadData()
                        return
                    }

                    if iTxtFiled.numberValue != nil{
                        if (iTxtFiled.numberValue.doubleValue < 0.5){
                            showAlertWith(self, message: "Special price can't set less than 0.5" , title: warningMessage.alertTitle.rawValue)
                            self.marinaPriceModel.specialDays[indexPath.row].price = 0.0
                            self.settingsTableView.reloadData()
                            return
                        }

                        self.marinaPriceModel.specialDays[indexPath.row].price = iTxtFiled.numberValue.doubleValue
                    }else{
                        self.marinaPriceModel.specialDays[indexPath.row].price = 0.0
                    }
                }
            default:
                break
            }
        }
    }


    @IBAction func textFieldTextDidChange(_ textField: UITextField){
        if let indexPath = textField.tableViewIndexPath(self.settingsTableView) as IndexPath?{
            switch indexPath.section{
            case 0:
                if let cell = settingsTableView.cellForRow(at: indexPath) as? EditDefaultChargeCell{
                    if let iTxtFiled = textField as? BKCurrencyTextField{
                        if iTxtFiled == cell.regularPriceTextField{
                            if iTxtFiled.numberValue != nil{
                                self.marinaPriceModel.regularPrice  = iTxtFiled.numberValue.doubleValue
                            }else{
                                self.marinaPriceModel.regularPrice = 0.0
                            }
                        }else{
                            if iTxtFiled.numberValue != nil{
                                self.marinaPriceModel.weekendPrice.price  = iTxtFiled.numberValue.doubleValue
                            }else{
                                self.marinaPriceModel.weekendPrice.price = 0.0
                            }
                        }
                    }
                }
            case 1:
                if let iTxtFiled = textField as? BKCurrencyTextField{
                    if iTxtFiled.numberValue != nil{
                        self.marinaPriceModel.specialDays[indexPath.row].price = iTxtFiled.numberValue.doubleValue

                    }else{
                        self.marinaPriceModel.specialDays[indexPath.row].price = 0.0

                    }
                }
            default:
                break
            }
        }
    }
}




extension PriceSettingViewController: PriceCalendarViewControllerDelegate{
    func getDateStringFromString(_ dateString: String) -> String
    {
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            dayTimePeriodFormatter.dateFormat = "MM/dd/YYYY"
            return dayTimePeriodFormatter.string(from: date)
        }else{
            return ""
        }
    }

    func priceCalendarViewController(viewController: PriceCalendarViewController, didSelectDates selectedDates: Array<String>) {
        self.uncommittedPrices.removeAll()
        for date in selectedDates{
            let datePrice = DayPrice(date: date, price: 0.0)
            self.uncommittedPrices.append(datePrice)
        }
        self.marinaPriceModel.specialDays.append(contentsOf: self.uncommittedPrices)
        self.settingsTableView.reloadData()
    }
}




