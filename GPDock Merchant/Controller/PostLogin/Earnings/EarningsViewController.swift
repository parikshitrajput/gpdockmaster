//
//  ViewController.swift
//  graphview_example_ib
//

import UIKit
//import Charts

//import ScrollableGraphView

//enum GraphType:String {
//    case today = "day"
//    case weekly = "week"
//    case monthly = "month"
//    case yearly = "year"
//    case range = ""
//}

enum GraphType:String {
    case today = "day"
    case weekly = "week"
    case monthly = "month"
    case yearly = "year"
    case range = ""
    func barSpace() -> CGFloat {
        var space : CGFloat = 0.0
        switch self {
        case .today:
            space = 40
        case .weekly:
            space = 25
        case .monthly:
            space = 35
        case .yearly:
            space = 25
        case .range:
            space = 35
        }
        return space
    }
}


class EarningsViewController: UIViewController  {
    func dataValue(atIndex pointIndex: Int) -> Double {
        return Double(self.data[pointIndex])
    
    }

    let linePlotingIndentifier = "line"
    let dotPlotingIndentifier = "dot"
    var dayArray = [[String]]()
    var hourArray = [[String]]()
    var arrayList = [[String]]()
    var marina : Marina!
    var user : User!
    var decimalPlaces = 0
    lazy var data: [Double] = []
    lazy var dataOccupancy: [Double] = []
    lazy var labels: [String] = []
    var graphConstraints = [NSLayoutConstraint]()
    var graphView: NKBarChart!
    @IBOutlet var graphContainner: UIView!
    @IBOutlet weak var TotalValueView: UIView!
    @IBOutlet weak var TotalValueLabel: UILabel!

    var selectedDates: BookingDates!
    @IBOutlet weak var todayButton: UIButton!
    @IBOutlet weak var weekButton: UIButton!
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var yearButton: UIButton!
    @IBOutlet weak var RangeButton: UIButton!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var bookingGraphButton: UIButton!
    @IBOutlet weak var occupancyGraphButton: UIButton!
     @IBOutlet weak var graphRepresentView: UIView!
    @IBOutlet weak var badgeLabel: UILabel!

    var isBookingsGraphPresent = false
    var isOccupancyGraphPresent = false
    var hourArray1 = [[String]]()
    var selectedGraphType = GraphType.monthly
    let selectedColor = UIColor(red:210.0/255.0, green:210.0/255.0, blue:210.0/255, alpha:1.0)
    override func viewDidLoad() {
        super.viewDidLoad()
        let userJSON = User.loadUserInfo()
        self.user = User(json: userJSON)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        self.isBookingsGraphPresent = true
        self.toggleSelection(self.monthButton)
        bookingGraphButton.backgroundColor = kApplicationLightGrayColor
       // bookingGraphButton.setTitleColor(.white, for: UIControlState())
        occupancyGraphButton.backgroundColor = UIColor.white
       // occupancyGraphButton.setTitleColor(.white, for: UIControlState())
       self.badgeLabel.text = "Bookings"
        self.createAndSetupGraph(self.graphContainner.frame)
       self.loadEarningDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue,fromDate:"", toDate:"")

    }
    
    
    @IBAction func onClickOfPayouts(_ sender: UIButton){
        let payoutsVC = AppStoryboard.Earning.viewController(PayoutsViewController.self)
        self.navigationController?.pushViewController(payoutsVC, animated: true)
    }

    @IBAction func onClickOfGateway(_ sender: UIButton){
        let gatewayVC = AppStoryboard.Earning.viewController(GatewayViewController.self)
        self.navigationController?.pushViewController(gatewayVC, animated: true)
    }
    @IBAction func onClickOfEditCharges(_ sender: UIButton){
        self.getMarinaPriceType(marinaID: self.marina.ID)

//        if marina.priceType == graduallyPriceType {
//            let slipPricingVC = AppStoryboard.PriceSetup.viewController(SetUpPriceViewController.self)
//            slipPricingVC.marina = marina
//            self.navigationController?.pushViewController(slipPricingVC, animated: true)
//        }else{
//            let slipPricingVC = AppStoryboard.PriceSetup.viewController(NormalPriceListViewController.self)
//            slipPricingVC.marina = marina
//            self.navigationController?.pushViewController(slipPricingVC, animated: true)
//        }

    }
    @IBAction func onClickOfBookingGraph(_ sender: UIButton){
        self.decimalPlaces = 0
        self.isOccupancyGraphPresent = false
        self.isBookingsGraphPresent = true
        self.badgeLabel.text = "Bookings"

        if self.selectedGraphType == .today{
            self.toggleSelection(self.todayButton)

        }
        if self.selectedGraphType == .weekly{
            self.toggleSelection(self.weekButton)
            
        }
        if self.selectedGraphType == .monthly{
            self.toggleSelection(self.monthButton)
            
        }
        if self.selectedGraphType == .yearly{
            self.toggleSelection(self.yearButton)
            
        }
        if self.selectedGraphType == .range{
            self.toggleSelection(self.RangeButton)
            self.onClickSelectDateButton()

        }
       //self.toggleSelection(self.monthButton)
          //self.selectedGraphType = .monthly
        bookingGraphButton.backgroundColor = kApplicationLightGrayColor
        //bookingGraphButton.setTitleColor(.white, for: UIControlState())
        occupancyGraphButton.backgroundColor = UIColor.white
        //occupancyGraphButton.setTitleColor(.white, for: UIControlState())

        self.loadEarningDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue, fromDate: "", toDate: "")
        
    }
    @IBAction func onClickOfOccupancyGraph(_ sender: UIButton){
        self.decimalPlaces = 2
        self.badgeLabel.text = "Occupancy"

        self.isBookingsGraphPresent = false
        self.isOccupancyGraphPresent = true
        if self.selectedGraphType == .today{
            self.toggleSelection(self.todayButton)
            
        }
        if self.selectedGraphType == .weekly{
            self.toggleSelection(self.weekButton)
            
        }
        if self.selectedGraphType == .monthly{
            self.toggleSelection(self.monthButton)
            
        }
        if self.selectedGraphType == .yearly{
            self.toggleSelection(self.yearButton)
            
        }
        if self.selectedGraphType == .range{
            self.toggleSelection(self.RangeButton)
            self.onClickSelectDateButton()

            
        }
//        self.toggleSelection(self.monthButton)
//        self.selectedGraphType = .monthly
        occupancyGraphButton.backgroundColor = kApplicationLightGrayColor
        //occupancyGraphButton.setTitleColor(.white, for: UIControlState())
        bookingGraphButton.backgroundColor = UIColor.white
        //bookingGraphButton.setTitleColor(.white, for: UIControlState())
        self.loadOccupancyDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue, fromDate: "", toDate: "")

    }
    @IBAction func onClickOfTodayButton(_ sender: UIButton){
        self.toggleSelection(sender)
        self.selectedGraphType = .today
        if self.isBookingsGraphPresent == true {
        self.loadEarningDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue,fromDate:"", toDate:"")
        }else{
         self.loadOccupancyDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue, fromDate: "", toDate: "")
        }
        
    }
    @IBAction func onClickOfWeekButton(_ sender: UIButton){
        self.toggleSelection(sender)
        self.selectedGraphType = .weekly
        if self.isBookingsGraphPresent == true{
        self.loadEarningDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue,fromDate:"", toDate:"")
        }else{
        self.loadOccupancyDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue, fromDate: "", toDate: "")
        }
    }
    @IBAction func onClickOfMonthButton(_ sender: UIButton){
        self.toggleSelection(sender)
        self.selectedGraphType = .monthly
        if self.isBookingsGraphPresent == true {
        self.loadEarningDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue,fromDate:"", toDate:"")
        }else{
        self.loadOccupancyDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue, fromDate: "", toDate: "")
        }

    }
    @IBAction func onClickOfYearButton(_ sender: UIButton){
        //self.toggleSelectedButton(button: yearButton)
        self.toggleSelection(sender)
        self.selectedGraphType = .yearly
        if self.isBookingsGraphPresent == true {
        
        self.loadEarningDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue,fromDate:"", toDate:"")
        }else{
        self.loadOccupancyDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: self.selectedGraphType.rawValue, fromDate: "", toDate: "")
        }
    }
    
    func toggleSelection(_ button: UIButton){
        if button == todayButton {
            self.setButtonSelected(todayButton)
            self.setButtonDeselected(monthButton)
            self.setButtonDeselected(yearButton)
            self.setButtonDeselected(weekButton)
            self.setButtonDeselected(RangeButton)
        }else  if button == monthButton {
            self.setButtonSelected(monthButton)
            self.setButtonDeselected(todayButton)
            self.setButtonDeselected(yearButton)
            self.setButtonDeselected(weekButton)
            self.setButtonDeselected(RangeButton)
        }else  if button == yearButton {
            self.setButtonSelected(yearButton)
            self.setButtonDeselected(todayButton)
            self.setButtonDeselected(monthButton)
            self.setButtonDeselected(weekButton)
            self.setButtonDeselected(RangeButton)
        }else  if button == weekButton {
            self.setButtonSelected(weekButton)
            self.setButtonDeselected(todayButton)
            self.setButtonDeselected(yearButton)
            self.setButtonDeselected(monthButton)
            self.setButtonDeselected(RangeButton)
        }else  if button == RangeButton {
            self.setButtonSelected(RangeButton)
            self.setButtonDeselected(todayButton)
            self.setButtonDeselected(yearButton)
            self.setButtonDeselected(weekButton)
            self.setButtonDeselected(monthButton)
        }
    }
    
    func setButtonSelected(_ button: UIButton){
        button.backgroundColor = kNavigationColor
        button.isSelected = true
        button.setTitleColor(.white, for: UIControlState())
    }
    func setButtonDeselected(_ button: UIButton){
        button.backgroundColor = selectedColor
        button.isSelected = false
        button.setTitleColor(.white, for: UIControlState())
    }
    
    @IBAction func onClickOfRangeButton(_ sender: UIButton){
        self.TotalValueView.isHidden = true
        self.toggleSelection(sender)
        self.selectedGraphType = .range
        self.onClickSelectDateButton()
    }
    
    
    
    
    func onClickSelectDateButton(){
        let calendarVC = AppStoryboard.Home.viewController(NKCalenderViewController.self)
        calendarVC.selectedBookingDate = self.selectedDates
        calendarVC.minDate = Calendar(identifier: .iso8601).date(byAdding: .year, value: -1, to: Date())
        calendarVC.maxDate = Date()
        calendarVC.delegate = self
        calendarVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        let nav = UINavigationController(rootViewController: calendarVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }
    private func setupConstraints() {
        self.graphView.translatesAutoresizingMaskIntoConstraints = false
        graphConstraints.removeAll()

        let topConstraint = NSLayoutConstraint(item: self.graphView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.graphContainner, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        let rightConstraint = NSLayoutConstraint(item: self.graphView, attribute: NSLayoutAttribute.right, relatedBy: NSLayoutRelation.equal, toItem: self.graphContainner, attribute: NSLayoutAttribute.right, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: self.graphView, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: self.graphContainner, attribute: NSLayoutAttribute.bottom, multiplier: 1, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: self.graphView, attribute: NSLayoutAttribute.left, relatedBy: NSLayoutRelation.equal, toItem: self.graphContainner, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0)
        //let heightConstraint = NSLayoutConstraint(item: self.graphView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 0)

        graphConstraints.append(topConstraint)
        graphConstraints.append(bottomConstraint)
        graphConstraints.append(leftConstraint)
        graphConstraints.append(rightConstraint)
        //graphConstraints.append(heightConstraint)
        self.view.addConstraints(graphConstraints)
    }

   
    func createAndSetupGraph(_ frame: CGRect){
        if self.graphView == nil{
            graphView = NKBarChart(frame: frame)
        }else{
            graphView.removeFromSuperview()
            graphView = NKBarChart(frame: frame)
        }
        graphView.decimalplaces = self.decimalPlaces
        self.graphContainner.addSubview(graphView)
        self.setupConstraints()
        self.setupBarChart(yValues:self.data, labels: self.labels)
        if todayButton.isSelected == true{
                       self.TotalValueView.isHidden = true
                    }else if weekButton.isSelected == true{
                     self.TotalValueView.isHidden = true
                    }else if monthButton.isSelected == true{
                        self.TotalValueView.isHidden = true
            
                    }else if yearButton.isSelected == true{
                        self.TotalValueView.isHidden = false
                        let multiples = data
                        let sum = multiples.reduce(0, +)
                        self.TotalValueLabel.text = String(format: "%0.0f",sum)
                        self.view.bringSubview(toFront: self.TotalValueView)
                    }else{
                        self.TotalValueView.isHidden = true
            
                    }
    }

   
//    func createAndSetupGraph(_ frame: CGRect){
//
//        if self.graphView == nil{
//            graphView = JYGraphView(frame: frame)
//        }else{
//                        graphView.removeFromSuperview()
//            graphView = JYGraphView(frame: frame)
//        }
//
//        // Set the data for the graph
//        graphView.graphData = data
//        graphView.graphDataLabels = labels
//
//        graphView.backgroundViewColor = UIColor.clear//colorFromHex(hexString: "#f7f7f9")
//        graphView.strokeColor = UIColor.darkGray
//        graphView.useCurvedLine = true
//        if todayButton.isSelected == true{
//            graphView.graphWidth = UInt(100 * data.count)
//           self.TotalValueView.isHidden = true
//        }else if weekButton.isSelected == true{
//            graphView.graphWidth = UInt(100 * data.count)
//
//        }else if monthButton.isSelected == true{
//            graphView.graphWidth = UInt(100 * data.count)
//            self.TotalValueView.isHidden = true
//
//        }else if yearButton.isSelected == true{
//            graphView.graphWidth = UInt(100 * data.count)
//            self.TotalValueView.isHidden = false
//            let multiples = data
//            let sum = multiples.reduce(0, +)
//            self.TotalValueLabel.text = String(format: "%0.0f",sum)
//            self.view.bringSubview(toFront: self.TotalValueView)
//        }else{
//            let totalDate = self.selectedDates.totalDays
//            graphView.graphWidth = 100 * totalDate
//            self.TotalValueView.isHidden = true
//
//        }
//
//
//        graphView.labelBackgroundColor = UIColor.clear
//        graphView.pointFillColor = UIColor.white
//        graphView.barColor = UIColor.clear
//        graphView.labelFont = UIFont(name: "AvenieNextCondensed-Regular", size: 15)
//        graphView.labelFontColor = UIColor.darkGray
//        self.graphContainner.addSubview(graphView)
//        self.setupConstraints()
//
//
//
//    }
}

extension EarningsViewController{

    func getDeviceTupple(_ key: String,value:AnyObject,day:Int)-> (key:String,value:AnyObject){
        let sHH = Int(key) ?? 0
        let newKey = (sHH < day) ? (day-sHH) : (day+24-sHH)
        return (key:"\(newKey)",value:value)
    }
    
    func mapWithDeviceData(graphData:Dictionary<String,AnyObject>,day:Int) -> Dictionary<String,AnyObject> {
        var mappedData = Dictionary<String,AnyObject>()
        for (key,value) in graphData{
            let mappedTupple = self.getDeviceTupple(key, value: value, day: day)
            mappedData.updateValue(mappedTupple.value, forKey: mappedTupple.key)
        }
        return mappedData
    }
    
    func loadOccupancyDataForGraphicalRepresentation(marinaID: String,keywoard: String,fromDate:String, toDate: String){
        CommonClass.showLoader(withStatus: "Loading..")
        
        PaymentService.sharedInstance.earningGraphForMarina(forMarina: marinaID, usingFilter: self.selectedGraphType.rawValue, fromDate:fromDate, toDate:toDate){ (graphDataDict,occuracyResult) in
            
            if let graphData = occuracyResult as? Dictionary<String,AnyObject>{
                self.labels.removeAll()
                self.data.removeAll()
                
                if self.selectedGraphType == .monthly{
                    
                    let myArrayOfTuples = graphData.sorted{
                        guard let d1 = $0.key.shortDateUS, let d2 = $1.key.shortDateUS else { return false }
                        return d2 > d1
                    }
                    for tuple in myArrayOfTuples {
                        self.labels.append(self.getDateFromString(tuple.key))
                        if let doubleV = tuple.value as? Double{
                            self.data.append(doubleV)
                        }else{
                            self.data.append(0.0)
                        }
                        
                    }
                    
                }else if self.selectedGraphType == .range{
                    let myArrayOfTuples = graphData.sorted{
                        guard let d1 = $0.key.shortDateUSMonth, let d2 = $1.key.shortDateUSMonth else { return false }
                        return d2 > d1
                    }
                    
                    for tuple in myArrayOfTuples {
                        self.labels.append(self.getDateMonthFromString(tuple.key))
                        if let doubleV = tuple.value as? Double{
                            self.data.append(doubleV)
                        }else{
                            self.data.append(0.0)
                        }
                    }
                }else if self.selectedGraphType == .yearly{
                    var monthData: [String] = []
                    self.yearData()
                    let date = Date()
                    let calendar = Calendar.current
                    let month = calendar.component(.month, from: date)
                    
                    var requiredKeySet:[String] = self.arrayList[(month-1)]
                    requiredKeySet = requiredKeySet.reversed()
                   

                    monthData.append(contentsOf: graphData.keys)
                    for i in 0..<requiredKeySet.count{
                        let v = graphData[requiredKeySet[i]] as? Double ?? 0.0
                        self.data.append(v)
                    }
                    self.labels = requiredKeySet.map({ (month) -> String in
                        let lbl = month.prefix(3)
                        return String(lbl)
                    })
                }else if self.selectedGraphType == .weekly{
                    var dayData: [String] = []
                    self.daydata()
                    let date = Date()
                    let calendar = Calendar.current
                    let day = calendar.component(.weekday, from: date)
                    var requiredKey:[String] = self.dayArray[(day%7)]
                    requiredKey = requiredKey.reversed()
                    dayData.append(contentsOf: graphData.keys)
                    for i in 0..<requiredKey.count{
                        let v = graphData[requiredKey[i]] as? Double ?? 0.0
                        self.data.append(v)
                    }
                    self.labels = requiredKey.map({ (day) -> String in
                        let lbl = day.prefix(3)
                        return String(lbl)
                    })
                }else if self.selectedGraphType == .today{
                    var hourData: [String] = []
                    let date = Date()
                    let calendar = Calendar.current
                    let day = calendar.component(.hour, from: date)
                    let mappedGraphData = self.mapWithDeviceData(graphData: graphData, day: day)
                    self.hourData()
                     self.hourDatas()
                    var requiredKeyhour:[String] = self.hourArray[(day - 2)]
                    var requiredKeyhours:[String] = self.hourArray1[(day - 2)]
                     requiredKeyhours = requiredKeyhours.reversed()
                    requiredKeyhour = requiredKeyhour.reversed()
                    hourData.append(contentsOf: mappedGraphData.keys)
                    self.labels = requiredKeyhours
                    for i in 0..<requiredKeyhour.count{
                       // self.labels.append("\(requiredKeyhour[i]) Hrs")
                        let v = mappedGraphData[requiredKeyhour[i]] as? Double ?? 0.0
                        self.data.append(v)
                    }
                }else{
                    for (key,value) in Array(graphData).sorted(by: {$0.0 < $1.0}) {
                        self.labels.append(key)
                        if let doubleV = value as? Double{
                            self.data.append(doubleV)
                        }else{
                            self.data.append(0.0)
                        }
                    }
                }
                //now check weather data contains some items and draw the graph
                //creat and setup graph
                if (self.data.count != 0){
                    self.createAndSetupGraph(self.graphContainner.frame)
                }
            }
            CommonClass.hideLoader()

        }
    }
    
    
    func loadEarningDataForGraphicalRepresentation(marinaID: String,keywoard: String,fromDate:String, toDate: String){
        CommonClass.showLoader(withStatus: "Loading..")
        
        PaymentService.sharedInstance.earningGraphForMarina(forMarina: marinaID, usingFilter: self.selectedGraphType.rawValue, fromDate:fromDate, toDate:toDate){ (graphDataDict,occuracyResult) in
            if let graphData = graphDataDict as? Dictionary<String,AnyObject>{
                self.labels.removeAll()
                self.data.removeAll()
                
                if self.selectedGraphType == .monthly{
                    
                    let myArrayOfTuples = graphData.sorted{
                        guard let d1 = $0.key.shortDateUS, let d2 = $1.key.shortDateUS else { return false }
                        return d2 > d1
                    }
                    for tuple in myArrayOfTuples {
                        self.labels.append(self.getDateFromString(tuple.key))
                        if let doubleV = tuple.value as? Double{
                            self.data.append(doubleV)
                        }else{
                            self.data.append(0.0)
                        }
                    }
                }else if self.selectedGraphType == .range{
                    let myArrayOfTuples = graphData.sorted{
                        guard let d1 = $0.key.shortDateUSMonth, let d2 = $1.key.shortDateUSMonth else { return false }
                        return d2 > d1
                    }
                    
                    for tuple in myArrayOfTuples {
                        self.labels.append(self.getDateMonthFromString(tuple.key))
                        if let doubleV = tuple.value as? Double{
                            self.data.append(doubleV)
                        }else{
                            self.data.append(0.0)
                        }
                    }
                }else if self.selectedGraphType == .yearly{
                    var monthData: [String] = []
                    self.yearData()
                    let date = Date()
                    let calendar = Calendar.current
                    let month = calendar.component(.month, from: date)
                    var requiredKeySet:[String] = self.arrayList[(month-1)]
                     requiredKeySet = requiredKeySet.reversed()
                    monthData.append(contentsOf: graphData.keys)
                    for i in 0..<requiredKeySet.count{
                        let v = graphData[requiredKeySet[i]] as? Double ?? 0.0
                        self.data.append(v)
                    }
                    self.labels = requiredKeySet.map({ (month) -> String in
                        let lbl = month.prefix(3)
                        return String(lbl)
                    })
                }else if self.selectedGraphType == .weekly{
                    var dayData: [String] = []
                    self.daydata()
                    let date = Date()
                    let calendar = Calendar.current
                    let day = calendar.component(.weekday, from: date)
                    var requiredKey:[String] = self.dayArray[(day%7)]
                    requiredKey = requiredKey.reversed()
                    dayData.append(contentsOf: graphData.keys)
                    for i in 0..<requiredKey.count{
                        let v = graphData[requiredKey[i]] as? Double ?? 0.0
                        self.data.append(v)
                    }
                    self.labels = requiredKey.map({ (day) -> String in
                        let lbl = day.prefix(3)
                        return String(lbl)
                    })
                }else if self.selectedGraphType == .today{
                    var hourData: [String] = []
                    let date = Date()
                    let calendar = Calendar.current
                    let day = calendar.component(.hour, from: date)
                    let mappedGraphData = self.mapWithDeviceData(graphData: graphData, day: day)
                    self.hourData()
                    self.hourDatas()
                    var requiredKeyhour:[String] = self.hourArray[(day - 2)]
                    var requiredKeyhours:[String] = self.hourArray1[(day - 2)]
                    requiredKeyhours = requiredKeyhours.reversed()
                    requiredKeyhour = requiredKeyhour.reversed()

                    hourData.append(contentsOf: mappedGraphData.keys)
                    self.labels = requiredKeyhours
                    for i in 0..<requiredKeyhour.count{
                        // self.labels.append("\(requiredKeyhour[i]) Hrs")
                        let v = mappedGraphData[requiredKeyhour[i]] as? Double ?? 0.0
                        self.data.append(v)
                    }
                }else{
                    for (key,value) in Array(graphData).sorted(by: {$0.0 < $1.0}) {
                        self.labels.append(key)
                        if let doubleV = value as? Double{
                            self.data.append(doubleV)
                        }else{
                            self.data.append(0.0)
                        }
                    }
                }

                if (self.data.count != 0){
                    self.createAndSetupGraph(self.graphContainner.frame)
                }
            }
            
            CommonClass.hideLoader()

        }
    }
    func hourDatas(){
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.minute, from: date)
        self.hourArray1.removeAll()
        let keyZero = ["12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM"]
        let keyOne = ["1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM"]
        let keyTwo = ["2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM"]
        let keyThree = ["3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM"]
        let keyFoue = ["4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM"]
        let keyFive = ["5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM"]
        let keySix = ["6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM"]
        let keySeven = ["7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM"]
        let keyEight = ["8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM"]
        let keyNine = ["9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM"]
        let keyTen = ["10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM"]
        let keyEleven = ["11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM"]
        let keyTwelve = ["12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM"]
        let keyThirteen = ["1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM"]
        let keyFourteen = ["2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM"]
        let keyFifteen = ["3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM"]
        let keySixteen = ["4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM","6:\(day) PM"]
        let keySeventeen = ["5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM","7:\(day) PM"]
        let keyEighteen = ["6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM","8:\(day) PM"]
        let keyNineteen = ["7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM","9:\(day) PM"]
        let keyTwenty = ["8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM","10:\(day) PM"]
        let keyTwentyOne = ["9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM","11:\(day) PM"]
        let keyTwentyTwo = ["10:\(day) PM","8:\(day) PM","6:\(day) PM","4:\(day) PM","2:\(day) PM","12:\(day) PM","10:\(day) AM","8:\(day) AM","6:\(day) AM","4:\(day) AM","2:\(day) AM","12:\(day) AM"]
        let keyTwentyThree = ["11:\(day) PM","9:\(day) PM","7:\(day) PM","5:\(day) PM","3:\(day) PM","1:\(day) PM","11:\(day) AM","9:\(day) AM","7:\(day) AM","5:\(day) AM","3:\(day) AM","1:\(day) AM"]
        self.hourArray1.append(keyZero)
        self.hourArray1.append(keyOne)
        self.hourArray1.append(keyTwo)
        self.hourArray1.append(keyThree)
        self.hourArray1.append(keyFoue)
        self.hourArray1.append(keyFive)
        self.hourArray1.append(keySix)
        self.hourArray1.append(keySeven)
        self.hourArray1.append(keyEight)
        self.hourArray1.append(keyNine)
        self.hourArray1.append(keyTen)
        self.hourArray1.append(keyEleven)
        self.hourArray1.append(keyTwelve)
        self.hourArray1.append(keyThirteen)
        self.hourArray1.append(keyFourteen)
        self.hourArray1.append(keyFifteen)
        self.hourArray1.append(keySixteen)
        self.hourArray1.append(keySeventeen)
        self.hourArray1.append(keyEighteen)
        self.hourArray1.append(keyNineteen)
        self.hourArray1.append(keyTwenty)
        self.hourArray1.append(keyTwentyOne)
        self.hourArray1.append(keyTwentyTwo)
        self.hourArray1.append(keyTwentyThree)
    }
    func yearData()
    {
        self.arrayList.removeAll()

        let keyJan = ["January","December","November","October","September","August","July","June","May","April","March","February"]
        let keyFeb = ["February","January","December","November","October","September","August","July","June","May","April","March"]
        let keyMar = ["March","February","January","December","November","October","September","August","July","June","May","April"]
        let keyApril = ["April","March","February","January","December","November","October","September","August","July","June","May"]
        let keyMay = ["May","April","March","February","January","December","November","October","September","August","July","June"]
        let keyJune = ["June","May","April","March","February","January","December","November","October","September","August","July"]
        let keyJuly = ["July","June","May","April","March","February","January","December","November","October","September","August"]
        let keyAug = ["August","July","June","May","April","March","February","January","December","November","October","September"]
        let keySept = ["September","August","July","June","May","April","March","February","January","December","November","October"]
        let keyOct = ["October","September","August","July","June","May","April","March","February","January","December","November"]
        let keyNov = ["November","October","September","August","July","June","May","April","March","February","January","December"]
        let keyDec = ["December","November","October","September","August","July","June","May","April","March","February","January"]

        self.arrayList.append(keyJan)
        self.arrayList.append(keyFeb)
        self.arrayList.append(keyMar)
        self.arrayList.append(keyApril)
        self.arrayList.append(keyMay)
        self.arrayList.append(keyJune)
        self.arrayList.append(keyJuly)
        self.arrayList.append(keyAug)
        self.arrayList.append(keySept)
        self.arrayList.append(keyOct)
        self.arrayList.append(keyNov)
        self.arrayList.append(keyDec)
    }
    func daydata(){
        self.dayArray.removeAll()

        let keyMonday = ["Monday","Sunday","Saturday","Friday","Thursday","Wednesday","Tuesday"]
        let keyTuesdsy = ["Tuesday","Monday","Sunday","Saturday","Friday","Thursday","Wednesday"]
        let keyWednesday = ["Wednesday","Tuesday","Monday","Sunday","Saturday","Friday","Thursday"]
        let keyThursday = ["Thursday","Wednesday","Tuesday","Monday","Sunday","Saturday","Friday"]
        let keyFriday = ["Friday","Thursday","Wednesday","Tuesday","Monday","Sunday","Saturday"]
        let keySaturday = ["Saturday","Friday","Thursday","Wednesday","Tuesday","Monday","Sunday"]
        let keySunday = ["Sunday","Saturday","Friday","Thursday","Wednesday","Tuesday","Monday"]
        self.dayArray.append(keySaturday)
        self.dayArray.append(keySunday)
        self.dayArray.append(keyMonday)
        self.dayArray.append(keyTuesdsy)
        self.dayArray.append(keyWednesday)
        self.dayArray.append(keyThursday)
        self.dayArray.append(keyFriday)

    }
    func hourData(){
        self.hourArray.removeAll()
        let keyZero = ["0","22","20","18","16","14","12","10","8","6","4","2"]
        let keyOne = ["1","23","21","19","17","15","13","11","9","7","5","3"]
        let keyTwo = ["2","0","22","20","18","16","14","12","10","8","6","4"]
        let keyThree = ["3","1","23","21","19","17","15","13","11","9","7","5"]
        let keyFoue = ["4","2","0","22","20","18","16","14","12","10","8","6"]
        let keyFive = ["5","3","1","23","21","19","17","15","13","11","9","7"]
        let keySix = ["6","4","2","0","22","20","18","16","14","12","10","8"]
        let keySeven = ["7","5","3","1","23","21","19","17","15","13","11","9"]
        let keyEight = ["8","6","4","2","0","22","20","18","16","14","12","10"]
        let keyNine = ["9","7","5","3","1","23","21","19","17","15","13","11"]
        let keyTen = ["10","8","6","4","2","0","22","20","18","16","14","12"]
        let keyEleven = ["11","9","7","5","3","1","23","21","19","17","15","13"]
        let keyTwelve = ["12","10","8","6","4","2","0","22","20","18","16","14"]
        let keyThirteen = ["13","11","9","7","5","3","1","23","21","19","17","15"]
        let keyFourteen = ["14","12","10","8","6","4","2","0","22","20","18","16"]
        let keyFifteen = ["15","13","11","9","7","5","3","1","23","21","19","17"]
        let keySixteen = ["16","14","12","10","8","6","4","2","0","22","20","18"]
        let keySeventeen = ["17","15","13","11","9","7","5","3","1","23","21","19"]
        let keyEighteen = ["18","16","14","12","10","8","6","4","2","0","22","20"]
        let keyNineteen = ["19","17","15","13","11","9","7","5","3","1","23","21"]
        let keyTwenty = ["20","18","16","14","12","10","8","6","4","2","0","22"]
        let keyTwentyOne = ["21","19","17","15","13","11","9","7","5","3","1","23"]
        let keyTwentyTwo = ["22","20","18","16","14","12","10","8","6","4","2","0"]
        let keyTwentyThree = ["23","21","19","17","15","13","11","9","7","5","3","1"]
        self.hourArray.append(keyZero)
        self.hourArray.append(keyOne)
        self.hourArray.append(keyTwo)
        self.hourArray.append(keyThree)
        self.hourArray.append(keyFoue)
        self.hourArray.append(keyFive)
        self.hourArray.append(keySix)
        self.hourArray.append(keySeven)
        self.hourArray.append(keyEight)
        self.hourArray.append(keyNine)
        self.hourArray.append(keyTen)
        self.hourArray.append(keyEleven)
        self.hourArray.append(keyTwelve)
        self.hourArray.append(keyThirteen)
        self.hourArray.append(keyFourteen)
        self.hourArray.append(keyFifteen)
        self.hourArray.append(keySixteen)
        self.hourArray.append(keySeventeen)
        self.hourArray.append(keyEighteen)
        self.hourArray.append(keyNineteen)
        self.hourArray.append(keyTwenty)
        self.hourArray.append(keyTwentyOne)
        self.hourArray.append(keyTwentyTwo)
        self.hourArray.append(keyTwentyThree)
    }

    func getDateFromString(_ dateString: String) -> String{
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            let dtstr = CommonClass.formattedDateWith(date, format: "MMM dd")
            return dtstr
        }else{
            return dateString
        }
    }
    
    func getDateMonthFromString(_ dateString: String) -> String{
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent
        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd"
        if let date = dayTimePeriodFormatter.date(from: dateString) as Date?{
            let dtstr = CommonClass.formattedDateWith(date, format: "MMM dd")
            return dtstr
        }else{
            return dateString
        }
    }
}

extension EarningsViewController{
    
    func setupBarChart(yValues:Array<Double>,labels: Array<String>){
        let dataEntries = self.generateDataEntries(dataValues: yValues, labels: labels)
        self.graphView.space = self.selectedGraphType.barSpace()
        self.graphView.barWidth = 20.0
        self.graphView.barColor = kGradiantStartColor
        self.graphView.textFont = fonts.Raleway.regular.font(.small)
        self.graphView.yValueColor = .black
        self.graphView.labelColor = .black
        self.graphView.dataEntries = dataEntries
    }
    
    func generateDataEntries(dataValues:Array<Double>,labels:Array<String>) -> [NKBarEntry] {
        var result: [NKBarEntry] = []
        for count in 0..<dataValues.count{
            let height = dataValues[count]/Double(dataValues.max() ?? 0.001)
            result.append(NKBarEntry(height: Float(height), yValue: dataValues[count], title: labels[count]))
        }
        return result
    }
    
}
extension EarningsViewController: NKCalenderViewControllerDelegate{
    func nkcalendarViewController(_ viewController: NKCalenderViewController, didSelectedDate selectedDates: BookingDates) {
        self.selectedDates = selectedDates
        if self.selectedDates.totalDays > 0{
        let fromDateStr = CommonClass.formattedDateWith(selectedDates.fromDate, format: "yyyy-MM-dd")
        let toDateStr = CommonClass.formattedDateWith(selectedDates.toDate, format: "yyyy-MM-dd")
            if self.isBookingsGraphPresent == true{
        self.loadEarningDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: "",fromDate:fromDateStr, toDate:toDateStr)
            }
            if self.isOccupancyGraphPresent == true {
        self.loadOccupancyDataForGraphicalRepresentation(marinaID: self.marina.ID, keywoard: "", fromDate: fromDateStr, toDate: toDateStr)
            }
        }else{
            dismiss(animated: true, completion: nil)
            showAlertWith(self, message: "Select Range", title: warningMessage.alertTitle.rawValue)
        }
        
    }
}


extension EarningsViewController{

    func getMarinaPriceType(marinaID: String) {
        CommonClass.showLoader(withStatus: "Processing..")

        MarinaService.sharedInstance.getUpdateMarinaPriceType(marinaID) {(success,resMarina,message) in
            if let newMarina = resMarina {
                CommonClass.hideLoader()
                  self.marina.priceType = newMarina.priceType
                self.marina.weekendType = newMarina.weekendType
                
                if self.marina.priceType == graduallyPriceType {
                    let slipPricingVC = AppStoryboard.PriceSetup.viewController(SetUpPriceViewController.self)
                    slipPricingVC.marina = self.marina
                    self.navigationController?.pushViewController(slipPricingVC, animated: true)
                }else{
                    let slipPricingVC = AppStoryboard.PriceSetup.viewController(NormalPriceListViewController.self)
                    slipPricingVC.marina = self.marina
                    self.navigationController?.pushViewController(slipPricingVC, animated: true)
                }
            }else{
                showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }

                
                
            }
            
        }
    
}

