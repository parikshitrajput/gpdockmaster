//
//  GatewayViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 28/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit


class GatewayViewController: UIViewController,BankDetailViewControllerDelegate {


    @IBOutlet weak var cardsTableView : UITableView!
    var user:User!
    var cards = Array<Card>()

    var marina : Marina!
    var account = Array<BankAccount>()
    var bankAccountHeader : BankAccountHeader!

    var selected = false
    override func viewDidLoad() {
        super.viewDidLoad()
        let userjson = User.loadUserInfo()
        self.user = User(json: userjson)
        self.marina = CommonClass.sharedInstance.getSelectedMarina()
        cardsTableView.dataSource = self
        cardsTableView.delegate = self
        self.bankAccountHeader = BankAccountHeader.instanceFromNib()
        self.bankAccountHeader.frame = CGRect(x: 0, y:0, width: cardsTableView.frame.size.width, height:50)
        self.cardsTableView.tableHeaderView?.frame.size = CGSize(width: cardsTableView.frame.size.width, height: CGFloat(50))
        cardsTableView.tableHeaderView = self.bankAccountHeader
        self.bankAccountHeader.accountLabelView.layer.backgroundColor = kNavigationColor.cgColor
        self.bankAccountHeader.chooseAccountLabel.text = " Choose an account as your default "
        //cardsTableView.tableHeaderView = UIView(frame: CGRect.zero)
        self.getBankAccount(self.user.ID)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // self.getBankAccount(self.user.ID)
        //showAlertWith(self, message: "Account added successfully ", title: "Important Message")

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
//    @objc func bookingDidCancelledByUser(_ notification: Notification) {
//        showAlertWith(self, message: "Account added successfully ", title: "Important Message")
//
//    }
    func sizeHeaderToFit() {
        let headerView = cardsTableView.tableHeaderView!
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        var frame = headerView.frame
        frame.size.height = 50
        headerView.frame = frame
        cardsTableView.tableHeaderView = headerView
    }
    
    func bankDetailViewController(viewController: BankDetailViewController, didAddCard card: BankAccount) {
        viewController.dismiss(animated: false, completion: nil)
        self.account.insert(card, at: 0)
        showAlertWith(nil, message: "Account added successfully ", title: warningMessage.alertTitle.rawValue)
        self.cardsTableView.reloadData()
    }

 func getBankAccount(_ userID:String ) -> Void {
    CommonClass.showLoader(withStatus: "Fetching..")
    PaymentService.sharedInstance.getBanksAccountForUser(self.user.ID) { (resAccount) in
        if let someAccount = resAccount{
            self.account.removeAll()
            self.account.append(contentsOf: someAccount)
            self.cardsTableView.reloadData()
        }
        CommonClass.hideLoader()
    }
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onClickBackButton(_ sender: UIBarButtonItem){
        self.navigationController?.pop(true)
    }


}
extension GatewayViewController :UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }


    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        var height:CGFloat = 0
        if section == 0{
            height = 0
        }else if section == 1{
            if account.count == 0{height = 0}else{height = 0}
        }else if section == 2 {
            height = 0
        }
        return height
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        switch section {
        case 0:
            rows = account.count //customerContext.customer.gpdockSources.count
        case 1:
            rows = 1
        default:
            rows = 0
        }
        return rows
    }

 


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "GatewayCell", for: indexPath) as! GatewayCell
                let bankAccount = account[indexPath.row]
                cell.cardNumberLabel.text =   "A/C :xxxx-xxxx-xxxx-\(bankAccount.last4)"
                cell.fundingTypeLabel.text =  bankAccount.bankName
                cell.removeButton.isHidden = bankAccount.isDefault
                cell.removeButton.addTarget(self, action: #selector(onClickRemoveButton(_:)), for: .touchUpInside)
                cell.radioButton.isSelected = bankAccount.isDefault
                return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewCardTableViewCell", for: indexPath) as! NewCardTableViewCell
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            let bankAccount = account[indexPath.row]
            if bankAccount.isDefault{ return}
            self.makeDefaultAccount(self.user.ID, accountID: bankAccount.ID, indexPath: indexPath)
        }else if indexPath.section == 1{
            self.navigateToAddNewCard()
        }
    }
    
    
    func navigateToAddNewCard(){
        let addNewPaymentOptionVC = AppStoryboard.Earning.viewController(BankDetailViewController.self)
        addNewPaymentOptionVC.delegate = self
        addNewPaymentOptionVC.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        let nav = UINavigationController(rootViewController: addNewPaymentOptionVC)
        nav.navigationBar.barTintColor = kNavigationColor
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        nav.navigationBar.isHidden = true
        self.navigationController?.present(nav, animated: true, completion: nil)
    }

    

    @IBAction func onClickRemoveButton(_ sender: UIButton){
        if let indexPath = sender.tableViewIndexPath(self.cardsTableView) as IndexPath?{
            let removeAccount = account[indexPath.row]
            self.showAlertToConfirmBooking(self.user.ID, bank: removeAccount, indexPath: indexPath)

        }
    }

func removeAccount(_ userID:String,bank:BankAccount,indexPath: IndexPath) -> Void {
    let removingIndexPath = indexPath
    PaymentService.sharedInstance.removeBankAccount(self.user.ID, bankAccountID:bank.ID) { [removingIndexPath] (success, message) in
        if success{
            self.account.remove(at: removingIndexPath.row)
            self.cardsTableView.beginUpdates()
            self.cardsTableView.deleteRows(at: [removingIndexPath], with: .automatic)
            self.cardsTableView.endUpdates()
        }else{
            showAlertWith(self, message: "\(message)", title: warningMessage.alertTitle.rawValue)
        }
    }
}
    
    
    func showAlertToConfirmBooking(_ userID:String,bank:BankAccount,indexPath: IndexPath){
        let alert = UIAlertController(title: "Confirm", message:"Are you sure you want to remove a account?", preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(cancelAction)
        let doneAction: UIAlertAction = UIAlertAction(title: "Done", style: .default)
        { action -> Void in
            self.removeAccount(self.user.ID, bank: bank, indexPath: indexPath)
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(doneAction)
        self.present(alert, animated: true, completion: nil)
    }

    func makeDefaultAccount(_ userID: String, accountID: String,indexPath: IndexPath) -> Void {
        if !CommonClass.isLoaderOnScreen{
            CommonClass.showLoader(withStatus: "Please wait..")
        }
        PaymentService.sharedInstance.makeDefaultBankAccount(self.user.ID, bankAccountID: accountID) { (success,resBankAccount, message)  in
                CommonClass.hideLoader()
                if success{
                    if let newDefaultAccount = resBankAccount{
                        for (index, _account) in self.account.enumerated(){
                            if _account.accountID == newDefaultAccount.accountID{
                                self.account[index].isDefault = true//newDefaultAccount.isDefault
                            }else{
                                self.account[index].isDefault = false
                            }
                        }
                        self.cardsTableView.reloadData()
                    }else{
                        showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
                    }
                    showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)

                }else{
                    showAlertWith(self, message: message, title: warningMessage.alertTitle.rawValue)
            }
            
            }
        }
    

}


