//
//  PriceCalendarViewController.swift
//  GPDock Merchant
//
//  Created by TecOrb on 17/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import FSCalendar

protocol PriceCalendarViewControllerDelegate {
    func priceCalendarViewController(viewController:PriceCalendarViewController, didSelectDates selectedDates: Array<String>)
}

class PriceCalendarViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate  {
    var specialPrices : Array<DayPrice>?
    @IBOutlet weak var calenarView : UIView!
    var calendar: FSCalendar!
    var delegate : PriceCalendarViewControllerDelegate?
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!

    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()

    var gregorian: Calendar!

    @IBAction func previousClicked(_ sender: UIButton) {
        let currentMonth: Date? = calendar?.currentPage
        let previousMonth: Date? = gregorian?.date(byAdding: .month, value: -1, to: currentMonth!)
        calendar?.setCurrentPage(previousMonth!, animated: true)
    }

    @IBAction func onClickClose(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction func onclickDone(_ sender: UIButton) {
        let sortedDates = calendar.selectedDates.sorted { (date1, date2) -> Bool in
            return date1.compare(date2) == .orderedAscending
            }.map { (date) -> String in
                return self.dateFormatter.string(from: date)
        }

        delegate?.priceCalendarViewController(viewController: self, didSelectDates: sortedDates)
        self.dismiss(animated: true, completion: nil)
    }


    @IBAction func nextClicked(_ sender: UIButton) {
        let currentMonth: Date? = calendar?.currentPage
        let nextMonth: Date? = gregorian?.date(byAdding: .month, value: 1, to: currentMonth!)
        calendar?.setCurrentPage(nextMonth!, animated: true)
    }


    override func viewDidLoad() {
        gregorian = Calendar(identifier: .iso8601)
        let width: CGFloat = self.view.frame.size.width-30
        let height: CGFloat = width + 40

        let calendar = FSCalendar(frame: CGRect(x: 0, y: 0, width: width, height: height))
        calendar.appearance.titleTodayColor = kNavigationColor
        calendar.backgroundColor = UIColor.white
        calendar.appearance.headerMinimumDissolvedAlpha = 0
        calendar.headerHeight = 50
        calendar.appearance.caseOptions = .weekdayUsesSingleUpperCase
        calendar.appearance.weekdayTextColor = kNavigationColor
        calendar.appearance.titleWeekendColor = .black
        calendar.appearance.borderSelectionColor = kNavigationColor
        calendar.appearance.selectionColor = UIColor.white
        calendar.appearance.borderRadius = 1
        calendar.appearance.weekdayFont = UIFont(name: fonts.Raleway.regular.rawValue, size: 16)
        calendar.pagingEnabled = true
        calendar.allowsMultipleSelection = true
        calendar.rowHeight = 40
        calendar.placeholderType = FSCalendarPlaceholderType.fillHeadTail
        self.calendar = calendar
        self.calenarView.addSubview(calendar)
        calendar.dataSource = self
        calendar.delegate = self

        CommonClass.makeViewCircularWithCornerRadius(self.calenarView, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 4)
        calendar.appearance.titleDefaultColor = UIColor.black
        calendar.appearance.titleSelectionColor = kNavigationColor
        calendar.appearance.headerTitleColor = kNavigationColor
        calendar.appearance.titleFont = UIFont(name: fonts.Raleway.regular.rawValue, size: fontSize.large.rawValue)
        calendar.appearance.headerTitleFont = UIFont(name: fonts.Raleway.semiBold.rawValue, size: fontSize.large.rawValue)
        calendar.appearance.todaySelectionColor = .white
        calendar.appearance.titleTodayColor = kNavigationColor
        calendar.appearance.todayColor = .white
        calendar.appearance.subtitleTodayColor = .black
        calendar.weekdayHeight = 40
        calendar.swipeToChooseGesture.isEnabled = true
        calendar.scrollDirection = .vertical
        calendar.scrollEnabled = false

        //calendar.register(RangePickerCell.self, forCellReuseIdentifier: "cell")
    }

    override  func viewDidLayoutSubviews() {
        self.doneButton.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.0, 1.0])
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        return (gregorian?.date(byAdding: .year, value: 1, to: Date())!)!
    }

    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if monthPosition == .previous || monthPosition == .next {
            calendar.setCurrentPage(date, animated: true)
        }
    }

    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {

        if let priceAndDates = self.specialPrices{
            let dateString = self.dateFormatter.string(from: date)

           if let priceForDate = priceAndDates.filter({ (datePrice) -> Bool in
                return datePrice.date == dateString
           }).first{
            return String(format: "$%0.2f", priceForDate.price)
            }
        }
        return nil //String(format: "%0.2f", priceForDate.price)
    }


    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if (date >  (gregorian?.date(byAdding: .day, value: -1, to: Date()))!) && (date < (gregorian?.date(byAdding: .month, value: 4, to: Date()))!) {
            return true
        }
        return false
    }
}
