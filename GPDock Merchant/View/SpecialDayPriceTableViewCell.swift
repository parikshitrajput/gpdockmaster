//
//  SpecialDayPriceTableViewCell.swift
//  GPDock
//
//  Created by Parikshit on 10/08/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class SpecialDayPriceTableViewCell: UITableViewCell {
    @IBOutlet weak var priceView: UIView!

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
