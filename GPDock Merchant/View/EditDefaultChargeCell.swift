//
//  EditDefaultChargeCell.swift
//  GPDock Merchant
//
//  Created by TecOrb on 29/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import BKMoneyKit

class EditDefaultChargeCell: UITableViewCell {
    @IBOutlet weak var regularPriceTextField: BKCurrencyTextField!
    @IBOutlet weak var weekendPriceTextField: BKCurrencyTextField!

    @IBOutlet weak var weekendDayLabel: UILabel!
    @IBOutlet weak var toggleWeekendPriceButton: UIButton!

    @IBOutlet weak var weekendDaySwitch: UISwitch!
    var indexPath : IndexPath!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        self.regularPriceTextField.numberFormatter.currencySymbol = "$"
        self.weekendPriceTextField.numberFormatter.currencySymbol = "$"

        self.toggleWeekendPriceButton.setImage(#imageLiteral(resourceName: "check_unsel"), for: .normal)
        self.toggleWeekendPriceButton.setImage(#imageLiteral(resourceName: "check_sel"), for: .selected)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}


