//
//  RatingTableViewCell.swift
//  GPDock Merchant
//
//  Created by Tecorb on 10/26/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class RatingTableViewCell: UITableViewCell {
    @IBOutlet weak var ratingView: NKFloatRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
