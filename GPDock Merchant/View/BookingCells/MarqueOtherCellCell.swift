//
//  MarqueOtherCellCell.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 10/02/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
 import MarqueeLabel
class MarqueOtherCellCell: UITableViewCell {
    @IBOutlet weak var otherDescriptionLabel: MarqueeLabel!
    @IBOutlet weak var otherTitleLabel: UILabel!
    @IBOutlet weak var otherIcon: UIImageView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
