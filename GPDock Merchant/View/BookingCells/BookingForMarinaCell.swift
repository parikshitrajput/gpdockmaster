//
//  BookingForMarinaCell.swift
//  GPDock Merchant
//
//  Created by TecOrb on 05/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BookingForMarinaCell: UITableViewCell {
    @IBOutlet weak var boatImageView : UIImageView!
    @IBOutlet weak var userNameLabel : UILabel!
    @IBOutlet weak var boatNameLabel : UILabel!
    @IBOutlet weak var bookingDate : UILabel!
    @IBOutlet weak var slipPosition : UILabel!
    @IBOutlet weak var totalNightLabel : UILabel!
    @IBOutlet weak var arrivalDepartureLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
