//
//  GoToRefundCell.swift
//  GPDock
//
//  Created by TecOrb on 14/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class GoToRefundCell: UITableViewCell {
    @IBOutlet weak var refundTitleLabel: UILabel!
    @IBOutlet weak var accIcon: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
