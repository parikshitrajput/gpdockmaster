//
//  SlipStatusCell.swift
//  GPDock Merchant
//
//  Created by TecOrb on 03/10/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class SlipStatusCell: UITableViewCell {
    @IBOutlet weak var statusLabel : UILabel!
    @IBOutlet weak var datesLabel : UILabel!
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var calenderButton : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
