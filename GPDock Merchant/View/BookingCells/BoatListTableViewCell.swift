//
//  BoatListTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 22/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BoatListTableViewCell: UITableViewCell {
    @IBOutlet weak var boatNameLabel : UILabel!
    @IBOutlet weak var boatImageView : UIImageView!
    @IBOutlet weak var lengthLabel : UILabel!
    @IBOutlet weak var widthLabel : UILabel!
    @IBOutlet weak var depthLabel : UILabel!

    @IBOutlet weak var electricAmpLabel : UILabel!
    @IBOutlet weak var totalBookingsLabel : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
