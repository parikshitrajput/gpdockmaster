//
//  BookingIDRepresentingCell.swift
//  GPDock
//
//  Created by TecOrb on 12/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BookingIDRepresentingCell: UITableViewCell {
    @IBOutlet weak var bookingIDLabel: UILabel!
    @IBOutlet weak var statusLabel:NKCustomLabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
