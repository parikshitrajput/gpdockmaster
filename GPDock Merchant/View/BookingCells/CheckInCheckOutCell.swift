//
//  CheckInCheckOutCell.swift
//  GPDock
//
//  Created by TecOrb on 13/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CheckInCheckOutCell: UITableViewCell {
    @IBOutlet weak var checkInDate : UILabel!
    @IBOutlet weak var checkInTime : UILabel!
    @IBOutlet weak var checkOutDate : UILabel!
    @IBOutlet weak var checkOutTime : UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
