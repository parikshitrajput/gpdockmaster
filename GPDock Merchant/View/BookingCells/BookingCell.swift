//
//  BookingTableViewCell.swift
//  GPDock
//
//  Created by TecOrb on 12/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BookingCell: UITableViewCell {
    @IBOutlet weak var marinaImageView : UIImageView!
    @IBOutlet weak var marinaTitle : UILabel!
    @IBOutlet weak var marinaAddress : UILabel!
    @IBOutlet weak var bookingDate : UILabel!
    @IBOutlet weak var totalNightLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
