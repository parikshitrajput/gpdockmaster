//
//  CalenderPayout.swift
//  GPDock Merchant
//
//  Created by Parikshit on 04/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CalenderPayout: UIView {

    @IBOutlet weak var selectedDateLabel : UILabel!
    @IBOutlet weak var selectedDateButton : UIButton!
    
    
    class func instanceFromNib() -> CalenderPayout {
        return UINib(nibName: "CalenderPayout", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CalenderPayout
    }

}
