//
//  PayoutCell.swift
//  GPDock Merchant
//
//  Created by TecOrb on 27/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class PayoutCell: UITableViewCell {
    @IBOutlet weak var dateLabel : UILabel!
    @IBOutlet weak var amountLabel : UILabel!
    @IBOutlet weak var cardLabel : UILabel!
    @IBOutlet weak var statusLabel : UILabel!
    @IBOutlet weak var infoButton : UIButton!
    @IBOutlet weak var circleView : UIView!
    @IBOutlet weak var cardContainnerView : UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        //let color = UIColor.colorFromHex(hexString: "#32303D")
        let colorCircleCiew = UIColor.colorFromHex(hexString: "#3666ED")
        CommonClass.makeViewCircular(self.circleView, borderColor: colorCircleCiew, borderWidth: 4)
        //self.cardContainnerView.addBottomBorderWithColor(kApplicationLightBleckColor, width: 1.0)
        //CommonClass.makeViewCircularWithCornerRadius(self.cardContainnerView, borderColor: color, borderWidth: 1, cornerRadius: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
