//
//  ShowDate.swift
//  GPDock
//
//  Created by Parikshit on 20/08/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class ShowDate: UIView {


    @IBOutlet weak var fromDateLabel : UILabel!
    @IBOutlet weak var toDateLabel : UILabel!
    
    
    class func instanceFromNib() -> ShowDate {
        return UINib(nibName: "ShowDate", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ShowDate
    }
}
