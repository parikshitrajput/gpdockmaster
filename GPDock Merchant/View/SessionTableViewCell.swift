//
//  AllPriceSelectTableViewCell.swift
//  GPDock
//
//  Created by Parikshit on 09/08/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class SessionTableViewCell: UITableViewCell {
    @IBOutlet weak var priceView: UIView!

    @IBOutlet weak var priceTypeLabel: UILabel!
    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var nightPriceLabel: UILabel!
    @IBOutlet weak var monthPriceLabel: UILabel!
    @IBOutlet weak var weekPriceLabel: UILabel!




    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
