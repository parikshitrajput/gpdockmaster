//
//  priceWithRangeSelectTableViewCell.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import BKMoneyKit
class PriceWithRangeSelectTableViewCell: UITableViewCell {
    @IBOutlet weak var rangeView: UIView!
    @IBOutlet weak var rangeSelectLabel: UILabel!
    @IBOutlet weak var rangePrice: BKCurrencyTextField!
    @IBOutlet weak var currencyLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        rangePrice.setLeftPaddingPoints(10)
        self.rangePrice.numberFormatter.currencySymbol = "$"
        self.rangePrice.numberFormatter.currencyCode = "USD"    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
