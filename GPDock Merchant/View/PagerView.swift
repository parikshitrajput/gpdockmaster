//
//  PagerView.swift
//  GPDock
//
//  Created by TecOrb on 05/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class PagerView: UIView {
    @IBOutlet weak var marinaImage: UIImageView!
    @IBOutlet weak var marinaName: UILabel!
    @IBOutlet weak var ratingView: NKFloatRatingView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!


    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func layoutSubviews() {

    }

}
