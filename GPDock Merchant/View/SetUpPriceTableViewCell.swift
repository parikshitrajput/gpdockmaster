//
//  DefaultSetUpPriceTableViewCell.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class SetUpPriceTableViewCell: UITableViewCell {
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceTypeLabel: UILabel!
    @IBOutlet weak var priceTypeDescriptionLabel: UILabel!
    @IBOutlet weak var priceTypeSelectButton:UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
