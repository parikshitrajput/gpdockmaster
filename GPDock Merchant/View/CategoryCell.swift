//
//  CategoryCell.swift
//  TaxiApp
//
//  Created by TecOrb on 13/05/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var categoryNameLabel:UILabel!
    @IBOutlet weak var timeLabel:UILabel!
    @IBOutlet weak var categoryIcon:UIImageView!
    var userSelected : Bool?
    override var bounds: CGRect{
        didSet{
            self.contentView.frame = bounds
        }
    }

    override func layoutSubviews() {
        if let sel = userSelected{
            if sel{
                CommonClass.makeViewCircular(self.categoryIcon, borderColor: UIColor.white, borderWidth: 0)
            }else{
                CommonClass.makeViewCircularWithCornerRadius(self.categoryIcon, borderColor: UIColor.clear, borderWidth: 0, cornerRadius: 1)
            }
        }
    }
}


class ClassCell: UICollectionViewCell {
    @IBOutlet weak var classNameLabel:UILabel!
    @IBOutlet weak var timeLabel:UILabel!

    override var bounds: CGRect{
        didSet{
            self.contentView.frame = bounds
        }
    }

    override func layoutSubviews() {

    }
}

