//
//  EditSpecialDayChargeCell.swift
//  GPDock Merchant
//
//  Created by Parikshit on 09/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import BKMoneyKit


class EditSpecialDayChargeCell: UITableViewCell {
   @IBOutlet weak var priceTextField: BKCurrencyTextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        self.priceTextField.numberFormatter.currencySymbol = "$"

    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

