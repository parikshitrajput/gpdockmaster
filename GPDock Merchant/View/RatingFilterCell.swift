//
//  RatingFilterCell.swift
//  GPDock
//
//  Created by TecOrb on 10/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class RatingFilterCell: UITableViewCell {
    @IBOutlet weak var ratingView : NKFloatRatingView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
