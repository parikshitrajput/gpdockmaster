//
//  CustomerReviewTableViewCell.swift
//  EatWeDo
//
//  Created by TecOrb on 31/03/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CustomerReviewTableViewCell: UITableViewCell {
    @IBOutlet weak var ratingView : NKFloatRatingView!
    @IBOutlet weak var ratingContainner : UIView!
    @IBOutlet weak var customerNameLabel : UILabel!
    @IBOutlet weak var reviewDateLabel : UILabel!
    @IBOutlet weak var customerImage : UIImageView!
    @IBOutlet weak var reviewTextLabel : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.ratingContainner, borderColor: UIColor.lightGray, borderWidth: 1, cornerRadius: 2)
    }
    
}
