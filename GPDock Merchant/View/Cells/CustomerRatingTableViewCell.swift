//
//  CustomerRatingTableViewCell.swift
//  EatWeDo
//
//  Created by TecOrb on 30/03/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class CustomerRatingTableViewCell: UITableViewCell {
    @IBOutlet weak var oneStarView : NKFloatRatingView!
    @IBOutlet weak var twoStarView : NKFloatRatingView!
    @IBOutlet weak var threeStarView : NKFloatRatingView!
    @IBOutlet weak var fourStarView : NKFloatRatingView!
    @IBOutlet weak var fiveStarView : NKFloatRatingView!

    @IBOutlet weak var oneStarReviewCount : UILabel!
    @IBOutlet weak var twoStarReviewCount : UILabel!
    @IBOutlet weak var threeStarReviewCount : UILabel!
    @IBOutlet weak var fourStarReviewCount : UILabel!
    @IBOutlet weak var fiveStarReviewCount : UILabel!

    @IBOutlet weak var oneStarCount : UILabel!
    @IBOutlet weak var twoStarCount : UILabel!
    @IBOutlet weak var threeStarCount : UILabel!
    @IBOutlet weak var fourStarCount : UILabel!
    @IBOutlet weak var fiveStarCount : UILabel!

    @IBOutlet weak var averageRatingButton : UIButton!
    @IBOutlet weak var ratingContainner : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.ratingContainner, borderColor: UIColor.lightGray, borderWidth: 1, cornerRadius: 2)
    }
}
