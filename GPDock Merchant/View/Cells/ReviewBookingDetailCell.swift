//
//  ReviewBookingDetailCell.swift
//  GPDock
//
//  Created by TecOrb on 19/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ReviewBookingDetailCell: UITableViewCell {
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemDetailText: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
