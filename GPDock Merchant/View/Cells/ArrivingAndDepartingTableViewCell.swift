//
//  ArrivingAndDepartingTableViewCell.swift
//  GPDock Merchant
//
//  Created by Parikshit on 22/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class ArrivingAndDepartingTableViewCell: UITableViewCell {
    @IBOutlet weak var statusLabel : UILabel!
    @IBOutlet weak var datesLabel : UILabel!
    @IBOutlet weak var bgView : UIView!
    @IBOutlet weak var checkInDate : UILabel!
    @IBOutlet weak var checkInTime : UILabel!
    @IBOutlet weak var checkOutDate : UILabel!
    @IBOutlet weak var checkOutTime : UILabel!
    @IBOutlet weak var boatNameLabel : UILabel!
    @IBOutlet weak var boatImageView : UIImageView!
    @IBOutlet weak var lengthLabel : UILabel!
    @IBOutlet weak var widthLabel : UILabel!
    @IBOutlet weak var depthLabel : UILabel!
    @IBOutlet weak var UserNameLabel : UILabel!
    @IBOutlet weak var bookingType : UILabel!
    @IBOutlet weak var otherTitleLabel: UILabel!
    @IBOutlet weak var otherDetailsLabel: UILabel!
    @IBOutlet weak var otherIcon: UIImageView!
   // @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
