//
//  PagerCollectionViewCell.swift
//  GPDock
//
//  Created by TecOrb on 28/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class PagerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var marinaImage: UIImageView!
    @IBOutlet weak var marinaName: UILabel!
    @IBOutlet weak var ratingView: NKFloatRatingView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!

    override var bounds: CGRect{
        didSet{
            self.contentView.frame = bounds

        }
    }
}
