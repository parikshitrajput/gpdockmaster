//
//  NavigationTitleView.swift
//  GPDock Merchant
//
//  Created by TecOrb on 22/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class NavigationTitleView: UIView {
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!


    class func instanceFromNib() -> NavigationTitleView {
        return UINib(nibName: "NavigationTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! NavigationTitleView
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
