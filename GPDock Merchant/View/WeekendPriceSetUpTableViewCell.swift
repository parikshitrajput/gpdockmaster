//
//  WeekendPriceSetUpTableViewCell.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 08/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class WeekendPriceSetUpTableViewCell: UITableViewCell {
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceTypeLabel: UILabel!
    @IBOutlet weak var priceTypeDescriptionLabel: UILabel!
    @IBOutlet weak var priceTypeSelectButton:UIButton!
    //@IBOutlet weak var weekendTypeLabel: UILabel!
    @IBOutlet weak var w2Friday: UILabel!
    @IBOutlet weak var w2Saturday: UILabel!
    @IBOutlet weak var w1Friday: UILabel!
    @IBOutlet weak var w1Saturday: UILabel!
    @IBOutlet weak var w1Sunday: UILabel!
    @IBOutlet weak var firstStackView: UIStackView!
    @IBOutlet weak var secondStackView: UIStackView!
    @IBOutlet weak var weekendDescriptionView: UIView!
    @IBOutlet weak var weekendView: UIView!
    
    @IBOutlet weak var priceLabel: UILabel!

    


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
