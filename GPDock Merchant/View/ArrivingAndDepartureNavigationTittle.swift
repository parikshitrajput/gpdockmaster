//
//  ArrivingAndDepartureNavigationTittle.swift
//  GPDock Merchant
//
//  Created by Parikshit on 10/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class ArrivingAndDepartureNavigationTittle: UIView {
    @IBOutlet weak var firstIDLabel: UILabel!
    @IBOutlet weak var seconfIDLabel: UILabel!

    class func instanceFromNib() -> ArrivingAndDepartureNavigationTittle {
        return UINib(nibName: "ArrivingAndDepartureNavigationTittle", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ArrivingAndDepartureNavigationTittle
    }
}
