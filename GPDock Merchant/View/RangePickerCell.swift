//
//  RangePickerCell.swift
//  GPDock Merchant
//
//  Created by TecOrb on 23/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import FSCalendar

enum SelectionType : Int {
    case none
    case single
    case leftBorder
    case middle
    case rightBorder
}
class RangePickerCell: FSCalendarCell {
    // The start/end of the range

    weak var selectionLayer: CALayer!
    // The middle of the range
    //weak var middleLayer: CALayer!
    weak var rightMiddleLayer: CALayer!
    weak var leftMiddleLayer: CALayer!

    var selectionType: SelectionType = .none {
        didSet {
            setNeedsLayout()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        let rMiddleLayer = CALayer()
        rMiddleLayer.backgroundColor = kNavigationColor.withAlphaComponent(0.1).cgColor
        rMiddleLayer.actions = ["hidden": NSNull()]
        contentView.layer.insertSublayer(rMiddleLayer, below: titleLabel?.layer)
        self.rightMiddleLayer = rMiddleLayer


        let lMiddleLayer = CALayer()
        lMiddleLayer.backgroundColor = kNavigationColor.withAlphaComponent(0.1).cgColor
        lMiddleLayer.actions = ["hidden": NSNull()]
        contentView.layer.insertSublayer(lMiddleLayer, below: titleLabel?.layer)
        self.leftMiddleLayer = lMiddleLayer

        let selectionLayer = CAShapeLayer()
        selectionLayer.backgroundColor = UIColor.white.cgColor//kNavigationColor.withAlphaComponent(0.1).cgColor
        selectionLayer.actions = ["hidden": NSNull()]
        contentView.layer.insertSublayer(selectionLayer, below: titleLabel?.layer)
        self.selectionLayer = selectionLayer




//
//        let middleLayer = CALayer()
//        middleLayer.backgroundColor = kNavigationColor.withAlphaComponent(0.1).cgColor
//        middleLayer.actions = ["hidden": NSNull()]
//        contentView.layer.insertSublayer(middleLayer, below: titleLabel?.layer)
//        self.middleLayer = middleLayer


        // Hide the default selection layer

        shapeLayer.isHidden = true

    }
    
    required init!(coder aDecoder: NSCoder!) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
//        if selectionType == .middle {
//            self.selectionLayer.path = UIBezierPath(rect: self.selectionLayer.bounds).cgPath
//        }
//        else if selectionType == .leftBorder {
//            self.selectionLayer.path = UIBezierPath(roundedRect: self.selectionLayer.bounds, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: self.selectionLayer.frame.width / 2, height: self.selectionLayer.frame.width / 2)).cgPath
//        }
//        else if selectionType == .rightBorder {
//            self.selectionLayer.path = UIBezierPath(roundedRect: self.selectionLayer.bounds, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: self.selectionLayer.frame.width / 2, height: self.selectionLayer.frame.width / 2)).cgPath
//        }
//        else if selectionType == .single {
//            let diameter: CGFloat = min(self.selectionLayer.frame.height, self.selectionLayer.frame.width)
//            self.selectionLayer.path = UIBezierPath(ovalIn: CGRect(x: self.contentView.frame.width / 2 - diameter / 2, y: self.contentView.frame.height / 2 - diameter / 2, width: diameter, height: diameter)).cgPath
//        }
        //titleLabel.layer.mask = selectionLayer
        titleLabel?.frame = contentView.bounds


    }

    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        let sh = contentView.bounds.height
        let sy = (contentView.bounds.height - sh)/2
        let selectedFrame = CGRect(x:contentView.bounds.origin.x,y:sy,width:sh,height:sh)
        selectionLayer.frame = selectedFrame
        selectionLayer.cornerRadius = sh/2
        selectionLayer.borderColor = kNavigationColor.cgColor
        selectionLayer.borderWidth = 2
        selectionLayer.masksToBounds = true

//        let mh = contentView.bounds.height-16
//        let my = (contentView.bounds.height - mh)/2
//        let middleFrame = CGRect(x:contentView.bounds.origin.x,y:my,width:contentView.bounds.width,height:mh)
//        middleLayer.frame = middleFrame

        //right middle
        let mrh = contentView.bounds.height-16
        let mry = (contentView.bounds.height - mrh)/2
        let rMiddleFrame = CGRect(x:contentView.bounds.width/2,y:mry,width:contentView.bounds.width/2,height:mrh)
        rightMiddleLayer.frame = rMiddleFrame

        //right middle
        let mlh = contentView.bounds.height-16
        let mly = (contentView.bounds.height - mlh)/2
        let lMiddleFrame = CGRect(x:contentView.bounds.origin.x,y:mly,width:contentView.bounds.width/2,height:mlh)
        leftMiddleLayer.frame = lMiddleFrame
    }
}

