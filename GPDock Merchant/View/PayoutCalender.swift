//
//  PayoutCalender.swift
//  GPDock Merchant
//
//  Created by Parikshit on 23/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class PayoutCalender: UIView {

@IBOutlet weak var selectedDateLabel : UILabel!
@IBOutlet weak var selectedDateButton : UIButton!


    class func instanceFromNib() -> PayoutCalender {
        return UINib(nibName: "PayoutCalender", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PayoutCalender
    }


}
