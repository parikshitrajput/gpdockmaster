//
//  MarinaLocationHomeCell.swift
//  GPDock
//
//  Created by TecOrb on 12/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class MarinaLocationHomeCell: UITableViewCell {
    @IBOutlet weak var locationNameLabel: NKCustomLabel!
    @IBOutlet weak var locationPhoto: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        CommonClass.makeViewCircularWithRespectToHeight(locationNameLabel, borderColor: UIColor.clear, borderWidth: 0)
        // Configure the view for the selected state
    }

}



class NoDataCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
