//
//  BankAccountHeader.swift
//  GPDock Merchant
//
//  Created by Parikshit on 23/01/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class BankAccountHeader: UIView {

    @IBOutlet weak var chooseAccountLabel : UILabel!
    @IBOutlet weak var accountLabelView : UIView!
    
    
    class func instanceFromNib() -> BankAccountHeader {
        return UINib(nibName: "BankAccountHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BankAccountHeader
    }


}
