//
//  FloorPlanNavigationTitleView.swift
//  GPDock
//
//  Created by TecOrb on 15/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class FloorPlanNavigationTitleView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!

    class func instanceFromNib() -> FloorPlanNavigationTitleView {
        return UINib(nibName: "FloorPlanNavigationTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! FloorPlanNavigationTitleView
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}


class TableFooterWithButton: UIView {
    @IBOutlet weak var button: UIButton!

    class func instanceFromNib() -> TableFooterWithButton {
        return UINib(nibName: "FloorPlanNavigationTitleView", bundle: nil).instantiate(withOwner: nil, options: nil)[1] as! TableFooterWithButton
    }
    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.button, borderColor: UIColor.clear, borderWidth: 1, cornerRadius: 2)
    }

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}

