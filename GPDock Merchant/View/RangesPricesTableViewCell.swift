//
//  RangesPricesTableViewCell.swift
//  GPDock
//
//  Created by Parikshit on 09/08/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit

class RangesPricesTableViewCell: UITableViewCell {
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceTypeLabel: UILabel!
    @IBOutlet weak var priceTypeDescriptionLabel: UILabel!
//    @IBOutlet weak var priceTypeSelectButton:UIButton!
    //@IBOutlet weak var weekendTypeLabel: UILabel!

    @IBOutlet weak var nightButton: UIButton!
    @IBOutlet weak var weekButton: UIButton!
    @IBOutlet weak var monthButton: UIButton!
    @IBOutlet weak var firstStackView: UIStackView!
    @IBOutlet weak var weekendDescriptionView: UIView!
    @IBOutlet weak var typeView: UIView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
