//
//  GatewayCell.swift
//  GPDock Merchant
//
//  Created by TecOrb on 28/09/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class GatewayCell: UITableViewCell {

    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var fundingTypeLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var radioButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func layoutSubviews() {
        self.radioButton.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
        self.radioButton.setImage(#imageLiteral(resourceName: "radioSel"), for: .selected)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
