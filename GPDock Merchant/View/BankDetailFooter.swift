//
//  BankDetailFooter.swift
//  GPDock Merchant
//
//  Created by Parikshit on 24/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class BankDetailFooter: UIView {
  //@IBOutlet weak var submitButton : UIButton!
    
    class func instanceFromNib() -> BankDetailFooter {
        return UINib(nibName: "BankDetailFooter", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! BankDetailFooter
    }
    
}
