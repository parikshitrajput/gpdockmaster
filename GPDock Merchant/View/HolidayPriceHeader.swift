//
//  HolidayPriceHeader.swift
//  GPDock Merchant
//
//  Created by TecOrb on 17/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit

class HolidayPriceHeader: UIView {
    @IBOutlet weak var selectDateButton : UIButton!
    @IBOutlet weak var datePriceView : UIView!

    //@IBOutlet weak var setAllSameButton : UIButton!

    override func layoutSubviews() {
        CommonClass.makeViewCircularWithCornerRadius(self.selectDateButton, borderColor: kApplicationGreenColor, borderWidth: 0, cornerRadius: 2)
        self.datePriceView.applyGradient(withColours: [kGradiantStartColor, kGradiantEndColor], gradientOrientation: .horizontal, locations: [0.1, 1.0])
//        self.setAllSameButton.setImage(#imageLiteral(resourceName: "check_light_unsel"), for: .normal)
//        self.setAllSameButton.setImage(#imageLiteral(resourceName: "check_blue_sel"), for: .selected)
    }

    class func instanceFromNib() -> HolidayPriceHeader {
        return UINib(nibName: "HolidayPriceHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! HolidayPriceHeader
    }

}
