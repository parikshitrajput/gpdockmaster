//
//  NotificationModel.swift
//  GPDock Merchant
//
//  Created by Parikshit on 08/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON


enum ServerNotificationType : String{
    case none = ""
    case customerCreateBooking = "customer_create_booking"
    case businessCreateBooking = "business_create_booking"
    case bookingCancelByCustomer = "booking_cancel_by_customer"
    case bookingCancelByBusiness = "booking_cancel_by_business"
    case bookingCompleteByBusiness = "booking_complete_by_business"
    case payoutByAdmin = "payout_by_admin"
    case payAtMarina = "pay_at_marina"
    case supportReplyByAdmin = "support_reply_by_admin"
    case changeMarinaStateByAdmin = "change_marina_state_by_admin"
    case rebookedInfo = "rebooked_info"
}

class NotificationModel: NSObject {
//
//    "id": 823,
//    "message": "Your booking has been cancelled and refund is initiated",
//    "status": true,
//    "notification_type": "booking_cancel_by_customer",
//    "seen_by_admin": false,
//    "admin_user_id": 22,
//    "received_at": " 14 hours ago",
    
    let kID =  "id"//: 1,
    let kMessage = "message"
    let kStatus = "status"
    let kNotificationType = "notification_type"
    let kSeenByAdmin = "seen_by_admin"
    let kAdminUserID = "admin_user_id"
    let kReceivedAt = "received_at"
    let kReceiver = "receiver"
    let kBooking = "booking"
    
    
    var ID =  ""//: 1,
    var  adminUserID = ""
    var message = ""
    var notificationType : ServerNotificationType = .none
    var isStatus:Bool = false
    var isSeenByAdmin:Bool = false
    var receivedAt = ""
    var booking  = Booking()
    var receiver = Receiver()
//    var QRCode = ""
//    var marinaID = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary:[String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }
        if let _adminUserID = dictionary[kAdminUserID] as? Int{
            self.adminUserID = "\(_adminUserID)"
        }else if let _adminUserID = dictionary[kAdminUserID] as? String{
            self.adminUserID = _adminUserID
        }
        
        if let _isStatus = dictionary[kStatus] as? Bool{
            self.isStatus = _isStatus
        }
        
        if let _isSeenByAdmin = dictionary[kSeenByAdmin] as? Bool{
            self.isSeenByAdmin = _isSeenByAdmin
        }
        if let _message = dictionary[kMessage] as? String{
            self.message = _message
        }
        if let _receivedAt = dictionary[kReceivedAt] as? String{
            self.receivedAt = _receivedAt
        }
        if let _notificationType = dictionary[kNotificationType] as? String{
            self.notificationType = ServerNotificationType(rawValue: _notificationType) ?? .none
        }
        if let _booking = dictionary[kBooking] as? Dictionary<String,AnyObject>{
            self.booking = Booking(dictionary: _booking)
        }
        
        if let _receiver = dictionary[kReceiver] as? Dictionary<String,AnyObject>{
            self.receiver = Receiver(dictionary: _receiver)
        }
        super.init()
    }
    
    
    init(json:JSON){
        
        if let _ID = json[kID].int
        {
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string{
            self.ID = _ID
        }
        if let _adminUserID = json[kAdminUserID].int
        {
            self.adminUserID = "\(_adminUserID)"
        }else if let _adminUserID = json[kAdminUserID].string{
            self.adminUserID = _adminUserID
        }
        if let _isStatus = json[kStatus].bool{
            self.isStatus = _isStatus
        }
        
        if let _isSeenByAdmin = json[kSeenByAdmin].bool{
            self.isSeenByAdmin = _isSeenByAdmin
        }
        if let _message = json[kMessage].string{
            self.message = _message
        }
        if let _notificationType = json[kNotificationType].string{
            self.notificationType = ServerNotificationType(rawValue: _notificationType) ?? .none
        }
        if let _receivedAt = json[kReceivedAt].string{
            self.receivedAt = _receivedAt
        }
        if let _booking = json[kBooking].dictionaryObject as Dictionary<String,AnyObject>?{
            self.booking = Booking(dictionary: _booking)
        }
        if let _receiver = json[kReceiver].dictionaryObject as Dictionary<String,AnyObject>?{
            self.receiver = Receiver(dictionary: _receiver)
        }
         super.init()
    }
    
}

class Receiver: NSObject {
    
//    fname : "Nakul"
//    lname : "Sharma"
//    contact : "+919624565545"
//    image : null
//    email : "nakul@tecorb.com"
    
    
    let kFirstName = "fname"
    let kLastName = "lname"
    let kEmail = "email"
    let kContact = "contact"
    let kProfileImage = "image"
    
    
    var firstName : String = ""
    var lastName : String = ""
    var email : String = ""
    var contact : String = ""
    var profileImage : String = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary : [String:AnyObject]){
        
        
        if let fName = dictionary[kFirstName] as? String{
            self.firstName = fName
        }
        
        if let lName = dictionary[kLastName] as? String{
            self.lastName = lName
        }
        if let userEmail = dictionary[kEmail] as? String{
            self.email = userEmail
        }
        
        if let userContact = dictionary[kContact] as? String{
            self.contact = userContact
        }
        if let userProfileImage = dictionary[kProfileImage] as? String{
            self.profileImage = userProfileImage
        }
        super.init()
    }
    
    init(json : JSON){
        
        if let fName = json[kFirstName].string as String?{
            self.firstName = fName
        }
        
        if let lName = json[kLastName].string as String?{
            self.lastName = lName
        }
        
        if let userEmail = json[kEmail].string as String?{
            self.email = userEmail
        }
        
        if let userContact = json[kContact].string as String?{
            self.contact = userContact
        }
        if let userProfileImage = json[kProfileImage].string as String?{
            self.profileImage = userProfileImage
        }
        super.init()
    }
    
  
}


class NotificationParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kNotifications = "notifications" //TODO:- Subject to change as bookings
    let kNotification = "notification"
    
    var responseCode:Int = 0
    var responseMessage = ""
    var notifications = [NotificationModel]()
    var notification = NotificationModel()
    
    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }
        if let bookingsArray = json[kNotifications].arrayObject as? [[String:AnyObject]]{
            for bookingDict in bookingsArray{
                let booking = NotificationModel(dictionary: bookingDict)
                self.notifications.append(booking)
            }
        }
        
        if let NotificationDict = json[kNotification].dictionaryObject as [String:AnyObject]?{
            self.notification = NotificationModel(dictionary: NotificationDict)
        }
        
        super.init()
    }
}





