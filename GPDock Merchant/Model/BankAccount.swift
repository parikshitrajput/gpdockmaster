//
//  BankAccount.swift
//  GPDock Merchant
//
//  Created by Parikshit on 28/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
class BankAccount: NSObject {
    /*
     "id" : 20,
     "account_type" : "individual",
     "last4" : "6789",
     "currency" : "usd",
     "country" : "US",
     "account_id" : "ba_1BT1eNGnjuiKNvVLyljCCkuW",
     "account_holder_name" : "Parikshit",
     "bank_name" : "STRIPE TEST BANK"
     default_bank_account
     */
    let kID = "id"
    let kLast4 = "last4"
    let kAccountType = "account_type"
    let kCountry = "country"
    let kCurrency = "currency"
    let kAccountID = "account_id"
    let kAccountHolderName = "account_holder_name"
    let kBankName = "bank_name"
    let kUser = "user"
    let kIsDefault = "default_bank_account"
    
    var ID : String = ""
    var last4: String = ""
    var accountType : String = ""
    var country : String = ""
    var currency : String = ""
    var accountID : String = ""
    var accountHolderName : String = ""
    var bankName : String = ""
    var isDefault : Bool = false
    var user = User()
    
    override init() {
        super.init()
    }
    
    init(params:Dictionary<String,AnyObject>) {
        
        if let _ID = params[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = params[kID] as? String{
            self.ID = _ID
        }
 
        if let _last4 = params[kLast4] as? String{
            self.last4 = _last4
        }
        if let _accountType = params[kAccountType] as? String{
            self.accountType = _accountType
        }
        if let _currency = params[kCurrency] as? String{
            self.currency = _currency
        }
        if let _accountID = params[kAccountID] as? Int{
            self.accountID = "\(_accountID)"
        }else if let _accountID = params[kAccountID] as? String{
            self.accountID = _accountID
        }
        if let _accountHolderName = params[kAccountHolderName] as? String{
            self.accountHolderName = _accountHolderName
        }
        if let _bankName = params[kBankName] as? String{
            self.bankName = _bankName
        }
        if let _user = params[kUser] as? Dictionary<String,AnyObject>{
            self.user = User(dictionary: _user)
        }
        if let _isDefault = params[kIsDefault] as? Bool{
            self.isDefault = _isDefault
        }

        super.init()
    }
    
    
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }
        
        if let _last4 = json[kLast4].string as String?{
            self.last4 = _last4
        }
        
        if let _accountType = json[kAccountType].string as String?{
            self.accountType = _accountType
        }
        if let _currency = json[kCurrency].string as String?{
            self.currency = _currency
        }
        if let _accountHolderName = json[kAccountHolderName].string as String?{
            self.accountHolderName = _accountHolderName
        }
        
        if let _bankName = json[kUser].string as String?{
            self.bankName = _bankName
        }
        if let _isDefault = json[kIsDefault].bool{
            self.isDefault = _isDefault
        }
        super.init()
    }
}


class BankAccountParser: NSObject {

//    let kResponseCode = "code"
//    let kResponseMessage = "message"
//    let kAccount = "account"
//    let kAccounts = "accounts"
    var responseCode = 0
    var responseMessage = ""
    var banksAccounts = Array<BankAccount>()
    var bankAccount = BankAccount()
    
    
    //    TODO:- It may subject to change if any other type of payment source would be added for payment by client
    //    var cards = Array<STPSourceProtocol>()
    
    override init() {
        super.init()
    }
    
    init(json: JSON) {
        if let _code = json["code"].int as Int?{
            self.responseCode = _code
        }
        
        if let _msg = json["message"].string as String?{
            self.responseMessage = _msg
        }
        if let _cardDict = json["account"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.bankAccount = BankAccount(params: _cardDict)
        }
        
        if let _cards = json["accounts"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for _cardDict in _cards{
                let card = BankAccount(params: _cardDict)
                self.banksAccounts.append(card)
            }
        }
        
        
    }
}
