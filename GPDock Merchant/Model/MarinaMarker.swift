//
//  MarinaMarker.swift
//  GPDock
//
//  Created by TecOrb on 28/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

//import UIKit
////import GoogleMaps
//
//class MarinaMarker: GMSMarker {
//    var marina: Marina!
//    var index:Int!
//    init(marina: Marina,index: Int) {
//        self.marina = marina
//        self.index = index
//        let coordinate = CLLocationCoordinate2D(latitude: marina.latitude, longitude: marina.longitude)
//        super.init()
//        self.position = coordinate
//        // self.title = String(format: "%.1f", marina.pricePerFeet)+"/ft"
//        self.icon = imageForMarinaMarker()
//    }
//
//    func  imageForMarinaMarker() -> UIImage {
//        let myview : UIView = UIView(frame: CGRect(x:0, y:0, width:50,height:78))
//        myview.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "mapTip"))
//        let myimageview = UIImageView(frame: CGRect(x:myview.frame.origin.x + 2,y: myview.frame.origin.y + 2, width:myview.frame.width - 2,height: myview.frame.height - 10))
//        myimageview.layer.cornerRadius = 5.0
//        myimageview.clipsToBounds = true
//        myimageview.contentMode = UIViewContentMode.scaleAspectFill
//
//
//
//        myimageview.clipsToBounds = true
//        myview.addSubview(myimageview)
//        let label = UILabel(frame: myimageview.frame)
//        label.textAlignment = .center
//        label.autoresizingMask = [.flexibleWidth,.flexibleHeight]
//        label.minimumScaleFactor = 0.5
//        label.adjustsFontSizeToFitWidth = true
//        label.text = "$"+String(format: "%.1f", marina.pricePerFeet)+"/ft "
//        label.font = UIFont.systemFont(ofSize: 9)
//        myview.addSubview(label)
//        label.center = CGPoint(x: myimageview.center.x, y: myimageview.center.y-10) // myimageview.center.y - 10
//        label.textColor = UIColor.black
//        myview.bringSubview(toFront: label)
//        return imageWithView(myview,scale: UIScreen.main.scale)
//    }
//
//    func  imageForSelectedMarinaMarker() -> UIImage {
//        let myview : UIView = UIView(frame: CGRect(x:0, y:0, width:50,height:78))
//        myview.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "mapTipSelected"))
//        let myimageview = UIImageView(frame: CGRect(x:myview.frame.origin.x + 2,y: myview.frame.origin.y + 2, width:myview.frame.width - 2,height: myview.frame.height - 10))
//        myimageview.layer.cornerRadius = 5.0
//        myimageview.clipsToBounds = true
//        myimageview.contentMode = UIViewContentMode.scaleAspectFill
//
//
//
//        myimageview.clipsToBounds = true
//        myview.addSubview(myimageview)
//        let label = UILabel(frame: myimageview.frame)
//        label.textAlignment = .center
//        label.autoresizingMask = [.flexibleWidth,.flexibleHeight]
//        label.minimumScaleFactor = 0.5
//        label.adjustsFontSizeToFitWidth = true
//        label.text = "$"+String(format: "%.1f", marina.pricePerFeet)+"/ft "
//        label.font = UIFont.systemFont(ofSize: 9)
//        myview.addSubview(label)
//        label.center = CGPoint(x: myimageview.center.x, y: myimageview.center.y-10) // myimageview.center.y - 10
//        label.textColor = UIColor.red
//        myview.bringSubview(toFront: label)
//        return imageWithView(myview,scale: UIScreen.main.scale)
////        let scImage = img.resizeImage(img.size.width + img.size.width*0.2)
////        return scImage //imageWithView(view: myview,scale: UIScreen.main.scale)
//    }
//
//    func imageWithView(_ view : UIView,scale:CGFloat) -> UIImage
//    {
//        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale)
//        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let myimg : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext()
//        return myimg
//    }
//
//
//}

