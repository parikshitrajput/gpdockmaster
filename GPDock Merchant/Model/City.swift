//
//  City.swift
//  GPDock
//
//  Created by TecOrb on 20/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class City: NSObject {

    let kID  = "id"
    let kName  = "name"
    let kImage  = "image"
    let kState = "state"

    var ID  = ""
    var name  = ""
    var image  = ""
    var state  = ""


    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }

        if let _name = json[kName].string as String?{
            self.name = _name
        }

        if let _image = json[kImage].string as String?{
            self.image = _image
        }
        if let _state = json[kState].string as String?{
            self.state = _state
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {

        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _name = dictionary[kName] as? String{
            self.name = _name
        }

        if let _image = dictionary[kImage] as? String{
            self.image = _image
        }

        if let _state = dictionary[kState] as? String{
            self.state = _state
        }
        super.init()
    }
}

class CityParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kCities = "cities"

    var responseCode:Int = 0
    var responseMessage = ""
    var cities = [City]()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let carArray = json[kCities].arrayObject as? [[String:AnyObject]]{
            for carDict in carArray{
                let city = City(dictionary: carDict)
                self.cities.append(city)
            }
        }
        super.init()
    }
}






//Marina Images model

class MarinaImage: NSObject {

    let kID  = "id"
    let kMarinaID  = "marina_id"
    let kUrl  = "url"

    var ID  = ""
    var marinaID  = ""
    var url  = ""

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }
        if let _mID = json[kMarinaID].int as Int?{
            self.marinaID = "\(_mID)"
        }else if let _mID = json[kMarinaID].string as String?{
            self.marinaID = _mID
        }
        if let _url = json[kUrl].string as String?{
            self.url = _url
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }
        if let _mID = dictionary[kMarinaID] as? Int{
            self.marinaID = "\(_mID)"
        }else if let _mID = dictionary[kMarinaID] as? String{
            self.marinaID = _mID
        }
        if let _url = dictionary[kUrl] as? String{
            self.url = _url
        }
        super.init()
    }
    
    
    
}


//Marina Services model

class ServiceParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kServices = "services"

    var responseCode:Int = 0
    var responseMessage = ""
    var services = [Service]()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let serviceArray = json[kServices].arrayObject as? [[String:AnyObject]]{
            for serviceDict in serviceArray{
                let service = Service(dictionary: serviceDict)
                self.services.append(service)
            }
        }
        super.init()
    }
}
class Service: NSObject {

    let kID  = "id"
    let kTitle  = "title"

    var ID  = ""
    var title  = ""

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }
        if let _title = json[kTitle].string as String?{
            self.title = _title
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _title = dictionary[kTitle] as? String{
            self.title = _title
        }
        super.init()
    }


    
}



//Marina Aminities model

class Aminity: NSObject {

    let kID  = "id"
    let kTitle  = "title"

    var ID  = ""
    var title  = ""

    override init() {
        super.init()
    }
    init(json: JSON) {
        if let _ID = json[kID].int as Int?{
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string as String?{
            self.ID = _ID
        }
        if let _title = json[kTitle].string as String?{
            self.title = _title
        }
        super.init()
    }
    init(dictionary: [String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _title = dictionary[kTitle] as? String{
            self.title = _title
        }
        super.init()
    }
    
    
    
}





