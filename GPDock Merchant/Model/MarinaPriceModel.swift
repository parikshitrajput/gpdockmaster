//
//  MarinaPriceModel.swift
//  GPDock Merchant
//
//  Created by TecOrb on 17/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class MarinaPriceModel: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kMarinaPrices = "marina_prices"

    let kRegularPrices = "default_price"
    let kWeekendPrice = "weekend_price"
    let kSpecificPrice = "specific_price"

    var code:Int = 0
    var message = ""
    var regularPrice = 0.0
    var weekendPrice = WeekendPrice()
    var specialDays = [DayPrice]()

    override init() {
        super.init()
    }
    
//    init(dictionary:[String:AnyObject]) {
//        
//        
//        super.init()
//    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.code = _rCode
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let marinaPriceDict = json[kMarinaPrices].dictionaryObject as [String:AnyObject]?{
            
            
            
            if let _price = marinaPriceDict[kRegularPrices] as? String{
                self.regularPrice = Double(_price) ?? 0.0
            }else if let _price = marinaPriceDict[kRegularPrices] as? Double{
                self.regularPrice = _price
            }else if let _price = marinaPriceDict[kRegularPrices] as? Int{
                self.regularPrice = Double(_price)
            }

            
            
            if let _weekendPrice = marinaPriceDict[kWeekendPrice] as? Dictionary<String,AnyObject>{
                self.weekendPrice = WeekendPrice(dictionary: _weekendPrice)
            }

            if let specialdaysDict = marinaPriceDict[kSpecificPrice] as? [String:AnyObject]{
                self.specialDays.removeAll()
                for(key,value) in specialdaysDict{
                    var price = 0.0
                    if let _price = value as? String{
                        price = Double(_price) ?? 0.0
                    }else if let _price = value as? Double{
                        price = _price
                    }else if let _price = value as? Int{
                        price = Double(_price)
                    }
                    let aDay = DayPrice(date: key, price: price)
                    self.specialDays.append(aDay)
                }
            }
        }
        super.init()
    }
}


class WeekendPrice: NSObject {
    let kWeekendType = "weekend_type"
    let kPrice = "price"
    
    var weekendType = ""
    var price = 0.0
    override init() {
        super.init()
    }
    init(dictionary:Dictionary<String,AnyObject>) {
        if let _weekendType = dictionary[kWeekendType] as? String{
            self.weekendType = _weekendType
        }
        
        if let _price = dictionary[kPrice] as? String{
            self.price = Double(_price) ?? 0.0
        }else if let _price = dictionary[kPrice] as? Double{
            self.price = _price
        }
        
        super.init()
    }
    
    
    init(json:JSON){
        if let _price = json[kPrice].double{
            self.price = _price
        }else if let _price = json[kPrice].string{
            self.price = Double(_price) ?? 0.0
        }
        if let _weekendType = json[kWeekendType].string{
            self.weekendType = _weekendType
        }
        super.init()

    }
}


class DayPrice: NSObject {
    let kDate = "date"
    let kPrice = "price"

    var date : String =  ""
    var price : Double =  0.0


    override init() {
        super.init()
    }

    init(date:String,price:Double) {
        self.date = date
        self.price = price
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let _date = dictionary[kDate] as? String{
            self.date = _date
        }

        if let _price = dictionary[kPrice] as? String{
            self.price = Double(_price) ?? 0.0
        }else if let _price = dictionary[kPrice] as? Double{
                self.price = _price
        }
        super.init()
    }

}
