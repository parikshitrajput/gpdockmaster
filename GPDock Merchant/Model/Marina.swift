//
//  Marina.swift
//  GPDock
//
//  Created by TecOrb on 23/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON





class Marina: NSObject {
    let kID =  "id"//: 1,
    let kTitle =  "title"//: "Harbourview Marina",
    let kDescription = "description"//: "About t one.",
    let kAddress = "address"//: "Bay Street",
    let kZipcode = "zipcode"//: null,
    let kCountry = "country"//: "USA",
    let kLatitude = "latitude"//: "26.545779",
    let kLongitude = "longitude"//: "-77.053126",
    let kPricePerFeet = "price_per_foot"//: "3",price_per_foot
    let kCheckIn = "checkin"//: "01:00 PM",
    let kCheckOut = "checkout"//: "11:00 AM",
    let kStatus = "status"//: true,
    let kRadioFrequency = "radio_frequency"//: "VHF 16",
    let kContact = "contact"//: "(242) 367-3910",
    let kReviewsCount = "reviews_count"//: 8,
    let kAverageRating = "average_rating"//: 4,
    let kCity = "city"
    let kUser = "user"
    let kImages = "images"
    let kImage = "image"
    let kServices = "services"
    let kAmenities = "amenities"
    let kQrImage = "qrimage"
    let kQrCode = "qrcode"
    let kPriceType = "price_type"
    let kWeekendType = "weekend_type"



    var ID =  ""//: 1,
    var title =  ""//: "Harbourview Marina",
    var marinaDescription = ""//: "About t one.",
    var address = ""//: "Bay Street",
    var zipcode = ""//: null,
    var country = ""//: "USA",
    var latitude : Double = 0.0//: "26.545779",
    var longitude:Double = 0.0//: "-77.053126",
    var pricePerFeet: Double = 0.0//"price_per_foot"//: "3",
    var checkIn = ""//: "01:00 PM",
    var checkOut = ""//: "11:00 AM",
    var status:Bool = false//: true,
    var radioFrequency = ""//: "VHF 16",
    var contact = ""//: "(242) 367-3910",
    var reviewsCount: Int = 0//: 8,
    var averageRating :Double = 0//"average_rating"//: 4,
    var city = City()
    var user = User()
    var images = [MarinaImage]()
    var image = MarinaImage()
    var services = [Service]()
    var aminities = [Aminity]()
    var qrImage = ""
    var qrCode = ""
    var priceType = ""
    var weekendType = ""

    override init() {
        super.init()
    }
    
    init(dictionary:[String:AnyObject]) {

        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }
        if let _title = dictionary[kTitle] as? String{
            self.title = _title
        }
        if let _desc = dictionary[kDescription] as? String{
            self.marinaDescription = _desc
        }
        if let _address = dictionary[kAddress] as? String{
            self.address = _address
        }
        if let _title = dictionary[kTitle] as? String{
            self.title = _title
        }
        if let _zipcode = dictionary[kZipcode] as? Int{
            self.zipcode = "\(_zipcode)"
        }else if let _zipcode = dictionary[kZipcode] as? String{
            self.zipcode = _zipcode
        }
        if let _country = dictionary[kCountry] as? String{
            self.country = _country
        }

        if let _Lat = dictionary[kLatitude] as? Double{
            self.latitude = _Lat
        }else if let _Lat = dictionary[kLatitude] as? String{
            self.latitude = Double(_Lat) ?? 0.0
        }

        if let _Longi = dictionary[kLongitude] as? Double{
            self.longitude = _Longi
        }else if let _Longi = dictionary[kLongitude] as? String{
            self.longitude = Double(_Longi) ?? 0.0
        }
        if let _pricePerFeet = dictionary[kPricePerFeet] as? Double{
            self.pricePerFeet = _pricePerFeet
        }else if let _pricePerFeet = dictionary[kPricePerFeet] as? String{
            self.pricePerFeet = Double(_pricePerFeet) ?? 0.0
        }

        if let _checkIn = dictionary[kCheckIn] as? String{
            self.checkIn = _checkIn
        }

        if let _checkOut = dictionary[kCheckOut] as? String{
            self.checkOut = _checkOut
        }

        if let _status = dictionary[kStatus] as? Bool{
            self.status = _status
        }

        if let _radioFrequency = dictionary[kRadioFrequency] as? String{
            self.radioFrequency = _radioFrequency
        }

        if let _contact = dictionary[kContact] as? Int{
            self.contact = "\(_contact)"
        }else if let _contact = dictionary[kContact] as? String{
            self.contact = _contact
        }

        if let _reviewsCount = dictionary[kReviewsCount] as? Int{
            self.reviewsCount = _reviewsCount
        }

        if let _avgRating = dictionary[kAverageRating] as? Double{
            self.averageRating = _avgRating
        }else if let _avgRating = dictionary[kAverageRating] as? String{
            self.averageRating = Double(_avgRating) ?? 0.0
        }

        if let _city = dictionary[kCity] as? [String:AnyObject]{
            self.city = City(dictionary: _city)
        }
        if let _user = dictionary[kUser] as? [String:AnyObject]{
            self.user = User(dictionary: _user)
        }

        if let _image = dictionary[kImage] as? [String:AnyObject]{
            self.image = MarinaImage(dictionary: _image)
        }

        if let _imagesArr = dictionary[kImages] as? [[String:AnyObject]]{
            for _image in _imagesArr{
                let image = MarinaImage(dictionary: _image)
                self.images.append(image)
            }
        }

        if let _servicesArr = dictionary[kServices] as? [[String:AnyObject]]{
            for _service in _servicesArr{
                let service = Service(dictionary: _service)
                self.services.append(service)
            }
        }
        if let _aminitiesArr = dictionary[kAmenities] as? [[String:AnyObject]]{
            for _aminity in _aminitiesArr{
                let aminity = Aminity(dictionary: _aminity)
                self.aminities.append(aminity)
            }
        }
        if let _qrImage = dictionary[kQrImage] as? String{
            self.qrImage = _qrImage
        }
        if let _qrCode = dictionary[kQrCode] as? String{
            self.qrCode = _qrCode
        }
        if let _priceType = dictionary[kPriceType] as? String{
            self.priceType = _priceType
        }
        if let _weekendType = dictionary[kWeekendType] as? String {
            self.weekendType = _weekendType
        }
        super.init()
    }
    
    func getMarinaPriceByDate(boatSize: BoatSize,fromDate:String,toDate:String,marinaPriceType:String,completionBlock:@escaping (_ marinaPrice:MarinaPriceReview?) -> Void) {
        
        let  boatLength = String(boatSize.length)
        MarinaService.sharedInstance.reviewBookingPrice(self.ID, boatLength:boatLength , fromDate: fromDate, toDate: toDate) { (suceess, marinaPrice) in
            completionBlock(marinaPrice)
        }
    }

    
    init(json:JSON){
    if let _ID = json[kID].int as Int?{
    self.ID = "\(_ID)"
    }else if let _ID = json[kID].string as String?{
    self.ID = _ID
    }
    if let _title = json[kTitle].string as String?{
    self.title = _title
    }
    if let _desc = json[kDescription].string as String?{
    self.marinaDescription = _desc
    }
    if let _address = json[kAddress].string as String?{
    self.address = _address
    }
    if let _title = json[kTitle].string as String?{
    self.title = _title
    }
    if let _zipcode = json[kZipcode].int as Int?{
    self.zipcode = "\(_zipcode)"
    }else if let _zipcode = json[kZipcode].string as String?{
    self.zipcode = _zipcode
    }
    if let _country = json[kCountry].string as String?{
    self.country = _country
    }

    if let _Lat = json[kLatitude].double as Double?{
    self.latitude = _Lat
    }else if let _Lat = json[kLatitude].string as String?{
    self.latitude = Double(_Lat) ?? 0.0
    }

    if let _Longi = json[kLongitude].double as Double?{
    self.longitude = _Longi
    }else if let _Longi = json[kLongitude].string as String?{
    self.longitude = Double(_Longi) ?? 0.0
    }
    if let _pricePerFeet = json[kPricePerFeet].double as Double?{
    self.pricePerFeet = _pricePerFeet
    }else if let _pricePerFeet = json[kPricePerFeet].string as String?{
    self.pricePerFeet = Double(_pricePerFeet) ?? 0.0
    }

    if let _checkIn = json[kCheckIn].string as String?{
    self.checkIn = _checkIn
    }

    if let _checkOut = json[kCheckOut].string as String?{
    self.checkOut = _checkOut
    }

    if let _status = json[kStatus].bool as Bool?{
    self.status = _status
    }

    if let _radioFrequency = json[kRadioFrequency].string as String?{
    self.radioFrequency = _radioFrequency
    }

    if let _contact = json[kContact].int as Int?{
    self.contact = "\(_contact)"
    }else if let _contact = json[kContact].string as String?{
    self.contact = _contact
    }

    if let _reviewsCount = json[kReviewsCount].int as Int?{
    self.reviewsCount = _reviewsCount
    }

    if let _avgRating = json[kAverageRating].double as Double?{
    self.averageRating = _avgRating
    }else if let _avgRating = json[kAverageRating].string as String?{
    self.averageRating = Double(_avgRating) ?? 0.0
    }

    if let _city = json[kCity].dictionaryObject as [String:AnyObject]?{
    self.city = City(dictionary: _city)
    }
    if let _user = json[kUser].dictionaryObject as [String:AnyObject]?{
    self.user = User(dictionary: _user)
    }

    if let _image = json[kImage].dictionaryObject as [String:AnyObject]?{
    self.image = MarinaImage(dictionary: _image)
    }

    if let _imagesArr = json[kImages].arrayObject as? [[String:AnyObject]]{
    for _image in _imagesArr{
    let image = MarinaImage(dictionary: _image)
    self.images.append(image)
    }
    }

    if let _servicesArr = json[kServices].arrayObject as? [[String:AnyObject]]{
    for _service in _servicesArr{
    let service = Service(dictionary: _service)
    self.services.append(service)
    }
    }
    if let _aminitiesArr = json[kAmenities].arrayObject as? [[String:AnyObject]]{
    for _aminity in _aminitiesArr{
    let aminity = Aminity(dictionary: _aminity)
    self.aminities.append(aminity)
    }
    }
    if let _qrImage = json[kQrImage].string as String?{
            self.qrImage = _qrImage
        }
    if let _qrCode = json[kQrCode].string as String?{
            self.qrCode = _qrCode
        }
    if let _priceType = json[kPriceType].string as String?{
            self.priceType = _priceType
        }
        if let _weekendType = json[kWeekendType].string as String? {
            self.weekendType = _weekendType
        }

    super.init()
    }




    func saveMarinaInfo(_ marina:Marina) {
        let documentPath = NSHomeDirectory() + "/Documents/"
        var marinaInfo = [String:String]()
        marinaInfo.updateValue(marina.ID, forKey:kID)
        marinaInfo.updateValue(marina.title, forKey:kTitle)
        marinaInfo.updateValue(marina.checkIn, forKey:kCheckIn)
        marinaInfo.updateValue(marina.checkOut, forKey:kCheckOut)
        marinaInfo.updateValue(marina.checkOut, forKey:kCheckOut)
        marinaInfo.updateValue(marina.priceType, forKey: kPriceType)
        marinaInfo.updateValue(marina.weekendType, forKey: kWeekendType)

        marinaInfo.updateValue("\(marina.pricePerFeet)", forKey:kPricePerFeet)

        do {
            let data = try JSON(marinaInfo).rawData(options: [.prettyPrinted])
            let path = documentPath + "marina"
            try data.write(to: URL(fileURLWithPath: path), options: .atomic)
            print_debug(path)
        }catch{
            print_debug("error in saving marinaInfo")
        }
        UserDefaults.standard.synchronize()
        NotificationCenter.default.post(name: .USER_DID_SELECT_MARINA_NOTIFICATION, object: nil, userInfo: ["marina":marina])
    }



    class func loadMarinaInfo()->JSON {
        let documentPath = NSHomeDirectory() + "/Documents/"
        let path = documentPath + "marina"
        var data = Data()
        var json : JSON
        do{
            data = try Data(contentsOf: URL(fileURLWithPath: path))
            json = JSON(data: data)
        }catch{
            json = JSON.init(data)
            print_debug("error in getting marinainfo")
        }
        return json
    }

}
class MarinaPriceReview: NSObject {
    var code:Int = 0
    var message = ""
    var totalPrice : Double = 0.0

    override init() {
        super.init()
    }
    
    init(json:JSON) {
        if let _rCode = json["code"].int as Int?{
            self.code = _rCode
        }
        if let _rMessage = json["message"].string as String?{
            self.message = _rMessage
        }
        
        
        if let _marinaPriceDict = json["marina_price"].dictionaryObject as Dictionary<String,AnyObject>?{

            if let _price = _marinaPriceDict["total_price"] as? Double{
                self.totalPrice = _price
            }else if let _price = _marinaPriceDict["total_price"] as? String{
                self.totalPrice = Double(_price) ?? 0.0
            }
        }
        super.init()
    }
}

class MarinaParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kMarinas = "marinas"
    let kSingleMarina = "marina"

    var marina = Marina()
    var responseCode:Int = 0
    var responseMessage = ""
    var marinas = [Marina]()

    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }
        if let rideArray = json[kMarinas].arrayObject as? [[String:AnyObject]]{
            for rideDict in rideArray{
                let marina = Marina(dictionary: rideDict)
                self.marinas.append(marina)
            }
        }
        
        //in case of single marina
        if let marinaDict = json[kSingleMarina].dictionaryObject as [String:AnyObject]?{
            self.marina = Marina(dictionary: marinaDict)
        }

        super.init()
    }
}
class PriceModel:NSObject{
    var price : Double = 0.0
    var totalDays : Int = 0
    var date : String = ""
    override init() {
        super.init()
    }
    
    init(date:String,price:Double) {
        self.date = date
        self.price = price
        super.init()
    }
    
    init(dictionary:Dictionary<String,AnyObject>) {
        if let _totalDays = dictionary["total_day"] as? Int{
            self.totalDays = _totalDays
        }else if let _totalDays = dictionary["total_day"] as? String{
            self.totalDays = Int(_totalDays) ?? 0
        }else if let _totalDays = dictionary["weekend_total"] as? Int{
            self.totalDays = _totalDays
        }else if let _totalDays = dictionary["weekend_total"] as? String{
            self.totalDays = Int(_totalDays) ?? 0
        }
        
        if let _price = dictionary["price"] as? Double{
            self.price = _price
        }else if let _price = dictionary["price"] as? String{
            self.price = Double(_price) ?? 0
        }
        
        if let _date = dictionary["date"] as? String{
            self.date = _date
        }
        
        super.init()
    }
   
}

class MarinaQRProfileParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kMarinas = "marina"
    
    
    var responseCode:Int = 0
    var responseMessage = ""
    var marina = Marina()
    
    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }
        if let marinaDict = json[kMarinas].dictionaryObject as [String:AnyObject]?{
            self.marina = Marina(dictionary: marinaDict)
        }
        
        super.init()
    }
}
