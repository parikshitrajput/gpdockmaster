//
//  Booking.swift
//  GPDock
//
//  Created by TecOrb on 02/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON




enum BookingStatus: String {
    case upcoming = "upcoming"
    case completed = "completed"
    case cancelled = "cancelled"
}

enum CancellationStatus: String {
    case cancellationInitiated = "Cancellation Initiated"
    case cancellationSuccess = "Cancellation Success"
    case refundInitiated = "Refund Initiated"
    case refundProcessed = "Refund Processed"
}




class Booking: NSObject {

    let kID =  "id"//: 1,
    let kPricePerFeet = ""
    let kFromDate = "from_date"
    let kToDate = "to_date"
    let kIsCancelled = "is_cancelled"
    let kIsCompleted = "is_completed"
    let kQRImage = "qrimage"
    let kQRCode = "qrcode"
    let kMarinaID = "marina_id"
    let kAmount = "amount"
    let kPaid = "paid"
    let kDeparture = "is_departure"
    let kArrival = "is_arrival"
    let kStatus = "status"
    let kCancelReason = "cancel_reason"
    let kTransactionID = "transaction_id"
    let kCompletedAt = "completed_at"
     let kCancelledAt = "cancelled_at"
    let kUser = "user"
    let kBoat = "boat"
    let kParkingSpace = "parking_space"
    let kMarina = "marina"
    let kRating = "rating"
    let kReview = "review"
    let kBookingType = "booking_type"
    let kBusinessAmount = "business_amount"
    
    
    var ID =  ""//: 1,
    var pricePerFeet:Double = 0.0
    var fromDate = ""
    var toDate = ""
    var isCancelled:Bool = false
    var isCompleted:Bool = false
    var QRImage = ""
    var QRCode = ""
    var marinaID = ""
    var amount:Double = 0.0
    var businessAmount: Double = 0.0
    var paid:Bool = false
    var departure:Bool = false
    var arrival: Bool = false
    var status : Bool = false
    var cancelReason = ""
    var transactionID = ""
    var completedAt = ""
    var cancelledAt = ""
    var bookingType = ""
    var user = User()
    var boat = Boat()
    var parkingSpace = ParkingSpace()
    var marina = Marina()
    var review = Review()
    var ratingOfMarinaOwer = RatingFromMarina()

    var bookingStatus : BookingStatus = BookingStatus.upcoming
    var cancellationStatus : CancellationStatus = CancellationStatus.cancellationSuccess

    //for
    override init() {
        super.init()
    }

    init(dictionary:[String:AnyObject]) {

        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }

        if let _ppf = dictionary[kPricePerFeet] as? Double{
            self.pricePerFeet = _ppf
        }else if let _ppf = dictionary[kPricePerFeet] as? String{
            self.pricePerFeet = Double(_ppf) ?? 0.0
        }

        if let _isCancelled = dictionary[kIsCancelled] as? Bool{
            self.isCancelled = _isCancelled
        }

        if let _iscompleted = dictionary[kIsCompleted] as? Bool{
            self.isCompleted = _iscompleted
        }

        if let _qr = dictionary[kQRCode] as? String{
            self.QRCode = _qr
        }

        if let _fromDate = dictionary[kFromDate] as? String{
            self.fromDate = CommonClass.dateWithString(_fromDate)
        }
        if let _toDate = dictionary[kToDate] as? String{
            self.toDate = CommonClass.dateWithString(_toDate)
        }
        if let _fromDate = dictionary[kCompletedAt] as? String{
            self.completedAt = CommonClass.dateWithString(_fromDate)
        }
        if let _toDate = dictionary[kCancelledAt] as? String{
            self.cancelledAt = CommonClass.dateWithString(_toDate)
        }

        if let _bookingType = dictionary[kBookingType] as? String{
            self.bookingType = _bookingType
        }


        if let _qrImage = dictionary[kQRImage] as? Int{
            self.QRImage = "\(_qrImage)"
        }else if let _qrImage = dictionary[kQRImage] as? String{
            self.QRImage = _qrImage
        }

        if let _marinaID = dictionary[kMarinaID] as? Int{
            self.marinaID = "\(_marinaID)"
        }else if let _marinaID = dictionary[kMarinaID] as? String{
            self.marinaID = _marinaID
        }

        if let _amount = dictionary[kAmount] as? Double{
            self.amount = _amount/100.0
        }else if let _amount = dictionary[kAmount] as? String{
            let amnt = Double(_amount) ?? 0.0
            self.amount = amnt/100.0
        }
        if let _businessAmount = dictionary[kBusinessAmount] as? Double{
            self.businessAmount = _businessAmount/100.0
        }else if let _businessAmount = dictionary[kBusinessAmount] as? String{
            let amnt = Double(_businessAmount) ?? 0.0
            self.businessAmount = amnt/100.0
        }
        if let _isPaid = dictionary[kPaid] as? Bool{
            self.paid = _isPaid
        }
        if let _isDeparture = dictionary[kDeparture] as? Bool{
            self.departure = _isDeparture
        }
        if let _isArrival = dictionary[kArrival] as? Bool{
            self.arrival = _isArrival
        }
        if let _status = dictionary[kStatus] as? Bool{
            self.status = _status
        }

        if let _cancelReason = dictionary[kCancelReason] as? String{
            self.cancelReason = _cancelReason
        }

        if let _trID = dictionary[kTransactionID] as? String{
            self.transactionID = _trID
        }

        if let _user = dictionary[kUser] as? Dictionary<String,AnyObject>{
            self.user = User(dictionary: _user)
        }
        if let _boat = dictionary[kBoat] as? Dictionary<String,AnyObject>{
            self.boat = Boat(dictionary: _boat)
        }
        if let _parkingSpace = dictionary[kParkingSpace] as? Dictionary<String,AnyObject>{
            self.parkingSpace = ParkingSpace(dictionary: _parkingSpace)
        }
        if let _marina = dictionary[kMarina] as? Dictionary<String,AnyObject>{
            self.marina = Marina(dictionary: _marina)
        }
        
        if let _reviewRating = dictionary[kReview] as? Dictionary<String,AnyObject>{
            self.review = Review(dictionary: _reviewRating)
        }
        if let _rating = dictionary[kRating] as? Dictionary<String,AnyObject>{
            self.ratingOfMarinaOwer = RatingFromMarina(dictionary: _rating)
        }



        super.init()

        if self.isCancelled{self.bookingStatus = BookingStatus.cancelled}
        else if (!self.isCancelled && self.isCompleted){self.bookingStatus = BookingStatus.completed}
        else{self.bookingStatus = BookingStatus.upcoming}


    }

    init(json:JSON){

        if let _ID = json[kID].int
        {
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string{
            self.ID = _ID
        }

        if let _ppf = json[kPricePerFeet].double{
            self.pricePerFeet = _ppf
        }else if let _ppf = json[kPricePerFeet].string{
            self.pricePerFeet = Double(_ppf) ?? 0.0
        }

        if let _isCancelled = json[kIsCancelled].bool{
            self.isCancelled = _isCancelled
        }

        if let _iscompleted = json[kIsCompleted].bool{
            self.isCompleted = _iscompleted
        }

        if let _qr = json[kQRCode].string{
            self.QRCode = _qr
        }

        if let _fromDate = json[kFromDate].string{
            self.fromDate = CommonClass.dateWithString(_fromDate)
        }
        if let _toDate = json[kToDate].string{
            self.toDate = CommonClass.dateWithString(_toDate)
        }
        if let _fromDate = json[kCompletedAt].string{
            self.completedAt = CommonClass.dateWithString(_fromDate)
        }
        if let _toDate = json[kCancelledAt].string{
            self.cancelledAt = CommonClass.dateWithString(_toDate)
        }
        if let _bookingType = json[kBookingType].string{
            self.bookingType = _bookingType
        }
        
        if let _qrImage = json[kQRImage].int{
            self.QRImage = "\(_qrImage)"
        }else if let _qrImage = json[kQRImage].string{
            self.QRImage = _qrImage
        }

        if let _marinaID = json[kMarinaID].int{
            self.marinaID = "\(_marinaID)"
        }else if let _marinaID = json[kMarinaID].string{
            self.marinaID = _marinaID
        }

        if let _amount = json[kAmount].double{
            self.amount = _amount
        }else if let _amount = json[kAmount].string{
            self.amount = Double(_amount) ?? 0.0
        }
        if let _businessAmount = json[kBusinessAmount].double{
            self.businessAmount = _businessAmount
        }else if let _businessAmount = json[kBusinessAmount].string{
            self.businessAmount = Double(_businessAmount) ?? 0.0
        }
        if let _isPaid = json[kPaid].bool{
            self.paid = _isPaid
        }
        if let _isDeparture = json[kDeparture].bool{
            self.departure = _isDeparture
        }
        if let _isArrival = json[kArrival].bool{
            self.arrival = _isArrival
        }
        if let _status = json[kStatus].bool{
            self.status = _status
        }

        if let _cancelReason = json[kCancelReason].string{
            self.cancelReason = _cancelReason
        }

        if let _trID = json[kTransactionID].string{
            self.transactionID = _trID
        }
        if let _completedAt = json[kCompletedAt].string as String?{
            self.completedAt = _completedAt
        }
        if let _completedAt = json[kCancelledAt].string as String?{
            self.cancelledAt = _completedAt
        }
        if let _user = json[kUser].dictionaryObject as Dictionary<String,AnyObject>?{
            self.user = User(dictionary: _user)
        }
        if let _boat = json[kBoat].dictionaryObject as Dictionary<String,AnyObject>?{
            self.boat = Boat(dictionary: _boat)
        }
        if let _parkingSpace = json[kParkingSpace].dictionaryObject as Dictionary<String,AnyObject>?{
            self.parkingSpace = ParkingSpace(dictionary: _parkingSpace)
        }
        if let _marina = json[kMarina].dictionaryObject as Dictionary<String,AnyObject>?{
            self.marina = Marina(dictionary: _marina)
        }
        
        if let _rating = json[kRating].dictionaryObject as Dictionary<String,AnyObject>?{
            self.ratingOfMarinaOwer = RatingFromMarina(dictionary: _rating)
        }
        
        if let _reviewRating = json[kReview].dictionaryObject as Dictionary<String,AnyObject>?{
            self.review = Review(dictionary: _reviewRating)
        }

        super.init()
        
        if self.isCancelled{self.bookingStatus = BookingStatus.cancelled}
        else if (!self.isCancelled && self.isCompleted){self.bookingStatus = BookingStatus.completed}
        else{self.bookingStatus = BookingStatus.upcoming}
    }
}

class RatingFromMarina: NSObject {
    
    let kRating = "rating"
    let kCreatedAt = "created_at"
    
    var rating:Float = 0.0
    var createdAt: String = ""
    override init() {
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        
        if let _rating = dictionary[kRating] as? String{
            self.rating = Float(_rating) ?? 0.0
        }else if let _rating = dictionary[kRating] as? Float {
            self.rating = _rating
        }
        
        if let _createdAt = dictionary[kCreatedAt] as? String{
            self.createdAt = _createdAt
        }
    }
    
    init(json:JSON){
//        if let _rating = json[kRating].double as Double?{
//            self.rating = _rating
//        }else
            if let _rating = json[kRating].string as String?{
            self.rating = Float(_rating) ?? 0.0
            }else if let _rating = json[kRating].float as Float? {
                self.rating = _rating
        }
        if let _createdAt = json[kCreatedAt].string as String?{
            self.createdAt = _createdAt
        }
        super.init()

    }
}




class BookingParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kBookings = "bookings" //TODO:- Subject to change as bookings
    let kBooking = "booking"
    
    var responseCode:Int = 0
    var responseMessage = ""
    var bookings = [Booking]()
    var booking = Booking()

    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }
        if let bookingsArray = json[kBookings].arrayObject as? [[String:AnyObject]]{
            for bookingDict in bookingsArray{
                let booking = Booking(dictionary: bookingDict)
                self.bookings.append(booking)
            }
        }
        
        if let bookingsArray = json[kBooking].arrayObject as? [[String:AnyObject]]{
            for bookingDict in bookingsArray{
                let booking = Booking(dictionary: bookingDict)
                self.bookings.append(booking)
            }
        }

        if let bookingDict = json[kBooking].dictionaryObject as [String:AnyObject]?{
            self.booking = Booking(dictionary: bookingDict)
        }
        
        super.init()
    }
}

class CountBooking: NSObject {
//    "today_booking_counts": 2,
//    "upcoming_booking_counts": 6
   // departure_booking_counts
    let kTodayBookingCount = "today_booking_counts"
    let kUpComingCount = "upcoming_booking_counts"
     let kDepartudeCount = "departure_booking_counts"
    var todayBookingCount:String = "0"
    var upComingBookingCount :String = "0"
     var departureBookingCount :String = "0"
    override init() {
        super.init()
    }
    
    init(dictionary:Dictionary<String,AnyObject>) {
        if let _todayCount = dictionary[kTodayBookingCount] as? Int{
           self.todayBookingCount = "\(_todayCount)"
        }else if let _todayCount = dictionary[kTodayBookingCount] as? String{
               self.todayBookingCount = _todayCount
        }
        if let _upcomingCount = dictionary[kUpComingCount] as? Int{
          self.upComingBookingCount = "\(_upcomingCount)"
        }else if let _upcomingCount = dictionary[kUpComingCount] as? String{
               self.upComingBookingCount = _upcomingCount
        }
        if let _departureCount = dictionary[kDepartudeCount] as? Int{
            self.departureBookingCount = "\(_departureCount)"
        }else if let _departureCount = dictionary[kDepartudeCount] as? String{
            self.departureBookingCount = _departureCount
        }
        super.init()
    }
    
}



class CountBookingParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
     let kCount = "count"
    
    var responseCode:Int = 0
    var responseMessage = ""
    var count = CountBooking()
    
    
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        
        if let countDict = json[kCount].dictionaryObject as [String:AnyObject]?{
            self.count = CountBooking(dictionary: countDict)
        }
        
        super.init()
    }
}

class RefundBookingDetail :NSObject {
//    "current_stage": "Not initiated",
//    "amount": 0,
//    "updated_at": null
    let kAmount = "amount"
    let kCurrentStage = "current_stage"
    let kUpdatedAt = "updated_at"
    
    
    var amount : Double = 0.0
    var currentStage : String = ""
    var updatedAt:String = ""
    override init() {
        super.init()
    }
      init(dictionary:Dictionary<String,AnyObject>) {
//        if let _amount = dictionary[kAmount] as? Double{
//            self.amount = _amount
//        }else if let _amount = dictionary[kAmount] as? String{
//            self.amount = Double(_amount) ?? 0.0
//        }
        if let _amount = dictionary[kAmount] as? Double{
            self.amount = _amount/100.0
        }else if let _amount = dictionary[kAmount] as? String{
            let amnt = Double(_amount) ?? 0.0
            self.amount = amnt/100.0
        }

        if let _updated = dictionary[kUpdatedAt] as? String{
            self.updatedAt = CommonClass.dateWithString(_updated)
        }
        if let _currentStage = dictionary[kCurrentStage] as? String{
            self.currentStage = _currentStage
        }
        
        super.init()
    }
    init(json:JSON){
        if let _amount = json[kAmount].double{
            self.amount = _amount
        }else if let _amount = json[kAmount].string{
            self.amount = Double(_amount) ?? 0.0
        }
        if let _currentStage = json[kCurrentStage].string{
            self.currentStage = _currentStage
        }
        if let _updated = json[kUpdatedAt].string{
            self.updatedAt = CommonClass.dateWithString(_updated)
        }

    }

}

class RefundBookingDetailPaser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kRefund = "refund"
    var responseCode:Int = 0
    var responseMessage = ""
    var refund = RefundBookingDetail()
    
    override init() {
        super.init()
    }
    
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }
        
        if let refundDict = json[kRefund].dictionaryObject as [String:AnyObject]?{
            self.refund = RefundBookingDetail(dictionary: refundDict)
        }
        super.init()
    }
}









class RefundPaser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kRefund = "refund"
    var responseCode:Int = 0
    var responseMessage = ""
    var refund = Refund()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let refundDict = json[kRefund].dictionaryObject as [String:AnyObject]?{
            self.refund = Refund(dictionary: refundDict)
        }
        super.init()
    }
}







class Refund: NSObject {
    let kProcessStages = "process_stages"


    let kCancellationInitiated = "cancellation_initiated"
    let kCancellationSuccessful = "cancellation_successful"
    let kRefundInitiated = "refund_initiated"
    let kRefundCompleted = "refund_completed"
    let kTrackingId = "tracking_id"
    let kAmount = "amount"
    let kCancellationCharge = "cancellationCharge"
    let kUpdatedAt = "updated_at"
    let kCard = "card"
    let kCreditTime = "credit_time"


    var cancellationInitiated:Bool = false
    var cancellationSuccessful:Bool = false
    var refundInitiated:Bool = false
    var refundCompleted:Bool = false
    var card = Card()
    var trackingId:String = ""
    var amount : Double = 0.0
    var cancellationCharge : Double = 0.0

    var updatedAt:String = ""

    var creditTime:String = ""

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let _amount = dictionary[kAmount] as? Double{
            self.amount = _amount
        }else if let _amount = dictionary[kAmount] as? String{
            self.amount = Double(_amount) ?? 0.0
        }

        if let _trackingID = dictionary[kTrackingId] as? String{
            self.trackingId = _trackingID
        }

        if let _cancellationCharge = dictionary[kCancellationCharge] as? Double{
            self.cancellationCharge = _cancellationCharge
        }else if let _cancellationCharge = dictionary[kCancellationCharge] as? String{
            self.cancellationCharge = Double(_cancellationCharge) ?? 0.0
        }


        if let _processStagesDict = dictionary[kProcessStages] as? [String:AnyObject]{
            if let _cancellationInitiated = _processStagesDict[kCancellationInitiated] as? Bool{
                self.cancellationInitiated = _cancellationInitiated
            }
            if let _cancellationSuccessful = _processStagesDict[kCancellationSuccessful] as? Bool{
                self.cancellationSuccessful = _cancellationSuccessful
            }

            if let _refundInitiated = _processStagesDict[kRefundInitiated] as? Bool{
                self.refundInitiated = _refundInitiated
            }

            if let _refundCompleted = _processStagesDict[kRefundCompleted] as? Bool{
                self.refundCompleted = _refundCompleted
            }
        }

        if let _card = dictionary[kCard] as? [String:AnyObject]{
            self.card = Card(params: _card)
        }

        if let _updatedOn = dictionary[kUpdatedAt] as? String{
            self.updatedAt = _updatedOn
        }

        if let _createdTime = dictionary[kCreditTime] as? String{
            self.creditTime = _createdTime
        }

        super.init()
    }
}





/*
 {
 "code": 200,
 "message": "success! Cancellation Refund details",
 "refundable": {
 "is_refundable": true,
 "cancellationPercentage": 0,
 "cancellationCharge": 0,
 "amountToBeRefund": 72000,
 "chargeApplyAfterHours": 24,
 "hoursPassedTillNow": 15.68,
 "message": "Refund will be process after cancellations! cancellation charge apply after 24 hours, 15.68 hours has been passed from booking"
 }
 }
 */

class RefundableDetailsPaser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kRefundableDetails = "refundable"
    var responseCode:Int = 0
    var responseMessage = ""
    var refundableDetails = RefundableDetails()

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let _refundableDetails = json[kRefundableDetails].dictionaryObject as [String:AnyObject]?{
            self.refundableDetails = RefundableDetails(dictionary: _refundableDetails)
        }

        super.init()
    }
}


class RefundableDetails: NSObject {

    /*
     "is_refundable": true,
     "cancellationPercentage": 0,
     "cancellationCharge": 0,
     "amountToBeRefund": 72000,
     "chargeApplyAfterHours": 24,
     "hoursPassedTillNow": 15.68,
     "message": "Refund will be process after cancellations! cancellation charge apply after 24 hours, 15.68 hours has been passed from booking"
     */

    let kIsRefundable = "is_refundable"
    let kAmountToBeRefund = "amountToBeRefund"
    let kCancellationCharge = "cancellationCharge"
    let kCancellationPercentage = "cancellationPercentage"
    let kChargeApplyAfterHours = "chargeApplyAfterHours"
    let kHoursPassedTillNow = "hoursPassedTillNow"
    let kmessage = "message"

    var isRefundable:Bool = false
    var message:String = ""
    var amountToBeRefund : Double = 0.0
    var cancellationCharge : Double = 0.0
    var cancellationPercentage : Double = 0.0
    var chargeApplyAfterHours : Double = 0.0
    var hoursPassedTillNow : Double = 0.0

    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String,AnyObject>) {
        if let _amountToBeRefund = dictionary[kAmountToBeRefund] as? Double{
            self.amountToBeRefund = _amountToBeRefund
        }else if let _amountToBeRefund = dictionary[kAmountToBeRefund] as? String{
            self.amountToBeRefund = Double(_amountToBeRefund) ?? 0.0
        }

        if let _message = dictionary[kmessage] as? String{
            self.message = _message
        }
        if let _isRefundable = dictionary[kIsRefundable] as? Bool{
            self.isRefundable = _isRefundable
        }

        if let _cancellationCharge = dictionary[kCancellationCharge] as? Double{
            self.cancellationCharge = _cancellationCharge
        }else if let _cancellationCharge = dictionary[kCancellationCharge] as? String{
            self.cancellationCharge = Double(_cancellationCharge) ?? 0.0
        }

        if let _cancellationPercentage = dictionary[kCancellationPercentage] as? Double{
            self.cancellationPercentage = _cancellationPercentage
        }else if let _cancellationPercentage = dictionary[kCancellationCharge] as? String{
            self.cancellationPercentage = Double(_cancellationPercentage) ?? 0.0
        }

        if let _chargeApplyAfterHours = dictionary[kChargeApplyAfterHours] as? Double{
            self.chargeApplyAfterHours = _chargeApplyAfterHours
        }else if let _chargeApplyAfterHours = dictionary[kChargeApplyAfterHours] as? String{
            self.chargeApplyAfterHours = Double(_chargeApplyAfterHours) ?? 0.0
        }

        if let _hoursPassedTillNow = dictionary[kHoursPassedTillNow] as? Double{
            self.hoursPassedTillNow = _hoursPassedTillNow
        }else if let _hoursPassedTillNow = dictionary[kHoursPassedTillNow] as? String{
            self.hoursPassedTillNow = Double(_hoursPassedTillNow) ?? 0.0
        }
        super.init()
    }
}


/*
 {
 "code": 200,
 "message": "success! Refund details",
 "refund": {
 "current_stage": "Cancellation initiated",
 "amount": "390000.0",
 "updated_at": "2017-09-15T13:00:51.000Z"
 }
 }
 */

class RefundCurrentStatus: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kRefund = "refund"

    let kCurrentStage = "current_stage"
    let kAmount = "amount"
    let kUpdatedAt = "updated_at"

    var responseCode:Int = 0
    var responseMessage = ""

    var currentStage = "Unknown"
    var amountToBeRefund:Double = 0.0
    var lastUpdatedOn = ""

    override init() {
        super.init()
    }

    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }

        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }

        if let _refundDict = json[kRefund].dictionaryObject{
            if let _amountToBeRefund = _refundDict[kAmount] as? Double{
                self.amountToBeRefund = _amountToBeRefund
            }else if let _amountToBeRefund = _refundDict[kAmount] as? String{
                self.amountToBeRefund = Double(_amountToBeRefund) ?? 0.0
            }

            if let _currentStage = json[kCurrentStage].string as String?{
                self.currentStage = _currentStage
            }
            
            if let _lastUpdateOn = json[kUpdatedAt].string as String?{
                self.lastUpdatedOn = _lastUpdateOn
            }
        }
        super.init()
    }
}



