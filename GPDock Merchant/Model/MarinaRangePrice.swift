//
//  MarinaRangePrice.swift
//  GPD Dockmaster
//
//  Created by Parikshit on 11/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
class MarinaRangePrice: NSObject {
    let kPrices = "prices"
    let kPriceFor = "price_for"
    let kSeasonStartDate = "season_start_date"
    let kSeasonEndDate = "season_end_date"
    
    var prices = [Prices]()
    var priceFor = ""
    var seasonStartDate = ""
    var seasonEndDate = ""
    
    override init() {
        super.init()
    }
    init(params:Dictionary<String,AnyObject>) {
        if let _prices = params[kPrices] as? [[String:AnyObject]]{
            for _price in _prices{
                let price = Prices(params: _price)
                self.prices.append(price)
            }
        }
        if let _priceFor = params[kPriceFor] as? String{
            self.priceFor = _priceFor
        }
        
        if let _startDate = params[kSeasonStartDate] as? String{
            self.seasonStartDate = _startDate
        }
        if let _endDate = params[kSeasonEndDate] as? String{
            self.seasonEndDate = _endDate
        }
        
        super.init()
        let sortedPrices = self.prices.sorted { (pr1, pr2) -> Bool in
            return pr1.sizeRange.from < pr2.sizeRange.from
        }
        self.prices = sortedPrices
    }
    
    init(json:JSON) {
        if let _prices = json[kPrices].arrayObject as? [[String:AnyObject]]{
            for _price in _prices{
                let price = Prices(params: _price)
                self.prices.append(price)
            }
        }
        if let _priceFor = json[kPriceFor].string{
            self.priceFor = _priceFor
        }
        
        if let _priceFor = json[kSeasonStartDate].string{
            self.seasonStartDate = _priceFor
        }
        if let _priceFor = json[kSeasonEndDate].string{
            self.seasonEndDate = _priceFor
        }
        
        super.init()
        let sortedPrices = self.prices.sorted { (pr1, pr2) -> Bool in
            return pr1.sizeRange.from < pr2.sizeRange.from
        }
        self.prices = sortedPrices
    }
    
    
}

struct SizeRange {
    var from: Int = 0
    var to : Int = 0
    
    init(rangeStr:String) {
        let rangeArray = rangeStr.components(separatedBy: "-")
        if rangeArray.count == 2{
            if let fromSize = rangeArray.first{
                self.from = Int(fromSize) ?? 0
            }
            if let toSize = rangeArray.last{
                self.to = Int(toSize) ?? 0
            }
        }
    }
}

class Prices: NSObject {
    let kRange = "range"
    let KPrice = "price"
    
    var range = ""
    var price: Double = 0.0
    var sizeRange : SizeRange = SizeRange(rangeStr: "0-0")
    
    override init() {
        super.init()
    }
    
    init(range:String,price:Double) {
        self.range = range
        self.price = price
        self.sizeRange = SizeRange(rangeStr: range)
        super.init()
    }
    init(params:Dictionary<String,AnyObject>) {
        if let _range = params[kRange] as? String {
            self.range = _range
            self.sizeRange = SizeRange(rangeStr: _range)
        }
        if let _price = params[KPrice] as? String{
            self.price = Double(_price) ?? 0.0
        }else if let _price = params[KPrice] as? Double{
            self.price = _price
        }
        super.init()
    }
    
}

class MarinaRangePriceParser: NSObject {
    
    var responseCode = 0
    var responseMessage = ""
    var priceRanges = Array<MarinaRangePrice>()
    var priceRange = MarinaRangePrice()
    override init() {
        super.init()
    }
    
    init(json: JSON) {
        if let _code = json["code"].int as Int?{
            self.responseCode = _code
        }
        
        if let _msg = json["message"].string as String?{
            self.responseMessage = _msg
        }
        if let _priceRange = json["price_range"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.priceRange = MarinaRangePrice(params: _priceRange)
        }
        
        if let _priceRange = json["price_range"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for _priceRangeDict in _priceRange{
                let priceRange = MarinaRangePrice(params: _priceRangeDict)
                self.priceRanges.append(priceRange)
            }
        }
        
        
    }
    
    
}

class MarinaPriceSessionReview: NSObject {
    var code:Int = 0
    var message = ""
    var regularCost = SessionPrices()
    var highSession = SessionPrices()
    var lowSession = SessionPrices()
    var weekEndCost = SessionPrices()
    var specialCosts = Array<PriceModel>()
    
    override init() {
        super.init()
    }
    
    init(json:JSON) {
        if let _rCode = json["code"].int as Int?{
            self.code = _rCode
        }
        if let _rMessage = json["message"].string as String?{
            self.message = _rMessage
        }
        
        
        if let _marinaPriceDict = json["marina_prices"].dictionaryObject as Dictionary<String,AnyObject>?{
            if let _regularPrice = _marinaPriceDict["default_price"] as? Dictionary<String,AnyObject>{
                self.regularCost = SessionPrices(dictionary: _regularPrice)
            }
            
            if let _highSession = _marinaPriceDict["high_season"] as? Dictionary<String,AnyObject>{
                self.highSession = SessionPrices(dictionary: _highSession)
            }
            if let _lowSession = _marinaPriceDict["low_season"] as? Dictionary<String,AnyObject>{
                self.lowSession = SessionPrices(dictionary: _lowSession)
            }
            
            if let _weekEndPrice = _marinaPriceDict["weekend_price"] as? Dictionary<String,AnyObject>{
                self.weekEndCost = SessionPrices(dictionary: _weekEndPrice)
            }
            
            
            
            if let specialdaysDict = _marinaPriceDict["specific_price"] as? [String:AnyObject]{
                self.specialCosts.removeAll()
                for(key,value) in specialdaysDict{
                    var price = 0.0
                    if let _price = value as? String{
                        price = Double(_price) ?? 0.0
                    }else if let _price = value as? Double{
                        price = _price
                    }else if let _price = value as? Int{
                        price = Double(_price)
                    }
                    let aDay = PriceModel(date: key, price: price)
                    self.specialCosts.append(aDay)
                }
            }
            
            
            
        }
        super.init()
    }
}



class SessionPrices: NSObject {
    
    
    let kNightPrice =  "night_price"
    let kWeekPrice = "week_price"
    let kMonthPrice = "month_price"
    let kHighSessionStartDate = "season_start_date"
    let kHighSessionEndDate = "season_end_date"
    let kLowSessionStartDate = "start_date"
    let kLowSessionEndDate = "end_date"
    let kPrice = "price"
    let kWeekendType = "weekend_type"
    
    var nightPrice: Double = 0.0
    var weekPrice: Double = 0.0
    var monthPrice: Double = 0.0
    var sessionStartDate = ""
    var sessionEndDate = ""
    var price: Double = 0.0
    var weekendType = ""
    
    override init() {
        super.init()
    }
    
    init(dictionary:Dictionary<String,AnyObject>) {
        
        
        if let _nightPrice = dictionary[kNightPrice] as? Double{
            self.nightPrice = _nightPrice
        }else if let _nightPrice = dictionary[kNightPrice] as? String{
            self.nightPrice = Double(_nightPrice) ?? 0.0
        }
        if let _weekPrice = dictionary[kWeekPrice] as? Double{
            self.weekPrice = _weekPrice
        }else if let _weekPrice = dictionary[kWeekPrice] as? String{
            self.weekPrice = Double(_weekPrice) ?? 0.0
        }
        
        if let _monthPrice = dictionary[kMonthPrice] as? Double{
            self.monthPrice = _monthPrice
        }else if let _monthPrice = dictionary[kMonthPrice] as? String{
            self.monthPrice = Double(_monthPrice) ?? 0.0
        }
        
        if let _startDate = dictionary[kHighSessionStartDate] as? String{
            self.sessionStartDate = _startDate
        }else if let _startDate = dictionary[kLowSessionStartDate] as? String{
            self.sessionStartDate = _startDate
        }
        //
        if let _endDate = dictionary[kHighSessionEndDate] as? String{
            self.sessionEndDate = _endDate
        }else if let _endDate = dictionary[kLowSessionEndDate] as? String{
            self.sessionEndDate = _endDate
        }
        
        if let _price = dictionary[kPrice] as? Double{
            self.price = _price
        }else if let _price = dictionary[kPrice] as? String{
            self.price = Double(_price) ?? 0.0
        }
        
        if let _weekendType = dictionary[kWeekendType] as? String{
            self.weekendType = _weekendType
        }
        
        super.init()
    }
    
    
    init(json:JSON) {
        
        //        if let _normal = json[kNight].dictionaryObject as Dictionary<String,AnyObject>?{
        //            self.night = PriceModel(dictionary: _normal)
        //        }
        //        if let _week = json[kWeek].dictionaryObject as Dictionary<String,AnyObject>?{
        //            self.week = PriceModel(dictionary: _week)
        //        }
        //        if let _month = json[kMonth].dictionaryObject as Dictionary<String,AnyObject>?{
        //            self.month = PriceModel(dictionary: _month)
        //        }
        super.init()
    }
    
    
}



