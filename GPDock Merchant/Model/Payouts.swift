//
//  Payouts.swift
//  GPDock Merchant
//
//  Created by Parikshit on 22/11/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON
class Payouts: NSObject {
    let kDate = "date"
    let kEvents = "events"
    
    var date:String = ""
      var events = Array<Events>()
    override init() {
        super.init()
    }
    init(dictionary:[String:AnyObject]) {
        if let _fromDate = dictionary[kDate] as? String{
            self.date = _fromDate
        }
        if let _events = dictionary[kEvents] as?  Array<Dictionary<String,AnyObject>> {
            self.events.removeAll()
            for _row in _events{
                let row = Events(dictionary: _row)
                self.events.append(row)
            }
        }
        super.init()
    }
     init(json:JSON){
       if let _fromDate = json[kDate].string as String?{
            self.date = _fromDate
        }
        if let _events = json[kEvents].arrayObject  as? [Dictionary<String,AnyObject>]{
            for event in _events{
                let payout = Events(dictionary: event)
                self.events.append(payout)
            }
        }
      super.init()
    }
    
}

class Events: NSObject {
    let kID = "id"
    let kAmount = "amount"
    let kIsPayout = "is_payout"
    let kPayOutAt = "payout_at"
    let kStripeTransactionID = "stripe_transaction_id"
    let kAccountNumber = "account_number"
    let kDescription = "description"
    let kPayforGrocey = "pay_for_grocery"
    let kPayforPumpout = "pay_for_pumpout"
    let kPayforGasoline = "pay_for_gasoline"
    let kPayforOthers = "pay_for_others"
    let kBookingID = "booking_id"
      let kCustomerName = "user_name"
    let kCustomerNoteType = "note"
//    "description": "paid to marina by admin",
//    "pay_for_grocery": null,
//    "pay_for_pumpout": null,
//    "pay_for_gasoline": null,
//    "pay_for_others": null
    
    var ID = ""
    var amount:Double = 0.0
    var isPayout:Bool = false
    var payoutAt = ""
    var stripeTransactionID = ""
    var accountNumber = ""
    var descripton = ""
    var customerNoteType = ""
    var payforGrocey:Double = 0.0
    var payforPumpout:Double = 0.0
    var payforGasoline:Double = 0.0
    var payforOthers:Double = 0.0
    var bookingID = ""
     var customerName = ""

    
    override init() {
        super.init()
    }
     init(dictionary:[String:AnyObject]) {
        if let _ID = dictionary[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = dictionary[kID] as? String{
            self.ID = _ID
        }
        if let _isPayout = dictionary[kIsPayout] as? Bool{
            self.isPayout = _isPayout
        }
        if let _amount = dictionary[kAmount] as? String {
            self.amount = Double(_amount) ?? 0.0
        }else if let _amount = dictionary[kAmount] as? Double {
            self.amount = _amount
        }
        if let _payoutAt = dictionary[kPayOutAt] as? String {
            self.payoutAt = _payoutAt
        }
        if let _customerNoteType = dictionary[kCustomerNoteType] as? String {
            self.customerNoteType = _customerNoteType
        }
        if let _description = dictionary[kDescription] as? String {
            self.descripton = _description
        }
        if let _payforGrocey = dictionary[kPayforGrocey] as? String {
            self.payforGrocey = Double(_payforGrocey) ?? 0.0
        }else if let _payforGrocey = dictionary[kPayforGrocey] as? Double {
            self.payforGrocey = _payforGrocey
        }
        if let _payforPumpout = dictionary[kPayforPumpout] as? String {
            self.payforPumpout = Double(_payforPumpout) ?? 0.0
        }else if let _payforPumpout = dictionary[kPayforPumpout] as? Double {
            self.payforPumpout = _payforPumpout
        }
        if let _payforGasoline = dictionary[kPayforGasoline] as? String {
            self.payforGasoline = Double(_payforGasoline) ?? 0.0
        }else if let _payforGasoline = dictionary[kPayforGasoline] as? Double {
            self.payforGasoline = _payforGasoline
        }
        if let _payforOthers = dictionary[kPayforOthers] as? String {
            self.payforOthers = Double(_payforOthers) ?? 0.0
        }else if let _payforOthers = dictionary[kPayforOthers] as? Double {
            self.payforOthers = _payforOthers
        }


        if let _stripeTransactionID = dictionary[kStripeTransactionID] as? String {
            self.stripeTransactionID = _stripeTransactionID
        }

        if let _accountNumber = dictionary[kAccountNumber] as? String {
            self.accountNumber = _accountNumber
        }
        if let _customerName = dictionary[kCustomerName] as? String {
            self.customerName = _customerName
        }
        
        if let _bookingID = dictionary[kBookingID] as? Int{
            self.bookingID = "\(_bookingID)"
        }else if let _bookingID = dictionary[kBookingID] as? String{
            self.bookingID = _bookingID
        }
        super.init()
    }
    
    init(json:JSON){
        if let _ID = json[kID].int
        {
            self.ID = "\(_ID)"
        }else if let _ID = json[kID].string{
            self.ID = _ID
        }
        if let _amount = json[kAmount].string{
            self.amount = Double(_amount) ?? 0.0
        }else if let _amount = json[kAmount].double{
            self.amount = _amount
        }
        
        if let _isPayout = json[kIsPayout].bool{
            self.isPayout = _isPayout
        }
        if let _payoutAt = json[kPayOutAt].string{
            self.payoutAt = _payoutAt
        }
        if let _stripeTransactionID = json[kStripeTransactionID].string{
            self.stripeTransactionID = _stripeTransactionID
        }
        if let _accountNumber = json[kAccountNumber].string{
            self.accountNumber = _accountNumber
        }
        if let _descripton = json[kDescription].string{
            self.descripton = _descripton
        }
        if let _customerNoteType = json[kCustomerNoteType].string{
            self.customerNoteType = _customerNoteType
        }
        if let _customerName = json[kCustomerName].string{
            self.customerName = _customerName
        }
        if let _payforGrocey = json[kPayforGrocey].string{
            self.payforGrocey = Double(_payforGrocey) ?? 0.0
        }else if let _payforGrocey = json[kPayforGrocey].double{
            self.payforGrocey = _payforGrocey
        }
        if let _payforPumpout = json[kPayforPumpout].string{
            self.payforPumpout = Double(_payforPumpout) ?? 0.0
        }else if let _payforPumpout = json[kPayforPumpout].double{
            self.payforPumpout = _payforPumpout
        }
        if let _payforGasoline = json[kPayforGasoline].string{
            self.payforGasoline = Double(_payforGasoline) ?? 0.0
        }else if let _payforGasoline = json[kPayforGasoline].double{
            self.payforGasoline = _payforGasoline
        }
        if let _payforOthers = json[kPayforOthers].string{
            self.payforOthers = Double(_payforOthers) ?? 0.0
        }else if let _payforOthers = json[kPayforOthers].double{
            self.payforOthers = _payforOthers
        }
        if let _bookingID = json[kBookingID].int
        {
            self.bookingID = "\(_bookingID)"
        }else if let _bookingID = json[kBookingID].string{
            self.bookingID = _bookingID
        }

        super.init()
    }
    
}



class PayoutsParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kPayouts = "payouts"
    let kPayout = "payout"
    
    var responseCode:Int = 0
    var responseMessage = ""
    var payouts = [Payouts]()
    
    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.responseCode = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.responseMessage = _rMessage
        }
        if let bookingsArray = json[kPayouts].arrayObject as? Array<Dictionary<String,AnyObject>>{
            self.payouts.removeAll()
            for bookingDict in bookingsArray{
                let payout = Payouts(dictionary: bookingDict)
                self.payouts.append(payout)
            }
        }
        
//        if let bookingDict = json[kPayout].dictionaryObject as [String:AnyObject]?{
//            self.payout = Payouts(dictionary: bookingDict)
//        }
        
        super.init()
    }
}
