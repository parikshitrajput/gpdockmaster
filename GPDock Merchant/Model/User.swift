//
//  User.swift
//  MyLaundryApp
//
//  Created by TecOrb on 15/12/16.
//  Copyright © 2016 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON


class User: NSObject {
    //key
    let kID = "id"
    let kFirstName = "fname"
    let kLastName = "lname"
    let kEmail = "email"
    let kContact = "contact"
    let kProfileImage = "image"
    let kUserRole = "role"
    let kUserName = "user_name"
    let kDescription = "description"

    let kAddress = "address"
    let kCity = "city"
    let kState = "state"
    let kCountry = "country"
    let kPinCode = "pincode"
    let kMarinas = "marinas"
    let kZipCode = "zipcode"
    let kStripeUID = "stripe_uid"
    let kCancelReason = "cancel_reason"
    let kStripeAccountID = "stripe_account_id"
    let kBoat = "boats"
    let kAPIKey = "api_key"

    

    //properties
    /*
     "zipcode" : "201301",
     "role" : "business",
     "stripe_uid" : "cus_BpIpxhH0F5D5on",
     "cancel_reason" : "testing",
     "stripe_account_id" : "acct_1BQYVIGnjuiKNvVL",

 */
    
    
    
    var ID : String = ""
    var userName: String = ""
    var firstName : String = ""
    var lastName : String = ""
    var email : String = ""
    var contact : String = ""
    var role : String = ""
    var profileImage : String = ""
    var address = ""
    var city = ""
    var state = ""
    var country = ""
    var pinCode = ""
    var zipCode = ""
    var stripeUID : String = ""
    var cancelReason : String = ""
    var stripeAccountID : String = ""
    var boatArr = Array<Boat>()

    var marinas = Array<Marina>()
    var userToken = ""

    override init() {
        super.init()
    }

    init(json : JSON){
        if let userId = json[kID].int as Int?{
            self.ID = "\(userId)"
        }else if let userId = json[kID].string as String?{
            self.ID = userId
        }

        if let fName = json[kFirstName].string as String?{
            self.firstName = fName
        }

        if let _userName = json[kUserName].string as String?{
            self.userName = _userName
        }
        if let lName = json[kLastName].string as String?{
            self.lastName = lName
        }
        
        if let userEmail = json[kEmail].string as String?{
            self.email = userEmail
        }

        if let userContact = json[kContact].string as String?{
            self.contact = userContact
        }

        if let _userRole = json[kUserRole].string as String?{
            self.role = _userRole
        }

        if let userProfileImage = json[kProfileImage].string as String?{
            self.profileImage = userProfileImage
        }

        if let _address = json[kAddress].string as String?{
            self.address = _address
        }
        if let _city = json[kCity].string as String?{
            self.city = _city
        }

        if let _state = json[kState].string as String?{
            self.state = _state
        }
        if let _country = json[kCountry].string as String?{
            self.country = _country
        }
        if let _cancelReason = json[kCancelReason].string as String?{
            self.cancelReason = _cancelReason
        }

        if let _pinCode = json[kPinCode].int as Int?{
            self.pinCode = "\(_pinCode)"
        }else if let _pinCode = json[kPinCode].string as String?{
            self.pinCode = _pinCode
        }
        if let _zipCode = json[kZipCode].int as Int?{
            self.zipCode = "\(_zipCode)"
        }else if let _zipCode = json[kZipCode].string as String?{
            self.zipCode = _zipCode
        }
        if let _stripeUID = json[kStripeUID].int as Int?{
            self.stripeUID = "\(_stripeUID)"
        }else if let _stripeUID = json[kStripeUID].string as String?{
            self.stripeUID = _stripeUID
        }
        if let _stripeAccountID = json[kStripeAccountID].int as Int?{
            self.stripeAccountID = "\(_stripeAccountID)"
        }else if let _stripeAccountID = json[kStripeAccountID].string as String?{
            self.stripeAccountID = _stripeAccountID
        }
        if let _marinas = json[kMarinas].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for _marina in _marinas{
                let marina = Marina(dictionary: _marina)
                self.marinas.append(marina)
            }
        }
        if let _events = json[kBoat].arrayObject  as? [Dictionary<String,AnyObject>]{
            for event in _events{
                let payout = Boat(dictionary: event)
                self.boatArr.append(payout)
            }
        }
        if let _apiKey = json[kAPIKey].string as String?{
            self.userToken = _apiKey
        }

        super.init()
    }

    init(dictionary : [String:AnyObject]){

        if let userId = dictionary[kID] as? NSInteger{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }

        if let fName = dictionary[kFirstName] as? String{
            self.firstName = fName
        }
        if let _userName = dictionary[kUserName] as? String{
            self.userName = _userName
        }
        if let lName = dictionary[kLastName] as? String{
            self.lastName = lName
        }
        if let userEmail = dictionary[kEmail] as? String{
            self.email = userEmail
        }

        if let userContact = dictionary[kContact] as? String{
            self.contact = userContact
        }

        if let _userRole = dictionary[kUserRole] as? String{
            self.role = _userRole
        }

        if let userProfileImage = dictionary[kProfileImage] as? String{
            self.profileImage = userProfileImage
        }

        if let _address = dictionary[kAddress] as? String{
            self.address = _address
        }
        if let _city = dictionary[kCity] as? String{
            self.city = _city
        }

        if let _state = dictionary[kState] as? String{
            self.state = _state
        }
        if let _country = dictionary[kCountry] as? String{
            self.country = _country
        }

        if let _pinCode = dictionary[kPinCode] as? Int{
            self.pinCode = "\(_pinCode)"
        }else if let _pinCode = dictionary[kPinCode] as? String{
            self.pinCode = _pinCode
        }
        if let _zipCode = dictionary[kZipCode] as? Int{
            self.zipCode = "\(_zipCode)"
        }else if let _zipCode = dictionary[kZipCode] as? String{
            self.zipCode = _zipCode
        }
        if let _stripeUID = dictionary[kStripeUID] as? Int{
            self.stripeUID = "\(_stripeUID)"
        }else if let _stripeUID = dictionary[kStripeUID] as? String{
            self.stripeUID = _stripeUID
        }
        if let _stripeAccountID = dictionary[kStripeAccountID] as? Int{
            self.stripeAccountID = "\(_stripeAccountID)"
        }else if let _stripeAccountID = dictionary[kStripeAccountID] as? String{
            self.stripeAccountID = _stripeAccountID
        }

        if let _marinas = dictionary[kMarinas] as? Array<Dictionary<String,AnyObject>>{
            for _marina in _marinas{
                let marina = Marina(dictionary: _marina)
                self.marinas.append(marina)
            }
        }
        if let _events = dictionary[kBoat] as?  Array<Dictionary<String,AnyObject>> {
            self.boatArr.removeAll()
            for _row in _events{
                let row = Boat(dictionary: _row)
                self.boatArr.append(row)
            }
        }
        if let _apiKey = dictionary[kAPIKey] as? String{
            self.userToken = _apiKey
        }
        super.init()
    }


    class func saveUserJSON(_ userInfo:Dictionary<String,AnyObject>) {
        let documentPath = NSHomeDirectory() + "/Documents/"
        do {
            let data = try JSON(userInfo).rawData(options: [.prettyPrinted])
            let path = documentPath + "user"
            try data.write(to: URL(fileURLWithPath: path), options: .atomic)
            print_debug(path)
        }catch{
            print_debug("error in saving userinfo")
        }
        UserDefaults.standard.synchronize()
    }
    
    
    func saveUserInfo(_ user:User) {
        let documentPath = NSHomeDirectory() + "/Documents/"
        var userInfo = [String:AnyObject]()
        userInfo.updateValue(user.ID as AnyObject, forKey:kID)
        userInfo.updateValue(user.firstName as AnyObject, forKey:kFirstName)
        userInfo.updateValue(user.lastName as AnyObject, forKey:kLastName)
        userInfo.updateValue(user.email as AnyObject, forKey:kEmail)
        userInfo.updateValue(user.contact as AnyObject, forKey:kContact)
        userInfo.updateValue(user.profileImage as AnyObject, forKey:kProfileImage)
        userInfo.updateValue(user.userName as AnyObject, forKey: kUserName)
        userInfo.updateValue(user.role as AnyObject, forKey: kUserRole)

        userInfo.updateValue(user.address as AnyObject, forKey: kAddress)
        userInfo.updateValue(user.city as AnyObject, forKey: kCity)
        userInfo.updateValue(user.country as AnyObject, forKey: kCountry)
        userInfo.updateValue(user.pinCode as AnyObject, forKey: kPinCode)
        userInfo.updateValue(user.state as AnyObject, forKey: kState)
        userInfo.updateValue(user.zipCode as AnyObject, forKey: kZipCode)
        userInfo.updateValue(user.userToken as AnyObject, forKey: kAPIKey)

        var marinas = [Dictionary<String,String>]()
        for marina in user.marinas{
            var marinaDict = Dictionary<String,String>()
            marinaDict.updateValue(marina.ID, forKey: kID)
            marinaDict.updateValue(marina.title, forKey: "title")
            
            marinaDict.updateValue("\(marina.pricePerFeet)", forKey: "price_per_foot")
            marinas.append(marinaDict)
        }
        userInfo.updateValue(marinas as AnyObject, forKey: kMarinas)

        //let map = ["employee":userInfo]

        do {
            let data = try JSON(userInfo).rawData(options: [.prettyPrinted])
            let path = documentPath + "user"
            try data.write(to: URL(fileURLWithPath: path), options: .atomic)
            print_debug(path)
        }catch{
            print_debug("error in saving userinfo")
        }
        UserDefaults.standard.synchronize()
    }



    class func loadUserInfo()->JSON {
        let documentPath = NSHomeDirectory() + "/Documents/"
        let path = documentPath + "user"
        var data = Data()
        var json : JSON
        do{
            data = try Data(contentsOf: URL(fileURLWithPath: path))
            json = JSON(data: data)
        }catch{
            json = JSON.init(data)
            print_debug("error in getting userinfo")
        }
        return json
    }


    class func logOut(_ completionBlock:@escaping (_ success:Bool,_ user:User?,_ message:String) -> Void){
        let user = User(json:User.loadUserInfo())
        LoginService.sharedInstance.logOut(user.ID) { (success, user, message) in
            completionBlock(success, user, message)
        }
    }
}

class UserParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"
    let kUsers = "users"
    let kUser = "user"

    var code = 0
    var message = ""
    var users = [User]()
    var user = User()

    override init() {
        super.init()
    }

    
    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.code = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let userDict = json[kUser].dictionaryObject as [String:AnyObject]?{
            self.user = User(dictionary: userDict)
        }

        if let usrs  = json[kUsers].arrayObject as? [[String: AnyObject]]
        {
            users.removeAll()
            for u in usrs{
                let usr = User(dictionary: u)
                users.append(usr)
            }
        }
    }

}

class OTPValidationParser: NSObject {
    let kResponseCode = "code"
    let kResponseMessage = "message"

    var code = 0
    var message = ""
    var isVarified = false
    var token = ""
    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _rCode = json[kResponseCode].int as Int?{
            self.code = _rCode
        }
        if let _rMessage = json[kResponseMessage].string as String?{
            self.message = _rMessage
        }

        if let resultDic = json["result"].dictionaryObject as [String:AnyObject]?{
            if let _varified = resultDic["is_verified"] as? Bool{
                self.isVarified = _varified
            }
            if let _token = resultDic["reset_password_token"] as? String{
                self.token = _token
            }

        }


    }

}







