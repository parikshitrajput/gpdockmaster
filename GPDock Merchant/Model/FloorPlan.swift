//
//  FloorPlan.swift
//  GPDock
//
//  Created by TecOrb on 15/07/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import SwiftyJSON

class FloorPlan: NSObject {
    let kCode = "code"
    let kMessage = "message"
    let kRows = "result"
    let kCounts = "counts"

    var responseCode = 0
    var responseMessage = ""
    var rows = Array<ParkingRow>()
    var availablity = Availablity()

    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _code = json[kCode].int as Int?{
            self.responseCode = _code
        }
        if let _message = json[kMessage].string as String?{
            self.responseMessage = _message
        }
        if let _rowArray = json[kRows].arrayObject as? [Dictionary<String,AnyObject>]{
            self.rows.removeAll()
            for _rowDict in _rowArray{
                let row = ParkingRow(dictionary: _rowDict)
                self.rows.append(row)
            }
        }

        if let _availablity = json[kCounts].dictionaryObject as Dictionary<String,AnyObject>?{
            self.availablity = Availablity(dictionary: _availablity)
        }

        super.init()
    }

}

class Availablity: NSObject {
    let kTotal = "total"
    let kAvailable = "available"
    let kBooked = "booked"
    var total: Int = 0
    var available : Int = 0
    var booked : Int = 0

    override init() {
        super.init()
    }
    init(dictionary: Dictionary<String,AnyObject>) {
        if let _total = dictionary[kTotal] as? Int{
            self.total = _total
        }else if let _total = dictionary[kTotal] as? String{
            self.total = Int(_total) ?? 0
        }
        if let _available = dictionary[kAvailable] as? Int{
            self.available = _available
        }else if let _available = dictionary[kAvailable] as? String{
            self.available = Int(_available) ?? 0
        }
        if let _booked = dictionary[kBooked] as? Int{
            self.booked = _booked
        }else if let _booked = dictionary[kBooked] as? String{
            self.booked = Int(_booked) ?? 0
        }
        super.init()
    }
}

class ParkingRow: NSObject {
    let kLeftFacing = "left"
    let kRightFacing = "right"
    let kParkingSpaces = "parking_spaces"
    let kTitle = "title"

    var leftFacingBoats = Array<ParkingSpace>()
    var rightFacingBoats = Array<ParkingSpace>()
    var title = ""
    override init() {
        super.init()
    }
    init(dictionary: Dictionary<String,AnyObject>) {
        var rowTitle = ""
        if let _title = dictionary[kTitle] as? String{
            self.title = _title
            rowTitle = _title
        }
        if let _parkingSpaces = dictionary[kParkingSpaces] as? Dictionary<String,AnyObject>{
            if let _leftBoats = _parkingSpaces[kLeftFacing] as? Array<Dictionary<String,AnyObject>>{
                for _leftBoat in _leftBoats{
                    let boat = ParkingSpace(dictionary: _leftBoat)
                    boat.parkingRowTitle = rowTitle
                    self.leftFacingBoats.append(boat)
                }
            }

            if let _rightBoats = _parkingSpaces[kRightFacing] as? Array<Dictionary<String,AnyObject>>{
                for _rightBoat in _rightBoats{
                    let boat = ParkingSpace(dictionary: _rightBoat)
                    boat.parkingRowTitle = rowTitle
                    self.rightFacingBoats.append(boat)
                }
            }
        }
        super.init()
    }

}

enum Facing: String {
    case left = "L"
    case right = "R"
}

class ParkingSpace: NSObject {
   /* "id": 21096,
    "title": "FR-6",
    "phase": "R",
    "length": 35,
    "width": 10,
    "depth": 6,
    "position": "6",
    "is_available": true,
    "is_booked": false*/

    let kID = "id"
    let kTitle = "title"
    let kPhase = "phase"
    let kLength = "length"
    let kWidth = "width"
    let kDepth = "depth"
    let kPosition = "position"
    let kIsAvailable = "is_available"
    let kIsBooked = "is_booked"
    let kIsArriving = "arrival"
    let kIsDeparting = "departure"
    let kIsPartiallyBooked = "is_partially_booked"
    //properties

    var ID = ""
    var title = ""
    var length = ""
    var width = ""
    var depth = ""
    var facing:Facing = Facing.left
    var boatSize: BoatSize = BoatSize()
    var position: Int = 0

    var isAvailable:Bool = true
    var isBooked:Bool = false
    var isArriving: Bool = false
    var isDeparting : Bool = false
    var isPartiallyBooked : Bool = false
    var parkingRowTitle = ""
    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String, AnyObject>) {

        if let userId = dictionary[kID] as? Int{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }
        if let _title = dictionary[kTitle] as? Int{
            self.title = "\(_title)"
        }else if let _title = dictionary[kTitle] as? String{
            self.title = _title
        }
        if let _length = dictionary[kLength] as? String{
            self.length = _length
        }
        if let _width = dictionary[kWidth] as? String{
            self.width = _width
        }
        if let _depth = dictionary[kDepth] as? String{
            self.depth = _depth
        }

        if let _facing = dictionary[kPhase] as? String{
            if _facing == "L"{self.facing = .left}else{self.facing = .right}
        }


        //size of boat
        if let _length = dictionary[kLength] as? Int{
            self.boatSize.length = _length
        }else if let _length = dictionary[kLength] as? String{
            self.boatSize.length = Int(_length) ?? 0
        }
        if let _width = dictionary[kWidth] as? Int{
            self.boatSize.width = _width
        }else if let _width = dictionary[kWidth] as? String{
            self.boatSize.width = Int(_width) ?? 0
        }
        if let _depth = dictionary[kDepth] as? Int{
            self.boatSize.depth = _depth
        }else if let _depth = dictionary[kDepth] as? String{
            self.boatSize.depth = Int(_depth) ?? 0
        }
        //end of size of boat


        if let _position = dictionary[kPosition] as? Int{
            self.position = _position
        }else if let _position = dictionary[kPosition] as? String{
            self.position = Int(_position) ?? -1
        }


        if let _isAvailable = dictionary[kIsAvailable] as? Bool{
            self.isAvailable = _isAvailable
        }
        if let _isBooked = dictionary[kIsBooked] as? Bool{
            self.isBooked = _isBooked
        }
        if let _isArriving = dictionary[kIsArriving] as? Bool{
            self.isArriving = _isArriving
        }
        if let _isDeparting = dictionary[kIsDeparting] as? Bool{
            self.isDeparting = _isDeparting
        }
        if let _isPartiallyBooked = dictionary[kIsPartiallyBooked] as? Bool{
            self.isPartiallyBooked = _isPartiallyBooked
        }

        super.init()
    }

    init(json:JSON) {

        if let userId = json[kID].int as Int?{
            self.ID = "\(userId)"
        }else if let userId = json[kID].string as String?{
            self.ID = userId
        }

        if let _title = json[kTitle].string as String?{
            self.title = _title
        }
        if let _length = json[kLength].int as Int?{
            self.length = "\(_length)"
        }else if let _length = json[kLength].string as String?{
            self.length = _length
        }
        if let _width = json[kWidth].string as String?{
            self.width = _width
        }
        if let _depth = json[kDepth].string as String?{
            self.depth = _depth
        }

        if let _facing = json[kPhase].string as String?{
            if _facing == "L"{self.facing = .left}else{self.facing = .right}
        }


        //size of boat
        if let _length = json[kLength].int as Int?{
            self.boatSize.length = _length
        }else if let _length = json[kLength].string as String?{
            self.boatSize.length = Int(_length) ?? 0
        }
        if let _width = json[kWidth].int as Int?{
            self.boatSize.width = _width
        }else if let _width = json[kWidth].string as String?{
            self.boatSize.width = Int(_width) ?? 0
        }
        if let _depth = json[kDepth].int as Int?{
            self.boatSize.depth = _depth
        }else if let _depth = json[kDepth].string as String?{
            self.boatSize.depth = Int(_depth) ?? 0
        }
        //end of size of boat


        if let _position = json[kPosition].int as Int?{
            self.position = _position
        }else if let _position = json[kPosition].string as String?{
            self.position = Int(_position) ?? -1
        }

        if let _isAvailable = json[kIsAvailable].bool as Bool?{
            self.isAvailable = _isAvailable
        }

        if let _isBooked = json[kIsBooked].bool as Bool?{
            self.isBooked = _isBooked
        }

        if let _isArriving = json[kIsArriving].bool as Bool?{
            self.isArriving = _isArriving
        }

        if let _isDeparting = json[kIsDeparting].bool as Bool?{
            self.isDeparting = _isDeparting
        }
        if let _isPartiallyBooked = json[kIsPartiallyBooked].bool as Bool?{
            self.isPartiallyBooked = _isPartiallyBooked
        }
        
        super.init()
    }



}








/*
 Here goes boat model
 and boat parser
 */

class Boat: NSObject {
    /*  "boat": {
     "id": 49,
     "name": "sultan",
     "image": null,
     "long": "22",
     "width": "21",
     "depth": "12"
     }*/

    let kID = "id"
    let kName = "name"
    let kImage = "image"
    let kLength = "long"
    let kWidth = "width"
    let kDepth = "depth"
    let kAvailablity = "availablity"

    //properties

    var ID = ""
    var name = ""
    var boatSize: BoatSize = BoatSize()
    var image = ""
    var availablity : Int = 0
    override init() {
        super.init()
    }

    init(dictionary:Dictionary<String, AnyObject>) {

        if let userId = dictionary[kID] as? Int{
            self.ID = "\(userId)"
        }else if let userId = dictionary[kID] as? String{
            self.ID = userId
        }

        if let _title = dictionary[kName] as? String{
            self.name = _title
        }
        if let _image = dictionary[kImage] as? String{
            self.image = _image
        }

        if let _avial = dictionary[kAvailablity] as? Int{
            self.availablity = _avial
        }else if let _avial = dictionary[kAvailablity] as? String{
            self.availablity = Int(_avial) ?? 0
        }

        //size of boat
        if let _length = dictionary[kLength] as? Int{
            self.boatSize.length = _length
        }else if let _length = dictionary[kLength] as? String{
            self.boatSize.length = Int(_length) ?? 0
        }
        if let _width = dictionary[kWidth] as? Int{
            self.boatSize.width = _width
        }else if let _width = dictionary[kWidth] as? String{
            self.boatSize.width = Int(_width) ?? 0
        }
        if let _depth = dictionary[kDepth] as? Int{
            self.boatSize.depth = _depth
        }else if let _depth = dictionary[kDepth] as? String{
            self.boatSize.depth = Int(_depth) ?? 0
        }

        super.init()
    }

    init(json:JSON) {

        if let userId = json[kID].int as Int?{
            self.ID = "\(userId)"
        }else if let userId = json[kID].string as String?{
            self.ID = userId
        }

        if let _title = json[kName].string as String?{
            self.name = _title
        }
        if let _image = json[kImage].string as String?{
            self.image = _image
        }

        if let _avial = json[kAvailablity].int as Int?{
            self.availablity = _avial
        }else if let _avial = json[kAvailablity].string as String?{
            self.availablity = Int(_avial) ?? 0
        }


        //size of boat
        if let _length = json[kLength].int as Int?{
            self.boatSize.length = _length
        }else if let _length = json[kLength].string as String?{
            self.boatSize.length = Int(_length) ?? 0
        }
        if let _width = json[kWidth].int as Int?{
            self.boatSize.width = _width
        }else if let _width = json[kWidth].string as String?{
            self.boatSize.width = Int(_width) ?? 0
        }
        if let _depth = json[kDepth].int as Int?{
            self.boatSize.depth = _depth
        }else if let _depth = json[kDepth].string as String?{
            self.boatSize.depth = Int(_depth) ?? 0
        }
        //end of size of boat
        
        super.init()
    }
    
    
    
}


class BoatParser: NSObject {
    let kCode = "code"
    let kMessage = "message"
    let kBoats = "boats"
    let kBoat = "boat"

    var responseCode:Int = 0
    var responseMessage = ""
    var boats = Array<Boat>()
    //in case of singal boat
    var boat = Boat()

    override init() {
        super.init()
    }
    init(json:JSON) {
        if let _code = json[kCode].int as Int?{
            self.responseCode = _code
        }

        if let _responseMessage = json[kMessage].string as String?{
            self.responseMessage = _responseMessage
        }
        if let _boatDict = json[kBoat].dictionaryObject as Dictionary<String,AnyObject>?{
            self.boat = Boat(dictionary: _boatDict)
        }

        if let _boats = json[kBoats].arrayObject as? Array<Dictionary<String,AnyObject>>{
            self.boats.removeAll()
            for _boatDict in _boats{
                let boat = Boat(dictionary: _boatDict)
                self.boats.append(boat)
            }
        }
        super.init()
    }
}





