//
//  NotificationService.swift
//  GPDock Merchant
//
//  Created by Parikshit on 08/12/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class NotificationService {
    static let sharedInstance = NotificationService()
    fileprivate init() {}
 
    func getNotificaation(_ userID:String,role:String,page:Int,perPage: Int, completionBlock:@escaping (Array<NotificationModel>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(role, forKey: "role")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("\(page)", forKey: "page")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \( NOTIFICATIONS_URL) with param \(params) and headers :\(head)")
        Alamofire.request( NOTIFICATIONS_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let notificationParser = NotificationParser(json: json)
                    if notificationParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if notificationParser.responseCode == 200{
                        completionBlock(notificationParser.notifications)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    
    
}
