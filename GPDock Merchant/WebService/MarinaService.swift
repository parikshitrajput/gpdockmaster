//
//  MarinaService.swift
//  GPDock
//
//  Created by TecOrb on 21/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import CoreLocation

class MarinaService {
    static let sharedInstance = MarinaService()

    func getBookingDetailsForSlip(_ slipID:String,dateFrom:String,toDate:String,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
//        let dFrom = CommonClass.formattedDateWith(dateFrom, format: "YYYY-MM-dd")
//        let dTo = CommonClass.formattedDateWith(toDate, format: "YYYY-MM-dd")
        params.updateValue(slipID, forKey: "parking_space_id")
        params.updateValue(dateFrom, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SLIP_BOOKING_DETAILS_URL) and headers :\(head)")
        Alamofire.request(SLIP_BOOKING_DETAILS_URL,method: .post,parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("slip booking json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }




    func getCitiesFromServer(_ keyword:String?,pageNumber: Int,perPage: Int,completionBlock:@escaping (AnyObject?) -> Void){
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        var params = ["page":"\(pageNumber)","per_page":"\(perPage)"]
        if let key = keyword{
            params.updateValue(key, forKey: "keyword")
        }
        print_debug("hitting \(CITIES_URL) headers :\(head)")
        Alamofire.request(CITIES_URL,method: .get, parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cities json is:\n\(json)")
                    let cityParser = CityParser(json: json)
                    if cityParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if cityParser.responseCode == 200{
                        completionBlock(cityParser.cities as AnyObject)
                    }else{
                        completionBlock(json["response_message"].string as AnyObject?)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }

    func getParkingAvailabilityCountForUser(_ userID:String, marinaID:String,bookingDate:BookingDates,completionBlock:@escaping (Array<Boat>?) -> Void){

        let fromDate = CommonClass.formattedDateWith(bookingDate.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.formattedDateWith(bookingDate.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")
        params.updateValue("1000", forKey: "per_page")
        params.updateValue("1", forKey: "page")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SEARCH_MARINA_AVAILABILITY_BY_USER_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SEARCH_MARINA_AVAILABILITY_BY_USER_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("user boats json is:\n\(json)")

                    let boatParser = BoatParser(json: json)
                    if boatParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    if boatParser.responseCode == 200{
                        completionBlock(boatParser.boats)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }

    func getParkingAvailabilityCountForMarina(_ marinaID:String,boatSize:BoatSize,bookingDate:BookingDates,completionBlock:@escaping (Int?) -> Void){
       /* "marina_id": "201",
        "length": 25,
        "width": 7,
        "depth": 5,
        "date_from":"2017-07-25",
        "date_to":"2017-07-27",
        "search_for": "count"*/
        let fromDate = CommonClass.formattedDateWith(bookingDate.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.formattedDateWith(bookingDate.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue("count", forKey: "search_for")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue("\(boatSize.length)", forKey: "length")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.depth)", forKey: "depth")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Availability json is:\n\(json)")
                    if let code = json["code"].int, code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    if let count = json["result"].int as Int?{
                        completionBlock(count)
                    }else if let count = json["result"].string as String?{
                        completionBlock(Int(count) ?? 0)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }

    func getFloorPlanForMarina(_ marinaID:String,boatSize:BoatSize,bookingDate:BookingDates,completionBlock:@escaping (FloorPlan?) -> Void){
        let fromDate = CommonClass.formattedDateWith(bookingDate.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.formattedDateWith(bookingDate.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue("\(boatSize.length)", forKey: "length")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.depth)", forKey: "depth")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        print_debug("hitting \(SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("floor plan json is:\n\(json)")
                    let floorPlan = FloorPlan(json: json)
                    if floorPlan.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if floorPlan.responseCode == 200{
                        completionBlock(floorPlan)
                    }else{
                        //showAlertWith(((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: floorPlan.responseMessage, title: "Important Message")
                        showAlertWith(((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: floorPlan.responseMessage, title: warningMessage.alertTitle.rawValue)

                        completionBlock(nil)
                    }
                }else{
                    showAlertWith(((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Oops! Something went wrong", title: warningMessage.alertTitle.rawValue)
                    completionBlock(nil)
                }
            case .failure(let error):
                showAlertWith(((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: error.localizedDescription, title: warningMessage.alertTitle.rawValue)
                completionBlock(nil)
            }
        }
    }



    func getFloorPlanForSingleDate(_ userID:String,marinaID:String,bookingDate:BookingDates,boatSize:BoatSize,completionBlock:@escaping (FloorPlan?) -> Void){
        let fromDate = CommonClass.formattedDateWith(bookingDate.fromDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue("\(boatSize.length)", forKey: "length")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.depth)", forKey: "depth")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        print_debug("hitting \(MARINA_FLOOR_PLAN_FOR_SINGLE_DATE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_FLOOR_PLAN_FOR_SINGLE_DATE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("floor plan json is:\n\(json)")
                    let floorPlan = FloorPlan(json: json)
                    if floorPlan.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if floorPlan.responseCode == 200{
                        completionBlock(floorPlan)
                    }else{
                       // showAlertWith(((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: floorPlan.responseMessage, title: "Important Message")
                        showAlertWith(((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: floorPlan.responseMessage, title: warningMessage.alertTitle.rawValue)

                        completionBlock(nil)
                    }
                }else{
                    showAlertWith(((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: "Oops! Something went wrong", title: warningMessage.alertTitle.rawValue)
                    completionBlock(nil)
                }
            case .failure(let error):
                showAlertWith(((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!, message: error.localizedDescription, title: warningMessage.alertTitle.rawValue)
                completionBlock(nil)
            }
        }
    }

    func getMarinaList(_ cityID:String?, latitude:Double?,longitude:Double?,page:Int,perPage:Int, completionBlock:@escaping (AnyObject?) -> Void){
        var params = ["page":"\(page)","per_page":"\(perPage)"]
        if let city = cityID{params.updateValue(city, forKey: "city_id")}
        if let lat = latitude{params.updateValue("\(lat)", forKey: "latitude")}
        if let longi = longitude{params.updateValue("\(longi)", forKey: "longitude")}

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MARINA_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_LIST_URL,method: .get ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("marinas json is:\n\(json)")
                    let marinaParser = MarinaParser(json: json)
                    if marinaParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if marinaParser.responseCode == 200{
                        completionBlock(marinaParser.marinas as AnyObject)
                    }else{
                        completionBlock(json["response_message"].string as AnyObject?)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }

    func getFAQsFromServer(completionBlock:@escaping (_ success:Bool, _ faqModels:Array<FAQModel>?, _ message:String) -> Void) {
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(FAQ_URL) and headers :\(head)")
        Alamofire.request(FAQ_URL,method: .get , headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("faqs json: \(json)")
                    let faqParser = FAQParser(json: json)
                    if faqParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((faqParser.code == 200),faqParser.faqs,faqParser.message)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }

    }


    func getRideHistoryFromServer(_ userID:String,pageNumber:Int,perPage: Int,completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["user_id":userID,"page":"\(pageNumber)","per_page":"\(perPage)"]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(USER_RIDE_HISTORY_URL) with param \(params) and headers :\(head)")
        Alamofire.request(USER_RIDE_HISTORY_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride history json is:\n\(json)")
                    if let code = json["code"].int, code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    // let rideParser = RideParser(json: json)
//                    if rideParser.responseCode == 200{
//                        completionBlock(rideParser.rides as AnyObject)
//                    }else{
//                        completionBlock(json["response_message"].string as AnyObject?)
//                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }

    func reviewBookingPrice(_ marinaID:String,boatLength:String,fromDate:String,toDate:String,completionBlock:@escaping (_ success:Bool,_ marinaPriceReview: MarinaPriceReview?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(fromDate, forKey: "from_date")
        params.updateValue(toDate, forKey: "to_date")
        params.updateValue(boatLength, forKey: "boat_length")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REVIEW_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(REVIEW_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("review price json is:\n\(json)")
                    
                    let marinaPriceReview = MarinaPriceReview(json: json)
                    if marinaPriceReview.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    
                    completionBlock(marinaPriceReview.code == 200,marinaPriceReview)
                }else{
                    completionBlock(false,nil)
                }
            case .failure:
                completionBlock(false,nil)
            }
        }
    }
    func getMarinaDetails(_ marinaID:String,completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["id":marinaID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MARINA_DETAILS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_DETAILS_URL,method: .get ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride history json is:\n\(json)")
                    if let _resCode = json["code"].int as Int?{
                        if _resCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }
                        if _resCode == 200{
                            if let dict = json["marina"].dictionaryObject as [String:AnyObject]?{
                                let marina = Marina(dictionary: dict)
                                completionBlock(marina as AnyObject)
                            }else{
                                showErrorWithMessage(json["message"].stringValue)
                                completionBlock(nil)
                            }
                        }else{
                            showErrorWithMessage(json["message"].stringValue)
                            completionBlock(nil)
                        }
                    }else{
                        completionBlock(json["message"].string as AnyObject?)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }


    func addBoatWith(_ userID: String,name:String,boatSize: BoatSize,boatImage:UIImage?,completionBlock:@escaping (Boat?) -> Void) {
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue("\(boatSize.length)", forKey: "long")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.width)", forKey: "depth")
        params.updateValue(name, forKey: "name")

        if let image = boatImage{
            if let base64String = UIImageJPEGRepresentation(image, 0.8)?.base64EncodedString() {
                params.updateValue(base64String, forKey: "image")
            }
        }


        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ADD_BOAT_URL) with param \(params) and headers :\(head)")
        Alamofire.request(ADD_BOAT_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("add boat json: \(json)")
                    let boatParser = BoatParser(json: json)
                    if boatParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if boatParser.responseCode == 200{
                        completionBlock(boatParser.boat)
                    }else{
                        showErrorWithMessage(boatParser.responseMessage)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops! \(error.localizedDescription)")
                completionBlock(nil)
            }
        }

    }

    func lockParkingSpaceFor(_ userID: String, parkingID:String,completionBlock:@escaping (Bool) -> Void){
        var params = ["parking_space_id":parkingID]
        params.updateValue(userID, forKey: "user_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        print_debug("hitting \(LOCK_MARINA_URL) with param \(params) and headers :\(head)")
        Alamofire.request(LOCK_MARINA_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("locking parking json: \(json)")
                    if let code = json["code"].int, code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if let code = json["code"].int as Int?,code == 200 {
                        if let isLocked = json["current_state"].bool as Bool?{
                            completionBlock(isLocked)
                        }else{
                            showErrorWithMessage("Couldn't locked your choice!")
                            completionBlock(false)
                        }
                    }else{
                        showErrorWithMessage("Couldn't locked your choice!")
                        completionBlock(false)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(false)
                }
            case .failure(let error):
                showErrorWithMessage("Oops! \(error.localizedDescription)")
                completionBlock(false)
            }
        }
    }




    func getMarinaReviewsAndRating(_ marinaID:String,ratingWanted wantRating:Bool,page:Int,perPage: Int,completionBlock:@escaping (AnyObject?) -> Void){
        var params = ["marina_id":marinaID]
        params.updateValue(wantRating ? "true" : "false", forKey: "need_ratings")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue("\(perPage)", forKey: "perPage")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MARINA_REVIEWS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_REVIEWS_URL,method: .get ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("reviews json: \(json)")
                    let reviewParser = ReviewParser(json: json)
                    if reviewParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                        if reviewParser.code == 200{
                            completionBlock(reviewParser)
                        }else{
                            showErrorWithMessage(reviewParser.message)
                    }

                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }


    func rideRequest(_ userID: String,carCategory:String, pickUpAddress:String,pickUpCoordinates: CLLocationCoordinate2D,dropOffAddress:String,dropOffCoordinates: CLLocationCoordinate2D,time: Double,distance:Double,completionBlock:@escaping (AnyObject?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(carCategory, forKey: "category_id")
        params.updateValue("\(pickUpAddress)", forKey: "start_location")
        params.updateValue("\(dropOffAddress)", forKey: "end_location")
        params.updateValue("\(pickUpCoordinates.latitude)", forKey: "start_lat")
        params.updateValue("\(pickUpCoordinates.longitude)", forKey: "start_lng")
        params.updateValue("\(dropOffCoordinates.latitude)", forKey: "end_lat")
        params.updateValue("\(dropOffCoordinates.longitude)", forKey: "end_lng")
        params.updateValue("\(time)", forKey: "time")
        params.updateValue("\(distance)", forKey: "distance")


        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(RIDE_REQUEST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(RIDE_REQUEST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride history json is:\n\(json)")
                    if let _resCode = json["code"].int as Int?{
                        if _resCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }

                        if _resCode == 200{
                            if let dict = json["ride"].dictionaryObject as [String:AnyObject]?{
                                let marina = Marina(dictionary: dict)
                                completionBlock(marina as AnyObject)
                            }else{
                                showErrorWithMessage(json["message"].stringValue)
                                completionBlock(nil)
                            }
                        }else{
                            showErrorWithMessage(json["message"].stringValue)
                            completionBlock(nil)
                        }
                    }else{
                        completionBlock(json["message"].string as AnyObject?)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }

    func cancelRide(_ rideID:String,reason:String,completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["ride_id":rideID]//,"reason":reason]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(USER_CANCEL_RIDE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(USER_CANCEL_RIDE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ride history json is:\n\(json)")
                    if let _resCode = json["response_code"].int as Int?{
                        if _resCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }

                        if _resCode == 200{
                            if let dict = json["ride"].dictionaryObject as [String:AnyObject]?{
                                let marina = Marina(dictionary: dict)
                                completionBlock(marina as AnyObject)
                            }else{
                                showErrorWithMessage(json["message"].stringValue)
                                completionBlock(nil)
                            }
                        }else{
                            showErrorWithMessage(json["message"].stringValue)
                            completionBlock(nil)
                        }
                    }else{
                        completionBlock(json["response_message"].string as AnyObject?)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops! Error Occurred")
                print(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }





    func getAllServicesFromServer(_ completionBlock:@escaping (Array<Service>?) -> Void){
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        // var params = ["page":"\(pageNumber)","per_page":"\(perPage)"]
//        if let key = keyword{
//            params.updateValue(key, forKey: "keyword")
//        }
        print_debug("hitting \(SERVICES_URL) headers :\(head)")
        Alamofire.request(SERVICES_URL,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("services json is:\n\(json)")
                    let serviceParser = ServiceParser(json: json)
                    if serviceParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    if serviceParser.responseCode == 200{
                        completionBlock(serviceParser.services)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure:
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(nil)
            }
        }
    }





    func getBookingWithParkingSpace(_ parkingSpaceID:String,bookingDate:BookingDates,completionBlock:@escaping (Booking?) -> Void){
        let fromDate = CommonClass.formattedDateWith(bookingDate.fromDate, format: "YYYY-MM-dd")

        var params = Dictionary<String,String>()
        params.updateValue(parkingSpaceID, forKey: "parking_space_id")

        params.updateValue(fromDate, forKey: "date_from")

//        if bookingDate.totalDays > 0{
//            let toDate = CommonClass.formattedDateWith(bookingDate.toDate, format: "YYYY-MM-dd")
//            params.updateValue(toDate, forKey: "date_to")
//        }else{
//             let toDate = CommonClass.formattedDateWith(bookingDate.toDate.addingTimeInterval(24*3600), format: "YYYY-MM-dd")
//            params.updateValue(toDate, forKey: "date_to")
//        }

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(PARKING_SPACE_BOOKING_FOR_DATES_URL) with param \(params) and headers :\(head)")
        Alamofire.request(PARKING_SPACE_BOOKING_FOR_DATES_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Availability json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.booking)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    
    func rateMarinaToBooking(marinaID:String,bookingID:String,rating:Float,completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["marina_id":marinaID,"booking_id":bookingID,"rating":"\(rating)"]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(MARINA_RATE_TO_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_RATE_TO_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("customer add review in json is :\n\(json)")
                    if let responseCode = json["code"].int as Int? {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }
                        if responseCode == 200 {
                             let booking = Booking(json: json)
                            completionBlock(booking as AnyObject?)
                        }else{
                            completionBlock(json["message"].string as AnyObject?)
                        }
                    }else{
                        completionBlock(json["message"].string as AnyObject?)
                    }
                }
            case .failure(let error):
                completionBlock(error.localizedDescription as AnyObject?)
            }
        }
    }

    func getMarinaPrices(by marinaID:String,completionBlock:@escaping (_ success:Bool, _ marinaPriceModel:MarinaPriceModel?, _ message:String) -> Void) {
        let params = ["marina_id":marinaID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(GET_PRICE_FOR_MARINA_URL) with param \(params) and headers :\(head)")
        Alamofire.request(GET_PRICE_FOR_MARINA_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("marina price json is :\n\(json)")
                    let marinaPriceModel = MarinaPriceModel(json: json)
                    if marinaPriceModel.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    completionBlock((marinaPriceModel.code == 200),marinaPriceModel,marinaPriceModel.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }


    func setMarinaPrices(for marinaID:String, regularPrice:Double, weekendPrice:Double,weekendType: String, specialPrice:Dictionary<String,Double>?, completionBlock:@escaping (_ success:Bool, _ marinaPriceModel:MarinaPriceModel?, _ message:String) -> Void) {
        var params = Dictionary<String,Any>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue("\(regularPrice)", forKey: "default_price")
        params.updateValue("\(weekendPrice)", forKey: "weekend_price")
        params.updateValue(specialPrice as Any, forKey: "specific_price")
        params.updateValue(weekendType, forKey: "weekend_type")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(SET_PRICE_FOR_MARINA_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SET_PRICE_FOR_MARINA_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Set marina price json is :\n\(json)")
                    let marinaPriceModel = MarinaPriceModel(json: json)
                    if marinaPriceModel.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((marinaPriceModel.code == 200),marinaPriceModel,marinaPriceModel.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    func removeDateWisePriceList(_ marinaID:String,date:String,completionBlock:@escaping (_ success:Bool,_ message: String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(date, forKey: "date")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REMOVE_DATE_WISE_PRICE_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(REMOVE_DATE_WISE_PRICE_LIST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug(" json is:\n\(json)")
                    let marinaPriceModel = MarinaPriceModel(json: json)
                    if marinaPriceModel.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    completionBlock(marinaPriceModel.code == 200, marinaPriceModel.message)
                }else{
                    completionBlock(false,"Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }
    func getUserEmail(_ email: String,completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["email":email]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(USER_EMAIL_URL) with param \(params) and headers :\(head)")
        Alamofire.request(USER_EMAIL_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("graph json is:\n\(json)")
                    if let responseCode = json["code"].int as Int? {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }

                        if responseCode == 208{
                            if let userEmailInfo = json["user"].dictionaryObject as Dictionary<String,AnyObject>?{
                                completionBlock(userEmailInfo as AnyObject?)
                            }
                        }else{
                            completionBlock(nil)
                        }
                    }else{
                        completionBlock(nil)
                    }
                }
            case .failure(let error):
                completionBlock(error.localizedDescription as AnyObject?)
            }
        }
    }
    func getUserEmailInfo(_ email: String,completionBlock:@escaping (_ refundableDetails:User?,_ detailBoat:Array<Boat>?) -> Void){
        let params = ["email":email]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(USER_EMAIL_URL) and headers :\(head)")
        Alamofire.request(USER_EMAIL_URL,method: .post, parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("refundable details json is:\n\(json)")
                    let userparser = UserParser(json: json)
                    let boatparser = BoatParser(json: json)
                    if userparser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    if userparser.code == 208{
                        completionBlock(userparser.user, boatparser.boats)
                    }else{
                        completionBlock(nil, boatparser.boats)
                    }
                }else{
                    completionBlock(nil, nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil, nil)
            }
        }
    }
    
    func getMarinaQRCode(_ marinaId:String, completionBlock:@escaping (_ marinaQRProfile: Marina?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(marinaId, forKey: "marina_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \( MARINA_QRCODE_PROFILE_URL) with param \(params) and headers :\(head)")
        Alamofire.request( MARINA_QRCODE_PROFILE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("qr Profile json is:\n\(json)")
                    let marinaQRParser = MarinaQRProfileParser(json: json)
                    if marinaQRParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if marinaQRParser.responseCode == 200{
                        completionBlock(marinaQRParser.marina)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    
    func addMarinaPriceByRange(for marinaID:String, priceFor:String,weekendType: String, price:Dictionary<String,String>?, completionBlock:@escaping (_ success:Bool, _ marinaPriceModel:MarinaRangePrice?, _ message:String) -> Void) {
        var params = Dictionary<String,Any>()
        
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(priceFor, forKey: "price_for")
        params.updateValue(price as Any, forKey: "price")
        params.updateValue(weekendType, forKey: "weekend_type")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(ADD_MARINA_PRICE_BY_RANGE) with param \(params) and headers :\(head)")
        Alamofire.request(ADD_MARINA_PRICE_BY_RANGE,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Set marina price json is :\n\(json)")
                    let marinaRangePrice = MarinaRangePriceParser(json: json)
                    if marinaRangePrice.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((marinaRangePrice.responseCode == 200),marinaRangePrice.priceRange,marinaRangePrice.responseMessage)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    func addMarinaSpecificPriceByDate(for marinaID:String, priceFor:String,priceDate:String, price:Dictionary<String,String>?, completionBlock:@escaping (_ success:Bool, _ marinaPriceModel:MarinaRangePrice?, _ message:String) -> Void) {
        var params = Dictionary<String,Any>()
        
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(priceFor, forKey: "price_for")
        params.updateValue(price as Any, forKey: "price")
        params.updateValue(priceDate, forKey: "price_date")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(ADD_MARINA_SPECIFIC_PRICE_BY_DATE) with param \(params) and headers :\(head)")
        Alamofire.request(ADD_MARINA_SPECIFIC_PRICE_BY_DATE,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Set marina price json is :\n\(json)")
                    let marinaRangePrice = MarinaRangePriceParser(json: json)
                    if marinaRangePrice.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((marinaRangePrice.responseCode == 200),marinaRangePrice.priceRange,marinaRangePrice.responseMessage)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    func getDatesAllSelectPrices(forMarina marinaID:String,completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["marina_id":marinaID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(MARINA_PRICES_RANGE_WITH_DATES) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_PRICES_RANGE_WITH_DATES,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("graph json is:\n\(json)")
                    if let responseCode = json["code"].int as Int? {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }
                        if responseCode == 200{
                            if let pricesDates = json["price_dates"].arrayObject as? Array<String>{
                                completionBlock(pricesDates as AnyObject?)
                            }
                        }else{
                            completionBlock(nil)
                        }
                    }else{
                        completionBlock(nil)
                    }
                }
            case .failure(let error):
                completionBlock(error.localizedDescription as AnyObject?)
            }
        }
    }
    func getMarinaPriceByRange(forMarinaID marinaID:String,priceFor:String,priceType:String,priceDate:String,weekendType: String,  completionBlock:@escaping (_ success:Bool, _ marinaPriceModel:MarinaRangePrice?, _ message:String) -> Void) {
        //var params = Dictionary<String,String>()
        let params = ["marina_id":marinaID,"price_for":priceFor,"price_date":priceDate,"weekend_type":weekendType,"price_type": priceType]
        
        //        params.updateValue(marinaID, forKey: "marina_id")
        //        params.updateValue(priceFor, forKey: "price_for")
        //        params.updateValue(priceDate, forKey: "price_date")
        //        params.updateValue(weekendType, forKey: "weekend_type")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(GET_MARINA_PRICE_RANGE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(GET_MARINA_PRICE_RANGE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Set marina price json is :\n\(json)")
                    let marinaRangePrice = MarinaRangePriceParser(json: json)
                    if marinaRangePrice.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((marinaRangePrice.responseCode == 200),marinaRangePrice.priceRange,marinaRangePrice.responseMessage)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
    
    func getNormalMarinaPrices(forMarinaID marinaID:String, priceFor:String,priceType: String,priceDate:String,weekendType: String,  completionBlock:@escaping (_ success:Bool, _ marinaPriceModel:MarinaPriceSessionReview?, _ message:String) -> Void) {
        //var params = Dictionary<String,String>()
        let params = ["marina_id":marinaID,"price_for":priceFor,"price_date":priceDate,"weekend_type":weekendType,"price_type": priceType]
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(GET_MARINA_PRICE_RANGE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(GET_MARINA_PRICE_RANGE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Set marina price json is :\n\(json)")
                    let marinaPriceSessionReview = MarinaPriceSessionReview(json: json)
                    if marinaPriceSessionReview.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((marinaPriceSessionReview.code == 200),marinaPriceSessionReview,marinaPriceSessionReview.message)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
   
    
    
    func getUpdateMarinaPriceType(_ marinaId:String ,completionBlock:@escaping (_ success:Bool, _ marina:Marina?, _ message:String) -> Void){
        let params = ["marina_id": marinaId]

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(UPDATE_MARINA_PRICE_TYPE) with param \(params) and headers :\(head)")
        Alamofire.request(UPDATE_MARINA_PRICE_TYPE, method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("marinas json is:\n\(json)")
                    let marinaParser = MarinaParser(json: json)
                    if marinaParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }
                    if marinaParser.responseCode == 200{
                        completionBlock(true, marinaParser.marina, marinaParser.responseMessage)
                    }else{
                        completionBlock(false,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Something went wrong!")
                }
            case .failure(let error):
                showErrorWithMessage("Oops! Error Occurred")
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }
    
}









