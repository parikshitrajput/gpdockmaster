//
//  PaymentService.swift
//  GPDock
//
//  Created by TecOrb on 01/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Stripe

class CardParser: NSObject {
    /* "code": 200,
     "message": "success! record found",
     "cards":
     */
    var responseCode = 0
    var responseMessage = ""
    var cards = Array<Card>()
    var card = Card()


//    TODO:- It may subject to change if any other type of payment source would be added for payment by client
//    var cards = Array<STPSourceProtocol>()

    override init() {
        super.init()
    }

    init(json: JSON) {
        if let _code = json["code"].int as Int?{
            self.responseCode = _code
        }

        if let _msg = json["message"].string as String?{
            self.responseMessage = _msg
        }
        if let _cardDict = json["card"].dictionaryObject as Dictionary<String,AnyObject>?{
            self.card = Card(params: _cardDict)
        }

        if let _cards = json["cards"].arrayObject as? Array<Dictionary<String,AnyObject>>{
            for _cardDict in _cards{
                let card = Card(params: _cardDict)
                self.cards.append(card)
            }
        }


    }
}

class Card:NSObject{
/*
    "id": 3,
    "card_token": "card_1AlxIOHCw2A9l24g1yf1va48",
    "exp_month": "1",
    "exp_year": "2020",
    "last4": "4242",
    "brand": "Visa",
    "funding_type": "credit",
    "is_default": true
    */
    let kID = "id"
    let kCardToken = "card_token"
    let kExpMonth = "exp_month"
    let kExpYear = "exp_year"
    let kLast4 = "last4"
    let kBrand = "brand"
    let kFundingType = "funding_type"
    let kIsDefault = "is_default"


    var ID = ""
    var cardToken : String = ""
    var expMonth: UInt = 0
    var expYear : UInt = 0
    var last4 : String = ""
    var brand : String = ""
    var fundingType :String = ""
    var isDefault : Bool = false

    override init() {
        super.init()
    }

    init(params:Dictionary<String,AnyObject>) {

        if let _ID = params[kID] as? Int{
            self.ID = "\(_ID)"
        }else if let _ID = params[kID] as? String{
            self.ID = _ID
        }

        if let _token = params[kCardToken] as? Int{
            self.cardToken = "\(_token)"
        }else if let _token = params[kCardToken] as? String{
            self.cardToken = _token
        }

        if let _expMonth = params[kExpMonth] as? UInt{
            self.expMonth = _expMonth
        }else if let _expMonth = params[kExpMonth] as? String{
            self.expMonth = UInt(_expMonth) ?? 0
        }

        if let _expYear = params[kExpYear] as? UInt{
            self.expYear = _expYear
        }else if let _expYear = params[kExpYear] as? String{
            self.expYear = UInt(_expYear) ?? 0
        }

        if let _last4 = params[kLast4] as? String{
            self.last4 = _last4
        }
        if let _brand = params[kBrand] as? String{
            self.brand = _brand
        }
        if let _fundingType = params[kFundingType] as? String{
            self.fundingType = _fundingType
        }
        if let _isDefault = params[kIsDefault] as? Bool{
            self.isDefault = _isDefault
        }
        super.init()
    }
}




class PaymentService {
    static let sharedInstance = PaymentService()
    fileprivate init() {}


    func getCardsForUser(_ userID:String,completionBlock:@escaping (Array<Card>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(CARDS_LIST_URL) with param \(params) and headers :\(head)")
        Alamofire.request(CARDS_LIST_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }

                    if cardParser.responseCode == 200{
                        completionBlock(cardParser.cards)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }

   
    
    func getBanksAccountForUser(_ userID:String,completionBlock:@escaping (Array<BankAccount>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(BANK_ACCOUNT_LISTS) with param \(params) and headers :\(head)")
        Alamofire.request(BANK_ACCOUNT_LISTS,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let accountParser = BankAccountParser(json: json)
                    if accountParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if accountParser.responseCode == 200{
                        completionBlock(accountParser.banksAccounts)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    
    func addBanksForUser(_ userID:String,sourceToken:String,completionBlock:@escaping (_ success:Bool,_ bankAccount:BankAccount?,_ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(sourceToken, forKey: "source_token")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ADD_BANK_ACCOUNT_URL) with param \(params) and headers :\(head)")
        Alamofire.request(ADD_BANK_ACCOUNT_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("bank json is:\n\(json)")
                    let accountParser = BankAccountParser(json: json)
                    if accountParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                      completionBlock((accountParser.responseCode == 200),accountParser.bankAccount, accountParser.responseMessage)
                }else{
                    //showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(false,nil, response.result.error?.localizedDescription ?? "Oops! Something went wrong")
                }
            case .failure(let error):
                //showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func makeDefaultBankAccount(_ userID:String,bankAccountID:String,completionBlock:@escaping (_ success:Bool,_ account:BankAccount?,_ message: String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(bankAccountID, forKey: "account_id")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MAKE_DEFAULT_BANK_ACCOUNT_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MAKE_DEFAULT_BANK_ACCOUNT_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("bank json is:\n\(json)")
                    let accountParser = BankAccountParser(json: json)
                    if accountParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(accountParser.responseCode == 200,accountParser.bankAccount,accountParser.responseMessage)
                }else{
                    completionBlock(false,nil,response.result.error?.localizedDescription ?? "Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    func removeBankAccount(_ userID:String,bankAccountID:String,completionBlock:@escaping (_ success:Bool,_ message: String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(bankAccountID, forKey: "bank_account_id")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REMOVE_BANK_ACCOUNT) with param \(params) and headers :\(head)")
        Alamofire.request(REMOVE_BANK_ACCOUNT,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("bank json is:\n\(json)")
                let accountParser = BankAccountParser(json: json)
                    if accountParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                     completionBlock(accountParser.responseCode == 200,accountParser.responseMessage)
                }else{
                       completionBlock(false,"Oops! Something went wrong")
                }
            case .failure(let error):
               completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }

    func addCardsForUser(_ userID:String,sourceToken:String,completionBlock:@escaping (Card?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(sourceToken, forKey: "source")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ADD_CARD_URL) with param \(params) and headers :\(head)")
        Alamofire.request(ADD_CARD_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if cardParser.responseCode == 200{
                        completionBlock(cardParser.card)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }


    func payAndCreateBookingFor(_ userID: String,bookingDates:BookingDates,parkingSpaceID: String,pricePerFeet:Double,boatID:String,amount:Double,sourceToken:String,cardID:String,willSave :Bool,completionBlock:@escaping (_ success:Bool,_ booking:Booking?,_ message:String)->Void){

        let fromDate = CommonClass.formattedDateWith(bookingDates.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.formattedDateWith(bookingDates.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")
        params.updateValue(parkingSpaceID, forKey: "parking_space_id")
        params.updateValue("\(pricePerFeet)", forKey: "price_per_foot")
        params.updateValue(boatID, forKey: "boat_id")
        let cents = String(format: "%.0lf", amount*100)
        params.updateValue(cents, forKey: "amount")
        params.updateValue(sourceToken, forKey: "source")
        params.updateValue(willSave ? "true" : "false", forKey: "is_saved")
        params.updateValue(cardID, forKey: "card_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        Alamofire.request(CREATE_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("booking json is:\n\(json)")
                    
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(true,bookingParser.booking,bookingParser.responseMessage)
                    }else{
                        completionBlock(false,nil,bookingParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,"Opps!, Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }


    func createOfflineBookingFor(_ userID: String,boatName: String, bookingDates:BookingDates,boatSize:BoatSize,parkingSpaceID: String,pricePerFeet:Double,firstName:String,lastName:String?,email:String,contact:String,boatImage:UIImage?,completionBlock:@escaping (_ success:Bool,_ booking:Booking?,_ message:String)->Void){

        let fromDate = CommonClass.formattedDateWith(bookingDates.fromDate, format: "YYYY-MM-dd")
        let toDate = CommonClass.formattedDateWith(bookingDates.toDate, format: "YYYY-MM-dd")
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "business_user_id")
        params.updateValue(fromDate, forKey: "date_from")
        params.updateValue(toDate, forKey: "date_to")
        params.updateValue(parkingSpaceID, forKey: "parking_space_id")
        params.updateValue("\(pricePerFeet)", forKey: "price_per_foot")
        params.updateValue(firstName, forKey: "first_name")
        if let lName = lastName{params.updateValue(lName, forKey: "last_name")}
        params.updateValue(email, forKey: "email")
        params.updateValue(contact, forKey: "contact")
         params.updateValue(boatName, forKey: "name")
        params.updateValue("\(boatSize.length)", forKey: "long")
        params.updateValue("\(boatSize.width)", forKey: "width")
        params.updateValue("\(boatSize.depth)", forKey: "depth")
        if let image = boatImage{
            if let base64String = UIImageJPEGRepresentation(image, 0.8)?.base64EncodedString() {
                params.updateValue(base64String, forKey: "image")
            }
        }

//        let cents = String(format: "%.0lf", amount*100)
//        params.updateValue(cents, forKey: "amount")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)

        Alamofire.request(CREATE_OFF_LINE_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("offline booking json is:\n\(json)")

                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(true,bookingParser.booking,bookingParser.responseMessage)
                    }else{
                        completionBlock(false,nil,bookingParser.responseMessage)
                    }
                }else{
                    completionBlock(false,nil,"Opps!, Some thing went wrong")
                }
            case .failure(let error):
                completionBlock(false,nil,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }




    func removeCard(_ cardID:String,completionBlock:@escaping (_ success:Bool,_ message: String) -> Void){

        let url = REMOVE_CARD_URL+cardID
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REMOVE_CARD_URL) and headers :\(head)")
        Alamofire.request(url,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)

                    print_debug("remove card json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(cardParser.responseCode == 200,cardParser.responseMessage)
                }else{
                    completionBlock(false,"Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }

    func makeDefaultCard(_ cardID:String,completionBlock:@escaping (_ success:Bool,_ message: String) -> Void){

        let url = MAKE_DEFAULT_CARD_URL+cardID
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(MAKE_DEFAULT_CARD_URL) and headers :\(head)")
        Alamofire.request(url,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)

                    print_debug("default card json is:\n\(json)")
                    let cardParser = CardParser(json: json)
                    if cardParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(cardParser.responseCode == 200,cardParser.responseMessage)
                }else{
                    completionBlock(false,"Oops! Something went wrong")
                }
            case .failure(let error):
                completionBlock(false,"Oops!\r\n\(error.localizedDescription)")
            }
        }
    }

    func getBookingDetails(_ bookingID:String,completionBlock:@escaping (_ booking:Booking?) -> Void){

        let url = BOOKING_DETAILS_URL+bookingID
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(BOOKING_DETAILS_URL) and headers :\(head)")
        Alamofire.request(url,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("booking json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.booking)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }


    func getBookingRefundStatus(_ bookingID:String,completionBlock:@escaping (_ refund:RefundBookingDetail?) -> Void){
        let params = ["booking_id":bookingID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REFUND_STATUS_URL) and headers :\(head)")
        Alamofire.request(REFUND_STATUS_URL,method: .post, parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("refund details json is:\n\(json)")
                    let refundParser = RefundBookingDetailPaser(json: json)
                    if refundParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if refundParser.responseCode == 200{
                        completionBlock(refundParser.refund)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }






    func getBookingRefundDetails(_ bookingID:String,completionBlock:@escaping (_ refund:Refund?) -> Void){
        let params = ["booking_id":bookingID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(REFUND_DETAILS_URL) and headers :\(head)")
        Alamofire.request(REFUND_DETAILS_URL,method: .post, parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("refund details json is:\n\(json)")
                    let refundParser = RefundPaser(json: json)
                    if refundParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if refundParser.responseCode == 200{
                        completionBlock(refundParser.refund)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }



    func checkAndGetBookingRefundableDetails(_ bookingID:String,completionBlock:@escaping (_ refundableDetails:RefundableDetails?) -> Void){
        let params = ["booking_id":bookingID]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(CHECK_REFUNDABLE_BOOKING_DETAILS_URL) and headers :\(head)")
        Alamofire.request(CHECK_REFUNDABLE_BOOKING_DETAILS_URL,method: .post, parameters:params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("refundable details json is:\n\(json)")
                    let refundableDetailsParser = RefundableDetailsPaser(json: json)
                    if refundableDetailsParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if refundableDetailsParser.responseCode == 200{
                        completionBlock(refundableDetailsParser.refundableDetails)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    completionBlock(nil)
                }
            case .failure(let error):
                print_debug(error.localizedDescription)
                completionBlock(nil)
            }
        }
    }

    func earningGraphForMarina(forMarina marinaID:String,usingFilter filter:String,fromDate:String,toDate:String,completionBlock:@escaping (AnyObject?,AnyObject?) -> Void){
        let params = ["marina_id":marinaID,"keyword":filter,"from_date":fromDate,"to_date":toDate]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(MARINA_EARNING_GRAPH_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_EARNING_GRAPH_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("graph json is:\n\(json)")
                    if let responseCode = json["code"].int as Int? {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }
                        if responseCode == 200{
                            if let graphData = json["graph_bookings"].dictionaryObject as Dictionary<String,AnyObject>?{
                                completionBlock(graphData as AnyObject?, nil)
                            }
                            if let occuracyResult = json["occupacy_result"].dictionaryObject as Dictionary<String,AnyObject>?{
                                completionBlock(nil, occuracyResult as AnyObject?)
                            }
//                            completionBlock(graphData as AnyObject?, occuracyResult as AnyObject?)
                        }else{
                            completionBlock(json["message"].string as AnyObject?, nil)
                        }
                    }else{
                        completionBlock(json["message"].string as AnyObject?, nil)
                    }
                }
            case .failure(let error):
                completionBlock(error.localizedDescription as AnyObject?,nil)
            }
        }
    }

    func payoutMarinaEarningGraph(forMarina marinaID:String,usingFilter filter:String,fromDate:String,toDate:String,completionBlock:@escaping (AnyObject?) -> Void){
        let params = ["marina_id":marinaID,"keyword":filter,"from_date":fromDate,"to_date":toDate]
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        debugPrint("hitting \(MARINA_PAYOUT_EARNING_GRAPH_URL) with param \(params) and headers :\(head)")
        Alamofire.request(MARINA_PAYOUT_EARNING_GRAPH_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("graph json is:\n\(json)")
                    if let responseCode = json["code"].int as Int? {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }
                        if responseCode == 200{
                            if let graphData = json["earnings_graph"].dictionaryObject as Dictionary<String,AnyObject>?{
                                completionBlock(graphData as AnyObject?)
                            }
                        }else{
                            completionBlock(nil)
                        }
                    }else{
                        completionBlock(nil)
                    }
                }
            case .failure(let error):
                completionBlock(error.localizedDescription as AnyObject?)
            }
        }
    }
    
}
