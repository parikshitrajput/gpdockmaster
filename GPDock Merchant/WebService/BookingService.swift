//
//  BookingService.swift
//  GPDock
//
//  Created by TecOrb on 04/08/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class BookingService {
    static let sharedInstance = BookingService()
    fileprivate init() {}
    
    func getPayoutsFor(_ marinaID:String,fromDate:String,toDate:String,page:Int,perPage: Int, completionBlock:@escaping (Array<Payouts>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(fromDate, forKey: "from_date")
        params.updateValue(toDate, forKey: "to_date")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("\(page)", forKey: "page")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(PAYOUTS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(PAYOUTS_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("payout json is:\n\(json)")
                    if let responseCode = json["code"].int as Int? {
                        if responseCode == 345{
                            CommonClass.hideLoader()
                            CommonClass.sharedInstance.hadleAccessTokenError(true)
                            return
                        }
                        if responseCode == 200{
                            let payoutParser = PayoutsParser(json: json)
                            completionBlock(payoutParser.payouts as AnyObject as? Array<Payouts>)
                        }
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    

    func getBookingsForUser(_ userID:String,status:String,page:Int,perPage: Int,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(status, forKey: "status")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("\(page)", forKey: "page")


        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(LIST_OF_BOOKINGS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(LIST_OF_BOOKINGS_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }



    func getBookingsForMarina(_ marinaID:String,status:String,page:Int,perPage: Int,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(status, forKey: "status")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("\(page)", forKey: "page")


        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(BOOKINGS_FOR_MARINA_URL) with param \(params) and headers :\(head)")
        Alamofire.request(BOOKINGS_FOR_MARINA_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }

    func searchBookings(_ marinaID:String,searchKey:String, status:String,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(status, forKey: "status")
        params.updateValue(searchKey, forKey: "search_key")

        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SEARCH_BOOKINGS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SEARCH_BOOKINGS_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }

    
    

    func scanQRCodeWithQRCodeID(qrCodeString qrCodeID:String,on marinaID:String,completionBlock:@escaping (_ success:Bool, _ booking: Booking?, _ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(qrCodeID, forKey: "qrcode")
        params.updateValue(marinaID, forKey: "marina_id")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(SCAN_QR_CODE_URL) with param \(params) and headers :\(head)")
        Alamofire.request(SCAN_QR_CODE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Booking json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock((bookingParser.responseCode == 200),bookingParser.booking,bookingParser.responseMessage)
                }else{
                    completionBlock(false,nil,response.error?.localizedDescription ?? "Could not finished!")
                }
            case .failure(let error):
                completionBlock(false,nil,error.localizedDescription)
            }
        }
    }

    
    func completeBookingOne(with bookingID:String,completionBlock:@escaping (_ booking: Booking?, _ message: String, _ success:Bool) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(bookingID, forKey: "booking_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(COMPLETE_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(COMPLETE_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Booking json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.booking, bookingParser.responseMessage, true)
                    }else{
                        completionBlock(nil,bookingParser.responseMessage, false)
                    }

                   // completionBlock(bookingParser.booking, bookingParser.responseMessage)
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil,"Something went wrong!", false)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil,"Something went wrong!", false)
            }
        }
    }


    func completeBooking(with bookingID:String,completionBlock:@escaping (_ booking: Booking?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(bookingID, forKey: "booking_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(COMPLETE_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(COMPLETE_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Booking json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(bookingParser.booking)
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }


    

    func cancelBookings(with bookingID:String,userId: String,completionBlock:@escaping (_ success: Bool) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(bookingID, forKey: "booking_id")
        params.updateValue(userId, forKey: "user_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(CANCEL_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(CANCEL_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    completionBlock(bookingParser.responseCode == 200)
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(false)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(false)
            }
        }
    }

    func cancelBookingsOne(with bookingID:String,userId: String,cancelReason:String,completionBlock:@escaping (_ success: Bool, _ message: String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(bookingID, forKey: "booking_id")
        params.updateValue(userId, forKey: "user_id")
        params.updateValue(cancelReason, forKey: "cancel_reason")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(CANCEL_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(CANCEL_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                     completionBlock(true, bookingParser.responseMessage)
                        
                    }else{
                        completionBlock(false, bookingParser.responseMessage)
                    }
                    //completionBlock(bookingParser.responseCode == 200, bookingParser.responseMessage)
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(false, "Oops! Something wrong went")
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(false, "Oops! Something wrong went")
            }
        }
    }
    func getParkingSpaceInfo(_ parkingSpaceID:String,status:String,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(parkingSpaceID, forKey: "parking_space_id")
        params.updateValue(status, forKey: "status")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \( BOOKINGS_FOR_PARKING_SPACE_URL) with param \(params) and headers :\(head)")
        Alamofire.request( BOOKINGS_FOR_PARKING_SPACE_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    
    func getListForBookings(_ marinaID:String,userID:String,dateFrom:Date,status:String,page:Int,perPage: Int,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
        let dFrom = CommonClass.formattedDateWith(dateFrom, format: "YYYY-MM-dd")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(status, forKey: "search_key")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue(dFrom, forKey: "date_from")

        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(LIST_FOR_BOOKINGS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(LIST_FOR_BOOKINGS_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    func getPartiallAndBookedListForBookings(_ marinaID:String,userID:String,dateFrom:Date,dateTo:Date,status:String,page:Int,perPage: Int,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
        let dFrom = CommonClass.formattedDateWith(dateFrom, format: "YYYY-MM-dd")
        let dTo = CommonClass.formattedDateWith(dateTo, format: "YYYY-MM-dd")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(status, forKey: "search_key")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue(dFrom, forKey: "date_from")
         params.updateValue(dTo, forKey: "date_to")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(LIST_FOR_BOOKED_AND_PARTIALLY_BOOKINGS_URL) with param \(params) and headers :\(head)")
        Alamofire.request(LIST_FOR_BOOKED_AND_PARTIALLY_BOOKINGS_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    func getDateWiseBookingListBookings(_ marinaID:String,userID:String,dateFrom:Date,dateTo:Date,status:String,page:Int,perPage: Int,completionBlock:@escaping (Array<Booking>?) -> Void){
        var params = Dictionary<String,String>()
        let dFrom = CommonClass.formattedDateWith(dateFrom, format: "YYYY-MM-dd")
        let dTo = CommonClass.formattedDateWith(dateTo, format: "YYYY-MM-dd")
        params.updateValue(marinaID, forKey: "marina_id")
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(status, forKey: "search_key")
        params.updateValue("\(perPage)", forKey: "per_page")
        params.updateValue("\(page)", forKey: "page")
        params.updateValue(dFrom, forKey: "date_from")
        params.updateValue(dTo, forKey: "date_to")
        
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(DATE_WISE_MARINA_LIST_BOOKING_URL) with param \(params) and headers :\(head)")
        Alamofire.request(DATE_WISE_MARINA_LIST_BOOKING_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("cards json is:\n\(json)")
                    let bookingParser = BookingParser(json: json)
                    if bookingParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if bookingParser.responseCode == 200{
                        completionBlock(bookingParser.bookings)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
  
    func getCountForBooking(with marinaID:String,completionBlock:@escaping (CountBooking?) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(marinaID, forKey: "marina_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(BOOKING_COUNT_URL) with param \(params) and headers :\(head)")
        Alamofire.request(BOOKING_COUNT_URL,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("Booking json is:\n\(json)")
                    let countParser = CountBookingParser(json:json)
                    if countParser.responseCode == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(true)
                        return
                    }
                    if countParser.responseCode == 200{
                        completionBlock(countParser.count)
                    }else{
                        completionBlock(nil)
                    }
                }else{
                    showErrorWithMessage("Oops! Something wrong went")
                    completionBlock(nil)
                }
            case .failure(let error):
                showErrorWithMessage("Oops!\r\n\(error.localizedDescription)")
                completionBlock(nil)
            }
        }
    }
    
    
    
}
