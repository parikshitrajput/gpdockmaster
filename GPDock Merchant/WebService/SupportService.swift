//
//  SupportService.swift
//  GPDock
//
//  Created by TecOrb on 07/05/18.
//  Copyright © 2018 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SupportService {
    static let sharedInstance = SupportService()
    fileprivate init() {}


    func getAllQueriesForUser(_ userID:String,completionBlock:@escaping (_ success:Bool,_ supportTickets:Array<SupportQuery>?, _ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ALL_SUPPORT_TICKETS) with param \(params) and headers :\(head)")
        Alamofire.request(ALL_SUPPORT_TICKETS,method: .get ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("tickets json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }else{
                        completionBlock((supportParser.code == 200),supportParser.supports,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,"Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }


    func getDetailsForSupportWith(_ id:String,completionBlock:@escaping (_ success:Bool,_ supportTicket: SupportQuery?, _ message:String) -> Void){
        let url = ALL_SUPPORT_TICKETS+"/\(id)"
        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ALL_SUPPORT_TICKETS) and headers :\(head)")
        Alamofire.request(url,method: .get, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("ticket details json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }else{
                        completionBlock((supportParser.code == 200),supportParser.support,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,"Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }


    func addCommentForSupportQuery(_ userID:String, supportID:String, message: String, completionBlock:@escaping (_ success:Bool,_ comment:SupportMessage?, _ message:String) -> Void){
        var params = Dictionary<String,String>()
        params.updateValue(userID, forKey: "user_id")
        params.updateValue(supportID, forKey: "support_id")
        params.updateValue(message, forKey: "message")

        let head =  CommonClass.sharedInstance.prepareHeader(withAuth: true)
        print_debug("hitting \(ADD_COMMENT_TO_SUPPORT) with param \(params) and headers :\(head)")
        Alamofire.request(ADD_COMMENT_TO_SUPPORT,method: .post ,parameters: params, headers:head).responseJSON { response in
            switch response.result {
            case .success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print_debug("comment json is:\n\(json)")
                    let supportParser = SupportTicketParser(json: json)
                    if supportParser.code == 345{
                        CommonClass.hideLoader()
                        CommonClass.sharedInstance.hadleAccessTokenError(false)
                        return
                    }else{
                        completionBlock((supportParser.code == 200),supportParser.comment,supportParser.message)
                    }
                }else{
                    completionBlock(false, nil,response.result.error?.localizedDescription ?? "Oops! Something wrong went")
                }
            case .failure(let error):
                completionBlock(false, nil, error.localizedDescription)
            }
        }
    }
}
