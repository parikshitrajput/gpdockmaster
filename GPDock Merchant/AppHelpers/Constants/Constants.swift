//
//  Constants.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
//

import UIKit
import Firebase

let DEBUG = true

let DEBUG_Log = false

let kNavigationColor = UIColor(red:54.0/255.0, green:102.0/255.0, blue:235.0/255, alpha:1.0)
//UIColor(red:16.0/255.0, green:122.0/255.0, blue:196.0/255, alpha:1.0)
let kApplicationGreenColor = UIColor(red:26.0/255.0, green:188.0/255.0, blue:96.0/255, alpha:1.0)
let kApplicationRedColor = UIColor(red:179.0/255.0, green:0.0/255.0, blue:27.0/255, alpha:1.0)
let kApplicationBrownColor = UIColor(red:101.0/255.0, green:67.0/255.0, blue:33.0/255, alpha:1.0)
let kGradiantStartColor = UIColor(red:48.0/255.0, green:118.0/255.0, blue:242.0/255, alpha:1.0)
let kGradiantEndColor = UIColor(red:73.0/255.0, green:59.0/255.0, blue:218.0/255, alpha:1.0)
let kApplicationBlueColor = UIColor(red:54.0/255.0, green:102.0/255.0, blue:235.0/255, alpha:1.0)
let kApplicationLightGrayColor = UIColor(red:234.0/255.0, green:235.0/255.0, blue:237.0/255, alpha:1.0)
let kApplicationLightBleckColor = UIColor(red:111.0/255.0, green:113.0/255.0, blue:121.0/255, alpha:1.0)
let warningMessageShowingDuration = 1.25


/*============== NOTIFICATIONS ==================*/
extension Notification.Name {
    public static let USER_DID_LOGGED_IN_NOTIFICATION = NSNotification.Name("UserDidLoggedInNotification")
    public static let USER_DID_SELECT_MARINA_NOTIFICATION = NSNotification.Name("UserDidSelectMarinaNotification")
    public static let BOOKING_DID_CANCELLED_BY_USER_NOTIFICATION = NSNotification.Name("BookingDidCancelledByUser")
    public static let BOOKING_DID_CANCELLED_BY_MARINA_OWNER_NOTIFICATION = NSNotification.Name("BookingDidCancelledByMarinaOwner")
    public static let BOOKING_DID_COMPLETED_BY_MARINA_OWNER_NOTIFICATION = NSNotification.Name("BookingDidCompletedByMarinaOwner")
    public static let BOOKING_DID_RATING_UPDATED_NOTIFICATION = NSNotification.Name("BookingDidRatingUpdated")
    public static let FIRInstanceIDTokenRefreshNotification = NSNotification.Name.MessagingRegistrationTokenRefreshed
    public static let USER_DID_UPDATE_PROFILE_NOTIFICATION = NSNotification.Name("UserDidUpdateProfileNotification")
    public static let BOOKING_DID_COMPLETED_BY_USER_NOTIFICATION = NSNotification.Name("BookingDidCompletedByUser")
    public static let UPDATE_BADGE = NSNotification.Name("updateBadge")
    public static let SHOW_ALERT_MESSAGE = NSNotification.Name("showAddMessage")
    public static let USER_DID_UPDATE_APPLICATION_VERSION = NSNotification.Name("userDidUpdateApplicationNotification")

}


let kNotificationType = "notification_type"
let bookingCancelByCustomer = "booking_cancel_by_custome"
let kRideAcceptedNotification = "ride_accepted"
let kRideRequestNotification = "ride_request"
let kRideCancelledNotification = "cancel_ride"
let kRideStartedNotification = "ride_started"
let kRideEndedNotification = "ride_ended"


let kUserDefaults = UserDefaults.standard

/*========== SOME GLOBAL VARIABLE FOR USERS ==============*/


let kIsLoggedIN = "is_logged_in"
let kIsSessionAlert = "IsSessionAlert"

let kNormalUpdateShownOnDate = "normalUpdateShownOnDate"

let kUserName = "user_name"
let kPassword = "password"
let kDeviceToken = "DeviceToken"

let kMarinaWeekEndPrice = "MarinaWeekEndPrice"
let kSpecialHolidayPrice = "SpecialHolidayPrice"
let kReportUsEmail = "admin@gpdock.com"
let tollFreeNumber = "1-888-202-3625"
let paidToMarinaPriceByAdmin = "paid to marina by admin"
let graduallyPriceType = "gradually"
//let STRIPE_LIVE_PUBLISHABLE_KEY = "pk_live_XSJyTGono74xfFkURjngEOmp"
let STRIPE_LIVE_PUBLISHABLE_KEY = DEBUG ?  "pk_test_s08TYuM2TqHyonJ8nqZUNq8d" : "pk_live_XSJyTGono74xfFkURjngEOmp"
/*================== API URLs ====================================*/

let BASE_URL = DEBUG ? "http://development.gpdock.com/api/v1/" : "https://www.gpdock.com/api/v1/" 

let NEW_VERSION_BASE_URL = DEBUG ? "http://development.gpdock.com/api/v2/" : "https://www.gpdock.com/api/v2/"

let LOGIN_URL = "\(BASE_URL)user/login"
let LOGOUT_URL = "\(BASE_URL)logout"

let APP_VERSION = "\(NEW_VERSION_BASE_URL)app/versions"
let MARINA_QRCODE_PROFILE_URL = "\(NEW_VERSION_BASE_URL)marina/qr/profile"
let ADD_MARINA_PRICE_BY_RANGE = "\(NEW_VERSION_BASE_URL)add/marina/price"
let ADD_MARINA_SPECIFIC_PRICE_BY_DATE = "\(NEW_VERSION_BASE_URL)add/marina/specificprice"
let MARINA_PRICES_RANGE_WITH_DATES = "\(NEW_VERSION_BASE_URL)marina/price/dates"
let GET_MARINA_PRICE_RANGE_URL = "\(NEW_VERSION_BASE_URL)marina/prices"

//For support chat
let ALL_SUPPORT_TICKETS = "\(NEW_VERSION_BASE_URL)supports"
let ADD_COMMENT_TO_SUPPORT = "\(NEW_VERSION_BASE_URL)new/support/reply"

let UPDATE_MARINA_PRICE_TYPE = "\(NEW_VERSION_BASE_URL)business/marina"


let UPDATE_DEVICE_TOKEN_URL = "\(BASE_URL)update/user/device"

let SIGN_UP_URL = "\(BASE_URL)users"
let SOCIAL_AUTH_URL = "\(BASE_URL)user/social/login"
let FORGOT_PASSWORD_URL = "\(BASE_URL)user/recovery"
let OTP_VARIFICATION_URL = "\(BASE_URL)user/otp/verification"
let RESET_PASSWORD_URL = "\(BASE_URL)user/password/recovery"
let UPDATE_PASSWORD_URL = "\(BASE_URL)user/password/reset"
let WRITE_TO_US_URL = "\(BASE_URL)user/supports"

//
let SEARCH_MARINA_AVAILABILITY_BY_BOOKING_DATES_AND_BOAT_SIZE_URL = "\(BASE_URL)marina/availablity"
let MARINA_FLOOR_PLAN_FOR_SINGLE_DATE_URL = "\(BASE_URL)booking/today_arrival"


let PARKING_SPACE_BOOKING_FOR_DATES_URL = "\(BASE_URL)space_booking_detail"

let SEARCH_MARINA_AVAILABILITY_BY_USER_URL = "\(BASE_URL)user/boats/availablity"
let REVIEW_BOOKING_URL = "\(NEW_VERSION_BASE_URL)bookingdates/price"
let MARINA_FLOOR_PLAN_URL = "\(BASE_URL)marina/availablity"
let CITIES_URL = "\(BASE_URL)cities"
let MARINA_LIST_URL = "\(BASE_URL)marinas"
let SERVICES_URL = "\(BASE_URL)services"
let PAYOUTS_URL = "\(BASE_URL)payouts"
let CARDS_LIST_URL = "\(BASE_URL)card/list"

let ADD_CARD_URL = "\(BASE_URL)cards"
let ADD_BANK_ACCOUNT_URL = "\(BASE_URL)user/account"
let BANK_ACCOUNT_LISTS = "\(BASE_URL)bank_account/list"
let REMOVE_BANK_ACCOUNT = "\(BASE_URL)bank_account/remove"
let MAKE_DEFAULT_BANK_ACCOUNT_URL = "\(BASE_URL)default/account"
let REMOVE_CARD_URL = "\(BASE_URL)card/remove/"
let MAKE_DEFAULT_CARD_URL = "\(BASE_URL)card/default/"
let CREATE_BOOKING_URL = "\(BASE_URL)bookings"
let CREATE_OFF_LINE_BOOKING_URL = "\(BASE_URL)bookings/offline"
let LIST_OF_BOOKINGS_URL = "\(BASE_URL)bookings/status"
let BOOKING_DETAILS_URL = "\(BASE_URL)bookings/"
let CANCEL_BOOKING_URL = "\(BASE_URL)bookings/cancel"
let COMPLETE_BOOKING_URL = "\(BASE_URL)bookings/complete"

let SCAN_QR_CODE_URL = "\(BASE_URL)bookings/qrcode/scan"
let MARINA_EARNING_GRAPH_URL = "\(BASE_URL)marina/graph/bookings"
let MARINA_PAYOUT_EARNING_GRAPH_URL = "\(BASE_URL)marina/graph/earnings"
let SLIP_BOOKING_DETAILS_URL = "\(BASE_URL)space_booking_detail"
let BOOKING_COUNT_URL = "\(BASE_URL)booking/counts"
let FAQ_URL = "\(BASE_URL)faqs/role/business"

let REFUND_DETAILS_URL = "\(BASE_URL)refund/details"
let REFUND_STATUS_URL = "\(BASE_URL)refund/status"
let CHECK_REFUNDABLE_BOOKING_DETAILS_URL = "\(BASE_URL)refundable/details"
let USER_BOAT_LIST_URL = "\(BASE_URL)boat/user/boats"
//secondPhase
let GET_PRICE_FOR_MARINA_URL = "\(NEW_VERSION_BASE_URL)marina/getprices"
let SET_PRICE_FOR_MARINA_URL = "\(NEW_VERSION_BASE_URL)add/normal/prices"
//
let REMOVE_DATE_WISE_PRICE_LIST_URL = "\(BASE_URL)remove/marina/price"
let BOOKINGS_FOR_MARINA_URL = "\(BASE_URL)marina/bookings"
let LIST_FOR_BOOKINGS_URL = "\(BASE_URL)marina/bookings/list"
let LIST_FOR_BOOKED_AND_PARTIALLY_BOOKINGS_URL = "\(BASE_URL)marina/partially/bookings/list"
let DATE_WISE_MARINA_LIST_BOOKING_URL = "\(BASE_URL)marina/bookings/list/datewise"
let BOOKINGS_FOR_PARKING_SPACE_URL = "\(BASE_URL)parking_spaces/booking"
let MARINA_RATE_TO_BOOKING_URL = "\(BASE_URL)ratings"
let SEARCH_BOOKINGS_URL = "\(BASE_URL)search/bookings"
let USER_PROFILE_URL = "\(BASE_URL)profile"
let USER_RIDE_HISTORY_URL = "\(BASE_URL)customer_booking_history"
let EDIT_PROFILE_URL = "\(BASE_URL)user/profile/update"
let ALL_CARS_URL = "\(BASE_URL)all_cars"
let RIDE_REQUEST_URL = "\(BASE_URL)ride_request"
let CHANGE_RIDE_REQUEST_URL = "\(BASE_URL)change_ride_request"
let MARINA_DETAILS_URL = "\(BASE_URL)marina/details"
let LOCK_MARINA_URL = "\(BASE_URL)marins/select/space"

let MARINA_REVIEWS_URL = "\(BASE_URL)/reviews"
let ADD_BOAT_URL = "\(BASE_URL)boat/add"

let ADD_RATING_URL = "\(BASE_URL)add_rating"
let COST_ESTIMATION_URL = "\(BASE_URL)fair_price"
let DRIVER_LOCATION_URL = "\(BASE_URL)driver_location"


let TERMS_AND_CONDITIONS_URL = "\(BASE_URL)term_condition"
let ABOUT_US_URL = "http://www.tecorb.com/"
let USER_CANCEL_RIDE_URL = "\(BASE_URL)user_cancel_ride"
let USER_EMAIL_URL = "\(BASE_URL)user/email"
let NOTIFICATIONS_URL = "\(BASE_URL)notifications"


/*================== SOCIAL LOGIN TYPE ====================================*/
enum SocialLoginType: String {
    case facebook = "Facebook"
    case google = "Google"
}

/*======================== CONSTANT MESSAGES ==================================*/



/*============== SOCIAL MEDIA URL SCHEMES ==================*/

let FACEBOOK_URL_SCHEME = "fb1349768231783456"
let SELF_URL_SCHEME = "com.marinetechusa.GPDock-Merchant"

let GOOGLE_URL_SCHEME = "com.googleusercontent.apps.323647632593-o6kmg8friq1n06pmm7ii1usjmajoeftp"
let GOOGLE_API_KEY = "AIzaSyCdR7ijSZ1kQS5oiVfJ57Ecq4uPMCcOhY0"
let kGoogleClientId = "323647632593-o6kmg8friq1n06pmm7ii1usjmajoeftp.apps.googleusercontent.com"

/*============== PRINTING IN DEBUG MODE ==================*/

func print_debug <T>(_ object : T){
    if DEBUG_Log{
        print(object)
    }
    
}

func print_log <T>(_ object : T){
//    NSLog("\(object)")
}



enum fontSize : CGFloat {
    case small = 12.0
    case medium = 14.0
    case large = 16.0
    case xLarge = 18.0
    case xXLarge = 20.0
    case xXXLarge = 32.0
}

enum fonts {

    enum Raleway : String {
        case regular = "Raleway-Regular"
        case semiBold = "Raleway-Semibold"
        case light = "Raleway-Light"

        func font(_ size : fontSize) -> UIFont {
            return UIFont(name: self.rawValue, size: size.rawValue)!
        }
    }
}




enum segue : String{

    case unwindToDasboard  = "SegueUnwindToDasboard"
}

enum warningMessage : String{
    case alertTitle = "Important Message"
    case validPassword = "Please enter a valid password. Passwords should be 6 characters long"
    case validPhoneNumber = "Please enter a valid phone number"
    case validName = "Please enter a valid name"
    case validEmailAddress = "Please enter a valid email"
    case emailCanNotBeEmpty = "Please enter your email"
    case restYourPassword = "An email was sent to you to rest your password"
    case changePassword = "Your password has been changed successfully"
    case logoutMsg = "You've been logged out successfully."
    case networkIsNotConnected = "Network is not connected!"
    case functionalityPending = "\r\nUnder Development. Please ignore it!\r\n"
}


enum helperNames : String {
    case loader = "loader.gif"
    case oops = "opps"
    case placeholder = "placeholder"
    case facebook = "facebook"
    case Cancel = "Cancel"
    case VendorDetailNotFound = "Vendor Details Not Found"

}


/*============== SHOW MESSAGE ==================*/


func showSuccessWithMessage(_ message: String)
{
    //    JDStatusBarNotification.show(withStatus: message, dismissAfter: warningMessageShowingDuration, styleName: JDStatusBarStyleSuccess)

    // TSMessage.showNotification(withTitle: message, type: TSMessageNotificationType.success)
}
func showErrorWithMessage(_ message: String)
{
    GPD_Dockmaster.showAlertWith(nil, message: message, title: "Important Message")
}

func showWarningWithMessage(_ message: String)
{
//    JDStatusBarNotification.show(withStatus: message, dismissAfter: warningMessageShowingDuration, styleName: JDStatusBarStyleWarning)
    //TSMessage.showNotification(withTitle: message, type: TSMessageNotificationType.warning)
}
func showMessage(_ message: String)
{
//    JDStatusBarNotification.show(withStatus: message, dismissAfter: warningMessageShowingDuration, styleName: JDStatusBarStyleDefault)
    //TSMessage.showNotification(withTitle: message, type: TSMessageNotificationType.message)
}

func showAlertWith(_ viewController: UIViewController?,message:String,title:String){
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    let okayAction = UIAlertAction(title: "Okay", style: .default) { (action) in
        alert.dismiss(animated: true, completion: nil)
    }
    let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
    subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
    alert.view.tintColor = UIColor.white
    alert.addAction(okayAction)
    var vc: UIViewController
    if let pvc = viewController{
        vc = pvc
    }else{
        vc = ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!
    }
    vc.present(alert, animated: true, completion: nil)
}


func showAlertWithReset(viewController: UIViewController?,message:String,title:String, complitionBlock : ((_ done: Bool) ->Void)? = nil){
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    
    let okayAction = UIAlertAction(title: "Okay", style: .cancel) { (action) in
        guard let handler = complitionBlock else{
            alert.dismiss(animated: false, completion: nil)
            return
        }
        handler(true)
        alert.dismiss(animated: false, completion: nil)
    }
    alert.addAction(okayAction)
    var vc: UIViewController
    if let pvc = viewController{
        vc = pvc
    }else{
        vc = ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!
    }
    vc.present(alert, animated: true, completion: nil)
}


func showAlertWithActionEmptyMarina(_ viewController: UIViewController?,message:String,title:String){
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    let okayAction = UIAlertAction(title: "", style: .default) { (action) in
        //alert.dismiss(animated: true, completion: nil)
    }
    let subview = (alert.view.subviews.first?.subviews.first?.subviews.first!)! as UIView
    subview.backgroundColor = UIColor(red: 74.0/255.0, green: 90.0/255.0, blue: 95.0/255.0, alpha: 0.5)
    alert.view.tintColor = UIColor.white
   // alert.addAction(okayAction)
    var vc: UIViewController
    if let pvc = viewController{
        vc = pvc
    }else{
        vc = ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController)!
    }
    vc.present(alert, animated: true, completion: nil)
}

func openLoginModule(in viewController: UIViewController) {

//    let nav = AppStoryboard.Main.viewController(LoginNavigationController.self)
//    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = nav
//    UIApplication.shared.applicationIconBadgeNumber = 0
    

    let nav = UINavigationController(rootViewController: AppStoryboard.Main.viewController(LoginViewController.self))
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.backgroundColor = .white
    viewController.present(nav, animated: true, completion: nil)

}




