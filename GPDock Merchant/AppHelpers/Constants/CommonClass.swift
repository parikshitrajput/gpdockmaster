//
//  CommonClass.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import SystemConfiguration


let loaderSize = CGSize(width: 120, height: 120)

let screenWidth = UIScreen.main.bounds.width

enum ImageAction: String {
    case Update = "update"
    case Remove = "remove"
    case None = ""

}

func generateBoundaryString() -> String {
    return "Boundary-\(UUID().uuidString)"

}

class CommonClass: NSObject {
    
    static let sharedInstance = CommonClass()
    
    override init() {
        super.init()
    }
    
    func prepareHeader(withAuth:Bool) -> Dictionary<String,String>{
        let accept = "application/json"
        var header = Dictionary<String,String>()
        if withAuth{
            let userJSON = User.loadUserInfo()
            let user = User(json: userJSON)
            let userToken = user.userToken
            if !userToken.isEmpty{
                header.updateValue(userToken, forKey: "accessToken")
            }
        }
        header.updateValue(accept, forKey: "Accept")
        return header
    }
    func clearAllPendingRequests(){
        Alamofire.SessionManager.default.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel()
            }
        }
    }
    
    func hadleAccessTokenError(_ showLoginModule:Bool){
        
        CommonClass.sharedInstance.clearAllPendingRequests()
        kUserDefaults.set(false, forKey: kIsLoggedIN)
        let user = User()
        user.saveUserInfo(user)
        kUserDefaults.synchronize()
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
            
            //(appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: false)
        }
        
        let appDelegate: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let presentingVC = appDelegate.window!.rootViewController!
        
//        if presentingVC is UINavigationController{
//            if let nav = presentingVC as? UINavigationController{
//                for vc in nav.viewControllers{
//                    if let tab = vc as? UITabBarController{
//                        for cvc in tab.childViewControllers{
//                            NotificationCenter.default.removeObserver(cvc)
//                        }
//                    }
//                    NotificationCenter.default.removeObserver(vc)
//                }
//            }
//        }
    
        self.showAlertForSesseion(vc: presentingVC, showLoginModule: showLoginModule)

    }
   
    func showAlertForSesseion(vc:UIViewController,showLoginModule:Bool){
        CommonClass.sharedInstance.clearAllPendingRequests()

        CommonClass.sharedInstance.isShowingSessionAlert = true
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Your session has expired, Please login for further process.", preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Okay", style: .default) {[vc] (action) in
            CommonClass.sharedInstance.isShowingSessionAlert = false

            if showLoginModule{
               openLoginModule(in: vc)
            }
            
        }
        
        alert.addAction(okayAction)
        if alert.isBeingPresented {
        CommonClass.sharedInstance.clearAllPendingRequests()
        }else{
            vc.present(alert, animated: true, completion: nil)
        }
        
        
    }
  
    func showGotoPhotoGallerySettingAlert(in viewController: UIViewController,completionBlock:@escaping(_ isEnabling:Bool)->Void){
        let alert = UIAlertController(title: warningMessage.alertTitle.rawValue, message: "Photo Library access seems disabled\r\nGo to settings to enabled", preferredStyle: UIAlertControllerStyle.alert)
        let okayAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            completionBlock(false)
            alert.dismiss(animated: true, completion: nil)
        }
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (action) in
            completionBlock(true)
            let url = UIApplicationOpenSettingsURLString
            if UIApplication.shared.canOpenURL(URL(string: url)!){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        
        alert.addAction(okayAction)
        alert.addAction(settingsAction)
        viewController.present(alert, animated: true, completion: nil)
    }

    func reRegisterForFirebase(){
        let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
        appDelegate.unregisterFirebaseToken { (done) in
            if done{
                appDelegate.registerFirebaseToken()
            }
        }
    }
    
    
    let colors: [UIColor] = [
        UIColor.red,
        UIColor.green,
        UIColor.blue,
        UIColor.orange,
        UIColor.yellow
    ]

    func getSelectedMarina() -> Marina{
        let marinajson = Marina.loadMarinaInfo()
        var marina = Marina(json: marinajson)
        //if marina.ID == ""{
            let userinfo = User.loadUserInfo()
        if let _marina = User(json: userinfo).marinas.first{
                marina = _marina
                marina.saveMarinaInfo(marina)
            }else{
                marina.title = "No marina Found!"
            }
        //}
        return marina
    }

    class func makeViewCircular(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = view.frame.size.width/2
        view.layer.masksToBounds = true
    }
    class func makeViewCircularWithRespectToHeight(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.masksToBounds = true
    }


    class func makeViewCircularWithCornerRadius(_ view:UIView,borderColor:UIColor,borderWidth:CGFloat, cornerRadius: CGFloat)
    {
        view.layer.borderColor = borderColor.cgColor
        view.layer.borderWidth = borderWidth
        view.layer.cornerRadius = cornerRadius
        view.layer.masksToBounds = true
    }

   class func validateMaxValue(_ textField: UITextField, maxValue: Int, range: NSRange, replacementString string: String) -> Bool{

        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        //if delete all characteres from textfield
        if(newString.isEmpty) {
            return true
        }
        //check if the string is a valid number
        let numberValue = Int(newString)
        if(numberValue == nil) {
            return false
        }
        return numberValue! <= maxValue
    }

    class var isConnectedToNetwork: Bool {

    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)

    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
    $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
    SCNetworkReachabilityCreateWithAddress(nil, $0)
    }
    }) else {

    return false
    }

    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
    return false
    }

    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)

    return (isReachable && !needsConnection)
    }

    class var isLoggedIn: Bool{
        var result = false
        if let r = kUserDefaults.bool(forKey:kIsLoggedIN) as Bool?{
            result = r
        }
        return result//kUserDefaults.bool(forKey: kIsLoggedIN)
    }

    var isShowingSessionAlert: Bool{
        get{
            var result = false
            if let r = kUserDefaults.bool(forKey:kIsSessionAlert) as Bool?{
                result = r
            }
            return result
        }
        
        set(newIsShowingSessionAlert){
            kUserDefaults.set(newIsShowingSessionAlert, forKey: kIsSessionAlert)
            kUserDefaults.synchronize()
        }
    }

     func setPlaceHolder(_ textField: UITextField, placeHolderString placeHolder: String, withColor color: UIColor){
        let p = NSAttributedString(string: placeHolder, attributes: [NSAttributedStringKey.foregroundColor : color])
        textField.attributedPlaceholder = p;
    }

    class var isRunningSimulator: Bool
        {
        get
        {
            return TARGET_OS_SIMULATOR != 0
        }
    }

    //MARK:- Loader display Methods
    class func showLoader()
    {
//        SVProgressHUD.setForegroundColor(UIColor(patternImage: #imageLiteral(resourceName: "background")))
        SVProgressHUD.setMinimumSize(loaderSize)
          SVProgressHUD.setDefaultMaskType(.black)
          SVProgressHUD.setBackgroundColor(UIColor.white)
         //SVProgressHUD.setForegroundColor(UIColor(colorLiteralRed: 211.0/255.0, green: 1.0/255.0, blue: 51.0/255.0, alpha: 1))
        SVProgressHUD.show()
    }


    class var isLoaderOnScreen: Bool
    {
        return SVProgressHUD.isVisible()
    }
    class func showError(withStatus status: String)
    {
        SVProgressHUD.showError(withStatus: status)
    }
    class func showSuccess(withStatus status: String)
    {
        SVProgressHUD.showSuccess(withStatus: status)
    }


    class func showLoader(withStatus status: String)
    {
    
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setForegroundColor(kNavigationColor)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        SVProgressHUD.setMinimumSize(loaderSize)
        SVProgressHUD.setImageViewSize(CGSize(width:60,height:60))
        SVProgressHUD.show(#imageLiteral(resourceName: "logoLoder"), status: status)
        
        //SVProgressHUD.show(withStatus: status)
    }
    class func updateLoader(withStatus status: String)
    {
        SVProgressHUD.setStatus(status)
        SVProgressHUD.setMinimumSize(loaderSize)

    }

    class func showLoader(withStatus status: String, inView view: UIView)
    {
        //SVProgressHUD.show(withStatus: status, on: view)
    }
    
    class func hideLoader()
    {
        SVProgressHUD.dismiss()
    }
    
   class func convertToShowFormatDate(dateString: String) -> String {
        
        let dateFormatterDate = DateFormatter()
        dateFormatterDate.dateFormat = "YYYY-MM-dd " //Your date format
        let serverDate: Date = dateFormatterDate.date(from: dateString)! //according to date format your date string
        let dateFormatterString = DateFormatter()
        dateFormatterString.dateFormat = "MMM dd YYYY" //Your New Date format as per requirement change it own
        let newDate: String = dateFormatterString.string(from: serverDate) //pass Date here
        // New formatted Date string
        
        return newDate
    }

    

    class func getETAFromDateString(_ dateString: String) -> String
    {
        var eta = ""
        let df  = DateFormatter()
        df.locale = Locale.current
        df.locale = Locale.autoupdatingCurrent
        df.timeZone = TimeZone.autoupdatingCurrent
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: dateString)!
        df.dateFormat = "EEEE"
        let etaDay = df.string(from: date);
        df.dateFormat = "MM-dd-YY"
        let etaDate = df.string(from: date);
        eta = "ETA \(etaDay), \(etaDate)"
        return eta
    }


    class func getDayAndDateFromDateString(_ dateString: String) -> String
    {
        var eta = ""
        let df  = DateFormatter()
        df.locale = Locale.current
        df.timeZone = TimeZone.current
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: dateString)!
        df.dateFormat = "EEEE"
        let etaDay = df.string(from: date);
        df.dateFormat = "MM-dd-YY"
        let etaDate = df.string(from: date);
        eta = "ETA \(etaDay), \(etaDate)"
        return eta
    }

    class func dateWithString(_ dateString: String) -> String {

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent

        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dfs = dateString.replacingOccurrences(of: "T", with: " ")
        let dArray  = dfs.components(separatedBy: ".")
        let sourceTimeZone = TimeZone(abbreviation: "GMT")
        let myTimeZone = TimeZone.current

        if dArray.count>0{
            if let d = dayTimePeriodFormatter.date(from: dArray[0]) as Date?{
                dayTimePeriodFormatter.dateFormat = "hh:mm a, dd-MMM-YYYY"
                //let date = dayTimePeriodFormatter.string(from: d)
                let sourceGMTOffset = sourceTimeZone?.secondsFromGMT(for: d)
                let destinationGMTOffset = myTimeZone.secondsFromGMT(for: d)
                let interval = destinationGMTOffset - sourceGMTOffset!
                let destinationDate = Date(timeInterval: TimeInterval(interval), since: d)
                dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd" //"EEE, MMM dd, h:mm a"
                dayTimePeriodFormatter.timeZone = myTimeZone
                let formattedDate = dayTimePeriodFormatter.string(from: destinationDate)
                return formattedDate//date
            }
        }
        return " "
    }

    class func formattedDateWithString(_ dateString: String,format :String) -> String {
        //"dd-MMM-YYYY, hh:mm a"
        //"EEEE dd-MMM-YYYY h:mm a"
        //"hh:mm a, dd-MMM-YYYY"
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent

        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dfs = dateString.replacingOccurrences(of: "T", with: " ")
        let dArray  = dfs.components(separatedBy: ".")
        let sourceTimeZone = TimeZone(abbreviation: "GMT")
        let myTimeZone = TimeZone.current

        if dArray.count>0{
            if let d = dayTimePeriodFormatter.date(from: dArray[0]) as Date?{
                dayTimePeriodFormatter.dateFormat = "hh:mm a, dd-MMM-YYYY"
                let sourceGMTOffset = sourceTimeZone?.secondsFromGMT(for: d)
                let destinationGMTOffset = myTimeZone.secondsFromGMT(for: d)
                let interval = destinationGMTOffset - sourceGMTOffset!
                let destinationDate = Date(timeInterval: TimeInterval(interval), since: d)
                dayTimePeriodFormatter.dateFormat = format
                dayTimePeriodFormatter.timeZone = myTimeZone
                let formattedDate = dayTimePeriodFormatter.string(from: destinationDate)
                return formattedDate//date
            }
        }
        return " "
    }
    
    class func formattedCompletedTaskDateWithString(_ dateStr: String,timeStr:String) -> String {
        //"dd-MMM-YYYY, hh:mm a"
        //"EEEE dd-MMM-YYYY h:mm a"
        //"hh:mm a, dd-MMM-YYYY"

        let dateString = "\(dateStr)T\(timeStr):00.000Z"

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.locale = Locale.autoupdatingCurrent
        dayTimePeriodFormatter.timeZone = TimeZone.autoupdatingCurrent

        dayTimePeriodFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dfs = dateString.replacingOccurrences(of: "T", with: " ")
        let dArray  = dfs.components(separatedBy: ".")
        let sourceTimeZone = TimeZone(abbreviation: "GMT")
        let myTimeZone = TimeZone.current

        if dArray.count>0{
            if let d = dayTimePeriodFormatter.date(from: dArray[0]) as Date?{
                dayTimePeriodFormatter.dateFormat = "hh:mm a, dd-MMM-YYYY"
                //let date = dayTimePeriodFormatter.string(from: d)
                let sourceGMTOffset = sourceTimeZone?.secondsFromGMT(for: d)
                let destinationGMTOffset = myTimeZone.secondsFromGMT(for: d)
                let interval = destinationGMTOffset - sourceGMTOffset!
                let destinationDate = Date(timeInterval: TimeInterval(interval), since: d)
                dayTimePeriodFormatter.dateFormat = "hh:mm a, dd MMM YYYY"
                dayTimePeriodFormatter.timeZone = myTimeZone
                let formattedDate = dayTimePeriodFormatter.string(from: destinationDate)
                return formattedDate//date
            }
        }
        return " "
    }

    class func shortSelectedDate(_ fromDate:Date, toDate: Date)->(fromDate: Date,toDate:Date){
        if fromDate.compare(toDate) == ComparisonResult.orderedDescending{
            return (toDate,fromDate)
        }else{
            return (fromDate,toDate)
        }
    }
    class func formattedDateWith(_ date:Date,format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = format
        let da = dateFormatter.string(from: date)
        return da
    }



    class func formattedTimeFromDateWith(_ date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.defaultDate = date
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let da = "\(dateFormatter.string(from: date)) UTC"
        return da
    }

    class func formattedDateForSubmittionFromDateWith(_ date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.autoupdatingCurrent
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.defaultDate = date
        dateFormatter.dateFormat = "dd-MM-YYYY"
        let da = dateFormatter.string(from: date)
        return da
    }



    class func getModifiedDateFromDateString(_ dateString: String) -> String
    {

        let df  = DateFormatter()
        df.locale = Locale.autoupdatingCurrent
        df.timeZone = TimeZone.autoupdatingCurrent
        df.dateFormat = "YYYY-MM-dd"
        let date = df.date(from: dateString)!
        df.dateFormat = "dd-MM-YY"
        return df.string(from: date);
    }

    //MARK:- Email Validation
    class func isValidEmailAddress(_ emailStr: String) -> Bool
    {
        if((emailStr.isEmpty) || emailStr.count == 0)
        {
            return false
        }
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@",emailRegex)
        if(emailPredicate.evaluate(with: emailStr)){
            return true
        }
        return false
    }
    class func validateUserName(_ username: String) -> Bool {

        let MINIMUM_LENGTH_LIMIT_USERNAME = 1
        let MAXIMUM_LENGTH_LIMIT_USERNAME = 20

        let nameRegex = "[a-zA-Z _.@ ]+$"
        let nameTest = NSPredicate(format: "SELF MATCHES %@", nameRegex)

        if username.count == 0
        {
            return false
        }

        else if username.count < MINIMUM_LENGTH_LIMIT_USERNAME
        {
            return false
        }

        else if username.count > MAXIMUM_LENGTH_LIMIT_USERNAME
        {
            return false
        }

        else if !nameTest.evaluate(with: username)
        {
            return false
        }

        else
        {
            return true
        }
    }

    //Password Validation
    class func validatePassword(_ password: String) -> Bool {

        let MINIMUM_LENGTH_LIMIT_PASSWORD = 6
        let MAXIMUM_LENGTH_LIMIT_PASSWORD = 20

        if password.count == 0
        {
            return false
        }

        else if password[password.startIndex] == " "//substring(to:password.startIndex) == " "
        {
            return false
        }

        else if password.count < MINIMUM_LENGTH_LIMIT_PASSWORD
        {
            return false
        }

        else if password.count > MAXIMUM_LENGTH_LIMIT_PASSWORD
        {
            return false
        }

        else
        {
            return true
        }
    }

    //Email Validation
    class func validateEmail(_ email : String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        if email.count == 0
        {
            return false
        }

        else if !emailTest.evaluate(with: email)
        {
            return false
        }

        else
        {
            return true
        }
    }


    class func matchConfirmPassword(_ password :String , confirmPassword : String)-> Bool{


        if password==confirmPassword {
            return true
        }
        else{
            return false
        }
    }
    class func validatePhoneNumber(_ phone : String) -> Bool {
        let PHONE_REGEX = "^\\d{10}"
        let PhoneTest = NSPredicate(format:"SELF MATCHES %@", PHONE_REGEX)
        if !PhoneTest.evaluate(with: phone){
            return false

        }
        else{
            return true
        }
    }
    
    class func validateNumber(_ phone : String) -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phone.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  phone == filtered
    }

    // class var normalUpdateShownDate
    class func setNormalUpdateKey(){
        kUserDefaults.set(Date().timeIntervalSinceReferenceDate, forKey: kNormalUpdateShownOnDate)
    }
    
    class var shouldShowNormalUpdate:Bool{
        if let date = kUserDefaults.double(forKey: kNormalUpdateShownOnDate) as Double?{
            return date+(7*24*60*60) < Date().timeIntervalSinceReferenceDate
        }
        return true
    }

    class func classNameAsString(_ obj: Any) -> String {
        return String(describing: type(of: obj)).components(separatedBy:"__").last!
    }

    func openLocationSetting(){
        if #available(iOS 10.0, *) {
            if let url = URL(string: "App-Prefs:root=LOCATION_SERVICES") {
                UIApplication.shared.open(url, completionHandler: .none)
            }
        } else {
            if let url = URL(string: "prefs:root=LOCATION_SERVICES") {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, completionHandler: .none)
                } else {
                    if UIApplication.shared.canOpenURL(url){
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
    }

    class func setupNoDataView(_ tableView:UITableView,message:String){
        let bgView = UIView()
        bgView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        let noReviewLabel = UILabel()
        noReviewLabel.sizeToFit()
        bgView.addSubview(noReviewLabel)
        noReviewLabel.center = CGPoint(x:tableView.center.x, y:tableView.frame.size.height*1/2)
        noReviewLabel.textAlignment = NSTextAlignment.center
        noReviewLabel.text = message
        tableView.backgroundView = noReviewLabel
    }

    func userAvatarImage(username:String) -> UIImage{
        let configuration = LetterAvatarBuilderConfiguration()
        
        configuration.username = (username.trimmingCharacters(in: .whitespaces).count == 0) ? "NA" : username
        configuration.lettersColor = UIColor.white
        configuration.backgroundColors = [kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor,kNavigationColor]
        return UIImage.makeLetterAvatar(withConfiguration: configuration) ?? UIImage(named:"user_image")!
    }
    
    //   class func getTitleOfGMSPlace(place: GMSPlace) -> String {
    //        var title = ""
    //        title = place.name
    //        if !title.isEmpty{
    //            return title
    //        }
    //        if let addressComponents = place.addressComponents{
    //            for ac in addressComponents{
    //                if title.isEmpty{
    //                    if ac.type == "premise"{
    //                        title = ac.name
    //                        break
    //                    }
    //                }
    //                if title.isEmpty{
    //                    if ac.type == "sublocality_level_2"{
    //                        title = ac.name
    //                        break
    //                    }
    //                }
    //
    //                if title.isEmpty{
    //                    if ac.type == "sublocality_level_1"{
    //                        title = ac.name
    //                        break
    //                    }
    //                }
    //                if title.isEmpty{
    //                    if ac.type == "locality"{
    //                        title = ac.name
    //                        break;
    //                    }
    //                }
    //            }
    //        }
    //        if title.isEmpty{
    //            return "Unknown"
    //        }else
    //        {
    //            return title
    //        }
    //    }
    
    
}
