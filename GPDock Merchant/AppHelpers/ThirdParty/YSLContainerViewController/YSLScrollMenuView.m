//
//  YSLScrollMenuView.m
//  YSLContainerViewController
//
//  Created by yamaguchi on 2015/03/03.
//  Copyright (c) 2015年 h.yamaguchi. All rights reserved.
//

#import "YSLScrollMenuView.h"
#import "GPD_Dockmaster-Swift.h"

#define SCREEN_WIDTH  [[UIScreen mainScreen]bounds].size.width
static const CGFloat kYSLScrollMenuViewWidth  = 120;
static const CGFloat kYSLScrollMenuViewMargin = 0;//10;
static const CGFloat kYSLIndicatorHeight = 3;

@interface YSLScrollMenuView ()


@property (nonatomic, strong) UIView *indicatorView;

@end

@implementation YSLScrollMenuView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // default
        _viewbackgroudColor = [UIColor whiteColor];
        _upcomingBadgeValue = 0;
        _todayBadgeValue = 0;
        _itemfont = [UIFont systemFontOfSize:16];
        _itemTitleColor = [UIColor colorWithRed:0.866667 green:0.866667 blue:0.866667 alpha:1.0];
        _itemSelectedTitleColor = [UIColor colorWithRed:0.333333 green:0.333333 blue:0.333333 alpha:1.0];
        _itemIndicatorColor = [UIColor colorWithRed:0.168627 green:0.498039 blue:0.839216 alpha:1.0];
        
        self.backgroundColor = _viewbackgroudColor;
        _scrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
        _scrollView.showsHorizontalScrollIndicator = NO;
        [self addSubview:_scrollView];
    }
    return self;
}

#pragma mark -- Setter

- (void)setViewbackgroudColor:(UIColor *)viewbackgroudColor
{
    if (!viewbackgroudColor) { return; }
    _viewbackgroudColor = viewbackgroudColor;
    self.backgroundColor = viewbackgroudColor;
}

- (void)setItemfont:(UIFont *)itemfont
{
    if (!itemfont) { return; }
    _itemfont = itemfont;
    for (UILabel *label in _itemTitleArray) {
        label.font = itemfont;
        
    }
}

- (void)setItemTitleColor:(UIColor *)itemTitleColor
{
    if (!itemTitleColor) { return; }
    _itemTitleColor = itemTitleColor;
//    for (UILabel *label in _itemTitleArray) {
//        label.textColor = itemTitleColor;
//    }
    for (UIButton *label in _itemTitleArray) {
        [label setTitleColor:itemTitleColor forState:UIControlStateNormal];
//        label.textColor = itemTitleColor;
    }
}

- (void)setItemIndicatorColor:(UIColor *)itemIndicatorColor
{
    if (!itemIndicatorColor) { return; }
    _itemIndicatorColor = itemIndicatorColor;
    _indicatorView.backgroundColor = itemIndicatorColor;
}

- (void)setItemTitleArray:(NSArray *)itemTitleArray
{
    if (_itemTitleArray != itemTitleArray) {
        _itemTitleArray = itemTitleArray;
        NSMutableArray *views = [NSMutableArray array];
        CGFloat width = (itemTitleArray.count>3) ? kYSLScrollMenuViewWidth : SCREEN_WIDTH/itemTitleArray.count;
        CGFloat margin = (itemTitleArray.count>3) ? kYSLScrollMenuViewMargin : 0;

        for (int i = 0; i < itemTitleArray.count; i++) {
            CGRect frame = CGRectMake(0, 0, width, CGRectGetHeight(self.frame));
            UIButton *itemView = [[UIButton alloc]initWithFrame:frame];
            if(self.todayBadgeValue){
                if(i==0){
                    itemView.badgeValue = [NSString stringWithFormat:@"%@",self.todayBadgeValue];
                }
            }
            if(self.upcomingBadgeValue){
                if(i==1){
                    itemView.badgeValue = [NSString stringWithFormat:@"%@",self.upcomingBadgeValue];

                }
            }
           
            
           // UILabel *itemView = [[UILabel alloc] initWithFrame:frame];
            [self.scrollView addSubview:itemView];
            itemView.tag = i;
            [itemView setTitle:itemTitleArray[i] forState:UIControlStateNormal];
            //itemView.text = itemTitleArray[i];
            itemView.userInteractionEnabled = YES;
            itemView.backgroundColor = [UIColor clearColor];
            itemView.titleLabel.textAlignment = NSTextAlignmentCenter;
            itemView.titleLabel.font = self.itemfont;
            itemView.badgeBGColor = [UIColor whiteColor];
            itemView.badgeTextColor = [UIColor blueColor];
            [itemView setTitleColor:_itemTitleColor forState: UIControlStateNormal];
            //itemView.titleLabel.textColor = _itemTitleColor;
            [views addObject:itemView];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(itemViewTapAction:)];
            [itemView addGestureRecognizer:tapGesture];
        }
        
        self.itemViewArray = [NSArray arrayWithArray:views];
        
        // indicator
        _indicatorView = [[UIView alloc]init];
        _indicatorView.frame = CGRectMake(margin, _scrollView.frame.size.height - kYSLIndicatorHeight, width, kYSLIndicatorHeight);
        _indicatorView.backgroundColor = self.itemIndicatorColor;
        [_scrollView addSubview:_indicatorView];
    }
}

#pragma mark -- public

- (void)setIndicatorViewFrameWithRatio:(CGFloat)ratio isNextItem:(BOOL)isNextItem toIndex:(NSInteger)toIndex
{
    CGFloat indicatorX = 0.0;
    CGFloat width = (self.itemTitleArray.count>3) ? kYSLScrollMenuViewWidth : SCREEN_WIDTH/self.itemViewArray.count;
    CGFloat margin = (self.itemTitleArray.count>3) ? kYSLScrollMenuViewMargin : 0;

    if (isNextItem) {
        indicatorX = ((margin + width) * ratio ) + (toIndex * width) + ((toIndex + 1) * margin);
    } else {
        indicatorX =  ((margin + width) * (1 - ratio) ) + (toIndex * width) + ((toIndex + 1) * margin);
    }
    
    if (indicatorX < margin || indicatorX > self.scrollView.contentSize.width - (margin + width)) {
        return;
    }
    _indicatorView.frame = CGRectMake(indicatorX, _scrollView.frame.size.height - kYSLIndicatorHeight, width, kYSLIndicatorHeight);

}

- (void)setItemTextColor:(UIColor *)itemTextColor
    seletedItemTextColor:(UIColor *)selectedItemTextColor
            currentIndex:(NSInteger)currentIndex
{
    if (itemTextColor) { _itemTitleColor = itemTextColor; }
    if (selectedItemTextColor) { _itemSelectedTitleColor = selectedItemTextColor; }
    
    for (int i = 0; i < self.itemViewArray.count; i++) {
        UIButton *label = self.itemViewArray[i];
        //UILabel *label = self.itemViewArray[i];

        if (i == currentIndex) {
            label.alpha = 0.0;
            [UIView animateWithDuration:0.75
                                  delay:0.0
                                options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                             animations:^{
                                 label.alpha = 1.0;
                                 [label setTitleColor:_itemSelectedTitleColor forState:UIControlStateNormal];
                                 //label.textColor = _itemSelectedTitleColor;
                             } completion:^(BOOL finished) {
                             }];
        } else {
            [label setTitleColor:_itemTitleColor forState:UIControlStateNormal];

           // label.textColor = _itemTitleColor;
        }
    }
}

#pragma mark -- private

// menu shadow
- (void)setShadowView
{
    UIView *view = [[UIView alloc]init];
    view.frame = CGRectMake(0, self.frame.size.height - 0.5, CGRectGetWidth(self.frame), 0.5);
    view.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:view];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat x = (self.itemViewArray.count>3) ? kYSLScrollMenuViewMargin : 0;

    for (NSUInteger i = 0; i < self.itemViewArray.count; i++) {
        CGFloat width = (self.itemViewArray.count>3) ? kYSLScrollMenuViewWidth : SCREEN_WIDTH/self.itemViewArray.count;
        CGFloat margin = (self.itemViewArray.count>3) ? kYSLScrollMenuViewMargin : 0;

        //CGFloat width = kYSLScrollMenuViewWidth;
        UIButton *itemView = self.itemViewArray[i];
        
        itemView.frame = CGRectMake(x, 10, width, self.scrollView.frame.size.height-10);
        x += width + margin;
        if(self.todayBadgeValue){
            if(i==0){
                itemView.badgeValue = [NSString stringWithFormat:@"%@",self.todayBadgeValue];
                itemView.badgeBGColor = [UIColor whiteColor];
                itemView.badgeTextColor = [UIColor colorWithRed:54.0/255.0 green:102.0/255.0 blue:235.0/255.0 alpha:1];
                itemView.badgeOriginX = itemView.frame.size.width - 33;
            }
        }
        if(self.upcomingBadgeValue){
            if(i==1){
                itemView.badgeValue = [NSString stringWithFormat:@"%@",self.upcomingBadgeValue];
                itemView.badgeBGColor = [UIColor whiteColor];
                itemView.badgeOriginX = itemView.frame.size.width - 15;
                itemView.badgeTextColor = [UIColor colorWithRed:54.0/255.0 green:102.0/255.0 blue:235.0/255.0 alpha:1];
            }
        }
        //x += width + kYSLScrollMenuViewMargin;
    }
    self.scrollView.contentSize = CGSizeMake(x, self.scrollView.frame.size.height);
    
    CGRect frame = self.scrollView.frame;
    if (self.frame.size.width > x) {
        frame.origin.x = (self.frame.size.width - x) / 2;
        frame.size.width = x;
    } else {
        frame.origin.x = 0;
        frame.size.width = self.frame.size.width;
    }
    self.scrollView.frame = frame;
    NSArray *colors = [[NSArray alloc]initWithObjects:[UIColor colorWithRed:48.0/255.0 green:118.0/255.0 blue:242.0/255.0 alpha:1.0],[UIColor colorWithRed:73.0/255.0 green:59.0/255.0 blue:218.0/255.0 alpha:1.0], nil];
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    
    [self applyGradientInObjCWithColours:colors];
}

#pragma mark -- Selector --------------------------------------- //
- (void)itemViewTapAction:(UITapGestureRecognizer *)Recongnizer
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollMenuViewSelectedIndex:)]) {
        [self.delegate scrollMenuViewSelectedIndex:[(UIGestureRecognizer*) Recongnizer view].tag];
    }
}

@end
