//
//  ZSeatSelector.swift
//  ZSeatSelector_Swift
//
//  Created by Ricardo Zertuche on 8/24/15.
//  Copyright © 2015 Ricardo Zertuche. All rights reserved.
//

import UIKit

protocol ZSeatSelectorDelegate {
    func seatSelected(_ seat: ZSeat)
    func getSelectedSeats(_ seats: NSMutableArray)
}
enum SeatSelectorRange{
    case singleDate,rangeOfDate
}

enum SlipStatus{
    case available,unavailable,booked,arriving,isDeparting,partiallyBooked,isArrivingAndDeparture
}


class ZSeatSelector: UIScrollView, UIScrollViewDelegate {

    var seatSelectorDelegate: ZSeatSelectorDelegate?
    var seat_width:     CGFloat = 30.0
    var seat_height:    CGFloat = 30.0
    var selected_seats          = NSMutableArray()
    var seat_price:     Float   = 10.0
    var rangeType : SeatSelectorRange = .singleDate

    var left_available_image     = UIImage()//Noraml image in merchant
    var right_available_image     = UIImage()

    var left_arriving_image     = UIImage()//Yellow image in merchant
    var right_arriving_image     = UIImage()
    
    var left_partially_image     = UIImage()//Blue image in merchant
    var right_partially_image     = UIImage()

    var left_booked_image     = UIImage()//Green image in merchant
    var right_booked_image     = UIImage()

    var left_departing_image     = UIImage()//Orange image in merchant
    var right_departing_image     = UIImage()


    var left_unavailable_image   = UIImage()//Gray image as occupied
    var right_unavailable_image   = UIImage()
    var left_halfArrivingAndDeparture_image   = UIImage()//half image as available and departure
    var right_halfArrivingAndDeparture_image   = UIImage()
//
//    var left_disabled_image      = UIImage()//Departure Orange
//    var right_disabled_image      = UIImage()
//
//    var left_selected_image      = UIImage()//arrival yellow
//    var right_selected_image      = UIImage()


    let zoomable_view       = UIView()
    let verticalPadding:CGFloat = 5
    let horizontalPadding:CGFloat = 0

    var selected_seat_limit:Int = 0
    var layout_type                = ""

    // MARK: - Init and Configuration

    func setSeatSize(_ size: CGSize){
        seat_width  = size.width
        seat_height = size.height
    }

    func floorPlan(_ map: FloorPlan) -> Void {
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false

        var initial_seat_x: CGFloat = 10
        var initial_seat_y: CGFloat = 60

        var maxHeight: CGFloat = 0.0

        for parkingRow in map.rows {

            var separatorMaxY:CGFloat = seat_height //+ CGFloat(verticalPadding)
            let dockNameOriginX:CGFloat = initial_seat_x
           var dockNameOriginY:CGFloat = 10

            var separatorOriginX:CGFloat = 10
        var separatorOriginY:CGFloat = 10

            for boat in parkingRow.leftFacingBoats{
                drawBoatWithPosition(initial_seat_x, initialBoat_y: initial_seat_y, boat: boat)
                initial_seat_y = initial_seat_y + seat_height + verticalPadding
                if initial_seat_y > separatorMaxY{
                    separatorMaxY = initial_seat_y
                }
                if initial_seat_y > maxHeight{
                    maxHeight = initial_seat_y
                }

            }


            initial_seat_y = 60
            separatorOriginX = initial_seat_x + seat_width + horizontalPadding
            separatorOriginY = initial_seat_y

            initial_seat_x = initial_seat_x + seat_width + (2*horizontalPadding+5)

            for boat in parkingRow.rightFacingBoats{
                drawBoatWithPosition(initial_seat_x, initialBoat_y: initial_seat_y, boat: boat)
                initial_seat_y = initial_seat_y + seat_height + verticalPadding

                if initial_seat_y > separatorMaxY{
                    separatorMaxY = initial_seat_y
                }
                if initial_seat_y > maxHeight{
                    maxHeight = initial_seat_y
                }
            }
            dockNameOriginY = separatorMaxY
            print("dockNameY--->\(dockNameOriginY)")
            print("separatorOriginY--->\(separatorOriginY)")
            let separatorView = UIView(frame: CGRect(x: separatorOriginX, y: 10, width: 5.0, height: separatorMaxY))
            separatorView.backgroundColor = UIColor.white
            self.zoomable_view.addSubview(separatorView)
            let dockNameWidth  = (2*seat_width) + (2*horizontalPadding) + 5.0
            let dockFulName = parkingRow.title
            let dockNameArr : [String] = dockFulName.components(separatedBy: " ")
            let dockFirstName : String = dockNameArr[0]
            let dockLastName : String = dockNameArr[1]
            let dockName = UILabel(frame: CGRect(x: dockNameOriginX, y: 10, width: dockNameWidth, height: 30))
            dockName.text = dockLastName.uppercased()
            dockName.textColor = UIColor.black
            dockName.backgroundColor = UIColor.clear//(red: 34.0/255.0, green: 155.0/255.0, blue: 74.0/255.0, alpha: 1.0)
            dockName.textAlignment = .center
            dockName.font = UIFont(name: fonts.Raleway.semiBold.rawValue, size: fontSize.xXLarge.rawValue)
            let dockNameTwo = UILabel(frame: CGRect(x: dockNameOriginX, y: 35, width: dockNameWidth, height: 20))
            dockNameTwo.text = dockFirstName.uppercased()
            dockNameTwo.textColor = UIColor.black
            dockNameTwo.backgroundColor = UIColor.clear//(red: 34.0/255.0, green: 155.0/255.0, blue: 74.0/255.0, alpha: 1.0)
            dockNameTwo.textAlignment = .center
            dockNameTwo.font = UIFont(name: fonts.Raleway.semiBold.rawValue, size: fontSize.small.rawValue)
            self.zoomable_view.addSubview(dockName)
            self.zoomable_view.addSubview(dockNameTwo)
            initial_seat_x = initial_seat_x + (2*seat_width)
            initial_seat_y = 60
        }

        zoomable_view.frame = CGRect(x: 0, y: 0, width: initial_seat_x, height: maxHeight+30)
        self.contentSize = zoomable_view.frame.size
        let newContentOffsetX: CGFloat = (self.contentSize.width - self.frame.size.width) / 2
        self.contentOffset = CGPoint(x: newContentOffsetX, y: 0)
        selected_seats = NSMutableArray()
        self.delegate = self
        self.addSubview(zoomable_view)
        self.setZoomScale()
        
    }


    func setZoomScale() {
        let minZoom = min(self.bounds.size.width / zoomable_view.bounds.size.width, self.bounds.size.height / zoomable_view.bounds.size.height);
        let currentZoom = max(self.bounds.size.width / zoomable_view.bounds.size.width, self.bounds.size.height / zoomable_view.bounds.size.height);
        self.minimumZoomScale = minZoom;
        self.zoomScale = currentZoom
    }

    func drawBoatWithPosition(_ initialBoat_x: CGFloat, initialBoat_y: CGFloat, boat: ParkingSpace) -> Void {
        let boatButton = ZSeat(frame: CGRect(
            x: initialBoat_x,
            y: initialBoat_y,
            width: CGFloat(seat_width),
            height: CGFloat(seat_height)))

        boatButton.boat = boat

        if self.rangeType == .singleDate{
            if !boat.isAvailable{
                self.setSeatAsUnavaiable(boatButton)
                boatButton.isUserInteractionEnabled = false
            }else{
                self.setSeatAsAvaiable(boatButton)
                boatButton.isUserInteractionEnabled = true
                if !boat.isBooked{
                    if boat.isDeparting{
                        self.setSeatAsDeparting(boatButton)
                    }
                    if ((boat.isDeparting) && (boat.isArriving)) {
                        self.setSeatAsAvailableAndDeparting(boatButton)
                    }
                }
                else{
                    self.setSeatAsBooked(boatButton)
                    if boat.isArriving{
                        self.setSeatAsArriving(boatButton)
                    }
                    if boat.isDeparting{
                        self.setSeatAsDeparting(boatButton)
                    }
                    if ((boat.isDeparting) && (boat.isArriving)) {
                        self.setSeatAsAvailableAndDeparting(boatButton)
                    }
                }
            }
        }else{
            if !boat.isAvailable{
                self.setSeatAsUnavaiable(boatButton)
                boatButton.isUserInteractionEnabled = false
            }else{
                self.setSeatAsAvaiable(boatButton)
                boatButton.isUserInteractionEnabled = true
                if boat.isBooked{
                    self.setSeatAsBooked(boatButton)
                    if boat.isPartiallyBooked{
                        self.setSeatAsPartially(boatButton)
                    }
                }

            }
        }
        boatButton.addTarget(self, action: #selector(ZSeatSelector.seatSelected(_:)), for: .touchUpInside)
        zoomable_view.addSubview(boatButton)
    }

    // MARK: - Seat Selector Methods

     @objc func seatSelected(_ sender: ZSeat) {


        seatSelectorDelegate?.seatSelected(sender)
        seatSelectorDelegate?.getSelectedSeats(selected_seats)
    }

    func checkSeatLimitWithSeat(_ sender: ZSeat) {
//        if selected_seats.count < selected_seat_limit {
//            setSeatAsSelected(sender)
//            selected_seats.add(sender)
//        }
//        else {
//            let seat_to_make_avaiable: ZSeat = selected_seats[0] as! ZSeat
//            if seat_to_make_avaiable.disabled {
//                self.setSeatAsDisabled(seat_to_make_avaiable)
//            }
//            else {
//                self.setSeatAsAvaiable(seat_to_make_avaiable)
//            }
//            selected_seats.removeObject(at: 0)
//            self.setSeatAsSelected(sender)
//            selected_seats.add(sender)
//        }
    }

    // MARK: - Seat Images & Availability

    func setAvailableImage() {
//        self.left_available_image = UIImage(named: "lA")!//normal
//        self.right_available_image = UIImage(named: "rA")!
//
//        self.left_booked_image = UIImage(named: "lS")!
//        self.right_booked_image = UIImage(named: "rS")!
//
//        self.left_arriving_image = UIImage(named: "lAR")!
//        self.right_arriving_image = UIImage(named: "rAR")!
//
//        self.left_departing_image = UIImage(named: "lD")!
//        self.right_departing_image = UIImage(named: "rD")!
//
//        self.left_unavailable_image = UIImage(named: "lU")!
//        self.right_unavailable_image = UIImage(named: "rU")!
//
//        self.left_partially_image = UIImage(named: "lPR")!
//        self.right_partially_image = UIImage(named: "pU")!
        
        self.left_available_image = UIImage(named: "lA_new")!//normal
        self.right_available_image = UIImage(named: "rA_new")!
        
        self.left_booked_image = UIImage(named: "lS_new")!
        self.right_booked_image = UIImage(named: "rS_new")!
        
        self.left_arriving_image = UIImage(named: "lAR_new")!
        self.right_arriving_image = UIImage(named: "rAR_new")!
        
        self.left_departing_image = UIImage(named: "lD_new")!
        self.right_departing_image = UIImage(named: "rD_new")!
        
        self.left_unavailable_image = UIImage(named: "lU_new")!
        self.right_unavailable_image = UIImage(named: "rU_new")!
        
        self.left_partially_image = UIImage(named: "lPU_new")!
        self.right_partially_image = UIImage(named: "rPU_new")!
        
        self.left_halfArrivingAndDeparture_image = UIImage(named: "lAD_new")!
        self.right_halfArrivingAndDeparture_image = UIImage(named: "rAD_new")!
    }

    //set images for the shlips if all states
    //1- Available
    func setSeatAsAvaiable(_ sender: ZSeat) {
        sender.slipStatus = .available
        sender.setImage((sender.boat.facing == .left) ? left_available_image : right_available_image, for: UIControlState())
        // sender.selected_seat = false
    }
    
    func setSeatAsPartially(_ sender: ZSeat) {
        sender.slipStatus = .partiallyBooked
        sender.setImage((sender.boat.facing == .left) ? left_partially_image : right_partially_image, for: UIControlState())
        // sender.selected_seat = false
    }

    //2- Arriving
    func setSeatAsArriving(_ sender: ZSeat) {
        sender.slipStatus = .arriving
        sender.setImage((sender.boat.facing == .left) ? left_arriving_image : right_arriving_image, for: UIControlState())
        // sender.selected_seat = false
    }

    //3- Departing
    func setSeatAsDeparting(_ sender: ZSeat) {
        sender.slipStatus = .isDeparting
        sender.setImage((sender.boat.facing == .left) ? left_departing_image : right_departing_image, for: UIControlState())
        // sender.selected_seat = false
    }

    //4- Booked
    func setSeatAsBooked(_ sender: ZSeat) {
        sender.slipStatus = .booked
        sender.setImage((sender.boat.facing == .left) ? left_booked_image : right_booked_image, for: UIControlState())
        // sender.selected_seat = false
    }
    // 5- Available and Departing
    func setSeatAsAvailableAndDeparting(_ sender: ZSeat) {
        sender.slipStatus = .isArrivingAndDeparture
        sender.setImage((sender.boat.facing == .left) ? left_halfArrivingAndDeparture_image : right_halfArrivingAndDeparture_image, for: UIControlState())
        // sender.selected_seat = false
    }
    
    func setSeatAsUnavaiable(_ sender: ZSeat) {
        sender.slipStatus = .unavailable
        sender.setImage((sender.boat.facing == .left) ? left_unavailable_image : right_unavailable_image, for: UIControlState())
//        sender.selected_seat = false
    }
//
//
//
//
//    func setSeatAsDisabled(_ sender: ZSeat) {
//        sender.setImage((sender.boat.facing == .left) ? left_disabled_image : right_disabled_image, for: UIControlState())
//        sender.selected_seat = false
//    }
//
//    func setSeatAsSelected(_ sender: ZSeat) {
//        sender.setImage((sender.boat.facing == .left) ? left_selected_image : right_selected_image, for: UIControlState())
//        sender.selected_seat = true
//    }

    // MARK: - UIScrollViewDelegate

    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        //print("zoom")
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.subviews[0]
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        //print(scale)
    }
}


class ZSeat: UIButton {
    var isAvailable : Bool = true
    var isBooked : Bool = false //fully for range selection and booked for occupied for single date
    var isArriving : Bool = false
    var isDeparting:Bool = false
    var isDisabled: Bool = false
    var isPartially: Bool = false
    var isArrivingAndDeparture: Bool = false
    var slipStatus : SlipStatus = .available
    var boat : ParkingSpace = ParkingSpace(){
        didSet{
            self.isAvailable = boat.isAvailable
            self.isBooked = boat.isBooked
            self.isDeparting = boat.isDeparting
            self.isArriving = boat.isArriving
            self.isPartially = boat.isPartiallyBooked
            self.isArrivingAndDeparture = (boat.isArriving) && (boat.isDeparting)
            self.isDisabled = ((!boat.isAvailable) && (!boat.isBooked) && (!boat.isDeparting) && (!boat.isArriving) && (!boat.isPartiallyBooked))
            //available = boat.isAvailable
            //disabled = (!boat.isAvailable || boat.isBooked)
        }
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
}
