//
//  UIIMage.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//
//


import UIKit

extension UIImage {
    func makeImageWithColorAndSize(_ color: UIColor, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        color.setFill()
        UIRectFill(CGRect(x:0, y:0, width:size.width, height:size.height))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }

//    func trim(trimRect trimRect :CGRect) -> UIImage {
//        if CGRectContainsRect(CGRect(origin: CGPointZero, size: self.size), trimRect) {
//            if let imageRef = CGImageCreateWithImageInRect(self.CGImage, trimRect) {
//                return UIImage(CGImage: imageRef)
//            }
//        }
//        
//        UIGraphicsBeginImageContextWithOptions(trimRect.size, true, self.scale)
//        self.drawInRect(CGRect(x: -trimRect.minX, y: -trimRect.minY, width: self.size.width, height: self.size.height))
//        let trimmedImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        
//        guard let image = trimmedImage else { return self }
//        
//        return image
//    }
}
