//
//  AppDelegate.swift
//  GPDock
//
//  Created by TecOrb on 08/06/17.
//  Copyright © 2017 Nakul Sharma. All rights reserved.
//"pk_test_s08TYuM2TqHyonJ8nqZUNq8d"//

import UIKit
import CoreData
//import Google
//import GoogleMaps
//import GooglePlaces
import SwiftyJSON

//import GoogleSignIn
import IQKeyboardManagerSwift
import UserNotifications
import Alamofire
import Stripe
import AVFoundation
import Firebase
import FirebaseMessaging
import FirebaseInstanceID
import Fabric
import Crashlytics
import Siren
@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var notificationCount = 0
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        NotificationCenter.default.removeObserver(self)
        self.registerForRemoteNotification()
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        UIApplication.shared.statusBarStyle = .default//.lightContent
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.tokenRefreshNotification(_:)), name: .FIRInstanceIDTokenRefreshNotification, object: nil)
        STPPaymentConfiguration.shared().publishableKey = STRIPE_LIVE_PUBLISHABLE_KEY   //"pk_test_s08TYuM2TqHyonJ8nqZUNq8d"
        
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().disabledToolbarClasses = [SupportTicketViewController.self]

//        GMSPlacesClient.provideAPIKey(GOOGLE_API_KEY)
//        GMSServices.provideAPIKey(GOOGLE_API_KEY)

        let splashVC = AppStoryboard.Home.viewController(SplashAnimationViewController.self)
        self.window?.rootViewController = splashVC

        UIApplication.shared.applicationIconBadgeNumber = 0
        //unregister the firebase token and if success and reregister
        self.unregisterFirebaseToken { (done) in
            if done{
                self.registerFirebaseToken()
            }
        }
       // window?.makeKeyAndVisible()
        //setupSiren()

        return true
    }

    /*==============Notifications Handling================*/


    
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async {
                        Messaging.messaging().delegate = self
                        UIApplication.shared.registerForRemoteNotifications()
                    }
                }
            }
        }else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            Messaging.messaging().delegate = self
            UIApplication.shared.registerForRemoteNotifications()
        }

    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        kUserDefaults.set(deviceTokenString, forKey: kDeviceToken)
        if let token = Messaging.messaging().fcmToken{
            kUserDefaults.set(token, forKey: kDeviceToken)
        }else{
            print_debug("already registered to firebase with token : \(kUserDefaults.value(forKey: kDeviceToken) ?? "--")")
        }
    }




    @objc func tokenRefreshNotification(_ notification: NSNotification) {
        if let refreshedToken = Messaging.messaging().fcmToken{
            kUserDefaults.set(refreshedToken, forKey: kDeviceToken)
            LoginService.sharedInstance.updateDeviceTokeOnServer()
        }

    }


    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print_debug("Device token for push notifications: FAIL -- ")
        print_debug(error.localizedDescription)
    }
    

    //Called when a notification is delivered to a foreground app.

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
        
    {
        self.notificationCount += 1
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound,UNNotificationPresentationOptions.badge])
    }

    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
         let userInfo = response.notification.request.content.userInfo
        let json = JSON.init(userInfo)
        print_debug("nitification json: \r\n \(json)")
        
        if let _ = userInfo["booking_id"] as? String{
            self.gotoNotificationScreen()
        }else if let _ = userInfo["booking_id"] as? Int{
            self.gotoNotificationScreen()
        }
//        if let userInfo = response.notification.request.content.userInfo as? Dictionary<String,AnyObject>{
//            self.application(UIApplication.shared, didReceiveRemoteNotification: userInfo)
//        }
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
            print_debug(userInfoDict)
 
        }
       // completionHandler(UIBackgroundFetchResult.newData)

    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if let userInfoDict = userInfo as? Dictionary<String,AnyObject>{
            print_debug(userInfoDict)
        }
        if ( application.applicationState == .inactive || application.applicationState == .background)
        {
            UIApplication.shared.applicationIconBadgeNumber = 0

            if let _ = userInfo["booking_id"] as? String{
                self.gotoNotificationScreen()
            }else if let _ = userInfo["booking_id"] as? Int{
                self.gotoNotificationScreen()
            }
        }
    }


    ///// setup Weekely Update
    
    

    /*=======================================Notifications Handling====================================*/
    
    
    
    func gotoNotificationScreen(){
        if CommonClass.isLoggedIn {
            let notificationVc = AppStoryboard.Home.viewController(MainNavigationController.self)
            self.window?.rootViewController = notificationVc
        }else {
            let loginVc = AppStoryboard.Main.viewController(LoginNavigationController.self)
            self.window?.rootViewController = loginVc
        }

        
    }
    
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // NotificationCenter.default.removeObserver(self)

        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: .USER_DID_UPDATE_APPLICATION_VERSION, object: nil)
        //Siren.shared.checkVersion(checkType: .weekly)

        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
//        FBSDKAppEvents.activateApp()
        //Siren.shared.checkVersion(checkType: .weekly)

        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.


        self.saveContext()
    }

    // MARK: - Core Data stack
    // MARK: - Core Data Saving support
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }

        return coordinator
    }()
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.tecorb.Bloom_Trade" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return (urls[urls.count-1] as NSURL) as URL
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "GPDock_Merchant", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {

        let container = NSPersistentContainer(name: "GPDock_Merchant")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {

                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()



    func saveContext () {
        var managedObjContext : NSManagedObjectContext!
        if #available(iOS 10.0, *) {
            managedObjContext = persistentContainer.viewContext
        } else {
            managedObjContext = self.managedObjectContext
        }

        if managedObjContext.hasChanges {
            do {
                try managedObjContext.save()

            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate : MessagingDelegate{
    
    func registerFirebaseToken() {
        if let token = InstanceID.instanceID().token() {
            kUserDefaults.set(token, forKey: kDeviceToken)
            LoginService.sharedInstance.updateDeviceTokeOnServer()
        }
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func unregisterFirebaseToken(completion: @escaping (Bool)->()) {
        InstanceID.instanceID().deleteID { (error) in
            completion(error == nil)
        }
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        //self.registerFirebaseToken()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {}
}





